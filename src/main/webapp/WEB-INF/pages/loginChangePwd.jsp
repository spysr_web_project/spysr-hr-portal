<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="container">
		<div class="col-sm-12">
			<div class="reset_pw">
					<div class="login-panel panel panel-default" style="    position: inherit;
    margin-top: 160px;">
                    <div class="panel-heading">
                        <h3 class="panel-title">Reset Password</h3>
                    </div>
                    <div class="panel-body">
					<form:form action="updateLoginChangePwd" autocomplete="off" onsubmit="return validateForm()" commandName="endUserForm" id="pwdForm">
						
						<div class="form_page form-horizontal">
						<form:hidden path="id"/>
					
						<div style="text-align:center;">
			          <font color="red" >${error}</font>
		          </div>
		          
				<div id="pwdUnEqualError" style="text-align:center; display: none; color: red;">New and Confirm Passwords must match</div>
				
						 <fieldset>
                             <div class="form-group">
								    <label class="control-label col-sm-4" for="pwd">Old Password:</label>
								    <div class="col-sm-8"> 
										<form:password path="password" class="form-control input-sm" id="password" placeholder="Old Password" autocomplete="off"></form:password>
										<div id="passwordError" style="display: none; color: red;">Password is Mandatory</div>						
									</div>
							</div>
			                <div class="form-group">
								    <label class="control-label col-sm-4" for="pwd">New Password:</label>
								    <div class="col-sm-8"> 
			
								<form:password path="newPassword" class="form-control input-sm" id="newPassword"
										placeholder="New Password" autocomplete="off"></form:password>
									<div id="newPwdError" style="display: none; color: red;">Password is Mandatory</div>						
								</div>
							</div>
					
					<div class="form-group">
								    <label class="control-label col-sm-4" for="pwd">Confirm Password:</label>
								    <div class="col-sm-8"> 
					<form:password path="confirmNewPassword" class="form-control input-sm" id="confirmNewPassword"
							placeholder="Confirm New Password" autocomplete="off"></form:password>
						<div id="cfmPwdError" style="display: none; color: red;">Password is Mandatory</div>						
				</div>
				</div>
				 <div class="form-group">
					   	  <div class="col-sm-offset-4 col-sm-6">
				<form:hidden path="id" value="${endUserForm.id}"/>
						<button class="btn btn-primary btn-sm" type="submit"><spring:message code="label.save"/></button>
						<button type="button" name="Back" onclick="javascript:window.location='/spysr-hr-portal/';" class="btn btn-danger btn-sm"><spring:message code="label.cancel"/></button>
				</div>
				</div>
			 </fieldset>
			 </div>
                       </div>
                       </form:form>
                    </div>
                </div>
			</div>	
		</div>
</div>
	
<script>
	function validateForm() {

		var oldPassword = document.getElementById('password').value;
		var newPassword = document.getElementById('newPassword').value;
		var confirmNewPassword = document.getElementById('confirmNewPassword').value;

		var canSubmit = true;

		if (oldPassword == '') {
			document.getElementById('passwordError').style.display = 'block';
			canSubmit = false;
		} else {
			document.getElementById('passwordError').style.display = 'none';
		}

		if (newPassword == '') {
			document.getElementById('newPwdError').style.display = 'block';
			canSubmit = false;
		} else {
			document.getElementById('newPwdError').style.display = 'none';
		}
		
		if (confirmNewPassword == '') {
			document.getElementById('cfmPwdError').style.display = 'block';
			canSubmit = false;
		} else {
			document.getElementById('cfmPwdError').style.display = 'none';
		}
		
		if(newPassword != confirmNewPassword) {
			document.getElementById("pwdForm").reset();
			document.getElementById('pwdUnEqualError').style.display = 'block';
			canSubmit = false;
		} else {
			document.getElementById('pwdUnEqualError').style.display = 'none';
		}

		if (canSubmit == false) {
			return false;
		}

	}
	
</script>

 <%-- <div class="container">
		<div class="col-sm-12 text-center">
			<div class="reset_pw">
					<div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Reset Password</h3>
                    </div>
                    <div class="panel-body">
                       <c:url value="/j_spring_security_check" var="loginUrl" />
					<form  action="updateLoginChangePwd" autocomplete="off" class="form-horizontal"  method="post" onsubmit="return validateForm()" commandName="endUserForm" id="pwdForm">
						<form:hidden path="id"/> 
                            <fieldset>
                                <div class="form-group">
								    <label class="control-label col-sm-4" for="pwd">Old Password:</label>
								    <div class="col-sm-8"> 
								      <input type="password" class="form-control input-sm" id="pwd" placeholder="Enter password">
								    </div>
								</div>
								<div class="form-group">
								    <label class="control-label col-sm-4" for="pwd">New Password:</label>
								    <div class="col-sm-8"> 
								      <input type="password" class="form-control input-sm" id="pwd" placeholder="Enter password">
								    </div>
								</div>
								<div class="form-group">
								    <label class="control-label col-sm-4" for="pwd">Confirm Password:</label>
								    <div class="col-sm-8"> 
								      <input type="password" class="form-control input-sm" id="pwd" placeholder="Enter password">
								    </div>
								</div>
                                <input type="submit" value="Save" class="btn btn-sm btn-success btn-block" style="margin: 0px auto;width: 30%;"/>
                            </fieldset>
                        </form>
                    </div>
                </div>
			</div>	
		</div>
</div> --%>