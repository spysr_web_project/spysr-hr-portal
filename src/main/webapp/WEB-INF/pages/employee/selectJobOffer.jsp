<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
  <!-- To fetch the request url -->
<c:set var="req" value="${requestScope['javax.servlet.forward.request_uri']}"/>      
<c:set var="baseURL" value="${fn:split(req,'/')}" />

	<!--  datepicker css js links -->
		<link href="<%=request.getContextPath()%>/resources/css/datepicker.min.css" rel="stylesheet">
		<script src="<%=request.getContextPath()%>/resources/js/jquery.min.js"></script>
		<script src="<%=request.getContextPath()%>/resources/js/datepicker.js"></script>
		
		<script src="<%=request.getContextPath()%>/resources/js/datepicker.en.js"></script>
	<!--  End datepicker css js links -->
      <script>
	function val() {
	}
	function myFunction(a)
	{
		
		var files = document.getElementById("fileValue"+a).files; 
		document.getElementById("fileName"+a).value="";
		var validExts = new Array("jpg", "jpeg", "png", "gif");
	    for (var i = 0; i < files.length; i++){    	
	    	var fileExt= /[^.]+$/.exec( files[i].name);    	
	    	
	    	if (validExts.indexOf(fileExt.toString()) < 0) {
	    	      alert("Invalid file selected, valid files are of " +
	    	               validExts.toString() + " types."); 
	    	      document.getElementById("fileValue"+a).value="";
	    	    }
	    	else {
	    		document.getElementById("fileName"+a).value += files[i].name+",";	
	    	}
	    	
	    }
	}
</script>



	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

<form:form action="updateOffer" enctype="multipart/form-data" method="post" commandName="jobOffersForm" onsubmit="return val();">		 
	<form:hidden path="id"/>
	
	<div class="col-sm-12">
		<div class="modal-header">
			<h4><b><spring:message code="label.jobOffers" /></b></h4>
		</div>
		<div class="add_emp_details_form">
			<div class="Emp_content" style="margin-bottom: 37px;">
			
					
				
		<div class="modal-body" style="width: 100%;margin:0px auto;">
			<div class="form-horizontal" role="form">
				 
					 <div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.technology" /></b>:</label>
				   		<div class="col-sm-8"> 
							<form:input path="technology" class="form-control input-sm" placeholder="Enter technology" value="" id="technology" readonly="true"/>
						</div>
					</div>   
			
					 <div class="form-group">
						 <label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.title" /></b>:</label>
					  	 <div class="col-sm-8"> 
														
					  	 <form:input path="jobTiltle" class="form-control input-sm" placeholder="Enter jobTiltle" value="" id="jobTiltle" readonly="true"/>
						 </div>
					</div>   
			
					<div class="form-group">
						 <label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.exp" /></b>:</label>
					  	 <div class="col-sm-8"> 
							 <form:input path="Exp" class="form-control input-sm" placeholder="Enter Exp" value="" id="Exp" readonly="true"/>
						 </div>
					</div>  
					<div class="form-group">
						 <label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.nameOfCandidate" /></b>:</label>
					  	 <div class="col-sm-8"> 
							 <form:input path="nameOfCandidate" class="form-control input-sm" placeholder="Enter Name of Candidate" value="" id="nameOfCandidate" />
						 </div>
					</div>  
					<div class="form-group">
						 <label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.exp" /></b>:</label>
					  	 <div class="col-sm-8"> 
							 <form:input path="candidateExp" class="form-control input-sm" placeholder="Enter Exp" value="" id="candidateExp"/>
						 </div>
					</div>  
					<div class="form-group">
						 <label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.companyName" /></b>:</label>
					  	 <div class="col-sm-8"> 
							 <form:input path="companyName" class="form-control input-sm" placeholder="Enter companyName" value="" id="companyName"/>
						 </div>
					</div>  
					
					 <div class="form-group">
						 <label class="control-label col-sm-4" for="pwd"></label>
					  	 <div class="col-sm-8"> 
			<form:input path="file" type="file" class="myform-control"
											onchange="checkfile(this);" id="imageFile" />

		</div> 
			
					<br><br>
					 <div class="form-group">
                    		<div class="col-sm-offset-4 col-sm-5">
							    <input type="submit" class="btn btn-primary btn-sm" value="Upload"/> 
							     <c:if test="${baseURL[1] == 'managers'}"><c:set var="back" value="jobOffers"/></c:if>
					<c:if test="${baseURL[1] == 'employees'}"><c:set var="back" value="jobOffers"/></c:if>
         <a href="${back}"><button type="button" class="btn btn-success btn-sm" aria-hidden="true">Back</button></a>
							</div>
					 </div>
				</div>
				</div>
				</div>
				</div>
				</div>
			    </form:form>
			</div>
			
	<script>
		$('ul.nav li.dropdown').hover(function() {
		  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
		}, function() {
		  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
		});
		
	</script>
	<script>
		$(document).ready(function() {
			var readURL = function(input) {
				if (input.files && input.files[0]) {
					var reader = new FileReader();

					reader.onload = function (e) {
						$('.profile-pic').attr('src', e.target.result);
					}
			
					reader.readAsDataURL(input.files[0]);
				}
			}
			

			$(".file-upload").on('change', function(){
				readURL(this);
			});
			
			$(".upload-button").on('click', function() {
			   $(".file-upload").click();
			});
		});
	
	function checkfile(sender) {
		var validExts = new Array(".bmp", ".dib", ".JPG", ".jpg", ".jpeg",
				".jpe", ".jfif", ".gif", ".tif", ".tiff", ".png",".pdf",".doc",".docx",".xslx");
		var fileExt = sender.value;
		fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
		if (validExts.indexOf(fileExt) < 0) {
			alert("Invalid file selected, valid files are of "
					+ validExts.toString() + " types.");
			var fld = document.getElementById("imageFile");

			fld.form.reset();
			fld.focus();
		}
	}

	$(document).on("click", ".open-dialog", function() {
		$(".modal-body #id").val($(this).data('id'));

	});
	
	 function val() {
		if (document.getElementById('imageFile').value == '') {

			document.getElementById('imageFile').style.borderColor = "red";
			return false;
		} else {
			document.getElementById('imageFile').style.borderColor = "green";

		}
	} 
	
	
	</script>
	<script>
		$('#datepicker-1').datepicker();
	</script>
	
  </body>