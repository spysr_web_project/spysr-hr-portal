<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
    <!-- To fetch the request url -->
<c:set var="req" value="${requestScope['javax.servlet.forward.request_uri']}"/>      
<c:set var="baseURL" value="${fn:split(req,'/')}" />
<div class="edit-admin-profile">

	<div class="header_customer">
		<h3 align="center">
			<spring:message code="label.proDetails" />
		</h3>
	</div>
	<form:form name="bankDetails" action="updateEmployeeDetails"
		commandName="endUserForm" onsubmit="return validateForm()">
		<table style="margin:0px auto;" width="600">
			<form:hidden path="id" />


			<tr>
				<td><b><spring:message code="label.userName" /> :</b></td>
				<td><form:input path="userName" id="userName" class="myform-control" value="" readonly="true" />
			</tr>

			<tr>
				<td><b><spring:message code="label.displayName" /> :</b></td>
				<td><form:input path="displayName" class="myform-control" id="displayName" value="" readonly="true" />
			</tr>


			<tr>
				<td><b><spring:message code="label.contactNumber" /> :</b></td>
				<td><form:input path="contactNo" class="myform-control" value="" readonly="true" /></td>

			</tr>

			<tr>
				<td><b><spring:message code="label.altContactNo" /> :</b></td>
				<td><form:input path="altContactNo" class="myform-control" value="" readonly="true" /></td>

			</tr>

			<tr>
				<td><b><spring:message code="label.email" /> :</b></td>
				<td><form:input path="email" value="" class="myform-control" readonly="true" /></td>
			</tr>
			<tr>
				<td><b><spring:message code="label.altEmail" /> :</b></td>
				<td><form:input path="altEmail" class="myform-control" value="" readonly="true" /></td>
			</tr>

			<form:hidden path="transactionId" value="" />
		</table>
		<div class="col-sm-12 col-md-12">&nbsp;</div>
		<div class="col-sm-12">
			<table style="margin:0px auto;">
				<tr>
				<c:if test="${baseURL[1] == 'officeManagers'}"><c:set var="back" value="editOfficeManagerProfile"/></c:if>
					<c:if test="${baseURL[1] == 'employees'}"><c:set var="back" value="editEmployeeProfile"/></c:if>
							 
					<td class="col-sm-8"><input type="submit"
						value="<spring:message code="label.update"/>"
						class="btn btn-primary"></td>
					<td><a href="${back}" class="btn btn-success"><spring:message
								code="label.back" /></a></td>
				</tr>
			</table>
		</div>

	</form:form>
</div>
<style>
	.myform-control{
		background: #AFAFAF;
    color: black;
    cursor: no-drop;
	}
</style>