<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
    <!-- To fetch the request url -->
<c:set var="req" value="${requestScope['javax.servlet.forward.request_uri']}"/>      
<c:set var="baseURL" value="${fn:split(req,'/')}" />
	<!--  datepicker css js links -->
		<link href="<%=request.getContextPath()%>/resources/css/datepicker.min.css" rel="stylesheet">
		<script src="<%=request.getContextPath()%>/resources/js/jquery.min.js"></script>
		<script src="<%=request.getContextPath()%>/resources/js/datepicker.js"></script>
		
		<script src="<%=request.getContextPath()%>/resources/js/datepicker.en.js"></script>
	<!--  End datepicker css js links -->
      <script>
	function val() {

		var typesofLeave = document.getElementById('typesofLeave').value;
		
		 var fromDate=document.getElementById('fromDate').value;
		
		var toDate=document.getElementById('toDate').value;
		
		var reason=document.getElementById('reason').value;
		

		var canSubmit = true;

		if (typesofLeave == '') {

			document.getElementById('typesofLeave').style.borderColor = "red";
			return false;
		} else {
			document.getElementById('typesofLeave').style.borderColor = "green";
		}	
		
	
		if (fromDate == '') {
			document.getElementById('fromDate').style.borderColor = "red";
			return false;
		} else {
			document.getElementById('fromDate').style.borderColor = "green";
		}
	
		
		if (toDate == '') {
			document.getElementById('toDate').style.borderColor = "red";
			return false;
		} else {
			document.getElementById('toDate').style.borderColor = "green";
		}
	
	  if (reason == '') {

			document.getElementById('reason').style.borderColor = "red";
			return false;
		} else {
			document.getElementById('reason').style.borderColor = "green";
		}
		
		
		
		if (canSubmit == false) {
			return false;
		}
	}
</script>



	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

<form:form action="insertLeaveApplied" method="post" commandName="leaveRequestForm" onsubmit="return val();">		 
	<form:hidden path ="email" />
	<div class="col-sm-12">
		<div class="modal-header">
			<h4><b><spring:message code="label.leavedetails" /></b></h4>
		</div>
		<div class="add_emp_details_form">
			<div class="Emp_content" style="margin-bottom: 37px;">
			
					<form:hidden path="eligibleLeave"/>
				
		<div class="modal-body" style="width: 100%;margin:0px auto;">
			<div class="form-horizontal" role="form">
				 
					 <div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.empid" /></b></label>
				   		<div class="col-sm-8"> 
				   		<form:input path="userName" class="form-control input-sm" placeholder="Enter userName"  id="userName"  readonly="true"/>
							<%-- <form:input path="userName" class="form-control input-sm" placeholder="Enter userName" value="${model.endUserForm.userName}" id="userName"  readonly="true"/> --%>
						</div>
					</div>   
					<div class="form-group">
						 <label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.eligibleleave" /></b><span style="color: red">*</span>:</label>
					  	 <div class="col-sm-8"> 
														
					  	 <form:input path="eligibleLeave" class="form-control input-sm" placeholder="Enter userName"  id="eligibleLeave"  readonly="true"/>
						 </div>
					</div>   
			
				<div class="form-group">
						 <label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.totaleave" /></b></label>
					  	 <div class="col-sm-8"> 
							 <form:input path="totalLeave" class="form-control input-sm" placeholder="Enter userName"  id="totalLeave"  readonly="true"/>
						 </div>
					</div> 
			
			
			 <div class="form-group">
			<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.name" /></b></label>
		   <div class="col-sm-8"> 
											
		  <form:input path="name" class="form-control input-sm" placeholder="Enter userName" value="${model.endUserForm.name}" id="name"  readonly="true"/>
			</div>
			</div>  
			
			 <div class="form-group">
			<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.doj" /></b></label>
		   <div class="col-sm-8"> 
											
		  <form:input path="doj" class="form-control input-sm" placeholder="Enter userName" value="${model.endUserForm.doj}" id="name"  readonly="true"/>
			</div>
			</div>  
			
					 <div class="form-group">
							<label class="control-label col-sm-4" id="DateDemo"><b><spring:message code="label.typesofleave" /></b><span style="color: red">*</span> :</label>
							<div class="col-sm-8"> 
					    <form:select path="typesofLeave" class="form-control input-sm" id="typesofLeave" >
							<form:option value="">
								<spring:message code="label.selectValue" />
							</form:option>
							<form:option value="Sick Leave"><spring:message code="label.sickleave"/></form:option>
							<form:option value="Earned Leave"><spring:message code="label.earnedleave"/></form:option>
							<form:option value="Casual Leave"><spring:message code="label.casualeave"/></form:option>
							<form:option value="Maternity leave"><spring:message code="label.maternityleave"/></form:option>
							<form:option value="Emergency Leave">
								<spring:message code="label.emergencyleave" />
							</form:option>
							<form:option value="Annunal leave">
								<spring:message code="label.annunalleave" />
							</form:option>
						</form:select>
						</div>
					</div>	
					<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.fromdate" /></b><span style="color: red">*</span> :</label>
						<div class="col-sm-8"> 
						  <form:input path="fromDate" data-position="top left" data-language="en" class="form-control input-sm datepicker-here" id="fromDate" placeholder="Enter from Date"/>
						</div>
					</div>
					 <div class="form-group">
						<label class="control-label col-sm-4"><b><spring:message code="label.todate" /></b><span style="color: red">*</span> :</label>
						<div class="col-sm-8"> 
							<div>
							 <form:input path="toDate" data-position="top left" data-language="en" class="form-control input-sm datepicker-here" id="toDate" placeholder="Enter to date"/>
							</div>
						</div>
					</div>  
					
					 <div class="form-group">
						<label class="control-label col-sm-4" for="email"><b><spring:message code="label.reasonforleave" /></b><span style="color: red">*</span>:</label>
						<div class="col-sm-8">
						  <form:textarea path="reason" type="text" class="form-control input-sm" id="reason" placeholder="Enter Reason"/>
						</div>
					</div>
			*	
				
				 <div class="form-group">
                    		<div class="col-sm-offset-4 col-sm-5">
							     <input type="submit" class="btn btn-primary btn-sm" value="Send"/>
							     <c:if test="${baseURL[1] == 'officeManagers'}"><c:set var="back" value="officeManager"/></c:if>
					<c:if test="${baseURL[1] == 'employees'}"><c:set var="back" value="employee"/></c:if>
					<a href ="${back}"  class="btn btn-primary btn-sm"><b><spring:message code="label.back"></spring:message></b></a>		     
							</div>
					 </div>
				</div>
				</div>
				</div>
				</div>
				</div>
			    </form:form>
			</div>
			
	<script>
		$('ul.nav li.dropdown').hover(function() {
		  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
		}, function() {
		  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
		});
		
	</script>
	<script>
		$(document).ready(function() {
			var readURL = function(input) {
				if (input.files && input.files[0]) {
					var reader = new FileReader();

					reader.onload = function (e) {
						$('.profile-pic').attr('src', e.target.result);
					}
			
					reader.readAsDataURL(input.files[0]);
				}
			}
			

			$(".file-upload").on('change', function(){
				readURL(this);
			});
			
			$(".upload-button").on('click', function() {
			   $(".file-upload").click();
			});
		});
	</script>
	<script>
		$('#datepicker-1').datepicker();
	</script>
	
  </body>