<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
    <!-- To fetch the request url -->
<c:set var="req" value="${requestScope['javax.servlet.forward.request_uri']}"/>      
<c:set var="baseURL" value="${fn:split(req,'/')}" />
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
<form:form action="leavecancelupdate" commandName="leaveRequestForm" onsubmit="return val();">
	<div class="col-sm-12">

 <div class="form-horizontal" role="form">

		<div class="modal-header">
				  <h4><b><spring:message code="label.canceleave"/></b></h4>
				</div>
		<div class="add_emp_details_form">
			<div class="Emp_content">
			
					<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.userName" /></b><span style="color: red">*</span>:</label>
						<div class="col-sm-8"> 
						<form:input path="userName" class="form-control" placeholder="Enter UserName"  value="${model.leaveRequestForm.userName}" readonly="true"/>
						</div>
						</div>
				
					<form:hidden path="id"/>
					<form:hidden path="name"/>
					<form:hidden path="reason"/>
			        <form:hidden path="eligibleLeave"/>
					<form:hidden path="totalLeave"/>
					<form:hidden path="leaveDate"/>
					
			         <div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.typesofleave" /></b><span style="color: red">*</span>:</label>
						<div class="col-sm-8"> 
						<form:input path="typesofLeave" class="form-control" placeholder="Enter Name"  value="${model.leaveRequestForm.typesofLeave}"  readonly="true"/>
						</div>
						</div>
			
					
							<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.totalnumberleaveDays" /></b><span style="color: red">*</span>:</label>
						<div class="col-sm-8"> 
						<form:input path="totalnumberleaveDays" class="form-control" placeholder="Enter Account No"  value="${model.leaveRequestForm.totalnumberleaveDays}" readonly="true"/>
						</div>
						</div>
						
						<%-- 	<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.leavedate"/></b><span style="color:red">*</span> :</label>
						<div class="col-sm-8"> 
						  <form:input path="leaveDate" class="form-control"   value="${model.LeaveRequestForm.leaveDate}" readonly="true"/>
						</div>
					</div> --%>
			
				<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.fromdate"/></b><span style="color:red">*</span> :</label>
						<div class="col-sm-8"> 
						  <form:input path="fromDate" class="form-control"  placeholder="Enter role"  value="${model.LeaveRequestForm.fromDate}" readonly="true"/>
						</div>
					</div>
					
					
						<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.todate"/></b><span style="color:red">*</span> :</label>
						<div class="col-sm-8"> 
						  <form:input path="toDate" class="form-control"  placeholder="Enter password"  value="${model.LeaveRequestForm.toDate}" readonly="true"/>
						</div>
					</div>
					
			
						<div class="form-group">
							<label class="control-label col-sm-4" id="DateDemo"><b><spring:message code="label.status" /></b><span style="color: red">*</span> :</label>
						<div class="col-sm-8"> 
						
						
						
						<form:select path="leaveStatus" class="form-control input-sm" id="status">
						<form:option value="">
							<spring:message code="label.selectValue" />
						</form:option>
						<form:option value="Cancel">
							<spring:message code="label.cancel" />
						</form:option>
						</form:select>
					</div>
						</div>	
		
		     		  <div class="form-group">
								<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.comment" /></b><span style="color: red">*</span>:</label>
						 <div class="col-sm-8"> 
						   	<form:textarea path="comment" class="form-control"  placeholder="Enter comment" id="comment"/>
						 </div>
					  </div>
	
					
						<div class="form-group" style="margin-top:15px;">
                    		<div class="col-sm-offset-5 col-sm-5">				
							
		                         <c:if test="${baseURL[1] == 'officeManagers'}"><c:set var="back" value="officeMngleavecancelDetails"/></c:if>
					<c:if test="${baseURL[1] == 'employees'}"><c:set var="back" value="employeeleavecancelDetails"/></c:if>
						<input type="submit" class="btn btn-primary btn-sm" value="Save" style="margin-top: 26px;width: 18%;"/>
							<a href="${back}" class="btn btn-primary btn-sm" style="margin-top: 26px;width: 18%;"><spring:message code="label.back"/></a>
					  </div>
			        </div>
								
								
								
		</form:form>
</div>
	<script>
		function val(){
					
				
					var status  = document.getElementById('status').value;
					var comment  = document.getElementById('comment').value;
					var canSubmit = true; 
					
					
					if (status == '') {

						document.getElementById('status').style.borderColor = "red";
						return false;
					} else {
						document.getElementById('status').style.borderColor = "green";
					}	
					if (comment == '') {

						document.getElementById('comment').style.borderColor = "red";
						return false;
					} else {
						document.getElementById('comment').style.borderColor = "green";
					}	
					
					
				
				
					if(canSubmit == false){
						return false;
					}
				}
			
				
		</script>