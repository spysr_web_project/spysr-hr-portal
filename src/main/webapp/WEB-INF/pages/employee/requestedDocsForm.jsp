<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!-- To fetch the request url -->
<c:set var="req" value="${requestScope['javax.servlet.forward.request_uri']}"/>      
<c:set var="baseURL" value="${fn:split(req,'/')}" />

	
 <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<form:form action="confirmDoc" method="post" name="requestedDocs" commandName="requestedDocsForm" onsubmit="return val();">
        
	<div class="modal-header">
		<h4><b><spring:message code="label.requestedDocs" /></b></h4>
	</div>
		<div class="add_emp_details_form">
			<div class="Emp_content">
				
			<form:hidden path="name"/>
			<form:hidden path="userName"/>
			<form:hidden path="address"/>
			<form:hidden path="email"/>
				
				 <div class="modal-body" style="width: 100%;margin:0px auto;">
				  <div class="form-horizontal" role="form">
					  <div class="form-group">
						<label class="control-label col-sm-4" for="email"><b><spring:message
							code="label.doc" /></b><span style="color:red">*</span> :</label>
						<div class="col-sm-8">
							<form:select id="fdPay" path="docName" class="form-control" style="width: 50%;">
								<form:option value="select"></form:option>
								<form:option value="Home Loan"></form:option>
								<form:option value="Id Proof"></form:option>
							</form:select>
						</div>
					</div> 
       
       
                     <div class="form-group">
							<c:if test="${baseURL[1] == 'managers'}"><c:set var="back" value="manager"/></c:if>
							<c:if test="${baseURL[1] == 'employees'}"><c:set var="back" value="employee"/></c:if>
							<c:if test="${baseURL[1] == 'officeManagers'}"><c:set var="back" value="officeManager"/></c:if>
						<div class="col-sm-offset-4 col-sm-5">
							<input type="submit" class="btn btn-info" onclick="showDialog(); return false;" value="<spring:message code="label.go"/>">
							<a href="${back}" class="btn btn-info"><spring:message code="label.back" /></a>
						</div>
                     </div>
                     </div>
                     </div>
                     </div>
                     
                  
            </div> 
         	</form:form>
    </div>

	<script>
	function val() {
		
		if (document.getElementById('fdPay').value == 'select') {
			document.getElementById('fdPay').style.borderColor = "red";
			return false;
		} else {
			document.getElementById('fdPay').style.borderColor = "green";
		}


	}
</script>
<style>
	.flexi_table {
    margin-bottom: 210px;
}
</style>