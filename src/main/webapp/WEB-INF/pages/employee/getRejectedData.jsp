<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
    <!-- To fetch the request url -->
<c:set var="req" value="${requestScope['javax.servlet.forward.request_uri']}"/>      
<c:set var="baseURL" value="${fn:split(req,'/')}" />
		<link href="<%=request.getContextPath()%>/resources/css/datepicker.min.css" rel="stylesheet">
		<script src="<%=request.getContextPath()%>/resources/js/jquery.min.js"></script>
		<script src="<%=request.getContextPath()%>/resources/js/datepicker.js"></script>
		
		<script src="<%=request.getContextPath()%>/resources/js/datepicker.en.js"></script>

 <style>
 
  td{
       background: #1DA3A6 !important;
    color:white;
  }
  .header {
   margin-bottom: 31px;
   margin-top: 63px;
  }
  .header h3{
   color: #646464 !important;
   font-size: 23px;
   font-family: 'Exo', sans-serif !important;
   letter-spacing: 1px;
   padding: 0px 0px;
   margin-top: 21px;
   margin-bottom: -7px;
   text-align:center;
  }
  </style>
  
  <script>
  
			function validation()
			{
				if (confirm("Are you sure??") == true) {
			      return true;
			    } else {
			       return false;
			    }
			}
		</script>

<body>


<div id="section2" class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
  <div class="col-sm-12">
    <div class="header">
     <h3> <spring:message code="label.timeSheetDetails"/> </h3>
    </div>
    
    <form:form action="timeSheetData" id="timeSheet" method="post" commandName="timeSheetForm" onsubmit="return validation();">	
    <form:hidden path="userName"/>
    <form:hidden path="name"/>
    <form:hidden path="email"/>
    <form:hidden path="role"/>
    <table class="table table-bordered table-inverse" >
     <thead>
      <tr>
         <th style="background: #0D6466;"><form:input value="" id="monday" path="monday" readonly="true"/></th>
         <th style="background: #0D6466;"><form:input value="" id="tuesday" path="tuesday" readonly="true"/></th>
         <th style="background: #0D6466;"><form:input value="" id="wednesday" path="wednesday" readonly="true"/></th>
         <th style="background: #0D6466;"><form:input value="" id="thursday" path="thursday" readonly="true"/></th>
         <th style="background: #0D6466;"><form:input value="" id="friday" path="friday" readonly="true"/></th>
      </tr>
     </thead>
     <tbody>
      <tr class="date">
         <td scope="row">
          <div class="form-group">
          <label for="sel1"><spring:message code="label.status"/></label>
          <form:select class="form-control" id="sel1" path="mondayStatus">
         <form:option value="Working"></form:option>
         <form:option value="Leave"></form:option>
         <form:option value="Holiday"></form:option>
          </form:select>
        </div>
       </td>
       <td scope="row">
          <div class="form-group">
         <label for="sel1"><spring:message code="label.status"/></label>
          <form:select class="form-control" id="sel1" path="tuesdayStatus">
         <form:option value="Woriking"></form:option>
         <form:option value="Leave"></form:option>
         <form:option value="Holiday"></form:option>
          </form:select>
        </div>
       </td>
       <td scope="row">
          <div class="form-group">
          <label for="sel1"><spring:message code="label.status"/></label>
          <form:select class="form-control" id="sel1" path="wednesdayStatus">
         <form:option value="Woriking"></form:option>
         <form:option value="Leave"></form:option>
         <form:option value="Holiday"></form:option>
          </form:select>
        </div>
       </td>
       <td scope="row">
          <div class="form-group">
         <label for="sel1"><spring:message code="label.status"/></label>
          <form:select class="form-control" id="sel1" path="thursdayStatus">
         <form:option value="Woriking"></form:option>
         <form:option value="Leave"></form:option>
         <form:option value="Holiday"></form:option>
          </form:select>
        </div>
       </td>
       <td scope="row">
          <div class="form-group">
         <label for="sel1"><spring:message code="label.status"/></label>
          <form:select class="form-control" id="sel1" path="fridayStatus">
         <form:option value="Woriking"></form:option>
         <form:option value="Leave"></form:option>
         <form:option value="Holiday"></form:option>
          </form:select>
        </div>
       </td>
      </tr>
      <tr class="status">
           <td>
          <div class="form-group">
          <label for="comment"><spring:message code="label.description"/>:</label>
          <form:textarea class="form-control" rows="2" id="comment" path="mondayWorkDescription"></form:textarea>
        </div>
       </td>
       <td>
          <div class="form-group">
          <label for="comment"><spring:message code="label.description"/>:</label>
         <form:textarea class="form-control" rows="2" id="comment" path="tuesdayWorkDescription"></form:textarea>
        </div>
       </td>
       <td>
          <div class="form-group">
          <label for="comment"><spring:message code="label.description"/>:</label>
          <form:textarea class="form-control" rows="2" id="comment" path="wednesdayWorkDescription"></form:textarea>
        </div>
       </td>
       <td>
          <div class="form-group">
          <label for="comment"><spring:message code="label.description"/>:</label>
         <form:textarea class="form-control" rows="2" id="comment" path="thursdayWorkDescription"></form:textarea>
        </div>
       </td>
       <td>
          <div class="form-group">
          <label for="comment"><spring:message code="label.description"/>:</label>
         <form:textarea class="form-control" rows="2" id="comment" path="fridayWorkDescription"></form:textarea>
        </div>
       </td>
      </tr>
      
        
     </tbody>
    </table>
      
    <div class="col-md-12 text-center">
       <input type="submit" class="btn btn-success" value="Update">
        <c:if test="${baseURL[1] == 'managers'}"><c:set var="back" value="rejectedMngTimeSheet"/></c:if>
					<c:if test="${baseURL[1] == 'employees'}"><c:set var="back" value="rejectedTimeSheet"/></c:if>
					<c:if test="${baseURL[1] == 'officeManagers'}"><c:set var="back" value="officeManager"/></c:if>
         <a href="${back}"><button type="button" class="btn btn-success btn-sm" aria-hidden="true"><spring:message code="label.back"/> </button></a>
      </div>
    </form:form>
  </div>
 </div>

</body>
</html>