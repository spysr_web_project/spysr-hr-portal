<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

	<script src="/spysr-hr-portal/resources/js/search.js"></script>
	<style>
.filter {
	margin: 0px auto;
	width: 281px;
	float: right;
	margin-top: 3px;
}
</style>
  <!-- To fetch the request url -->
<c:set var="req" value="${requestScope['javax.servlet.forward.request_uri']}"/>      
<c:set var="baseURL" value="${fn:split(req,'/')}" />
			  
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
      	<div class="col-sm-12">
			<div class="modal-header">
				<h4><spring:message code="label.jobOffers"></spring:message></h4>
			</div>
				
			
				
				<div class="input-group filter">
					<span class="input-group-addon">Search</span> 
					<input id="filter" type="text" class="form-control" placeholder="Type here...">
				</div>
				
					<div class="add_emp_details">
						 <table class="table table-bordered table-striped" id="filter" data-rt-breakpoint="600">
                                        <thead>
                                            <tr>
                                     
                                                <th scope="col" data-rt-column="ID"><spring:message code="label.id"/></th>
                                                <th scope="col" data-rt-column="Project"><spring:message code="label.title"/></th>
                                               <%--  <th scope="col" data-rt-column="Status"><spring:message code="label.exp"/></th> --%>
                                                  <th scope="col" data-rt-column="Status"><spring:message code="label.technology"/></th>
                                                   <th scope="col" data-rt-column="Status"><spring:message code="label.lastDate"/></th>
                                              <th scope="col" data-rt-column="Status"><spring:message code="label.action"/></th>
                                        </thead>
                                        <tbody class="searchable">
                                        <c:if test="${! empty jobOffers}">

					             	<c:forEach items="${jobOffers}" var="job">
                                            <tr>                                         
                            
                                <td><c:out value="${job.id}"></c:out></td>
								<td><c:out value="${job.jobTiltle}"></c:out></td>
								<%-- <td><c:out value="${job.Exp}"></c:out></td> --%>
								<td><c:out value="${job.technology}"></c:out></td>
								<fmt:formatDate var='date' value="${job.lastDate}"  pattern="dd/MM/yyyy"/>
								<td><c:out value="${date}"></c:out>
								
								<td><a href="selectJobOffer?id=${job.id}" class="btn btn-primary"><spring:message code="label.select" /></a></td>
                                            </tr>  
                                            	</c:forEach>
					                         </c:if>                                        
                                        </tbody>
                                    </table>
					</div>
				</div>
			</div>
			 <div class="form-group">
                    		<div class="col-md-12 text-center">
							     
							     <c:if test="${baseURL[1] == 'managers'}"><c:set var="back" value="manager"/></c:if>
					<c:if test="${baseURL[1] == 'employees'}"><c:set var="back" value="employee"/></c:if>
            <br>  <a href="${back}"><button type="button" class="btn btn-success btn-sm" aria-hidden="true"><spring:message code="label.back" /></button></a>
							</div>
					 </div>
  
	<script>
		$('ul.nav li.dropdown').hover(function() {
		  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
		}, function() {
		  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
		});
		
	</script>
	
  </body>
</html>
