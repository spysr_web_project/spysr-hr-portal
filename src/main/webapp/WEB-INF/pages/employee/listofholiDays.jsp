<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
    <!-- To fetch the request url -->
<c:set var="req" value="${requestScope['javax.servlet.forward.request_uri']}"/>      
<c:set var="baseURL" value="${fn:split(req,'/')}" />
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<div class="col-sm-12">
		 <div class="modal-header">
			<h4><b><spring:message code="label.holidaysList"/></b></h4>
	     </div>
			
					<div class="add_emp_details">
						 <table class="table table-bordered table-striped" id="tb1" data-rt-breakpoint="600">
                                        <thead>
                                            <tr>
                                                <th scope="col" data-rt-column="ID"><spring:message code="label.serialNo"/></th>
                                               
                                              <th scope="col" data-rt-column="Project"><spring:message code="label.holidayDate"/></th>
                                               <th scope="col" data-rt-column="Status"><spring:message code="label.holiday"/></th>
                                           
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <c:if test="${! empty holidayList}">

					             	<c:forEach items="${holidayList}" var="holiday">
                                            <tr>                                         
                                 <td><c:out value="${holiday.serialNo}"></c:out></td>
                                 
                                   <fmt:formatDate var='date' value="${holiday.holidayDate}"  pattern="dd/MM/yyyy"/>
								<td><c:out value="${date}"></c:out>
                                 <%--    <td><c:out value="${holiday.holidayDate}"></c:out></td> --%>
							     <td><c:out value="${holiday.holidayName}"></c:out></td>
                                              </tr>  
                                            	</c:forEach>
					                         </c:if>                                        
                                        </tbody>
                                    </table>
                                    
         <div class="col-sm-12">
		<div class="text-center">
		<c:if test="${baseURL[1] == 'officeManagers'}"><c:set var="back" value="officeManager"/></c:if>
					<c:if test="${baseURL[1] == 'employees'}"><c:set var="back" value="employee"/></c:if>
		<a href ="${back}"  class="btn btn-primary btn-md" style="margin-top: 26px;width: 18%;"><b><spring:message code="label.back"></spring:message></b></a>
		</div>
		
		</div>  
					</div>
				</div>
			</div>
      

	<script>
		$('ul.nav li.dropdown').hover(function() {
		  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
		}, function() {
		  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
		});
		
	</script>
	
  </body>
</html>
