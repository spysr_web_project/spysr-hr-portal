<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
    <!-- To fetch the request url -->
<c:set var="req" value="${requestScope['javax.servlet.forward.request_uri']}"/>      
<c:set var="baseURL" value="${fn:split(req,'/')}" />

	<script src="<%=request.getContextPath()%>/resources/js/search.js"></script>
	<style>
		.filter{
			margin: 0px auto;
		    width: 281px;
		    float: right;
		    margin-top: 3px;	
		}
	</style>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			<div class="col-sm-12">
				<div class="modal-header">
				  <h4><b><spring:message code="label.timeSheetDetails" /></b><span style="color: red">*</span></h4>
				</div>
			</div>
				<div class="col-sm-12">
					<div class="input-group filter"> <span class="input-group-addon">Filter</span>
						<input id="filter" type="text" class="form-control" placeholder="Type here...">
					</div>
					<div class="add_emp_details">
						 <table class="table table-bordered table-striped example" id="filter" data-rt-breakpoint="600">
                                        <thead>
                                            <tr>
                                                <th scope="col" data-rt-column="ID"><spring:message code="label.id"/></th> 
                                                <th scope="col" data-rt-column="Project"><spring:message code="label.userName"/></th>
                                             
                                                <th scope="col" data-rt-column="Status"><spring:message code="label.name"/></th>
                                              <th scope="col" data-rt-column="Status"><spring:message code="label.date"/></th>
                                               <th scope="col" data-rt-column="Progress"><spring:message code="label.status"/></th>
						                         <th scope="col" data-rt-column="Progress"><spring:message code="label.action"/></th>
                                                <th>&nbsp;</th>
                                            </tr>
                                        </thead>
                                        <tbody class="searchable">
                                        <c:if test="${! empty timeSheets}">

					             	<c:forEach items="${timeSheets}" var="timeSheet">
                                            <tr>                                         
                                <td><c:out value="${timeSheet.id}"></c:out></td> 
								<td><c:out value="${timeSheet.userName}"></c:out></td>
					
								<td><c:out value="${timeSheet.name}"></c:out></td>
								  <fmt:formatDate value="${timeSheet.friday}" pattern="dd/MM/yyyy" var="date"></fmt:formatDate>
								<td><c:out value="${date}"></c:out></td>
								<td><c:out value="${timeSheet.status}"></c:out></td>
						       <td><a href="getData?id=${timeSheet.id}" class="btn btn-primary btn-sm"><spring:message code="label.select" /></a></td>

							

                                            </tr>  
                                            	</c:forEach>
					                         </c:if>                                        
                                        </tbody>
                                    </table>
					</div>
				</div>
			</div>
			<%-- <div class="modal-body">
      <c:if test="${baseURL[1] == 'managers'}"><c:set var="back" value="manager"/></c:if>
					<c:if test="${baseURL[1] == 'employees'}"><c:set var="back" value="employee"/></c:if>
         <a href="${back}"><button type="button" class="btn btn-success btn-sm" aria-hidden="true">Back </button></a>
      </div> --%>
   
	<script>
		$('ul.nav li.dropdown').hover(function() {
		  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
		}, function() {
		  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
		});
		
	</script>
	
  </body>
</html>
