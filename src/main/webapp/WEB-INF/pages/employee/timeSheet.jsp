<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
    <!-- To fetch the request url -->
<c:set var="req" value="${requestScope['javax.servlet.forward.request_uri']}"/>      
<c:set var="baseURL" value="${fn:split(req,'/')}" />
		<link href="<%=request.getContextPath()%>/resources/css/datepicker.min.css" rel="stylesheet">
		<script src="<%=request.getContextPath()%>/resources/js/jquery.min.js"></script>
		<script src="<%=request.getContextPath()%>/resources/js/datepicker.js"></script>
		
		<script src="<%=request.getContextPath()%>/resources/js/datepicker.en.js"></script>

 <style>
 
  td{
       background: #1DA3A6 !important;
    color:white;
  }
  .header {
   margin-bottom: 31px;
   margin-top: 63px;
  }
  .header h3{
   color: #646464 !important;
   font-size: 23px;
   font-family: 'Exo', sans-serif !important;
   letter-spacing: 1px;
   padding: 0px 0px;
   margin-top: 21px;
   margin-bottom: -7px;
   text-align:center;
  }
  </style>
  
  <script>
  
			function Val() {

				var tDate = document.getElementById('timeSheet').value;
				from = tDate.split("/");
				var date = new Date(from[2], from[1] - 1, from[0]);
				var tDay = new Date(from[2], from[1] - 1, from[0]);
				var wDay = new Date(from[2], from[1] - 1, from[0]);
				var thDay = new Date(from[2], from[1] - 1, from[0]);
				var fDay = new Date(from[2], from[1] - 1, from[0]);
				
				if (date.getDay() == 1) {
					document.getElementById('timeSheet').style.borderColor = "green";
					
					var n1 = date.getDate();
					var n2 = tDay.getDate()+1;
					tDay.setDate(n2);
					var n3 = wDay.getDate()+2;
					wDay.setDate(n3);
					var n4 = thDay.getDate()+3;
					thDay.setDate(n4);
					var n5 = fDay.getDate()+4;
					fDay.setDate(n5);
					
					document.getElementById('monday').value = date.getDate() + '/' + (date.getMonth() + 1)   + '/' +  date.getFullYear();
					document.getElementById('tuesday').value = tDay.getDate() + '/' + (tDay.getMonth() + 1)   + '/' +  tDay.getFullYear();
					document.getElementById('wednesday').value = wDay.getDate() + '/' + (wDay.getMonth() + 1)   + '/' +  wDay.getFullYear();
					document.getElementById('thursday').value = thDay.getDate() + '/' + (thDay.getMonth() + 1)   + '/' +  thDay.getFullYear();
					document.getElementById('friday').value = fDay.getDate() + '/' + (fDay.getMonth() + 1)   + '/' +  fDay.getFullYear();
					$("#timeData").show();
					$("#timeData1").show();
				} else {
					document.getElementById('timeSheet').style.borderColor = "red";
					$("#timeData").hide();
					$("#timeData1").hide();
					return false;
				}

			}
			
			function validation()
			{
				if (confirm("Are you sure??") == true) {
			      return true;
			    } else {
			       return false;
			    }
			}
		</script>

<body>


<div id="section2" class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	    <div class="modal-header">
	    	 <h4> <spring:message code="label.timeSheetDetails"/> </h4>
	    </div>
	    <div class="error">
	     <p style="text-align:center;color:red;">${success}</p>
	    </div>
    <div class="add_emp_details">
	    <form id="contactForm" method="post" class="form-horizontal">
		     <div class="form-group" style="margin-top: 11px;padding: 4px 11px;">
			      <label class="col-md-2 control-label"><spring:message code="label.weekStart"/></label>
			      <div class="col-md-3">
			       <input type="text" id="timeSheet" data-multiple-dates="0" data-multiple-dates-separator=", " data-position="bottom left" data-language="en" class="datepicker-here form-control input-sm">
			      </div>
			      <div class="col-md-3">
			       <button type="button" class="btn btn-primary btn-sm" value="Time sheet" onclick="Val();"><spring:message code="label.timeSheet"/></button>
			      </div>
		     </div>
	    </form>
	 </div>   
    <form:form action="timeSheetData" id="timeSheet" method="post" commandName="timeSheetForm" onsubmit="return validation();">	
	    <form:hidden path="userName"/>
	    <form:hidden path="name"/>
	    <form:hidden path="email"/>
	    <form:hidden path="role"/>
			    <table class="table table-bordered table-inverse" id="timeData" style="display: none">
			     <thead>
			      <tr>
			         <th style="background: #0D6466;"><form:input  id="monday" path="monday" readonly="true"/></th>
			         <th style="background: #0D6466;"><form:input  id="tuesday" path="tuesday" readonly="true"/></th>
			         <th style="background: #0D6466;"><form:input  id="wednesday" path="wednesday" readonly="true"/></th>
			         <th style="background: #0D6466;"><form:input  id="thursday" path="thursday" readonly="true"/></th>
			         <th style="background: #0D6466;"><form:input  id="friday" path="friday" readonly="true"/></th>
			      </tr>
			     </thead>
			     <tbody>
			      <tr class="date">
			         <td scope="row">
			          <div class="form-group">
			          <label for="sel1"><spring:message code="label.status"/></label>
			          <form:select class="form-control" id="sel1" path="mondayStatus">
			         <form:option value="Working"></form:option>
			         <form:option value="Leave"></form:option>
			         <form:option value="Holiday"></form:option>
			          </form:select>
			        </div>
			       </td>
			       <td scope="row">
			          <div class="form-group">
			         <label for="sel1"><spring:message code="label.status"/></label>
			          <form:select class="form-control" id="sel1" path="tuesdayStatus">
			         <form:option value="Working"></form:option>
			         <form:option value="Leave"></form:option>
			         <form:option value="Holiday"></form:option>
			          </form:select>
			        </div>
			       </td>
			       <td scope="row">
			          <div class="form-group">
			          <label for="sel1"><spring:message code="label.status"/></label>
			          <form:select class="form-control" id="sel1" path="wednesdayStatus">
			         <form:option value="Working"></form:option>
			         <form:option value="Leave"></form:option>
			         <form:option value="Holiday"></form:option>
			          </form:select>
			        </div>
			       </td>
			       <td scope="row">
			          <div class="form-group">
			         <label for="sel1"><spring:message code="label.status"/></label>
			          <form:select class="form-control" id="sel1" path="thursdayStatus">
			         <form:option value="Working"></form:option>
			         <form:option value="Leave"></form:option>
			         <form:option value="Holiday"></form:option>
			          </form:select>
			        </div>
			       </td>
			       <td scope="row">
			          <div class="form-group">
			         <label for="sel1"><spring:message code="label.status"/></label>
			          <form:select class="form-control" id="sel1" path="fridayStatus">
			          <form:option value="Working"></form:option>
			          <form:option value="Leave"></form:option>
			          <form:option value="Holiday"></form:option>
			          </form:select>
			        </div>
			       </td>
			      </tr>
			      <tr class="status">
			           <td>
			          <div class="form-group">
			          <label for="comment"><spring:message code="label.description"/>:</label>
			          <form:textarea class="form-control" rows="2" id="comment" path="mondayWorkDescription"></form:textarea>
			        </div>
			       </td>
			       <td>
			          <div class="form-group">
			          <label for="comment"><spring:message code="label.description"/>:</label>
			         <form:textarea class="form-control" rows="2" id="comment" path="tuesdayWorkDescription"></form:textarea>
			        </div>
			       </td>
			       <td>
			          <div class="form-group">
			          <label for="comment"><spring:message code="label.description"/>:</label>
			          <form:textarea class="form-control" rows="2" id="comment" path="wednesdayWorkDescription"></form:textarea>
			        </div>
			       </td>
			       <td>
			          <div class="form-group">
			          <label for="comment"><spring:message code="label.description"/>:</label>
			         <form:textarea class="form-control" rows="2" id="comment" path="thursdayWorkDescription"></form:textarea>
			        </div>
			       </td>
			       <td>
			          <div class="form-group">
			          <label for="comment"><spring:message code="label.description"/>:</label>
			         <form:textarea class="form-control" rows="2" id="comment" path="fridayWorkDescription"></form:textarea>
			        </div>
			       </td>
			      </tr>
			      
			        
			     </tbody>
			    </table>
      
			 
    
 
     			<div class="text-center" id="timeData1" style="display: none">
				      <div class="form-group">
					   	  <div class="col-sm-offset-3 col-sm-5">
					       <input type="submit" class="btn btn-success btn-sm" value="Submit">
					        <c:if test="${baseURL[1] == 'managers'}"><c:set var="back" value="manager"/></c:if>
										<c:if test="${baseURL[1] == 'employees'}"><c:set var="back" value="employee"/></c:if>
										<c:if test="${baseURL[1] == 'officeManagers'}"><c:set var="back" value="officeManager"/></c:if>
					        &nbsp;&nbsp; <a href="${back}"><button type="button" class="btn btn-success btn-sm" aria-hidden="true"><spring:message code="label.back" /></button></a>
					      </div>
				      </div>
			     </div>
		</form:form>
	
</div>
</body>
</html>