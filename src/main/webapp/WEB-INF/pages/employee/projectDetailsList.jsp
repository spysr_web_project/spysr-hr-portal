<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
<style>
		.filter{
			margin: 0px auto;
		    width: 281px;
		    float: right;
		    margin-top: 3px;	
		}
		td{
		    word-break: break-all;
		    }
	</style>
		<div class="col-sm-12">
				 <div class="modal-header">
					<h4><b><spring:message code="label.projectList"/></b></h4>
				 </div> 
			
					<div class="add_emp_details">
						 <table class="table table-bordered table-striped" id="tb1" data-rt-breakpoint="600">
                                        <thead>
                                            <tr>
                                                <th scope="col" data-rt-column="ID"><spring:message code="label.serialNo"/></th>
                                               
                                                <th scope="col" data-rt-column="Status"><spring:message code="label.projectName"/></th>
                                                 <th scope="col" data-rt-column="Project"><spring:message code="label.projectDetails"/></th>
                                                  <th scope="col" data-rt-column="Project"><spring:message code="label.version"/></th>
                                                 <th scope="col" data-rt-column="Project"><spring:message code="label.membersInvolved"/></th> 
                                            
                                            </tr>
                                        </thead>
                                        <tbody>
                                        <c:if test="${! empty projectList}">

					             	<c:forEach items="${projectList}" var="project">
                                            <tr>                                         
                                      <td><c:out value="${project.serialNo}"></c:out></td>
                                      <td><c:out value="${project.projectName}"></c:out></td>
							          <td><c:out value="${project.projectDetails}"></c:out></td>
							           <td><c:out value="${project.version }"></c:out></td>
							          <td><c:out value="${project.membersInvolved}"></c:out></td>
                                            </tr>  
                                            	</c:forEach>
					                         </c:if>                                        
                                        </tbody>
                                    </table>
                                    
                           <div class="col-sm-12">
		                        <div class="text-center">
					<a href="/spysr-hr-portal/employees/employee" class="btn btn-primary btn-md" style="margin-top: 26px;width: 18%;"><spring:message code="label.back"/></a>
					  </div>
			        </div>         
                                    
                                    
					</div>
				</div>
			</div>
      

	<script>
		$('ul.nav li.dropdown').hover(function() {
		  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
		}, function() {
		  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
		});
		
	</script>
	
  </body>
</html>
