<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
    <!-- To fetch the request url -->
<c:set var="req" value="${requestScope['javax.servlet.forward.request_uri']}"/>      
<c:set var="baseURL" value="${fn:split(req,'/')}" />
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style>
	h5{
		color:green;
		text-align:center;
	}
</style>
</head>
<body>
<div class="modal fade" id="overlay">
  <div class="modal-dialog">
    <div class="modal-content" style="padding:15px;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h5 class="modal-title"><spring:message code="label.timeSheetUpdate" /></h5>
      </div>
      <div class="modal-body">
      <c:if test="${baseURL[1] == 'managers'}"><c:set var="back" value="manager"/></c:if>
					<c:if test="${baseURL[1] == 'employees'}"><c:set var="back" value="employee"/></c:if>
					<c:if test="${baseURL[1] == 'officeManagers'}"><c:set var="back" value="officeManager"/></c:if>
         <a href="${back}"><button type="button" class="btn btn-success btn-sm" aria-hidden="true"><spring:message code="label.backHome" /></button></a>
      </div>
    </div>
  </div>
</div>
<script>
$('#overlay').modal('show');

setTimeout(function() {
    $('#overlay').modal('hide');
}, 5000);
</script>
</body>
</html>