<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<script>
	function validateForm() {

		var newPassword = document.getElementById('userName');

		var confirmNewPassword = document.getElementById('confirmNewPassword');

		var canSubmit = true;

		if (document.getElementById('userName').value == '') {
			document.getElementById('userNameError').style.display = 'block';
			canSubmit = false;
		} else {
			document.getElementById('userNameError').style.display = 'none';
		}

		if (document.getElementById('email').value == '') {
			document.getElementById('emailError').style.display = 'block';
			canSubmit = false;
		} else {
			document.getElementById('emailError').style.display = 'none';
		}

		if (canSubmit == false) {
			return false;
		}

	}
</script>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 <style>
 	.login-panel.panel.panel-default {
    width: 50%;
    margin: 0px auto;
}
 </style>
 <style>
 .login-panel.panel.panel-default {
    width: 55%;
    margin: 0px auto;
    position: relative;
    margin-top: 13%;
}
 </style>
<div class="container">
	<div class="row">
		<div class="login-panel panel panel-default" style="margin-top: 13%;">
                    <div class="panel-heading">
                        <h3 class="panel-title">Details</h3>
                    </div>
                       <div class="error">
                 <p style="text-align:center;color:red;">${success}</p>
                    </div>
                    <div class="panel-body">
                       <c:url value="/j_spring_security_check" var="loginUrl" />
			<form:form action="checkForgotPassword" autocomplete="off"
			onsubmit="return validateForm()" commandName="endUserForm">
                            <fieldset>
                                <div class="form-group">
                                	<label class="control-label col-sm-4">User Name:</label>
                                	<div class="col-sm-8">
					                   <form:input path="userName" class="form-control input-sm" placeholder="user Name" id="userName"  type="text" autofocus="" style="margin-bottom: 18px;"/>
					                   <div id="userNameError" style="display: none; color: red;">User
												Name is Mandatory</div>
											<div id="userNameError" style="display: none; color: red;">User
												Name is Mandatory</div>
              
									</div>
                                </div>
                                <div class="form-group">
                                	<label class="control-label col-sm-4">Email:</label>
                                	<div class="col-sm-8">
                                   		   <form:input path="email" class="form-control input-sm" placeholder="Enter Email" id="email" type="text" autofocus="" style="margin-bottom: 18px;"/>
                                   		  <div id="emailError" style="display: none; color: red;">Email is Mandatory</div>
										  <div id="emailError" style="display: none; color: red;">Email is Mandatory</div>
                                   	</div>
                                </div>
                              	<div align="center">
                                	<input type="submit" value="Save" class="btn btn-md btn-success btn-block" style="display:inline-block; width: 45%;"/>
                                 	<a class="btn btn-md btn-success btn-block" style="display:inline-block; width: 45%;margin-top: 0px;" href="logout">Back</a>
                                </div>
                            </fieldset>
                       </form:form>
                    </div>
                    
                </div>
                
	</div>
</div>
