<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
 

<body>


<div id="section2" class="container-fluid">
	<div class="section">
		
		<div class="col-sm-6">
			<div class="side1">
				<div class="logo" align="center">
					<img src="<%=request.getContextPath()%>/resources/images/logo.png" class="img-responsive logo_img"> 
					<h3>SpYsR HR</h3>
				</div>
			</div>
		</div>
		
		<div class="col-sm-6">
			<div class="side2">
				<div class="header">
					<!-- <h3> Login </h3> -->
				</div>
				<div>
				<div style="text-align:center;">
					<font color="red" style="position: absolute; left: 64px;padding: 8px; border-radius: 5px;">${error}</font>
		        </div>
					<div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Please Sign In</h3>
                    </div>
                    <div class="panel-body">
                       <c:url value="/j_spring_security_check" var="loginUrl" />
					<form  action="${loginUrl}" autocomplete="off"  method="post" id="loginForm">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Username" id="j_username" name="j_username" type="text" autofocus="">
									<p id="err"></p>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" id="j_password" name="j_password" type="password" value="">
                                </div>
                                <div class="checkbox">
                                    <label>
                                     <!--    <input name="remember" type="checkbox" value="Remember Me">Remember Me -->
										&nbsp;&nbsp;<a href="/spysr-hr-portal/auth/forgotPassword" class="forgot">Forgot Password</a>
                                    </label>
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <!-- <a href="#" class="btn btn-lg btn-success btn-block">Login</a> -->
                                <input type="submit" value="Login" class="btn btn-md btn-success btn-block" style="width: 45%;display: inline-block;margin-top: 5px;"/>
                                <input type="reset" value="Cancel" class="btn btn-md btn-success btn-block" style="width: 45%;float: right;display: inline-block;"/>
                            </fieldset>
                        </form>
                    </div>
                </div>
				</div>
			</div>	
		</div>
	</div>
</div>
</body>

