<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<link href="<%=request.getContextPath()%>/resources/css/HoldOn.css" rel="stylesheet" type="text/css">
<script src="<%=request.getContextPath()%>/resources/js/HoldOn.js"></script>
  <!-- To fetch the request url -->
<c:set var="req" value="${requestScope['javax.servlet.forward.request_uri']}"/>      
<c:set var="baseURL" value="${fn:split(req,'/')}" />


	<style>
		.filter{
			margin: 0px auto;
		    width: 281px;
		    float: right;
		    margin-top: 3px;	
		}
	</style>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			<div class="col-sm-12">
				<div class="modal-header">
				  <h4><b><spring:message code="label.busPassBill"/></b><span style="color: red">*</span></h4>
				</div>
			</div>
				<div class="col-sm-12">
					<div class="input-group filter"> <span class="input-group-addon">Search</span>
						<input id="filter" type="text" class="form-control" placeholder="Type here...">
					</div>
					<div class="add_emp_details">
						 <table class="table table-bordered table-striped example" id="filter" data-rt-breakpoint="600">
                                        <thead>
                                            <tr>
                                                <th scope="col" data-rt-column="ID"><spring:message code="label.id"/></th> 
                                                <th scope="col" data-rt-column="Project"><spring:message code="label.userName"/></th>
                                             
                                                <th scope="col" data-rt-column="Status"><spring:message code="label.name"/></th>
                                                <th scope="col" data-rt-column="Status"><spring:message code="label.presence"/></th>
                                                 <th scope="col" data-rt-column="Status"><spring:message code="label.amount"/></th>
                                              <th scope="col" data-rt-column="Status"><spring:message code="label.veg"/></th>
                                               <th scope="col" data-rt-column="Status"><spring:message code="label.nonVeg"/></th>
                                              <th scope="col" data-rt-column="Status"><spring:message code="label.foodAmount"/></th>
						                         <th scope="col" data-rt-column="Progress"><spring:message code="label.action"/></th>
                                              
                                            </tr>
                                        </thead>
                                        <tbody class="searchable">
                                        <c:if test="${! empty foodBills}">

					             	<c:forEach items="${foodBills}" var="foodBill">
                                            <tr>                                         
                                <td><c:out value="${foodBill.id}"></c:out></td> 
								<td><c:out value="${foodBill.userName}"></c:out></td>
					
								<td><c:out value="${foodBill.name}"></c:out></td>
								<td><c:out value="${foodBill.presents}"></c:out></td>
								<td><c:out value="${foodBill.money}"></c:out></td>
								<td><c:out value="${foodBill.veg}"></c:out></td>
								<td><c:out value="${foodBill.nonVeg}"></c:out></td>
								<td><c:out value="${foodBill.foodBill}"></c:out></td>
								  <fmt:formatDate value="${foodBill.date}" pattern="dd/MM/yyyy" var="date"></fmt:formatDate>
								<%-- <td><c:out value="${date}"></c:out></td> --%>
								
						       <td><a href="getSeperate?id=${foodBill.id}" class="btn btn-primary btn-sm"><spring:message code="label.edit" /></a></td>

							

                                            </tr>  
                                            	</c:forEach>
					                         </c:if>                                        
                                        </tbody>
                                    </table>
					</div>
				</div>
		
			
			
			
				<form:form action="sendingFoodBillData" id="timeSheet" method="post" commandName="timeSheetForm" onsubmit="return validation();">
				<form:hidden path="month"/>
				<form:hidden path="year"/>					
                 <div class="col-md-12 text-center">
				    <br><input type="submit" class="btn btn-success" value="Send Mail" onclick="testHoldon('sk-rect');">  <a href="busPassBill"><button type="button" class="btn btn-success btn-sm" aria-hidden="true"><spring:message code="label.back"/></button></a>
				</div>
				</form:form>
   	</div>
	<script>
		$('ul.nav li.dropdown').hover(function() {
		  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
		}, function() {
		  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
		});
		
		function val()
		{
			
			document.getElementById("mondayBill").readOnly = false;	
			
			
		}
		
		function testHoldon(themeName){
			HoldOn.open({
				theme:themeName,
			
			});
			
			setTimeout(function(){
				HoldOn.close();
			},8000);
		}
		
		
	</script>
	
  </body>
</html>
