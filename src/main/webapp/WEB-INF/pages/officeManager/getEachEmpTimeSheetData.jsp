<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
  <!-- To fetch the request url -->
<c:set var="req" value="${requestScope['javax.servlet.forward.request_uri']}"/>      
<c:set var="baseURL" value="${fn:split(req,'/')}" />
		<link href="<%=request.getContextPath()%>/resources/css/datepicker.min.css" rel="stylesheet">
		<script src="<%=request.getContextPath()%>/resources/js/jquery.min.js"></script>
		<script src="<%=request.getContextPath()%>/resources/js/datepicker.js"></script>
		
		<script src="<%=request.getContextPath()%>/resources/js/datepicker.en.js"></script>

 <style>
 
  td{
       background: #1DA3A6 !important;
    color:white;
  }
  .header {
   margin-bottom: 31px;
   margin-top: 63px;
  }
  .header h3{
   color: #646464 !important;
   font-size: 23px;
   font-family: 'Exo', sans-serif !important;
   letter-spacing: 1px;
   padding: 0px 0px;
   margin-top: 21px;
   margin-bottom: -7px;
   text-align:center;
  }
  </style>
 
<body>


<div id="section2" class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
  <div class="col-sm-12">
    <div class="modal-header">
     <h4> <spring:message code="label.timeSheetDetails"/> </h4>
    </div>
    
    <form:form action="#" id="timeSheet" method="post" commandName="timeSheetForm">	
    <form:hidden path="userName"/>
   
  
      <table class="table table-bordered table-inverse">
      		<tr>
      			<td> 
      				 <div class="form-group">
		         	 <label for="comment"><spring:message code="label.name"/>:</label>
		         	 <form:input class="form-control input-sm" path="name" readonly="true" style="font-weight: 700;"/>
		       		 </div>
		        </td>
		        <td> 
      				 <div class="form-group">
		         	 <label for="comment"><spring:message code="label.role"/>:</label>
		         	  <form:input class="form-control input-sm" path="role" readonly="true"  style="font-weight: 700;"/>
		       		 </div>
		        </td>
		        <td> 
      				 <div class="form-group">
		         	 <label for="comment"><spring:message code="label.email"/>:</label>
		         	  <form:input class="form-control input-sm" path="email" readonly="true"  style="font-weight: 700;"/>
		       		 </div>
		        </td>
      		</tr>
      		
      </table>
    
    <table class="table table-bordered table-inverse">
     <thead>
      <tr>
         <th style="background: #0D6466;"><form:input value="" id="monday" path="monday" readonly="true"/></th>
         <th style="background: #0D6466;"><form:input value="" id="tuesday" path="tuesday" readonly="true"/></th>
         <th style="background: #0D6466;"><form:input value="" id="wednesday" path="wednesday" readonly="true"/></th>
         <th style="background: #0D6466;"><form:input value="" id="thursday" path="thursday" readonly="true"/></th>
         <th style="background: #0D6466;"><form:input value="" id="friday" path="friday" readonly="true"/></th>
      </tr>
     </thead>
     <tbody>
      <tr class="date">
         <td scope="row">
          <div class="form-group">
          <label for="sel1"><spring:message code="label.status"/></label> 
          <form:input class="form-control input-sm" path="mondayStatus" readonly="true" style="font-weight: 700;"/>
          </div>
       </td>
       <td scope="row">
          <div class="form-group">
         <label for="sel1"><spring:message code="label.status"/></label>
         <form:input class="form-control input-sm" path="tuesdayStatus" readonly="true" style="font-weight: 700;"/>
        </div>
       </td>
       <td scope="row">
          <div class="form-group">
          <label for="sel1"><spring:message code="label.status"/></label>
          <form:input class="form-control input-sm" path="wednesdayStatus" readonly="true" style="font-weight: 700;"/>
        </div>
       </td>
       <td scope="row">
          <div class="form-group">
         <label for="sel1"><spring:message code="label.status"/></label>
         <form:input class="form-control input-sm" path="thursdayStatus" readonly="true" style="font-weight: 700;"/>
        </div>
       </td>
       <td scope="row">
          <div class="form-group">
         <label for="sel1"><spring:message code="label.status"/></label>
         <form:input class="form-control input-sm" path="fridayStatus" readonly="true" style="font-weight: 700;"/>
        </div>
       </td>
      </tr>
      <tr class="status">
           <td>
          <div class="form-group">
          <label for="comment"><spring:message code="label.description"/>:</label>
          <form:textarea class="form-control" rows="2" id="comment" path="mondayWorkDescription" readonly="true"></form:textarea>
        </div>
       </td>
       <td>
          <div class="form-group">
          <label for="comment"><spring:message code="label.description"/>:</label>
         <form:textarea class="form-control" rows="2" id="comment" path="tuesdayWorkDescription" readonly="true"></form:textarea>
        </div>
       </td>
       <td>
          <div class="form-group">
          <label for="comment"><spring:message code="label.description"/>:</label>
          <form:textarea class="form-control" rows="2" id="comment" path="wednesdayWorkDescription" readonly="true"></form:textarea>
        </div>
       </td>
       <td>
          <div class="form-group">
          <label for="comment"><spring:message code="label.description"/>:</label>
         <form:textarea class="form-control" rows="2" id="comment" path="thursdayWorkDescription" readonly="true"></form:textarea>
        </div>
       </td>
       <td>
          <div class="form-group">
          <label for="comment"><spring:message code="label.description"/>:</label>
         <form:textarea class="form-control" rows="2" id="comment" path="fridayWorkDescription" readonly="true"></form:textarea>
        </div>
       </td>
      </tr>
      <tr class="date">
         <td scope="row">
          <div class="form-group">
          <label for="sel1"><spring:message code="label.foodBill"/></label> 
          <form:input class="form-control input-sm" path="mondayBill" readonly="true" style="font-weight: 700;"/>
          </div>
       </td>
       <td scope="row">
          <div class="form-group">
         <label for="sel1"><spring:message code="label.foodBill"/></label>
         <form:input class="form-control input-sm" path="tuesdayBill" readonly="true" style="font-weight: 700;"/>
        </div>
       </td>
       <td scope="row">
          <div class="form-group">
          <label for="sel1"><spring:message code="label.foodBill"/></label>
          <form:input class="form-control input-sm" path="wednesdayBill" readonly="true" style="font-weight: 700;"/>
        </div>
       </td>
       <td scope="row">
          <div class="form-group">
         <label for="sel1"><spring:message code="label.foodBill"/></label>
         <form:input class="form-control input-sm" path="thursdayBill" readonly="true" style="font-weight: 700;"/>
        </div>
       </td>
       <td scope="row">
          <div class="form-group">
         <label for="sel1"><spring:message code="label.foodBill"/></label>
         <form:input class="form-control input-sm" path="fridayBill" readonly="true" style="font-weight: 700;"/>
        </div>
       </td>
      </tr>
        
     </tbody>
    </table>
   				 <div class="col-md-12 text-center">
					<br><label for="sel1"><spring:message code="label.weekBill"/></label>:${amount}
				</div>
      
                <div class="col-md-12 text-center" style="margin-bottom:15px;">
					<br>  <a href="monthlyBill"><button type="button" class="btn btn-success btn-sm" aria-hidden="true"><spring:message code="label.back"/></button></a>
				</div>
					
    </form:form>
  </div>
 </div>

</body>
</html>