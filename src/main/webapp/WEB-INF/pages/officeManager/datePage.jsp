<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
  
		<link href="<%=request.getContextPath()%>/resources/css/datepicker.min.css" rel="stylesheet">
		<script src="<%=request.getContextPath()%>/resources/js/jquery.min.js"></script>
		<script src="<%=request.getContextPath()%>/resources/js/datepicker.js"></script>
		
		<script src="<%=request.getContextPath()%>/resources/js/datepicker.en.js"></script>


  
  <script>
  
			function validation() {

				var tDate = document.getElementById('monday').value;
				from = tDate.split("/");
				var date = new Date(from[2], from[1] - 1, from[0]);
				
				if (date.getDay() == 1 || date.getDay() == 2 || date.getDay() == 3 || date.getDay() == 4 || date.getDay() == 5) {
					document.getElementById('monday').style.borderColor = "green";
					
					
				} else {
					document.getElementById('monday').style.borderColor = "red";
					
					return false;
				}

			}
			
			/* function validation()
			{
				if (confirm("Are you sure??") == true) {
			      return true;
			    } else {
			       return false;
			    }
			} */
		</script>

<body>


<div id="section2" class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
  <div class="col-sm-12">
    <div class="modal-header">
     <h4> <spring:message code="label.weekBillDetails"/> </h4>
    </div>
    <div class="error">
     <p style="text-align:center;color:red;">${success}</p>
    </div>
    <div class="add_emp_details_form">
			<div class="Emp_content">
				
				
				<div class="modal-body" style="width: 100%;margin:0px auto;">
				  <div class="form-horizontal" role="form">
    <form:form action="getWeekBill" id="timeSheet" method="post" commandName="timeSheetForm" onsubmit="return validation();">	
      <div class="form-group">
	      <label class="col-md-2 control-label"><spring:message code="label.weekDate"/></label>
	      <div class="col-md-5">
	      	 	<form:input path="monday" id="monday" data-multiple-dates="0" data-multiple-dates-separator=", " data-position="bottom left" data-language="en" class="datepicker-here form-control input-sm"/>
	      </div>
      </div>
   	  <div class="form-group">
	   	  <div class="col-sm-offset-2 col-sm-5">
	       	<input type="submit" class="btn btn-success btn-sm" value="Get Details">
	       
	        &nbsp;&nbsp; <a href="officeManager"><button type="button" class="btn btn-success btn-sm" aria-hidden="true"><spring:message code="label.back" /></button></a>
	      </div>
      </div>
     
    </form:form>
    </div>
    </div>
  </div>
 </div>
</div>
</div>
</body>
</html>