
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<script>
	function val() {

		if (canSubmit == false) {
			return false;
		}
	}
	
</script>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

	 
	
	<div class="modal-header">
		<h4><b><spring:message code="label.busPassBill"/></b><span style="color:red">*</span></h4>
	</div>
	  <div class="error">
     <p style="text-align:center;color:red;">${success}</p>
    </div>
		<div class="add_emp_details_form">
			<div class="Emp_content">
				
				<form:form action="getUpdate" method="post" commandName="foodBillForm" onsubmit="return val();">
				<form:hidden path="id"/>		
				
				  <div class="form-horizontal" role="form">
					  <div class="form-group">
						<label class="control-label col-sm-4" for="email"><b><spring:message code="label.userName"/></b><span style="color:red">*</span> :</label>
						<div class="col-sm-8">
						  <form:input path="userName" type="text" class="form-control input-sm" id="userName" readonly="true"/>
						</div>
					</div>
					
					
					<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.name"/></b><span style="color:red">*</span> :</label>
						<div class="col-sm-8"> 
						  <form:input path="name" type="text" class="form-control input-sm" id="title" readonly="true"/>
						</div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.month"/></b><span style="color:red">*</span> :</label>
						<div class="col-sm-8"> 
						  <form:input path="month" type="text" class="form-control input-sm" id="title" readonly="true"/>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.year"/></b><span style="color:red">*</span> :</label>
						<div class="col-sm-8"> 
						  <form:input path="year" type="text" class="form-control input-sm" id="title" readonly="true"/>
						</div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.amount"/></b><span style="color:red">*</span> :</label>
						<div class="col-sm-8"> 
						  <form:input path="money" type="text" class="form-control input-sm" id="title" readonly="true"/>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.foodAmount"/></b><span style="color:red">*</span> :</label>
						<div class="col-sm-8"> 
						  <form:input path="foodBill" type="text" class="form-control input-sm" id="title" readonly="true"/>
						</div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.presence"/></b><span style="color:red">*</span> :</label>
						<div class="col-sm-8"> 
						  <form:input path="presents" type="text" class="form-control input-sm" id="title"/>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.veg"/></b><span style="color:red">*</span> :</label>
						<div class="col-sm-8"> 
						  <form:input path="veg" type="text" class="form-control input-sm" id="title"/>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.nonVeg"/></b><span style="color:red">*</span> :</label>
						<div class="col-sm-8"> 
						  <form:input path="nonVeg" type="text" class="form-control input-sm" id="title"/>
						</div>
					</div>
					
					
			
					
					<div class="form-group">
		                        <div class="text-center">
							<input type="submit" class="btn btn-primary btn-sm" value="Update" style="margin-top: 26px;width: 18%;"/>
							<a href="busPassBill" class="btn btn-primary btn-sm" style="margin-top: 26px;width: 18%;"><spring:message code="label.back"/></a>
					  </div>
			        </div>
					
				</div>
				</form:form>
				</div>
				
				</div>
				
			</div>
			
	 
	<script>
		$('ul.nav li.dropdown').hover(function() {
		  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
		}, function() {
		  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
		});
		
	</script>
	<script>
		$(document).ready(function() {
			var readURL = function(input) {
				if (input.files && input.files[0]) {
					var reader = new FileReader();

					reader.onload = function (e) {
						$('.profile-pic').attr('src', e.target.result);
					}
			
					reader.readAsDataURL(input.files[0]);
				}
			}
			

			$(".file-upload").on('change', function(){
				readURL(this);
			});
			
			$(".upload-button").on('click', function() {
			   $(".file-upload").click();
			});
		});
	</script>
	<script>
		$('#datepicker-1').datepicker();
	
</script>
</body>