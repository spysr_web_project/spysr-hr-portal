<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
  
		<link href="<%=request.getContextPath()%>/resources/css/datepicker.min.css" rel="stylesheet">
		<script src="<%=request.getContextPath()%>/resources/js/jquery.min.js"></script>
		<script src="<%=request.getContextPath()%>/resources/js/datepicker.js"></script>
		
		<script src="<%=request.getContextPath()%>/resources/js/datepicker.en.js"></script>


  
  <script>
  
			
			function validation() {

				var month = document.getElementById('month').value;
				
				var year = document.getElementById('year').value;

				if (month == '0') {

					document.getElementById('month').style.borderColor = "red";

					return false;

				} else {
					document.getElementById('month').style.borderColor = "green";
				}
				if (year == 0) {

					document.getElementById('year').style.borderColor = "red";

					return false;

				} else {
					document.getElementById('year').style.borderColor = "green";
				}

			}
		</script>

<body>


<div id="section2" class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
  <div class="col-sm-12">
    <div class="modal-header">
     <h4> <spring:message code="label.monthBill"/> </h4>
    </div>
    <div class="error">
     <p style="text-align:center;color:red;">${success}</p>
    </div>
    <div class="add_emp_details_form">
			<div class="Emp_content">
				
				
				<div class="modal-body" style="width: 100%;margin:0px auto;">
				  <div class="form-horizontal" role="form">
    <form:form action="getMonthBill" id="timeSheet" method="post" commandName="timeSheetForm" onsubmit="return validation();">	
      <div class="form-group">
	     <label class="col-md-1 control-label"><spring:message code="label.month"/></label>
	     <div class="col-md-3">
	      	 	<form:select path="month" id="month" data-multiple-dates="0" data-multiple-dates-separator=", " data-position="bottom left" data-language="en"  style="width: 100%;display: inline;" class="datepicker-here form-control input-sm">
	      <form:option value="0">select</form:option>
	      <form:option value="01">January</form:option>
	      <form:option value="02">February</form:option>
	      <form:option value="03">March</form:option>
	      <form:option value="04">April</form:option>
	      <form:option value="05">May</form:option>
	      <form:option value="06">June</form:option>
	      <form:option value="07">July</form:option>
	      <form:option value="08">August</form:option>
	      <form:option value="09">September</form:option>
	      <form:option value="10">October</form:option>
	      <form:option value="11">November</form:option>
	      <form:option value="12">December</form:option>
	      </form:select> 
		</div>
		
	       <label class="col-md-1 control-label"><spring:message code="label.year"/></label>
	       <div class="col-md-3">
	      <form:select path="year" id="year" data-multiple-dates="0" data-multiple-dates-separator=", " data-position="bottom left" data-language="en"  style="width: 100%;display: inline;" class="datepicker-here form-control input-sm">
	      <form:option value="0">select</form:option>
	      <form:option value="2016"></form:option>
	      <form:option value="2017"></form:option>
	      <form:option value="2018"></form:option>
	      <form:option value="2019"></form:option>
	      <form:option value="2020"></form:option>
	      <form:option value="2021"></form:option>
	      <form:option value="2023"></form:option>
	      <form:option value="2024"></form:option>
	      <form:option value="2025"></form:option>
	      <form:option value="2026"></form:option>
	      <form:option value="2027"></form:option>
	      <form:option value="2028"></form:option>
	      <form:option value="2029"></form:option>
	      <form:option value="2030"></form:option>
	      </form:select>
	      </div>
      </div>
   	  <div class="form-group">
	   	  <div class="col-sm-offset-1 col-sm-5">
	       	<input type="submit" class="btn btn-success btn-sm" value="Get Details">
	       
	        &nbsp;&nbsp; <a href="officeManager"><button type="button" class="btn btn-success btn-sm" aria-hidden="true"><spring:message code="label.back" /></button></a>
	      </div>
      </div>
     
    </form:form>
    </div>
    </div>
  </div>
 </div>
</div>
</div>
</body>
</html>