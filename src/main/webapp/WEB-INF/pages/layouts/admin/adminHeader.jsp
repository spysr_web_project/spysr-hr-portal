<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" href="../../favicon.ico">

<title>SpYsR HR</title>


<!-- Bootstrap core CSS -->
<!-- Font Awesome CSS -->
<link href="<%=request.getContextPath()%>/resources/css/datepicker3.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/css/bootstrap2.min.css"
	rel="stylesheet">

<link
	href="<%=request.getContextPath()%>/resources/css/bootstrap.min.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/css/font-awesome.css"
	rel="stylesheet">
<link
	href="<%=request.getContextPath()%>/resources/css/font-awesome.min.css"
	rel="stylesheet">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/resources/css/bootstrap.min.css">
<link href="<%=request.getContextPath()%>/resources/css/dashboard.css"
	rel="stylesheet">



<script src="<%=request.getContextPath()%>/resources/js/jquery.min.js"></script>
<script
	src="<%=request.getContextPath()%>/resources/js/bootstrap.min.js"></script>

<script
	scr="<%=request.getContextPath()%>/resources/js/bootstrap-datepicker.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/hover.js"></script>

<!-- Custom styles for this template -->
<style>
li:hover .submenu {
	display: block;
	max-height: 200px;
}

.submenu a:hover {
	color: #fff;
	text-decoration: none;
}

.submenu {
	overflow: hidden;
	max-height: 0;
	-webkit-transition: all 0.5s ease-out;
	background-color: #042037;
}

.submenu>li {
	position: relative;
	display: block;
	padding: 10px 15px;
}

.submenu li a {
	color: #E7E7E7;
	font-weight: 500;
	font-size: 12px;
	letter-spacing: 0.5px;
}

.submenu>li>a:focus,.submenu>li>a:hover {
	text-decoration: none;
	olor: #0b5173;
}

.submenu>.active>a,.submenu>.active>a:hover,.submenu>.active>a:focus {
	color: #fff;
	text-shadow: rgba(0, 0, 0, 0.25) 0 -1px 0;
}
</style>

</head>

<body>

	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid header_main">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="adminPage"><img
					src="<%=request.getContextPath()%>/resources/images/logo.png"
					class="img-responsive comy_logo hidden-xs"><span
					class="comy_name">SpYsR HR</span></a>
			</div>
			<div class="sub_menu">

				<div id="navbar" class="navbar-collapse collapse">
					<ul class="nav navbar-nav navbar-left nav_height">
						<li style="position: absolute; top: -3px;"><img
							src="<%=request.getContextPath()%>/resources/images/human-resources-management-icon.png"
							class="img-responsive comy_mascot hidden-xs"></li>
					</ul>
					<ul class="nav navbar-nav navbar-right nav_height">

						<%-- 	<div class="pull-left">

				<div class="prof_img"><a href="editAdminProfile?id=${requestCurrentUser.id}"><span><img src="${requestCurrentUser.imageName}" class="profile_img"></span><span class="customer_name"><spring:message code="label.welcome" /> ${requestCurrentUser.displayName}</span></a></div>
         </div> 
		 --%>
						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown"><spring:message
									code="label.menuThemes" /> <b class="caret"></b></a>
							<ul class="dropdown-menu mydrop">
								<li><a href="themeChange?theme=themeRed"><spring:message
											code="label.red" /></a></li>
								<li><a href="themeChange?theme=themeBlue"><spring:message
											code="label.blue" /></a></li>
								<li><a href="themeChange?theme=themeGreen"><spring:message
											code="label.green" /></a></li>
								<li><a href="themeChange?theme=themeOrange"><spring:message
											code="label.orange" /></a></li>
							</ul></li>
						<li class="dropdown"><a href="#" class="dropdown-toggle"
							data-toggle="dropdown"><spring:message code="label.language" />
								<b class="caret"></b></a>
							<ul class="dropdown-menu mydrop scroll">
								<li><a href="getLocaleLang?lang=en"
									<c:if test="${model.user.prefferedLanguage == 'en'}"> selected="selected"</c:if>><i
										class="flags flag6"></i>English</a></li>
								<li><a href="getLocaleLang?lang=sp"
									<c:if test="${model.user.prefferedLanguage == 'sp'}"> selected="selected"</c:if>><i
										class="flags flag2"></i>Spanish</a></li>
								<li><a href="getLocaleLang?lang=de"
									<c:if test="${model.user.prefferedLanguage == 'de'}"> selected="selected"</c:if>><i
										class="flags flag3"></i>German</a></li>
								<li><a href="getLocaleLang?lang=fr"
									<c:if test="${model.user.prefferedLanguage == 'fr'}"> selected="selected"</c:if>><i
										class="flags flag4"></i>French</a></li>
								<li><a href="getLocaleLang?lang=it"
									<c:if test="${model.user.prefferedLanguage == 'it'}"> selected="selected"</c:if>><i
										class="flags flag5"></i>Italian</a></li>
								<li><a href="getLocaleLang?lang=ar"
									<c:if test="${model.user.prefferedLanguage == 'ar'}"> selected="selected"</c:if>><i
										class="flags flag7"></i>Arabic</a></li>
								<li><a href="getLocaleLang?lang=pr"
									<c:if test="${model.user.prefferedLanguage == 'pr'}"> selected="selected"</c:if>><i
										class="flags flag8"></i>Portugese</a></li>
								<li><a href="getLocaleLang?lang=ch"
									<c:if test="${model.user.prefferedLanguage == 'ch'}"> selected="selected"</c:if>><i
										class="flags flag9"></i>Chinese</a></li>
								<li><a href="getLocaleLang?lang=jp"
									<c:if test="${model.user.prefferedLanguage == 'jp'}"> selected="selected"</c:if>><i
										class="flags flag10"></i>Japnese</a></li>
								<li><a href="getLocaleLang?lang=rs"
									<c:if test="${model.user.prefferedLanguage == 'rs'}"> selected="selected"</c:if>><i
										class="flags flag11"></i>Russian</a></li>
								<li><a href="getLocaleLang?lang=hn"
									<c:if test="${model.user.prefferedLanguage == 'hn'}"> selected="selected"</c:if>><i
										class="flags flag12"></i>Hindi</a></li>
								<li><a href="getLocaleLang?lang=bu"
									<c:if test="${model.user.prefferedLanguage == 'bu'}"> selected="selected"</c:if>><i
										class="flags flag13"></i>Bulgerian</a></li>
								<li><a href="getLocaleLang?lang=hu"
									<c:if test="${model.user.prefferedLanguage == 'hu'}"> selected="selected"</c:if>><i
										class="flags flag14"></i>Hungerian</a></li>
								<li><a href="getLocaleLang?lang=cz"
									<c:if test="${model.user.prefferedLanguage == 'cz'}"> selected="selected"</c:if>><i
										class="flags flag15"></i>Czhec</a></li>
								<li><a href="getLocaleLang?lang=uk"
									<c:if test="${model.user.prefferedLanguage == 'uk'}"> selected="selected"</c:if>><i
										class="flags flag16"></i>Ukreninan</a></li>
								<li><a href="getLocaleLang?lang=cr"
									<c:if test="${model.user.prefferedLanguage == 'cr'}"> selected="selected"</c:if>><i
										class="flags flag17"></i>Croatian</a></li>
							</ul></li>
						<li><a href="editAdminProfile?id=${requestCurrentUser.id}"><spring:message
									code="label.myProfile" /></a></li>
						<li><a href="<c:url value="/j_spring_security_logout"/>"><spring:message
									code="label.logout" /></a></li>
					</ul>
				</div>
			</div>
	</nav>

	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-3 col-md-2 sidebar">
				<ul class="nav nav-sidebar">

					<!--   <li><a href="#"><i class="fa fa-dashboard"></i>&nbsp;Dashboard</a></li> -->
					<li><a href="employeeDetails"><i class="fa fa-users"></i>&nbsp;<b><spring:message
									code="label.employees"></spring:message></b> <span class="sr-only">(current)</span></a></li>
					<li><a href="getUsersForBlockUnblockRenew"><i
							class="fa fa-ban"></i>/&nbsp;<i class="fa fa-retweet"
							aria-hidden="true"></i>&nbsp;<b><spring:message
									code="label.accountBlockRenew"></spring:message></b> <span
							class="sr-only">(current)</span></a></li>
					<li><a href="approvalPendingManager"><i
							class="fa fa-wheelchair" aria-hidden="true"></i><b><spring:message
									code="label.employeeaprrovalPending"></spring:message></b> </a></li>
					<li><a href="projectDetails"><i class="fa fa-th"
							aria-hidden="true"></i> <b><spring:message
									code="label.productDetails" /></b></a></li>
					<li><a href="testDemo"><i class="fa fa-th"
							aria-hidden="true"></i> <b><spring:message
									code="label.testDemo" /></b></a></li>
					<li><a href="noticeDetails"><i class="fa fa-th-list"
							aria-hidden="true"></i><b><spring:message
									code="label.noticedetails"></spring:message></b></a></li>
					<li><a href="employeeLeaveAppliedDetails"><i
							class="fa fa-table" aria-hidden="true"></i><b><spring:message
									code="label.leaveApprovalPendingDetails"></spring:message></b></a></li>
					<li><a href="#"><i class="fa fa-table" aria-hidden="true"></i><b><spring:message
									code="label.approval"></spring:message></b></a>
						<ul class="submenu">
							<li><a href="approveTimeSheetDetails"><i
									class="fa fa-table" aria-hidden="true"></i><b><spring:message
											code="label.timeSheetDetails"></spring:message></b></a></li>
						</ul></li>

					<li><a href="leaveTypeList"><i class="fa fa-table"
							aria-hidden="true"></i><b>Leave Type</b></a></li>
					<li><a href="timeSheetDetails"><i class="fa fa-table"
							aria-hidden="true"></i><b><spring:message
									code="label.timeSheetDetails"></spring:message></b></a></li>
					<li><a href="jobOffers"><i class="fa fa-table"
							aria-hidden="true"></i><b><spring:message
									code="label.jobOffers"></spring:message></b></a></li>
					<li><a href="busPassBill"><i class="fa fa-table"
							aria-hidden="true"></i><b><spring:message code="label.bills"></spring:message></b></a>
						<%-- <li><a href="foodbillCalculation"><i class="fa fa-table" aria-hidden="true"></i><b><spring:message code="label.foodbill"></spring:message></b></a></li>            
	        </ --%>
						<ul>
			</div>
		</div>
	</div>

	<script>
			$('ul.nav li.dropdown').hover(function() {
			  $(this).find('.mydrop').stop(true, true).delay(200).fadeIn(500);
			}, function() {
			  $(this).find('.mydrop').stop(true, true).delay(200).fadeOut(500);
			});
		
		</script>
	<script>
	 $(function(){
    	 var url = window.location.pathname;  
    	 var activePage = url.substring(url.lastIndexOf('/')+1);
    	$('.nav li a').each(function(){  
    	var currentPage = this.href.substring(this.href.lastIndexOf('/')+1);

    	if (activePage == currentPage) {
    	$(this).parent().addClass('active'); 
    	} 
    	});
    	 })
		$(document).ready(function(){
		    $(".badge").click(function(){
		        $(this).hide(500);
		    });
		});
		
		$(".nav a").on("click", function(){
			   $(".nav").find(".active").removeClass("active");
			   $(this).parent().addClass("active");
			});
	</script>


</body>