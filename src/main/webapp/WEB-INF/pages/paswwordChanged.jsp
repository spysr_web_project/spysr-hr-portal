<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<style>
	h5{
		color:green;
		text-align:center;
	}
</style>
</head>
<body>
<div class="modal fade" id="overlay">
  <div class="modal-dialog">
    <div class="modal-content" style="padding:15px;">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h5 class="modal-title">Password Change successfully !</h5>
      </div>
      <div class="modal-body">
         <a href="/spysr-hr-portal/auth/logout"><button type="button" class="btn btn-success btn-sm" aria-hidden="true">Back to home</button></a>
      </div>
    </div>
  </div>
</div>
<script>
$('#overlay').modal('show');

setTimeout(function() {
    $('#overlay').modal('hide');
}, 5000);
</script>
</body>
</html>