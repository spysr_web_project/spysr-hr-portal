<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<script>
	function validateForm() {

		var newPassword = document.getElementById('newPassword');
		
		var confirmNewPassword = document.getElementById('confirmNewPassword');

		var canSubmit = true;

		if (document.getElementById('newPassword').value == '') {
			document.getElementById('newPasswordError').style.display = 'block';
			canSubmit = false;
		} else {
			document.getElementById('newPasswordError').style.display = 'none';
		}
		
		if (document.getElementById('confirmNewPassword').value == '') {
			document.getElementById('confirmNewPasswordError').style.display = 'block';
			canSubmit = false;
		} else {
			document.getElementById('confirmNewPasswordError').style.display = 'none';
		}
		
		if(document.editProfile.newPassword.value == document.editProfile.confirmNewPassword.value )
			{
			document.getElementById('alertPWD').style.display = 'none';			
		} else {
			
			document.getElementById('alertPWD').style.display = 'block';
			canSubmit = false;
		}
	

		if (canSubmit == false) {
			return false;
		}

	}
</script>

<style>
	.login-panel.panel.panel-default {
    width: 64%;
    margin: 0px auto;
    position: absolute;
    top: 35%;
    left: 17%;
}
</style>	
	
	
	<div class="container">
	<div class="row">
		<div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Reset Password</h3>
                    </div>
                    <div class="panel-body">
                       <c:url value="/j_spring_security_check" var="loginUrl" />
			
					<form:form action="newForgotPassword" name="editProfile" autocomplete="off" onsubmit="return validateForm()" commandName="endUserForm">
						
					<form:hidden path="id"/>
					<%-- 	<form:hidden path="transactionId"/> --%>
				 <div id="alertPWD" style="display: none; color: red;">Both should be matched</div>
				<div id="alertPWD" style="display: none; color: red;">Both should be matched</div>
                            <fieldset>
                                <div class="form-group">
                                	<label class="control-label col-sm-3">New Password :</label>
                                	<div class="col-sm-9">
                   <form:password path="newPassword" class="form-control" placeholder="Enter New Password" id="newPassword"  autofocus="" style="margin-bottom: 18px;"/>
                  <div id="newPasswordError" style="display: none; color: red;">New Password is Mandatory</div>
				<div id="newPasswordError" style="display: none; color: red;">New Password is Mandatory</div>
									</div>
                                </div>
                                <div class="form-group">
                                	<label class="control-label col-sm-3">Confirm New password :</label>
                                	<div class="col-sm-9">
                                   		   <form:password path="confirmNewPassword" class="form-control" placeholder="confirm New Password" id="confirmNewPassword"  autofocus="" style="margin-bottom: 18px;"/>
              						    <div id="confirmNewPasswordError" style="display: none; color: red;">Confirm New Password is Mandatory</div>
										<div id="confirmNewPasswordError" style="display: none; color: red;">Confirm New Password is Mandatory</div>
                                </div>
                               </div>
                               <div class="form-group">
                               <div class="col-sm-offset-3 col-sm-8">
                                	 <input type="submit" value="Save" class="btn btn-md btn-success btn-block" style="width: 45%;margin-top:5px;display: inline-block;"/>
                                	<!--  <input type="button" value="back" class="btn btn-md btn-success btn-block" style="width: 45%;float: right;display: inline-block;"/> -->
                                		<a class="btn btn-md btn-success btn-block" style="display:inline-block; width: 45%;margin-top: 0px;" href="logout">Back</a>
                               </div>
                               </div>
                            </fieldset>
                       </form:form>
                    </div>
                    
                </div>
                
	</div>
</div>