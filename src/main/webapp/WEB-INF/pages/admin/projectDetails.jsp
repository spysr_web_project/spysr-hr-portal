<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

	<script src="<%=request.getContextPath()%>/resources/js/search.js"></script>
	<style>
		.filter{
			margin: 0px auto;
		    width: 281px;
		    float: right;
		    margin-top: 3px;	
		}
		td{
		    word-break: break-all;
		    }
	</style>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			  
				<div class="col-sm-12">
					<div class="add_emp">
						<a href="addProjects"> <button type="button" class="btn btn-info btn-md page-header"><b><spring:message code="label.addproject"></spring:message></b></button></a>
					</div>
				</div>
				<div class="col-sm-12">
					<div class="input-group filter"> <span class="input-group-addon">Search</span>
						<input id="filter" type="text" class="form-control" placeholder="Type here...">
					</div>
					<div class="add_emp_details">
						 <table class="table table-responsive table-bordered table-striped" id="filter" style="width:100%" data-rt-breakpoint="600">
                                        <thead>
                                            <tr>
                                                <th scope="col" data-rt-column="ID"><spring:message code="label.serialNo"/></th>
                                               
                                                <th scope="col" data-rt-column="Status"><spring:message code="label.projectName"/></th>
                                                 <th scope="col" data-rt-column="Project"><spring:message code="label.projectDetails"/></th>
                                                 <th scope="col" data-rt-column="Project"><spring:message code="label.version"/></th>
                                                 <th scope="col" data-rt-column="Project"><spring:message code="label.membersInvolved"/></th> 
                                                 <th scope="col" data-rt-column="Project"><spring:message code="label.action"/></th> 
                                            </tr>
                                        </thead>
                                        <tbody class="searchable">
                                        <c:if test="${! empty productList}">

					             		<c:forEach items="${productList}" var="project">
                                            <tr>                                         
                                      <td><c:out value="${project.serialNo}"></c:out></td>
                                      <td><c:out value="${project.projectName}"></c:out></td>
                                    	
							          <td><span><c:out value="${project.projectDetails}"></c:out></span></td>
							         
							          <td><c:out value="${project.version }"></c:out></td>
							          <td><c:out value="${project.membersInvolved}"></c:out></td>
							        <td><a href="selectprojectadminUpdate?id=${project.id}" class="btn btn-primary btn-sm"><spring:message code="label.edit" /></a></td>
                                            </tr>  
                                            	</c:forEach>
					                         </c:if>                                        
                                        </tbody>
                                    </table>
                                          
                                    
					</div>
					<div class="col-sm-12" style="margin-top:15px;">
                                    <div class="form-group">
										<div class="col-sm-offset-5 col-sm-5">
											 <a href ="/spysr-hr-portal/admin/adminPage"  class="btn btn-primary btn-sm"><b><spring:message code="label.back"></spring:message></b></a>	
										</div>
				                    </div>  
					</div>
			</div>
      
</div>
	<script>
		$('ul.nav li.dropdown').hover(function() {
		  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
		}, function() {
		  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
		});
		
	</script>
	
  </body>
</html>
