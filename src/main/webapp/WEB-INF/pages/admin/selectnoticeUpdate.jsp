<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<!--  datepicker css js links -->
<link
	href="<%=request.getContextPath()%>/resources/css/datepicker.min.css"
	rel="stylesheet">
<script src="<%=request.getContextPath()%>/resources/js/jquery.min.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/datepicker.js"></script>

<script
	src="<%=request.getContextPath()%>/resources/js/datepicker.en.js"></script>
<!--  End datepicker css js links -->
<script>
	function val() {

		var designation = document.getElementById('designation').value;

		var department = document.getElementById('department').value;

		var currentRole = document.getElementById('currentRole').value;

		if (canSubmit == false) {
			return false;
		}
	}
</script>


<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

	<form:form action="selectnoticeUpdate2" method="post"
		commandName="noticeDetailsForm" onsubmit="return val();"
		enctype="multipart/form-data">
		<div class="col-sm-12">

			<div class="add_emp_details_form">



				<div class="modal-header">
					<h4>
						<b><spring:message code="label.noticedetails" /></b><span
							style="color: red">*</span>
					</h4>
				</div>
				<div class="add_emp_details_form">
					<div class="Emp_content">
						<div class="modal-body" style="width: 100%; margin: 0px auto;">
							<div class="form-horizontal" role="form">

								<div class="form-group">
									<label class="control-label col-sm-4" for="pwd"><b><spring:message
												code="label.serialNo" /></b><span style="color: red">*</span>:</label>
									<div class="col-sm-8">
										<form:hidden path="id" />
										<form:input path="serialNo" class="form-control input-sm"
											placeholder="Enter serialNo"
											value="${model.noticeDetailsForm.serialNo}" />
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-sm-4" for="pwd"><b><spring:message
												code="label.title" /></b><span style="color: red">*</span>
										:</label>
									<div class="col-sm-8">
										<form:input path="title" class="form-control input-sm"
											id="title" placeholder="Enter Title"
											value="${model.noticeDetailsForm.title}" />
									</div>
								</div>

								<div class="form-group">
									<label class="control-label col-sm-4" for="pwd"><b><spring:message
												code="label.description" /></b><span style="color: red">*</span>
										:</label>
									<div class="col-sm-8">
										<form:textarea path="description" maxlength="255"
											class="form-control input-sm" id="description"
											placeholder="Enter description"
											value="${model.noticeDetailsForm.description}" />
									</div>
								</div>
								
								
									<div class="form-group">
									<label class="control-label col-sm-4" for="pwd"><b><spring:message
												code="label.noticeDate" /></b><span style="color: red">*</span>
										:</label>
									<div class="col-sm-8">
										<div id="sandbox-container">
										   <form:input path="noticeDate" data-multiple-dates="0" data-multiple-dates-separator=", " data-position="top left" data-language="en" class="form-control input-sm datepicker-here" placeholder="Enter noticeDate" id="datepicker" />
		     <div id="date1Error" style="display: none; color: red;"><spring:message code="label.validation"/></div>
			 <div id="date2Error" style="display: none; color: red;">To Date cannot be greater than today's date</div>
			 <div id="date3Error" style="display: none; color: red;"><spring:message code="label.dateConfirmation"/></div>				
									</div>
								</div>
								

								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>





		<div class="col-sm-12">
			<div class="text-center">
				<input type="submit" class="btn btn-primary btn-sm" value="Save"
					style="margin-top: 26px; width: 14%;" />
				<!-- Save <i class="fa fa-save"></i> -->
				<td><a href="/spysr-hr-portal/admin/adminPage"
					class="btn btn-primary btn-sm" value="Back"
					style="margin-top: 26px; width: 14%;"><spring:message
							code="label.back" /></a></td>
			</div>

		</div>
	</form:form>
</div>
<script>
	$('ul.nav li.dropdown').hover(
			function() {
				$(this).find('.dropdown-menu').stop(true, true).delay(200)
						.fadeIn(500);
			},
			function() {
				$(this).find('.dropdown-menu').stop(true, true).delay(200)
						.fadeOut(500);
			});
</script>
<script>
	$(document).ready(function() {
		var readURL = function(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function(e) {
					$('.profile-pic').attr('src', e.target.result);
				}

				reader.readAsDataURL(input.files[0]);
			}
		}

		$(".file-upload").on('change', function() {
			readURL(this);
		});

		$(".upload-button").on('click', function() {
			$(".file-upload").click();
		});
	});
</script>
<script>
	$('#datepicker-1').datepicker();
</script>
