<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <form:form action="updateEditAdminPWD" name="editProfile"
				autocomplete="off" onsubmit="return validateForm()"
				commandName="endUserForm">
        
	<div class="modal-header">
		<h4><b><spring:message code="label.changePassword"/></b><span style="color:red">*</span></h4>
	</div>
		<div class="add_emp_details_form">
			<div class="Emp_content">
				
					<form:hidden path="id" />
					<div id="alertPWD" style="display: none; color: red;">
						<spring:message code="label.passVal" />
					</div>
				
				 <div class="modal-body" style="width: 80%;">
				  <div class="form-horizontal" role="form">
					  <div class="form-group">
						<label class="control-label col-sm-4" for="email"><spring:message code="label.newPass" /><span style="color:red">*</span> :</label>
						<div class="col-sm-8">
						 <form:password path="newPassword" class="form-control input-sm" placeholder="Enter a Password" id="newPassword" />
						</div>
					</div> 
       
                      <span class="form-group">
						<label class="control-label col-sm-4" for="pwd"></label>
						<span class="col-sm-8"> 
						 <label id="newPasswordError" class="error" style="display: none; color: red;"><b><spring:message code="label.passVal1"/></b><span style="color:red">*</span>:</label>
						</span>
						</span>
						
       
       	     	    <div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.confirmPass" /></b><span style="color:red">*</span> :</label>
						<div class="col-sm-8"> 
						<form:password path="confirmNewPassword" name="subject" id="confirmNewPassword" class="form-control input-sm" placeholder="Confrim New Password" />
						</div>
					</div>
					
					  <div class="form-group">
						<label class="control-label col-sm-4" for="pwd"></label>
						<div class="col-sm-8"> 
						 <label id="confirmNewPasswordError" class="error" style="display: none; color: red;"><b><spring:message code="label.passVal2"/></b><span style="color:red">*</span>:</label>
						</div>
						
					
					
                     <div class="form-group">
						
						<div class="col-sm-offset-4 col-sm-5">
							 <input type="submit" value="<spring:message code="label.update"/>" class="btn btn-primary btn-md">
				 		     <a href="/spysr-hr-portal/admin/editAdminProfile?id=1" class="btn btn-success"><spring:message code="label.back" /></a>
						</div>
                     </div>
                     </div>
                     </div>
                     </div>
          </div>
          </form:form>  
          
    </div>


<script>
	function validateForm() {

		var newPassword = document.getElementById('newPassword');

		var confirmNewPassword = document.getElementById('confirmNewPassword');
		var password = /^[A-Za-z0-9!@#$%^&*()_]{6,20}$/;
		var canSubmit = true;

		if (newPassword.value.match(/^[A-Za-z0-9!@#$%^&*()_]{6,20}$/)
				&& !(document.getElementById('newPassword').value == '')) {

			document.getElementById('newPasswordError').style.display = 'none';

		} else {
			document.getElementById('newPasswordError').style.display = 'block';
			canSubmit = false;
		}

		if (document.getElementById('confirmNewPassword').value == '') {
			document.getElementById('confirmNewPassword').style.borderColor = "red";
			return false;
		} else {
			document.getElementById('confirmNewPassword').style.borderColor = "green";
		}

		if (document.editProfile.newPassword.value == document.editProfile.confirmNewPassword.value) {
			document.getElementById('alertPWD').style.display = 'none';
		} else {

			document.getElementById('alertPWD').style.display = 'block';
			canSubmit = false;
		}

		if (canSubmit == false) {
			return false;
		}

	}
</script>