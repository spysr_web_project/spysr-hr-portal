<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
<form:form action="leavePendingemployeeRequesthowConfrim" commandName="leaveRequestForm" onsubmit="return val();">
	<div class="col-sm-12">

 <div class="form-horizontal" role="form">

		<div class="modal-header">
				  <h4><b><spring:message code="label.leaveApprovalPendingDetails"/></b></h4>
				</div>
		<div class="add_emp_details_form">
			<div class="Emp_content">
			             <form:hidden path="email"/>
				          <form:hidden path="id"/>
			              <form:hidden path="reason"/>
			            <%--   <form:hidden path="leaveStatus"/> --%>
						  <form:hidden path="eligibleLeave"/>
						  <form:hidden path="totalLeave"/>
				           <form:hidden path="leaveLength"/>
				             <form:hidden path="leaveDate"/>
				           <%--  <form:hidden path="leaveStatus"/> --%>
				              <form:hidden path="eligibleLeave"/>
				                <form:hidden path="totalLeave"/>
					<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.empid" /></b></label>
						<div class="col-sm-8"> 
						<form:input path="userName" class="form-control" placeholder="Enter UserName"  value="${model.leaveRequestForm.userName}" readonly="true"/>
						</div>
						</div>
			
					
						<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.name" /></b></label>
						<div class="col-sm-8"> 
				     <form:input path="name" class="form-control" placeholder="Enter department"  value="${model.leaveRequestForm.name}"  readonly="true" />
						</div>
					</div>
					
			
			<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.typesofleave" /></b></label>
						<div class="col-sm-8"> 
						<form:input path="typesofLeave" class="form-control" placeholder="Enter Name"  value="${model.leaveRequestForm.typesofLeave}"  readonly="true"/>
						</div>
						</div>
			
							<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.totalnumberleaveDays" /></b></label>
						<div class="col-sm-8"> 
						<form:input path="totalnumberleaveDays" class="form-control" placeholder="Enter totalnumber leaveDays"  value="${model.leaveRequestForm.totalnumberleaveDays}" readonly="true"/>
						</div>
						</div>
						
					<%-- 
							<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.leavedate"/></b><span style="color:red">*</span> :</label>
						<div class="col-sm-8"> 
						  <form:input path="leaveDate" class="form-control"   value="${model.LeaveRequestForm.leaveDate}" readonly="true"/>
						</div>
					</div> --%>
			
				<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.fromdate"/></b></label>
						<div class="col-sm-8"> 
						  <form:input path="fromDate" class="form-control"  placeholder="Enter role"  value="${model.LeaveRequestForm.fromDate}" readonly="true"/>
						</div>
					</div>
					
					
						<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.todate"/></b></label>
						<div class="col-sm-8"> 
						  <form:input path="toDate" class="form-control"  placeholder="Enter password"  value="${model.LeaveRequestForm.toDate}" readonly="true"/>
						</div>
					</div>
					
			
						<div class="form-group">
						<label class="control-label col-sm-4" id="DateDemo"><b><spring:message code="label.status" /></b><span style="color: red">*</span> :</label>
						<div class="col-sm-8"> 
						
						
						
						<form:select path="status" class="form-control input-sm" id="status">
						<form:option value="">
							<spring:message code="label.selectValue" />
						</form:option>
						<form:option value="Approved">
							<spring:message code="label.approve" />
						</form:option>
						<form:option value="Rejected">
							<spring:message code="label.reject" />
						</form:option>
					</form:select>
					</div>
						</div>	
		
		       <div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.comment" /></b><span style="color: red">*</span>:</label>
						<div class="col-sm-8"> 
						<form:textarea path="comment" class="form-control"  placeholder="Enter comment" id="comment"/>
						</div>
						</div>
	
					
	</div>
	</div></div>									
					 <div class="form-group">
		                 <div class="text-center">
							<input type="submit" class="btn btn-primary btn-sm" value="Save" style="margin-top: 26px;width: 18%;"/>
							<a href="/spysr-hr-portal/admin/employeeLeaveAppliedDetails" class="btn btn-primary btn-sm" value="Save" style="margin-top: 26px;width: 18%;"><spring:message code="label.back"/></a>
					 	 </div>
			        </div>
								
								
								
		</form:form>
</div>
	<script>
		function val(){
					
				
					var status  = document.getElementById('status').value;
					var comment  = document.getElementById('comment').value;
					var canSubmit = true; 
					
					
					if (status == '') {

						document.getElementById('status').style.borderColor = "red";
						return false;
					} else {
						document.getElementById('status').style.borderColor = "green";
					}	
					if (comment == '') {

						document.getElementById('comment').style.borderColor = "red";
						return false;
					} else {
						document.getElementById('comment').style.borderColor = "green";
					}	
					
					
				
				
					if(canSubmit == false){
						return false;
					}
				}
			
				
		</script>