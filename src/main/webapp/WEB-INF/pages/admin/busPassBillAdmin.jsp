<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
  
		<link href="<%=request.getContextPath()%>/resources/css/datepicker.min.css" rel="stylesheet">
		<script src="<%=request.getContextPath()%>/resources/js/jquery.min.js"></script>
		<script src="<%=request.getContextPath()%>/resources/js/datepicker.js"></script>
		
		<script src="<%=request.getContextPath()%>/resources/js/datepicker.en.js"></script>


  
  <script>
  
			
			function validation() {

				var userName = document.getElementById('userName').value;

				if (userName == 'Select') {
					document.getElementById('userName').style.borderColor = "red";

					return false;

				} else {
					document.getElementById('userName').style.borderColor = "green";
				}

			}
		</script>

<body>


<div id="section2" class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
  <div class="col-sm-12">
    <div class="modal-header">
     <h4> <spring:message code="label.bills"/> </h4>
    </div>
    <div class="error">
     <p style="text-align:center;color:red;">${success}</p>
    </div>
    <div class="add_emp_details_form">
			<div class="Emp_content">
				
				
				<div class="modal-body" style="width: 100%;margin:0px auto;">
				  <div class="form-horizontal" role="form">
    <form:form action="getBusPassBill" id="" method="post" commandName="foodBillForm" onsubmit="return validation();">	
      <div class="form-group">
	      <label class="col-md-2 control-label"><spring:message code="label.bills"/></label>
	      <div class="col-md-5">
	      	 	<form:select path="userName" id="userName" data-multiple-dates="0" data-multiple-dates-separator=", " data-position="bottom left" data-language="en" class="datepicker-here form-control input-sm">
	      	 	<form:option value="Select"></form:option>
	      	 	<form:options items="${foodBillForm.foodBills}"
									itemValue="userName" itemLabel="userName" />
	      	 	</form:select>
	      </div>
      </div>
   	  <div class="form-group">
	   	  <div class="col-sm-offset-2 col-sm-5">
	       	<input type="submit" class="btn btn-success btn-sm" value="Get Details">
	       
	        &nbsp;&nbsp; <a href="adminPage"><button type="button" class="btn btn-success btn-sm" aria-hidden="true"><spring:message code="label.back" /></button></a>
	      </div>
      </div>
     
    </form:form>
    </div>
    </div>
  </div>
 </div>
</div>
</div>
</body>
</html>