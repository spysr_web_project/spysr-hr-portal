<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<div class="admin-profile" style="width: 60%;margin:0px auto;">
		<div class="form-horizontal" role="form">
			<form:form name="bankDetails" action="updateAdminDetails" commandName="endUserForm" onsubmit="return validateForm()">
					<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message
										code="label.userName" /></b>:</label>
										
							<form:hidden path="id" />
										
						<div class="col-sm-8"> 
				   			 <form:input path="userName" class="form-control input-sm" id="userName" autocomplete="off" readonly="true"></form:input>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message
										code="label.displayName" /></b>:</label>
						<div class="col-sm-8"> 
				  			<form:input path="displayName" placeholder="Enter  Display Name" class="form-control input-sm" id="displayName" readonly="true" autocomplete="off"></form:input>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message
										code="label.email" /></b>:</label>
						<div class="col-sm-8"> 
				   <form:input path="email" class="form-control input-sm" id="email" autocomplete="off" readonly="true"></form:input>
						</div>
					</div>
				<%-- 	<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message
										code="label.altEmail" /></b>:</label>
						<div class="col-sm-8"> 
				   <form:input path="altEmail" class="form-control input-sm" id="altEmail" autocomplete="off" readonly="true"></form:input>
						</div>
					</div> --%>
					<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message
										code="label.contactNumber" /></b>:</label>
						<div class="col-sm-8"> 
				   			<form:input path="contactNo" class="form-control input-sm" id="contactNo" autocomplete="off" readonly="true"></form:input>
						</div>
					</div>
					<%-- <div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message
										code="label.altContactNo" /></b>:</label>
						<div class="col-sm-8"> 
				   			<form:input path="altContactNo" class="form-control input-sm" id="altContactNo" autocomplete="off" readonly="true"></form:input>
						</div>
					</div> --%>
					<div class="col-sm-12">
						<div class="text-center">
							<input type="submit" class="btn btn-success btn-sm" value="Confirm" style="margin-top: 26px;width: 15%;"/>
		                   <a href="/spysr-hr-portal/admin/editAdminProfile?id=1" class="btn btn-success btn-sm" style="margin-top: 26px;width: 15%;"><spring:message code="label.back"/></a>
						</div>
		
					</div>
			</form:form>
		</div>
	</div>
</div>
