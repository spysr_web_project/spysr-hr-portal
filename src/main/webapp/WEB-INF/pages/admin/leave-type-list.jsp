<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>


 <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			  <div class="col-sm-12">
					<div class="add_emp">
						<a href="#" data-toggle="modal" data-target="#leavetracker-settings-leavetype-add" data-id="2" data-status="Blocked" class="open-rateDialog btn btn-primary">Add Leave Type</a>
					</div>
			</div>
				
	
				<div class="col-sm-12">
					<div class="input-group filter"> <span class="input-group-addon"><spring:message code="label.search"/></span>
						<input id="filter" type="text" class="form-control" placeholder="Type here...">
					</div>
					<div class="add_emp_details">
						 <table class="table table-bordered table-striped example" id="filter" data-rt-breakpoint="600">
                                        <thead>
                                            <tr>
                                                <th scope="col" data-rt-column="desc"><spring:message code="label.description"/></th> 
                                                <th scope="col" data-rt-column="leaveType"><spring:message code="label.leaveType"/></th>
                                                <th scope="col" data-rt-column="name"><spring:message code="label.name"/></th>
                                                <th scope="col" data-rt-column="leaveLength"><spring:message code="label.leaveLength"/></th>
                                                <th scope="col" data-rt-column="leaveFor"><spring:message code="label.leaveFor"/></th>
                                                <th scope="col" data-rt-column="leavePeriod"><spring:message code="label.leavePeriod"/></th>
                                                 <th scope="col" data-rt-column="leaveCount"><spring:message code="label.leaveCount"/></th> 
                                                <th scope="col" data-rt-column="leaveApplicableFor"><spring:message code="label.leaveApplicableFor"/></th>
                                                <th scope="col" data-rt-column="Progress"><spring:message code="label.action"/></th>
                                            </tr>
                                        </thead>
                                        <tbody class="searchable">
                                        <c:if test="${! empty leaveTypeList}">

							             	<c:forEach items="${leaveTypeList}" var="leaveType">
			                                       <tr>                                         
						                                <td><c:out value="${leaveType.decription}"></c:out></td> 
														<td><c:out value="${leaveType.leaveType}"></c:out></td>
														<td><c:out value="${leaveType.name}"></c:out></td>
														<td><c:out value="${leaveType.leaveLength}"></c:out></td>
													    <td><c:out value="${leaveType.leaveFor}"></c:out></td>
													    <td><c:out value="${leaveType.leavePeriod}"></c:out></td>
													    <td><c:out value="${leaveType.leaveApplicableFor}"></c:out></td>
													    <td><c:out value="${leaveType.leaveCount}"></c:out></td>
						                                <td><a href="leaveTypeUpdate?id=${leaveType.id}" class="btn btn-primary btn-sm"><spring:message code="label.edit" /></a></td>
			
												  </tr>  
		                                     </c:forEach>
					                        </c:if>                                        
                                        </tbody>
                                    </table>
					</div>
				</div>
			</div>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<form:form action="leavePendingemployeeRequesthowConfrim"
		commandName="leaveRequestForm" onsubmit="return val();">
		<div class="col-sm-12">

			<div class="form-horizontal" role="form">

				<div class="modal-header">
					<h4>
						<b><spring:message code="label.leaveApprovalPendingDetails" /></b>
					</h4>
				</div>
				<div class="add_emp_details_form">
					<div class="Emp_content">
						<form:hidden path="email" />
						<form:hidden path="id" />
						<form:hidden path="reason" />
						  <form:hidden path="leaveStatus"/>
						<form:hidden path="eligibleLeave" />
						<form:hidden path="totalLeave" />
						<form:hidden path="leaveLength" />
						<form:hidden path="leaveDate" />
						 <form:hidden path="leaveStatus"/>
						<form:hidden path="eligibleLeave" />
						<form:hidden path="totalLeave" />
						<div class="form-group">
							<label class="control-label col-sm-4" for="pwd"><b><spring:message
										code="label.empid" /></b></label>
							<div class="col-sm-8">
								<form:input path="userName" class="form-control"
									placeholder="Enter UserName"
									value="${model.leaveRequestForm.userName}" readonly="true" />
							</div>
						</div>


						<div class="form-group">
							<label class="control-label col-sm-4" for="pwd"><b><spring:message
										code="label.name" /></b></label>
							<div class="col-sm-8">
								<form:input path="name" class="form-control"
									placeholder="Enter department"
									value="${model.leaveRequestForm.name}" readonly="true" />
							</div>
						</div>


						<div class="form-group">
							<label class="control-label col-sm-4" for="pwd"><b><spring:message
										code="label.typesofleave" /></b></label>
							<div class="col-sm-8">
								<form:input path="typesofLeave" class="form-control"
									placeholder="Enter Name"
									value="${model.leaveRequestForm.typesofLeave}" readonly="true" />
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-sm-4" for="pwd"><b><spring:message
										code="label.totalnumberleaveDays" /></b></label>
							<div class="col-sm-8">
								<form:input path="totalnumberleaveDays" class="form-control"
									placeholder="Enter totalnumber leaveDays"
									value="${model.leaveRequestForm.totalnumberleaveDays}"
									readonly="true" />
							</div>
						</div>

						
							<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.leavedate"/></b><span style="color:red">*</span> :</label>
						<div class="col-sm-8"> 
						  <form:input path="leaveDate" class="form-control"   value="${model.LeaveRequestForm.leaveDate}" readonly="true"/>
						</div>
					</div>

						<div class="form-group">
							<label class="control-label col-sm-4" for="pwd"><b><spring:message
										code="label.fromdate" /></b></label>
							<div class="col-sm-8">
								<form:input path="fromDate" class="form-control"
									placeholder="Enter role"
									value="${model.LeaveRequestForm.fromDate}" readonly="true" />
							</div>
						</div>


						<div class="form-group">
							<label class="control-label col-sm-4" for="pwd"><b><spring:message
										code="label.todate" /></b></label>
							<div class="col-sm-8">
								<form:input path="toDate" class="form-control"
									placeholder="Enter password"
									value="${model.LeaveRequestForm.toDate}" readonly="true" />
							</div>
						</div>


						<div class="form-group">
							<label class="control-label col-sm-4" id="DateDemo"><b><spring:message
										code="label.status" /></b><span style="color: red">*</span> :</label>
							<div class="col-sm-8">



								<form:select path="status" class="form-control input-sm"
									id="status">
									<form:option value="">
										<spring:message code="label.selectValue" />
									</form:option>
									<form:option value="Approved">
										<spring:message code="label.approve" />
									</form:option>
									<form:option value="Rejected">
										<spring:message code="label.reject" />
									</form:option>
								</form:select>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-sm-4" for="pwd"><b><spring:message
										code="label.comment" /></b><span style="color: red">*</span>:</label>
							<div class="col-sm-8">
								<form:textarea path="comment" class="form-control"
									placeholder="Enter comment" id="comment" />
							</div>
						</div>


					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="text-center">
					<input type="submit" class="btn btn-primary btn-sm" value="Save"
						style="margin-top: 26px; width: 18%;" /> <a
						href="/spysr-hr-portal/admin/employeeLeaveAppliedDetails"
						class="btn btn-primary btn-sm" value="Save"
						style="margin-top: 26px; width: 18%;"><spring:message
							code="label.back" /></a>
				</div>
			</div>
	</form:form>
 --%>
	<div class="modal popRgtModel tabPopModel in myModalLabel"
		id="leavetracker-settings-leavetype-add" role="dialog"
		aria-labelledby="modal" aria-hidden="false">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header PR">
					<button aria-label="Close" data-dismiss="modal" class="close"
						type="button">
						<span aria-hidden="true"><div class="IC-cls-1px red S21 CP"></div></span>
					</button>
					<h6 class="addPopTitle" id="zp_modal_header">New Leave Type</h6>
				</div>
				<div class="custom-tab">
					<ul role="tablist" class="show-grid">
						<li class="col-sm-6 active"><a data-toggle="tab"
							href="#Leavetype_Basic" id="basic" aria-expanded="true">Leave
								Entitlement</a></li>
						<li class="col-sm-6"><a data-toggle="tab"
							href="#Leavetype_Advance" aria-expanded="false"> Advanced
								Settings</a></li>
						<div class="clearfix"></div>
					</ul>
				</div>
				<div class="modal-body ZPRPbody" id="zp_modal_body">
					<div class="tab-content">
						<div class="tab-pane fade active in" id="Leavetype_Basic">
							<form class="form-horizontal pad" role="form">
								<div class="form-group">
									<label class="control-label col-sm-3"><span class="red">*
											&nbsp; </span>Name : </label>
									<div class="col-sm-6">
										<input type="text" name="name" class="form-control"><span
											class="ERR MT10" style="display: none;"></span>
										<div class="colorPalete dropdown">
											<ul class="PR">
												<li name="leavecolor" data-toggle="dropdown" class="CL1"></li>
												<ul class="dropdown-menu ZPCmn-Arrow left gray">
													<li class="CL1 active"></li>
													<li class="CL2"></li>
													<li class="CL3"></li>
													<li class="CL4"></li>
													<li class="CL5"></li>
													<li class="CL6"></li>
													<li class="CL7"></li>
													<li class="CL8"></li>
													<li class="CL9"></li>
													<li class="CL10"></li>
												</ul>
											</ul>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-3">Type : </label>
									<div class="col-sm-6">
										<div class="select2-container" id="s2id_autogen424">
											<a href="javascript:void(0)" onclick="return false;"
												class="select2-choice" tabindex="-1"> <span>Paid</span><abbr
												class="select2-search-choice-close" style="display: none;"></abbr>
												<div>
													<b></b>
												</div></a><input class="select2-focusser select2-offscreen"
												type="text" id="s2id_autogen425">
											<div class="select2-drop select2-with-searchbox"
												style="display: none">
												<div class="select2-search">
													<input type="text" autocomplete="off" class="select2-input">
												</div>
												<ul class="select2-results">
												</ul>
											</div>
										</div>
										<select name="type" class="select2-offscreen form-control input-sm"  tabindex="-1"><option
												value="0">Paid</option>
											<option value="3">Unpaid</option>
											<option value="1">On Duty</option>
											<option value="5">Restricted Holiday</option></select>
									</div>
								</div>
								<div class="form-group" id="LT_Unit">
									<label class="control-label col-sm-3">Leave Unit : </label>
									<div class="col-sm-6">
										<label class="radio DIB MR8"><input type="radio"
											id="day" name="leaveunit" value="379493000000039887"
											checked="checked"><span class="ML2">Days </span></label> <label
											class="radio DIB MR8 restricted"><input type="radio"
											id="hour" name="leaveunit" value="379493000000039889"><span
											class="ML2">Hours</span></label>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-3">Description : </label>
									<div class="col-sm-6">
										<textarea rows="5" class="form-control" name="remarks"
											maxlength="5000"></textarea>
									</div>
								</div>
								<div class="form-group">
									<label class="col-sm-12 text-left  MB15 MT15 S16"><b>Applicable
											: </b></label>
									<div class="col-sm-9">
										<div class="LT_Applicable">
											<div id="zp_doubleComponent">
												<ul class="applcnList row">
													<li class="col-sm-6 text-center active"><a
														data-toggle="tab" href="#zp_dc_applicableFor"
														aria-expanded="true">Applicable For </a></li>
													<li class="col-sm-6 text-center"><a data-toggle="tab"
														href="#zp_dc_notApplicableFor" aria-expanded="true">Not
															Applicable For </a></li>
												</ul>
												<div class="tab-content">
													<div id="zp_dc_applicableFor" class="tab-pane active">
														<div class="form-group">
															<div id="zp_dc_applicableFor_inner"
																class="col-sm-12 appliforid">
																<div class="input-group dropdown"
																	id="commonPickListComp">
																	<div id="key_list"
																		class="form-control DN input-lbltags">
																		<span class="clearfix DN"></span>
																		<div id="zp_shared_list_Departments">
																			<div id="zp_shared_sub_list_header"
																				class=" appltagh DN">
																				<span class="clearfix"></span>
																				<p id="header_name">
																					<i class="IC-mini S12"
																						onmousedown="ZPDoublePickList.expandCollapseHeader(event,this)"></i>Departments
																				</p>
																				<button class=" btn btn-default btn-sm FR DN"
																					type="button"
																					onclick="ZPDoublePickList.clearSharedDisplayList(event,this)"
																					key="Departments">Clear</button>
																				<div class="clearfix"></div>
																			</div>
																			<div id="zp_shared_sub_list_records"
																				class="cust-labelTags MT3 MB10"></div>
																		</div>
																		<span class="clearfix DN"></span>
																		<div id="zp_shared_list_Designations">
																			<div id="zp_shared_sub_list_header"
																				class=" appltagh DN">
																				<span class="clearfix"></span>
																				<p id="header_name">
																					<i class="IC-mini S12"
																						onmousedown="ZPDoublePickList.expandCollapseHeader(event,this)"></i>Designations
																				</p>
																				<button class=" btn btn-default btn-sm FR DN"
																					type="button"
																					onclick="ZPDoublePickList.clearSharedDisplayList(event,this)"
																					key="Designations">Clear</button>
																				<div class="clearfix"></div>
																			</div>
																			<div id="zp_shared_sub_list_records"
																				class="cust-labelTags MT3 MB10"></div>
																		</div>
																		<span class="clearfix DN"></span>
																		<div id="zp_shared_list_Genders">
																			<div id="zp_shared_sub_list_header"
																				class=" appltagh DN">
																				<span class="clearfix"></span>
																				<p id="header_name">
																					<i class="IC-mini S12"
																						onmousedown="ZPDoublePickList.expandCollapseHeader(event,this)"></i>Genders
																				</p>
																				<button class=" btn btn-default btn-sm FR DN"
																					type="button"
																					onclick="ZPDoublePickList.clearSharedDisplayList(event,this)"
																					key="Genders">Clear</button>
																				<div class="clearfix"></div>
																			</div>
																			<div id="zp_shared_sub_list_records"
																				class="cust-labelTags MT3 MB10"></div>
																		</div>
																		<span class="clearfix DN"></span>
																		<div id="zp_shared_list_Locations">
																			<div id="zp_shared_sub_list_header"
																				class=" appltagh DN">
																				<span class="clearfix"></span>
																				<p id="header_name">
																					<i class="IC-mini S12"
																						onmousedown="ZPDoublePickList.expandCollapseHeader(event,this)"></i>Locations
																				</p>
																				<button class=" btn btn-default btn-sm FR DN"
																					type="button"
																					onclick="ZPDoublePickList.clearSharedDisplayList(event,this)"
																					key="Locations">Clear</button>
																				<div class="clearfix"></div>
																			</div>
																			<div id="zp_shared_sub_list_records"
																				class="cust-labelTags MT3 MB10"></div>
																		</div>
																		<span class="clearfix DN"></span>
																		<div id="zp_shared_list_MaritalStatus">
																			<div id="zp_shared_sub_list_header"
																				class=" appltagh DN">
																				<span class="clearfix"></span>
																				<p id="header_name">
																					<i class="IC-mini S12"
																						onmousedown="ZPDoublePickList.expandCollapseHeader(event,this)"></i>Marital
																					Status
																				</p>
																				<button class=" btn btn-default btn-sm FR DN"
																					type="button"
																					onclick="ZPDoublePickList.clearSharedDisplayList(event,this)"
																					key="MaritalStatus">Clear</button>
																				<div class="clearfix"></div>
																			</div>
																			<div id="zp_shared_sub_list_records"
																				class="cust-labelTags MT3 MB10"></div>
																		</div>
																		<span class="clearfix DN"></span>
																		<div id="zp_shared_list_Roles">
																			<div id="zp_shared_sub_list_header"
																				class=" appltagh DN">
																				<span class="clearfix"></span>
																				<p id="header_name">
																					<i class="IC-mini S12"
																						onmousedown="ZPDoublePickList.expandCollapseHeader(event,this)"></i>Roles
																				</p>
																				<button class=" btn btn-default btn-sm FR DN"
																					type="button"
																					onclick="ZPDoublePickList.clearSharedDisplayList(event,this)"
																					key="Roles">Clear</button>
																				<div class="clearfix"></div>
																			</div>
																			<div id="zp_shared_sub_list_records"
																				class="cust-labelTags MT3 MB10"></div>
																		</div>
																		<span class="clearfix DN" style="display: inline;"></span>
																		<div id="zp_shared_list_Users">
																			<div id="zp_shared_sub_list_header"
																				class=" appltagh DN" style="display: block;">
																				<span class="clearfix"></span>
																				<p id="header_name">
																					<i class="IC-mini S12"
																						onmousedown="ZPDoublePickList.expandCollapseHeader(event,this)"></i>Users
																				</p>
																				<button class="btn btn-default btn-sm FR"
																					type="button"
																					onclick="ZPDoublePickList.clearSharedDisplayList(event,this)"
																					key="Users">Clear</button>
																				<div class="clearfix"></div>
																			</div>
																			<div id="zp_shared_sub_list_records"
																				class="cust-labelTags MT3 MB10">
																				<span id="-1" alldata="true"
																					class="label label-primary FL MR5">All Users<i
																					class="IC-cls S8 ML5"
																					onclick="ZPDoublePickList.removeRecordFromSharedList(event,this, &quot;zp_dc_applicableFor_inner&quot;, &quot;Users&quot;)"></i></span>
																			</div>
																		</div>
																	</div>
																	<span aria-expanded="false"
																		class="input-group-addon BL0I" data-toggle="dropdown"><i
																		class="IC-addlist gry CP"></i></span>
																	<div
																		class="dropdown-menu ZPCusPd ZPappBox  ZPCmn-Arrow top gray ZPSchPoP">
																		<div class="ZPTabVt">
																			<div class="lfttab">
																				<ul id="leftSideActiveKeyList" role="tablist"
																					class="nav nav-pills nav-stacked">
																					<li id="activeField_Departments"
																						role="presentation" activekey="Departments"
																						class="showThisKey"><a data-toggle="tab"
																						role="tab"
																						onclick="ZPDoublePickList.getRecords(this)"
																						aria-expanded="true">Departments</a></li>
																					<li id="activeField_Designations"
																						role="presentation" activekey="Designations"
																						class="showThisKey"><a data-toggle="tab"
																						role="tab"
																						onclick="ZPDoublePickList.getRecords(this)"
																						aria-expanded="true">Designations</a></li>
																					<li id="activeField_Genders" role="presentation"
																						activekey="Genders" class="showThisKey"><a
																						data-toggle="tab" role="tab"
																						onclick="ZPDoublePickList.getRecords(this)"
																						aria-expanded="true">Genders</a></li>
																					<li id="activeField_Locations" role="presentation"
																						activekey="Locations" class="showThisKey"><a
																						data-toggle="tab" role="tab"
																						onclick="ZPDoublePickList.getRecords(this)"
																						aria-expanded="true">Locations</a></li>
																					<li id="activeField_MaritalStatus"
																						role="presentation" activekey="MaritalStatus"
																						class="showThisKey"><a data-toggle="tab"
																						role="tab"
																						onclick="ZPDoublePickList.getRecords(this)"
																						aria-expanded="true">Marital Status</a></li>
																					<li id="activeField_Roles" role="presentation"
																						activekey="Roles" class="showThisKey"><a
																						data-toggle="tab" role="tab"
																						onclick="ZPDoublePickList.getRecords(this)"
																						aria-expanded="true">Roles</a></li>
																					<li id="activeField_Users" role="presentation"
																						activekey="Users" class="showThisKey"><a
																						data-toggle="tab" role="tab"
																						onclick="ZPDoublePickList.getRecords(this)"
																						aria-expanded="true">Users</a></li>
																				</ul>
																			</div>
																			<div class="rhttabscrl">
																				<div class="tab-content">
																					<div class="tab-pane active" role="tabpanel">
																						<div>
																							<div class="ZPtopSearch">
																								<label class="checkbox DIB FL PT8 MR10"
																									title="Select All" id="zp_sharedto_all_label"><input
																									id="zp_sharedto_all_checkbox" type="checkbox"
																									onclick="ZPDoublePickList.allCheckSelection('zp_dc_applicableFor_inner')"><span></span></label>
																								<div class="ZPsearch" id="ZPsearch">
																									<div class="dvsearch Dropdown PR">
																										<span
																											class="ZPico-search FL MT2 ML8 ZPtooltip IC-lens S16"
																											zptitle="Search Category"></span>
																									</div>
																									<input type="search" class="ZPselect"
																										id="dvZPselect" placeholder="Search" value=""
																										name="Search">
																								</div>
																							</div>
																							<div class="ZPapplCb">
																								<ul id="searchResult_ul"></ul>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													<div id="zp_dc_notApplicableFor" class="tab-pane">
														<div class="form-group">
															<div id="zp_dc_notApplicableFor_inner"
																class="col-sm-12 appliforid">
																<div class="input-group dropdown"
																	id="commonPickListComp">
																	<div id="key_list" class="form-control DN">
																		<span class="clearfix DN"></span>
																		<div id="zp_shared_list_Departments">
																			<div id="zp_shared_sub_list_header"
																				class=" appltagh DN">
																				<span class="clearfix"></span>
																				<p id="header_name">
																					<i class="IC-mini S12"
																						onmousedown="ZPDoublePickList.expandCollapseHeader(event,this)"></i>Departments
																				</p>
																				<button class=" btn btn-default btn-sm FR DN"
																					type="button"
																					onclick="ZPDoublePickList.clearSharedDisplayList(event,this)"
																					key="Departments">Clear</button>
																				<div class="clearfix"></div>
																			</div>
																			<div id="zp_shared_sub_list_records"
																				class="cust-labelTags MT3 MB10"></div>
																		</div>
																		<span class="clearfix DN"></span>
																		<div id="zp_shared_list_Designations">
																			<div id="zp_shared_sub_list_header"
																				class=" appltagh DN">
																				<span class="clearfix"></span>
																				<p id="header_name">
																					<i class="IC-mini S12"
																						onmousedown="ZPDoublePickList.expandCollapseHeader(event,this)"></i>Designations
																				</p>
																				<button class=" btn btn-default btn-sm FR DN"
																					type="button"
																					onclick="ZPDoublePickList.clearSharedDisplayList(event,this)"
																					key="Designations">Clear</button>
																				<div class="clearfix"></div>
																			</div>
																			<div id="zp_shared_sub_list_records"
																				class="cust-labelTags MT3 MB10"></div>
																		</div>
																		<span class="clearfix DN"></span>
																		<div id="zp_shared_list_Locations">
																			<div id="zp_shared_sub_list_header"
																				class=" appltagh DN">
																				<span class="clearfix"></span>
																				<p id="header_name">
																					<i class="IC-mini S12"
																						onmousedown="ZPDoublePickList.expandCollapseHeader(event,this)"></i>Locations
																				</p>
																				<button class=" btn btn-default btn-sm FR DN"
																					type="button"
																					onclick="ZPDoublePickList.clearSharedDisplayList(event,this)"
																					key="Locations">Clear</button>
																				<div class="clearfix"></div>
																			</div>
																			<div id="zp_shared_sub_list_records"
																				class="cust-labelTags MT3 MB10"></div>
																		</div>
																		<span class="clearfix DN"></span>
																		<div id="zp_shared_list_Roles">
																			<div id="zp_shared_sub_list_header"
																				class=" appltagh DN">
																				<span class="clearfix"></span>
																				<p id="header_name">
																					<i class="IC-mini S12"
																						onmousedown="ZPDoublePickList.expandCollapseHeader(event,this)"></i>Roles
																				</p>
																				<button class=" btn btn-default btn-sm FR DN"
																					type="button"
																					onclick="ZPDoublePickList.clearSharedDisplayList(event,this)"
																					key="Roles">Clear</button>
																				<div class="clearfix"></div>
																			</div>
																			<div id="zp_shared_sub_list_records"
																				class="cust-labelTags MT3 MB10"></div>
																		</div>
																		<span class="clearfix DN" style="display: none;"></span>
																		<div id="zp_shared_list_Users">
																			<div id="zp_shared_sub_list_header"
																				class=" appltagh DN" style="display: none;">
																				<span class="clearfix"></span>
																				<p id="header_name">
																					<i class="IC-mini S12"
																						onmousedown="ZPDoublePickList.expandCollapseHeader(event,this)"></i>Users
																				</p>
																				<button class="btn btn-default btn-sm FR DN"
																					type="button"
																					onclick="ZPDoublePickList.clearSharedDisplayList(event,this)"
																					key="Users">Clear</button>
																				<div class="clearfix"></div>
																			</div>
																			<div id="zp_shared_sub_list_records"
																				class="cust-labelTags MT3 MB10"></div>
																		</div>
																	</div>
																	<span aria-expanded="false"
																		class="input-group-addon BL0I" data-toggle="dropdown"><i
																		class="IC-addlist gry CP"></i></span>
																	<div
																		class="dropdown-menu ZPCusPd ZPappBox  ZPCmn-Arrow top gray ZPSchPoP">
																		<div class="ZPTabVt">
																			<div class="lfttab">
																				<ul id="leftSideActiveKeyList" role="tablist"
																					class="nav nav-pills nav-stacked">
																					<li id="activeField_Departments"
																						role="presentation" activekey="Departments"
																						class="showThisKey"><a data-toggle="tab"
																						role="tab"
																						onclick="ZPDoublePickList.getRecords(this)"
																						aria-expanded="true">Departments</a></li>
																					<li id="activeField_Designations"
																						role="presentation" activekey="Designations"
																						class="showThisKey"><a data-toggle="tab"
																						role="tab"
																						onclick="ZPDoublePickList.getRecords(this)"
																						aria-expanded="true">Designations</a></li>
																					<li id="activeField_Locations" role="presentation"
																						activekey="Locations" class="showThisKey"><a
																						data-toggle="tab" role="tab"
																						onclick="ZPDoublePickList.getRecords(this)"
																						aria-expanded="true">Locations</a></li>
																					<li id="activeField_Roles" role="presentation"
																						activekey="Roles" class="showThisKey"><a
																						data-toggle="tab" role="tab"
																						onclick="ZPDoublePickList.getRecords(this)"
																						aria-expanded="true">Roles</a></li>
																					<li id="activeField_Users" role="presentation"
																						activekey="Users" class="" style="display: none;"><a
																						data-toggle="tab" role="tab"
																						onclick="ZPDoublePickList.getRecords(this)"
																						aria-expanded="true">Users</a></li>
																				</ul>
																			</div>
																			<div class="rhttabscrl">
																				<div class="tab-content">
																					<div class="tab-pane active" role="tabpanel">
																						<div>
																							<div class="ZPtopSearch">
																								<label class="checkbox DIB FL PT8 MR10"
																									title="Select All" id="zp_sharedto_all_label"><input
																									id="zp_sharedto_all_checkbox" type="checkbox"
																									onclick="ZPDoublePickList.allCheckSelection('zp_dc_notApplicableFor_inner')"><span></span></label>
																								<div class="ZPsearch" id="ZPsearch">
																									<div class="dvsearch Dropdown PR">
																										<span
																											class="ZPico-search FL MT2 ML8 ZPtooltip IC-lens S16"
																											zptitle="Search Category"></span>
																									</div>
																									<input type="search" class="ZPselect"
																										id="dvZPselect" placeholder="Search" value=""
																										name="Search">
																								</div>
																							</div>
																							<div class="ZPapplCb">
																								<ul id="searchResult_ul"></ul>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="form-group" id="LT_Accrual">
									<label class="col-sm-12 text-left MB15 MT15 S16"><b>Leave
											Entitlement</b></label> <label class="control-label col-sm-3">Period
										: </label>
									<div class="col-sm-6">
										<div class="select2-container" id="s2id_autogen426">
											<a href="javascript:void(0)" onclick="return false;"
												class="select2-choice" tabindex="-1"> <span>Annual</span><abbr
												class="select2-search-choice-close" style="display: none;"></abbr>
												<div>
													<b></b>
												</div></a><input class="select2-focusser select2-offscreen"
												type="text" id="s2id_autogen427">
											<div class="select2-drop select2-with-searchbox"
												style="display: none">
												<div class="select2-search">
													<input type="text" autocomplete="off" class="select2-input">
												</div>
												<ul class="select2-results">
												</ul>
											</div>
										</div>
										<select name="accrual_period" class="select2-offscreen"
											tabindex="-1"><option value="379493000000056001">Bi
												Weekly</option>
											<option value="379493000000096001">Half Monthly</option>
											<option value="379493000000039897">Monthly</option>
											<option value="379493000000039895">Bi Monthly</option>
											<option value="379493000000039899">Quarterly</option>
											<option value="379493000000039901">Half yearly</option>
											<option value="-1" selected="selected">Annual</option></select><span
											class="oeweek">Accrual</span>
									</div>
								</div>
								<div class="form-group" id="accrual_time" style="display: none;">
									<label class="control-label col-sm-3">On : </label>
									<div class="col-sm-6">
										<div class="select2-container" id="s2id_autogen428">
											<a href="javascript:void(0)" onclick="return false;"
												class="select2-choice" tabindex="-1"> <span>Beginning
													of accrual period</span><abbr class="select2-search-choice-close"
												style="display: none;"></abbr>
												<div>
													<b></b>
												</div></a><input class="select2-focusser select2-offscreen"
												type="text" id="s2id_autogen429">
											<div class="select2-drop select2-with-searchbox"
												style="display: none">
												<div class="select2-search">
													<input type="text" autocomplete="off" class="select2-input"
														tabindex="-1">
												</div>
												<ul class="select2-results">
												</ul>
											</div>
										</div>
										<select name="accrual_time" class="select2-offscreen"
											tabindex="-1"><option id="begining"
												value="379493000000039903">Beginning of accrual
												period</option>
											<option id="end" value="379493000000039905">End of
												accrual period</option>
											<option id="doj" value="379493000000075001">Based on
												Date of Joining</option></select>
									</div>
								</div>
								<div class="form-group" id="ApplyGorup">
									<label class="control-label col-sm-3">For : </label>
									<div class="col-sm-6">
										<label class="radio DIB MR8"><input type="radio"
											value="All" name="applyon" checked="checked"><span
											class="ML2">All Employees</span></label> <label class="radio DIB MR8"><input
											type="radio" value="Exp" name="applyon"><span
											class="ML2">Experience based</span></label>
									</div>
								</div>
								<div class="form-group collapse in" id="AllEmp">
									<label class="control-label col-sm-3">Leave Count : </label>
									<div class="col-sm-2">
										<input type="text" class="form-control" name="leavecount"
											value="0" maxlength="6">
									</div>
									<div class="col-sm-6 MT7">
										<span class="leaveunithelp">day(s)</span>&nbsp;<span>/</span>&nbsp;<span
											id="leaveperiodhelp">year</span>
									</div>
								</div>
								<div class="form-group" id="accrual_reset"
									style="display: none;">
									<label class="control-label col-sm-3"> Accrual Reset :
									</label>
									<div class="col-sm-6">
										<div class="select2-container" id="s2id_autogen430">
											<a href="javascript:void(0)" onclick="return false;"
												class="select2-choice" tabindex="-1"> <span>No
													reset</span><abbr class="select2-search-choice-close"
												style="display: none;"></abbr>
												<div>
													<b></b>
												</div></a><input class="select2-focusser select2-offscreen"
												type="text" id="s2id_autogen431">
											<div class="select2-drop select2-with-searchbox"
												style="display: none">
												<div class="select2-search">
													<input type="text" autocomplete="off" class="select2-input"
														tabindex="-1">
												</div>
												<ul class="select2-results">
												</ul>
											</div>
										</div>
										<select name="accrual_reset" class="select2-offscreen"
											tabindex="-1"><option value="0" selected="selected">No
												reset</option>
											<option value="1">Reset existing accrual count to 0
												on accrue date</option></select>
									</div>
								</div>
							</form>
							<div class="table-responsive collapse" id="ExperienceConf"
								style="display: none;">
								<table class="table table-hover">
									<thead>
										<tr class="TblGryBg">
											<th width="15"></th>
											<th width="55%">Experience</th>
											<th>Leave Count&nbsp;<span class="leaveunithelp">day(s)</span>&nbsp;<span>/</span>&nbsp;<span
												id="leaveperiodhelp">year</span></th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<tr class="ZPLRow" id="exp_tr" style="display: none">
											<td></td>
											<td>
												<div
													class="select2-container select2-container-disabled wid3 FL MR15"
													id="s2id_autogen432">
													<a href="javascript:void(0)" onclick="return false;"
														class="select2-choice" tabindex="-1"> <span>After</span><abbr
														class="select2-search-choice-close" style="display: none;"></abbr>
														<div>
															<b></b>
														</div></a><input class="select2-focusser select2-offscreen"
														type="text" id="s2id_autogen433" disabled="disabled">
													<div class="select2-drop select2-with-searchbox"
														style="display: none">
														<div class="select2-search">
															<input type="text" autocomplete="off"
																class="select2-input">
														</div>
														<ul class="select2-results">
														</ul>
													</div>
												</div>
												<select class="wid3 FL MR15 select2-offscreen"
												disabled="disabled" tabindex="-1"><option>After</option></select>
											<!-- <label class="form-control FL MR15" style="width:140px">After</label> -->
												<input type="text" value="0"
												class="form-control MR8 FL wid1"><span
												class="FL MR8 MT8 gry">Y</span> <input type="text" value="0"
												class="form-control MR8 FL wid1"><span
												class="FL MT8 gry">M</span>
											</td>
											<td><input type="text" value="0"
												class="form-control ML40 FL wid1"></td>
											<td><i class="IC-cls-fill ML5 S16 red FL MT5 CP remove"></i>
												<i class="IC-plus-fill S16 blu FL MT5 ML5 CP add"></i></td>
										</tr>
										<tr class="ZPLRow exp_row">
											<td></td>
											<td>
												<div class="select2-container wid3 FL MR15"
													id="s2id_exp_row1_select">
													<a href="javascript:void(0)" onclick="return false;"
														class="select2-choice" tabindex="-1"> <span>For
															new joinees</span><abbr class="select2-search-choice-close"
														style="display: none;"></abbr>
														<div>
															<b></b>
														</div></a><input class="select2-focusser select2-offscreen"
														type="text" id="s2id_autogen434">
													<div class="select2-drop select2-with-searchbox"
														style="display: none">
														<div class="select2-search">
															<input type="text" autocomplete="off"
																class="select2-input">
														</div>
														<ul class="select2-results">
														</ul>
													</div>
												</div>
												<select class="wid3 FL MR15 select2-offscreen"
												id="exp_row1_select" tabindex="-1"><option
														value="new">For new joinees</option>
													<option value="after">After</option></select><input type="text"
												disabled="" value="0" class="form-control MR8 FL wid1"><span
												class="FL MR8 MT8 gry">Y</span> <input type="text"
												disabled="" value="0" class="form-control MR8 FL wid1"><span
												class="FL MT8 gry">M</span>
											</td>
											<td><input type="text" value="0"
												class="form-control ML40 FL wid1"></td>
											<td><i class="IC-plus-fill S16 blu FL MT5 ML5 CP add"></i></td>
										</tr>
									</tbody>
								</table>
								<span class="red"></span>
							</div>
						</div>
						<div class="tab-pane fade PR15 PL15 ZPLScmn"
							id="Leavetype_Advance">
							<label
								class="col-sm-12 PL30 MB15 MT10 S15 restrictedCheck daybasedCheck"><b>Holiday
									/ Weekend Settings</b></label>
							<div class="ML30">
								<div class="ZPFnoti daybasedCheck restrictedCheck"
									id="restricted default">
									<label class="checkbox DIB"><input type="checkbox"
										name="exclude_holidays" checked="checked"><span>
									</span></label><span> Exclude holidays for Leave</span>
								</div>
								<div class="ZPFnoti daybasedCheck restrictedCheck"
									id="restricted default">
									<label class="checkbox DIB"><input type="checkbox"
										name="exclude_weekends" checked="checked"><span>
									</span></label><span> Exclude weekends for Leave</span>
								</div>
								<div class="ZPFnoti restrictedCheck daybasedCheck includeWH"
									style="width: 80%; height: 68px; margin-top: 0px; display: block;">
									<label class="checkbox PR" style="top: 14px"><input
										type="checkbox" name="holidayinclu_limit_enable"><span>
									</span></label><span
										style="line-height: 2.5; padding-left: 24px; display: block;">
										Include &nbsp;
										<div class="select2-container select2-container-disabled DIB"
											id="s2id_autogen443"
											style="width: 350px; vertical-align: middle;">
											<a href="javascript:void(0)" onclick="return false;"
												class="select2-choice" tabindex="-1"> <span>all
													holidays and weekends in the leave period</span><abbr
												class="select2-search-choice-close" style="display: none;"></abbr>
												<div>
													<b></b>
												</div></a><input class="select2-focusser select2-offscreen"
												type="text" id="s2id_autogen444" disabled="disabled">
											<div class="select2-drop select2-with-searchbox"
												style="display: none">
												<div class="select2-search">
													<input type="text" autocomplete="off" class="select2-input"
														tabindex="-1">
												</div>
												<ul class="select2-results">
												</ul>
											</div>
										</div>
										<select class="DIB select2-offscreen" disabled="disabled"
										style="width: 350px;" name="holidayinclu_period" tabindex="-1"><option
												value="1" selected="selected">all holidays and
												weekends in the leave period</option>
											<option value="0">only holidays and weekends that
												follow the number of days specified</option></select>&nbsp;, if the applied
										leave period exceeds &nbsp;<input type="text"
										class="form-control ctxtbox" disabled="disabled"
										name="holidayinclu_limit" maxlength="2">&nbsp;
										<div class="select2-container select2-container-disabled DIB"
											id="s2id_autogen445"
											style="width: 150px; vertical-align: middle;">
											<a href="javascript:void(0)" onclick="return false;"
												class="select2-choice" tabindex="-1"> <span>calendar
													days </span><abbr class="select2-search-choice-close"
												style="display: none;"></abbr>
												<div>
													<b></b>
												</div></a><input class="select2-focusser select2-offscreen"
												type="text" id="s2id_autogen446" disabled="disabled">
											<div class="select2-drop select2-with-searchbox"
												style="display: none">
												<div class="select2-search">
													<input type="text" autocomplete="off" class="select2-input"
														tabindex="-1">
												</div>
												<ul class="select2-results">
												</ul>
											</div>
										</div>
										<select class="DIB select2-offscreen" disabled="disabled"
										style="width: 150px;" name="holidayinclu_days" tabindex="-1"><option
												value="0" selected="selected">calendar days</option>
											<option value="1">business days</option></select>
									</span><em id="holidayinclu_limit_err" class="ERR DN ML20">Decimal
										values not allowed</em>
								</div>
							</div>
							<label class="col-sm-12 PL30 MB15 MT15 S15"><b>Leave
									Restrictions </b></label>
							<div class="ML30">
								<div class="ZPFnoti daybasedCheck restrictedCheck"
									id="restricted">
									<label class="checkbox DIB"><input type="checkbox"
										name="enable_quarterday"><span> </span></label><span>
										Enable quarter-day option</span>
								</div>
								<div class="ZPFnoti daybasedCheck restrictedCheck PT3"
									id="restricted">
									<label class="checkbox DIB"><input type="checkbox"
										name="enable_halfday"><span> </span></label><span>
										Enable half-day option</span>
								</div>
								<div class="ZPFnoti restrictedCheck PT3">
									<label class="checkbox DIB " id="restricted default"><input
										type="checkbox" checked="checked" name="exceed_maxcount"><span>
									</span></label><span> Do not allow beyond the permitted limit </span>
								</div>
								<div class="ZPFnoti restrictedCheck">
									<label class="checkbox DIB"><input type="checkbox"
										name="max_consecutive_enable"><span> </span></label><span>
										Maximum number of consecutive <em class="leaveunithelp">day(s)</em>
										of Leave allowed
									</span>&nbsp;<input type="text" pattern="[0-9]{3}"
										class="form-control ctxtbox" disabled="disabled"
										name="max_consecutive" maxlength="3"><em
										id="max_consecutive_err" class="ERR DN ML20">Decimal
										values not allowed</em>
								</div>
								<div class="ZPFnoti">
									<label class="checkbox DIB"><input type="checkbox"
										name="notice_period_enable"><span> </span></label><span>
										Leave application should be submitted before &nbsp;<input
										type="text" class="form-control ctxtbox" disabled="disabled"
										name="notice_period" maxlength="6">&nbsp; day(s)
									</span><em id="notice_period_err" class="ERR DN ML20">Decimal
										values not allowed</em>
								</div>
								<div class="ZPFnoti daybasedCheck restrictedCheck">
									<label class="checkbox DIB"><input type="checkbox"
										name="show_fileupload_after_enable"><span> </span></label><span>
										Enable file upload option if the applied leave period exceeds
										<input type="text" class="form-control ctxtbox"
										disabled="disabled" name="show_fileupload_after" maxlength="5">
										consecutive days
									</span>
								</div>
								<div class="ZPFnoti restrictedCheck daybasedCheck PT3"
									id="restricted">
									<label class="checkbox DIB"><input type="checkbox"
										name="roundoff_enable"><span> </span></label><span>
										Round-off permitted Leave count </span>
									<div
										class="select2-container select2-container-disabled DIB W120 ML5"
										id="s2id_autogen447" style="vertical-align: middle;">
										<a href="javascript:void(0)" onclick="return false;"
											class="select2-choice" tabindex="-1"> <span>Nearest</span><abbr
											class="select2-search-choice-close" style="display: none;"></abbr>
											<div>
												<b></b>
											</div></a><input class="select2-focusser select2-offscreen" type="text"
											id="s2id_autogen448" disabled="disabled">
										<div class="select2-drop select2-with-searchbox"
											style="display: none">
											<div class="select2-search">
												<input type="text" autocomplete="off" class="select2-input"
													tabindex="-1">
											</div>
											<ul class="select2-results">
											</ul>
										</div>
									</div>
									<select class="DIB W120 ML5 select2-offscreen"
										disabled="disabled" name="roundoff" tabindex="-1"><option
											selected="selected" value="2">Nearest</option>
										<option value="1">Maximum</option>
										<option value="0">Minimum</option></select>
								</div>
							</div>
							<label class="col-sm-12 PL30 MB15 MT15 S15"><b>Clubbing
									Policy</b></label>
							<div class="ML30">
								<div class="ZPFnoti">
									<label class="checkbox DIB"><input type="checkbox"
										name="blocked_clubs_enable"><span> </span></label><span>
										Do not club with &nbsp;
										<div
											class="select2-container select2-container-multi select2-container-disabled DIB W400 VM"
											id="s2id_autogen441">
											<ul class="select2-choices">
												<li class="select2-search-field"><input type="text"
													autocomplete="off" class="select2-input"
													id="s2id_autogen442" style="width: 20px;"
													disabled="disabled"></li>
											</ul>
											<div class="select2-drop select2-drop-multi"
												style="display: none;">
												<ul class="select2-results">
												</ul>
											</div>
										</div>
										<select name="blocked_clubs"
										class="DIB W400 VM select2-offscreen" multiple="multiple"
										disabled="disabled" tabindex="-1"></select>
									</span>
								</div>
							</div>
							<form class="form-horizontal pad" role="form">
								<label class="col-sm-12 text-left MB15 MT15 PT15 S16"><b>New
										joinee Leave entitlement details </b></label>
								<div class="form-group">
									<label class="control-label col-sm-4">Consider Date of
										Joining : </label>
									<div class="col-sm-8">
										<div class="inlCbox">
											<label class="checkbox"><input type="checkbox"
												name="considerDOJ"><span></span></label>
										</div>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-4">Probation period
										: </label>
									<div class="col-sm-8">
										<label class="radio DIB MR8"><input
											class="DOJ caldays" type="radio" name="leave_period"
											value="379493000000039909" disabled="disabled"><span
											class="ML2">Calendar days </span></label> <label
											class="radio DIB MR8"><input class="DOJ busdays"
											type="radio" name="leave_period" value="379493000000039907"
											disabled="disabled"><span class="ML2">Business
												days </span></label>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-4">No. of waiting
										period day(s) : </label>
									<div class="col-sm-2">
										<input type="text" class="form-control DOJ"
											name="waiting_period" maxlength="6" style="width: 40px"
											disabled="disabled">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-4">Initial value
										during probation period : </label>
									<div class="col-sm-2">
										<input type="text" class="form-control DOJ"
											name="initial_value" maxlength="6" style="width: 40px"
											disabled="disabled">
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-sm-4">Joining month
										Leave count : </label>
									<div class="col-sm-8">
										If the date of joining is after&nbsp;<input type="text"
											class="form-control DOJ" name="nj_daysaftercredit"
											maxlength="6" style="width: 40px; display: inline;"
											disabled="disabled">&nbsp;&nbsp;of the month, then
										the Leave count for the month should be&nbsp;&nbsp;<input
											type="text" style="width: 70px; display: inline;"
											class="form-control DOJ MT5" name="nj_creditleavecount"
											maxlength="6" disabled="disabled">&nbsp;<span
											class="leaveunithelp">day(s)</span> <span
											id="nj_daysaftercredit_err" class="ERR DN">The given
											value should be a date</span>
									</div>
								</div>
								<label class="col-sm-12 text-left MB15 MT15 S16 rollover"><b>Roll
										over / Carry forward settings </b></label>
								<div class="form-group rollover">
									<label class="control-label col-sm-4">Enable Roll over
										/ Carry forward: </label>
									<div class="col-sm-8">
										<div class="inlCbox">
											<label class="checkbox" id="restricted"><input
												type="checkbox" name="rollover"><span></span></label>
										</div>
									</div>
								</div>
								<div class="form-group MT10 rollover">
									<label class="control-label col-sm-4">Maximum Roll Over
										Accumulation Count : </label>
									<div class="col-sm-2">
										<input type="text" class="form-control"
											name="max_accumulation" maxlength="6" disabled="disabled">
									</div>
								</div>
								<div class="form-group MT30">
									<label class="col-sm-12 text-left S16 ML10"><b>Expiry
									</b></label>
									<div class="form-group">
										<label class="col-sm-3 control-label P10I Matry">Start
											Date :</label>
										<div class="col-sm-3">
											<div class="btnCal ZPft PT5">
												<input type="text" name="start_date" class="form-control"
													value=""><span class="ERR MT10"
													style="display: none;"></span>
											</div>
										</div>
										<label class="col-sm-2 control-label P10I Matry">Expiry
											Date :</label>
										<div class="col-sm-3">
											<div class="btnCal ZPft PT5">
												<input type="text" name="expiry_date" class="form-control"
													value="">
											</div>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" id="zp_modal_blubtn" class="btn btn-primary">Submit</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
		function val(){
					
				
					var status  = document.getElementById('status').value;
					var comment  = document.getElementById('comment').value;
					var canSubmit = true; 
					
					
					if (status == '') {

						document.getElementById('status').style.borderColor = "red";
						return false;
					} else {
						document.getElementById('status').style.borderColor = "green";
					}	
					if (comment == '') {

						document.getElementById('comment').style.borderColor = "red";
						return false;
					} else {
						document.getElementById('comment').style.borderColor = "green";
					}	
					
					
				
				
					if(canSubmit == false){
						return false;
					}
				}
			
				
		</script>