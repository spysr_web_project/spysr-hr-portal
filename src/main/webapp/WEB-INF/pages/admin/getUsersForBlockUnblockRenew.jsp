<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<script src="<%=request.getContextPath()%>/resources/js/search.js"></script>
<style>
.filter {
	margin: 0px auto;
	width: 281px;
	float: right;
	margin-top: 3px;
}

.save_btn {
	margin-top: 21px;
	margin-bottom: 21px;
}
</style>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

	<div class="col-sm-12">
		<div class="modal-header">
			<h4>
				<b><spring:message code="label.userTable" /></b>
			</h4>
		</div>
	</div>
	<div class="col-sm-12">
		<div class="input-group filter">
			<span class="input-group-addon"><spring:message
					code="label.search" /></span> <input id="filter" type="text"
				class="form-control" placeholder="Type here...">
		</div>
		<div class="add_emp_details">
			<table class="table table-bordered table-striped example" id="filter"
				data-rt-breakpoint="600">
				<thead>
					<tr>
						<th><spring:message code="label.id" /></th>
						<th><spring:message code="label.empid" /></th>
						<th><spring:message code="label.role" /></th>
						<th><spring:message code="label.status" /></th>
						<th><spring:message code="label.reason" /></th>

						<th><spring:message code="label.action" /></th>
						<th><spring:message code="label.action" /></th>


					</tr>
				</thead>
				<tbody class="searchable">
					<c:if test="${! empty userList}">
						<c:forEach items="${userList}" var="user">
							<tr>
								<td><c:out value="${user.id}"></c:out></td>
								<td><c:out value="${user.userName}"></c:out></td>
								<td><c:out value="${user.currentRole}"></c:out></td>
								<td><c:out value="${user.accessStatus}"></c:out></td>
								<td><c:out value="${user.reason}"></c:out></td>


								<td><c:choose>
										<c:when test="${user.accessStatus=='Blocked'}">
											<a href="#" class="btn btn-primary" disabled="true"><spring:message
													code="label.block" /></a>
										</c:when>
										<c:otherwise>
											<a href="#" data-toggle="modal" data-target="#login-modal"
												data-id="${user.id}" data-status="Blocked"
												class="open-rateDialog btn btn-primary"><spring:message
													code="label.block" /></a>
										</c:otherwise>
									</c:choose></td>
								<td><c:choose>
										<c:when test="${user.accessStatus=='Unblocked'}">
											<a href="#" class="btn btn-primary" disabled="true"><spring:message
													code="label.unblock" /></a>
										</c:when>
										<c:otherwise>
											<a href="#" data-toggle="modal" data-target="#login-modal"
												data-id="${user.id}" data-status="Unblocked"
												class="open-rateDialog btn btn-primary"><spring:message
													code="label.unblock" /></a>
										</c:otherwise>
									</c:choose></td>

							</tr>
						</c:forEach>
					</c:if>

				</tbody>
			</table>
		</div>
	</div>
</div>
<section>
	<div class="container">
		<div class="modal fade" id="login-modal" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header login_modal_header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
						<h3 class="modal-title" id="myModalLabel">
							<spring:message code="label.blockUnblock" />
						</h3>
					</div>
					<div class="modal-body login-modal">
						<div class="clearfix"></div>
						<div id='social-icons-conatainer'>
							<div class='modal-body-left'>
								<form:form id="form" action="blockUnblockAccount"
									commandName="endUserForm">
									<div class="form-group">
										<form:hidden path="id" id="id" />
										<label class="col-sm-2"><b><spring:message
													code="label.status" /></b></label>
										<div class="col-sm-10">
											<form:input path="accessStatus" class="form-control"
												id="status" readonly="true" />
											<br>
										</div>
									</div>
									<div class="form-group">
										<label class="col-sm-2"><b><spring:message
													code="label.reason" /></b></label>
										<div class="col-sm-10">
											<form:textarea path="reason" id="reason" class="form-control" />
										</div>
									</div>
									<div class="col-sm-offset-2 col-sm-8">
										<div class="save_btn">
											<input type="submit" class="btn btn-primary" value="Submit">
											<button class="btn" data-dismiss="modal" aria-hidden="true">
												<spring:message code="label.close" />
											</button>
										</div>
									</div>
								</form:form>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>

	</div>
</section>
<script>
$(document).on("click", ".open-rateDialog", function () {
    $(".modal-body #id").val( $(this).data('id') );
    $(".modal-body #status").val( $(this).data('status') );
    
});


$('ul.nav li.dropdown').hover(function() {
	  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
	}, function() {
	  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
	});
</script>
