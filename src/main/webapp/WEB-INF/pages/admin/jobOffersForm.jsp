<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

	<!--  datepicker css js links -->
		<link href="<%=request.getContextPath()%>/resources/css/datepicker.min.css" rel="stylesheet">
		<script src="<%=request.getContextPath()%>/resources/js/jquery.min.js"></script>
		<script src="<%=request.getContextPath()%>/resources/js/datepicker.js"></script>
		
		<script src="<%=request.getContextPath()%>/resources/js/datepicker.en.js"></script>
	<!--  End datepicker css js links -->
      <script>
	function val() {

			
		if (document.getElementById('technology').value == '') {
			document.getElementById('technology').style.borderColor = "red";
			return false;
		} else {
			document.getElementById('technology').style.borderColor = "green";
		}
		if (document.getElementById('jobTiltle').value == '') {
			document.getElementById('jobTiltle').style.borderColor = "red";
			return false;
		} else {
			document.getElementById('jobTiltle').style.borderColor = "green";
		}
		if (document.getElementById('Exp').value == '') {
			document.getElementById('Exp').style.borderColor = "red";
			return false;
		} else {
			document.getElementById('Exp').style.borderColor = "green";
		}
		if (document.getElementById('lastDate').value == '') {
			document.getElementById('lastDate').style.borderColor = "red";
			return false;
		} else {
			document.getElementById('lastDate').style.borderColor = "green";
		}
		if (document.getElementById('positions').value == '') {
			document.getElementById('positions').style.borderColor = "red";
			return false;
		} else {
			document.getElementById('positions').style.borderColor = "green";
		}
		if (document.getElementById('jobDescription').value == '') {
			document.getElementById('jobDescription').style.borderColor = "red";
			return false;
		} else {
			document.getElementById('jobDescription').style.borderColor = "green";
		}
	
		
		
		if (canSubmit == false) {
			return false;
		}
	
	
	}
</script>



	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">


	
	<div class="col-sm-12">
		<div class="modal-header">
			<h4><b><spring:message code="label.jobOffers" /></b><span style="color: red">*</span></h4>
		</div>
		 <div class="error">
     <p style="text-align:center;color:red;">${success}</p>
    </div>
		<div class="add_emp_details_form">
			<div class="Emp_content" style="margin-bottom: 37px;">
			<form:form action="addJobOffers" method="post" commandName="jobOffersForm" onsubmit="return val();">		 
					
				
		<div class="modal-body" style="width: 100%;margin:0px auto;">
			<div class="form-horizontal" role="form">
				 
					 <div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.technology" /></b><span style="color: red">*</span>:</label>
				   		<div class="col-sm-8"> 
							<form:input path="technology" class="form-control input-sm" placeholder="Enter technology"  id="technology"/>
						</div>
					</div>   
			
					 <div class="form-group">
						 <label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.title" /></b><span style="color: red">*</span>:</label>
					  	 <div class="col-sm-8"> 
														
					  	 <form:input path="jobTiltle" class="form-control input-sm" placeholder="Enter jobTitle"  id="jobTiltle"/>
						 </div>
					</div>   
			
					<div class="form-group">
						 <label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.exp" /></b><span style="color: red">*</span>:</label>
					  	 <div class="col-sm-8"> 
							 <form:input path="Exp" class="form-control input-sm" placeholder="Enter Exp"  id="Exp"/>
						 </div>
					</div>
					
					<div class="form-group">
						 <label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.positions" /></b><span style="color: red">*</span>:</label>
					  	 <div class="col-sm-8"> 
							 <form:input path="positions" class="form-control input-sm" placeholder="Enter no of positions"  id="positions"/>
						 </div>
					</div> 
					
					<div class="form-group">
						 <label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.jobDescription" /></b><span style="color: red">*</span>:</label>
					  	 <div class="col-sm-8"> 
							 <form:textarea path="jobDescription" class="form-control input-sm" placeholder="Enter jobDescription"  id="jobDescription"/>
						 </div>
					</div>   
			
			
			
					<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.applyDate" /></b><span style="color: red">*</span> :</label>
						<div class="col-sm-8"> 
						  <form:input path="lastDate"  data-position="top left" data-language="en" class="form-control input-sm datepicker-here" id="lastDate" placeholder="Enter lastDate"/>
						</div>
					</div>
					
					 <div class="form-group">
                    		<div class="col-sm-offset-4 col-sm-5">
							     <input type="submit" class="btn btn-primary btn-sm" value="Send"/>
							    <a href="/spysr-hr-portal/admin/adminPage"><button type="button"
							class="btn btn-success btn-sm" aria-hidden="true">
							<spring:message code="label.back" />
						</button></a> 
							</div>
					 </div>
				
				</div>
			</div>
			</form:form>
				</div>
				
				</div>
				 
				</div>
				
				</div>
				
			
	<script>
		$('ul.nav li.dropdown').hover(function() {
		  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
		}, function() {
		  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
		});
		
	</script>
	<script>
		$(document).ready(function() {
			var readURL = function(input) {
				if (input.files && input.files[0]) {
					var reader = new FileReader();

					reader.onload = function (e) {
						$('.profile-pic').attr('src', e.target.result);
					}
			
					reader.readAsDataURL(input.files[0]);
				}
			}
			

			$(".file-upload").on('change', function(){
				readURL(this);
			});
			
			$(".upload-button").on('click', function() {
			   $(".file-upload").click();
			});
		});
	</script>
	<script>
		$('#datepicker-1').datepicker();
	</script>
	
  </body>