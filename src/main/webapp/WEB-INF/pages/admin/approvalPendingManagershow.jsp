<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
<form:form action="approvalPendingManagershowConfrim" commandName="endUserForm" onsubmit="return val();">
<div class="form-horizontal" role="form">
<div class="modal-header">
 <h4><b><spring:message code="label.employeedetails"/></b><span style="color: red">*</span></h4>
				</div>
		<div class="add_emp_details_form">
			<div class="Emp_content">
			
					<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.empid" /></b><span style="color: red">*</span>:</label>
						<div class="col-sm-8"> 
						<form:input path="userName" class="form-control" placeholder="Enter UserName"  value="${model.endUserForm.userName}" id="userName" readonly="true"/>
						</div>
						</div>
				
					
						<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.designation" /></b><span style="color: red">*</span> :</label>
						<div class="col-sm-8"> 
				     <form:input path="designation" class="form-control" placeholder="Enter designation"  value="${model.endUserForm.designation}" id="designation" readonly="true" />
						</div>
					</div>
					
						<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.department" /></b><span style="color: red">*</span> :</label>
						<div class="col-sm-8"> 
				     <form:input path="department" class="form-control" placeholder="Enter department"  value="${model.endUserForm.department}" id="department" readonly="true" />
						</div>
					</div>
					
					
				<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.currentRole" /></b><span style="color: red">*</span> :</label>
						<div class="col-sm-8"> 
				     <form:input path="currentRole" class="form-control" placeholder="Enter currentRole"  value="${model.endUserForm.currentRole}" id="currentRole" readonly="true" />
						</div>
					</div>	
					
						<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.createdBy" /></b><span style="color: red">*</span> :</label>
						<div class="col-sm-8"> 
				     <form:input path="createdby" class="form-control" placeholder="Enter currentRole"  value="${model.endUserForm.createdby}" id="currentRole" readonly="true" />
						</div>
					</div>	
					
		              	<form:hidden path="id"/>
						<form:hidden path="fatherName"/>
					   <form:hidden path="eligibleLeave"/>
						<form:hidden path="totalLeave"/>
						<form:hidden path="bname"/>
						<form:hidden path="imageName"/>
						<form:hidden path="image"/>
						<form:hidden path="name"/>
						<form:hidden path="branch"/>
						<form:hidden path="accountNo"/>
						<form:hidden path="dob"/>
						<form:hidden path="doj"/>
						<form:hidden path="pan"/>
					   <form:hidden path="localAddress"/>
					     <form:hidden path="gender"/>
					     <form:hidden path="permanentAddress"/>
						<form:hidden path="role"/>
						<form:hidden path="password"/>
						<form:hidden path="contactNo"/>
						<form:hidden path="email"/>
					    <form:hidden path="totalPackage"/>
						<form:hidden path="fixedPackage"/>
						<form:hidden path="variablePackage"/>
						<form:hidden path="basicSalary"/>
						<form:hidden path="hra"/>
						<form:hidden path="conveyAllowance"/>
						<form:hidden path="medicalAllowance"/>
						<form:hidden path="specialAllowance"/>
						<form:hidden path="professionalTax"/>
						<form:hidden path="conveyAllowance"/>
	                    <form:hidden path="foodamountDeduct"/>
		                <form:hidden path="professionalTax"/>
		                <form:hidden path="tds"/>
						<div class="form-group">
						<label class="control-label col-sm-4" id="DateDemo"><b><spring:message code="label.status" /></b><span style="color: red">*</span> :</label>
						<div class="col-sm-8"> 
						
						
						
						<form:select path="status" class="form-control input-sm" id="status">
						<form:option value="">
							<spring:message code="label.selectValue" />
						</form:option>
						<form:option value="Approved">
							<spring:message code="label.approve" />
						</form:option>
						<form:option value="Rejected">
							<spring:message code="label.reject" />
						</form:option>
					</form:select>
					</div>
						</div>	
		
		       <div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.comment" /></b><span style="color: red">*</span>:</label>
						<div class="col-sm-8"> 
						<form:textarea path="comment" class="form-control" placeholder="Enter Comment" id="comment"/>
						</div>
						</div>
	
					
										
								<div class="col-sm-12">
		                        <div class="text-center">
						<input type="submit" class="btn btn-primary btn-md" value="Save" style="margin-top: 26px;width: 18%;"/>
							<a href="/spysr-hr-portal/admin/approvalPendingManager" class="btn btn-primary btn-md" value="Save" style="margin-top: 26px;width: 18%;"><spring:message code="label.back"/></a>
					  </div>
			        </div>
								
								
								
		</form:form>
		</div>
</div>
</div>
	<script>
		function val(){
					
				
					var status  = document.getElementById('status').value;
					var comment  = document.getElementById('comment').value;
					var canSubmit = true; 
					
					
					if (status == '') {

						document.getElementById('status').style.borderColor = "red";
						return false;
					} else {
						document.getElementById('status').style.borderColor = "green";
					}	
					if (comment == '') {

						document.getElementById('comment').style.borderColor = "red";
						return false;
					} else {
						document.getElementById('comment').style.borderColor = "green";
					}	
					
					
				
				
					if(canSubmit == false){
						return false;
					}
				}
			
				
		</script>