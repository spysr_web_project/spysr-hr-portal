<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


	<script src="<%=request.getContextPath()%>/resources/js/search.js"></script>
	<style>
		.filter{
			margin: 0px auto;
		    width: 281px;
		    float: right;
		    margin-top: 3px;	
		}
	</style>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			
				<div class="col-sm-12">
				<div class="input-group filter"> <span class="input-group-addon">Search</span>
					<input id="filter" type="text" class="form-control" placeholder="Type here...">
				</div>
					<div class="add_emp_details">
						 <table class="table table-bordered table-striped example" id="filter" data-rt-breakpoint="600">
                                        <thead>
                                            <tr>
                                                <th scope="col" data-rt-column="ID"><spring:message code="label.empid"/></th> 
                                                <th scope="col" data-rt-column="Project"><spring:message code="label.name"/></th>
                                                <th scope="col" data-rt-column="Status"><spring:message code="label.email"/></th>
                                                <th scope="col" data-rt-column="Status"><spring:message code="label.monday"/></th>
                                                <th scope="col" data-rt-column="Status"><spring:message code="label.mondayStatus"/></th>
                                                  <th scope="col" data-rt-column="Status"><spring:message code="label.tuesday"/></th>
                                                <th scope="col" data-rt-column="Status"><spring:message code="label.tuesdayStatus"/></th>
                                                 <th scope="col" data-rt-column="Status"><spring:message code="label.wednesday"/></th>
                                                 <th scope="col" data-rt-column="Status"><spring:message code="label.wednesdayStatus"/></th>
                                                 
                                                        <th scope="col" data-rt-column="Status"><spring:message code="label.thursday"/></th>
                                                 <th scope="col" data-rt-column="Status"><spring:message code="label.thursdayStatus"/></th>
                                                      <th scope="col" data-rt-column="Status"><spring:message code="label.friday"/></th>
                                                 <th scope="col" data-rt-column="Status"><spring:message code="label.fridayStatus"/></th>
                                                 <th scope="col" data-rt-column="Status"><spring:message code="label.status"/></th>
                                             
                                                <th scope="col" data-rt-column="Progress"><spring:message code="label.action"/></th>
                                                <th>&nbsp;</th>
                                            </tr>
                                        </thead>
                                        <tbody class="searchable">
                                        <c:if test="${! empty timeList}">

					             	<c:forEach items="${timeList}" var="time">
                                            <tr>                                         
                                <td><c:out value="${time.userName}"></c:out></td> 
								<td><c:out value="${time.name}"></c:out></td>
								<td><c:out value="${time.email}"></c:out></td>
							
							      <fmt:formatDate var='date' value="${time.monday}"  pattern="dd/MM/yyyy"/>
								<td><c:out value="${date}"></c:out>
								<td><c:out value="${time.mondayStatus}"></c:out></td>
								
								      <fmt:formatDate var='date' value="${time.tuesday}"  pattern="dd/MM/yyyy"/>
								<td><c:out value="${date}"></c:out>
							
							    <td><c:out value="${time.tuesdayStatus}"></c:out></td>
							        <fmt:formatDate var='date' value="${time.wednesday}"  pattern="dd/MM/yyyy"/>
								<td><c:out value="${date}"></c:out>
							  
							     <td><c:out value="${time.wednesdayStatus}"></c:out></td>
							     
							       <fmt:formatDate var='date' value="${time.thursday}"  pattern="dd/MM/yyyy"/>
								<td><c:out value="${date}"></c:out>
							 
							      <td><c:out value="${time.thursdayStatus}"></c:out></td>
							      
							        <fmt:formatDate var='date' value="${time.friday}"  pattern="dd/MM/yyyy"/>
								<td><c:out value="${date}"></c:out>
								
							     
							       <td><c:out value="${time.fridayStatus}"></c:out></td>
							        <td><c:out value="${time.status}"></c:out></td>
                                <td><a href="selectfoodbillUpdate?id=${time.id}" class="btn btn-primary btn-sm"><spring:message code="label.edit" /></a></td>
                                <td><a href="viewTimeSheet?id=${time.id}" class="btn btn-primary btn-sm"><spring:message code="label.view" /></a></td>
							

                                            </tr>  
                                            	</c:forEach>
					                         </c:if>                                        
                                        </tbody>
                                    </table>
					</div>
				</div>
			</div>
    
	<script>
		$('ul.nav li.dropdown').hover(function() {
		  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
		}, function() {
		  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
		});
		
	</script>
	
  </body>
</html>
