<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


	<script src="<%=request.getContextPath()%>/resources/js/search.js"></script>
	<style>
		.filter{
			margin: 0px auto;
		    width: 281px;
		    float: right;
		    margin-top: 3px;	
		}
	</style>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
			  <div class="col-sm-12">
					<div class="add_emp">
						<a href="addEmployee"> <button type="button" class="btn btn-info btn-md page-header"><spring:message code="label.addEmployee"/></button></a>
					</div>
			</div>
				
	
				<div class="col-sm-12">
					<div class="input-group filter"> <span class="input-group-addon"><spring:message code="label.search"/></span>
						<input id="filter" type="text" class="form-control" placeholder="Type here...">
					</div>
					<div class="add_emp_details">
						 <table class="table table-bordered table-striped example" id="filter" data-rt-breakpoint="600">
                                        <thead>
                                            <tr>
                                                <th scope="col" data-rt-column="ID"><spring:message code="label.empid"/></th> 
                                                <th scope="col" data-rt-column="Project"><spring:message code="label.name"/></th>
                                                <th scope="col" data-rt-column="Status"><spring:message code="label.designation"/></th>
                                                <th scope="col" data-rt-column="Status"><spring:message code="label.role"/></th>
                                                <th scope="col" data-rt-column="Status"><spring:message code="label.createdBy"/></th>
                                                <th scope="col" data-rt-column="Progress"><spring:message code="label.action"/></th>
                                              
                                            </tr>
                                        </thead>
                                        <tbody class="searchable">
                                        <c:if test="${! empty userList}">

							             	<c:forEach items="${userList}" var="user">
			                                       <tr>                                         
						                                <td><c:out value="${user.userName}"></c:out></td> 
														<td><c:out value="${user.name}"></c:out></td>
														<td><c:out value="${user.designation}"></c:out></td>
														<td><c:out value="${user.currentRole}"></c:out></td>
													    <td><c:out value="${user.createdby}"></c:out></td>
						                                <td><a href="selectUserUpdate?id=${user.id}" class="btn btn-primary btn-sm"><spring:message code="label.edit" /></a></td>
			
												  </tr>  
		                                     </c:forEach>
					                        </c:if>                                        
                                        </tbody>
                                    </table>
					</div>
				</div>
			</div>
    
	<script>
		$('ul.nav li.dropdown').hover(function() {
		  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
		}, function() {
		  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
		});
		
	</script>
	
  </body>
</html>
