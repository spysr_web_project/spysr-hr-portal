<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

<div class="details" style=" margin-top: 44px;margin-bottom: 44px;">
					<div class="modal-header">
					 	<h4>Admin Profile</h4>
					</div>
	<div class="add_emp_details">
			<form:form action="updateAdminDetails" name="editProfile"
				autocomplete="off" commandName="endUserForm"
				onsubmit="return val();">
			<form:hidden path="id" />	
		<div class="col-sm-12 col-md-12 col-lg-12">&nbsp;</div>
			<div class="admin-profile" style="width: 60%;">
				<div class="form-horizontal" role="form">
				
				<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message
										code="label.uploadImage" /></b>:</label>
						<div class="col-sm-8"> 
				   			  <img class="profile-pic" src="${endUserForm.imageName}" /> 
				   			    <a href="#" id='modal-launcher' data-toggle="modal" data-target="#login-modal" 
										       data-id="${endUserForm.id}" 	
											class="open-dialog btn btn-primary"  id="cancel" ><spring:message code="label.uploadPhoto"/></a> </div>
							<input class="file-upload" type="file" accept="image/*"/>
							<div class="form-group">
								<div class="Pro_img1" style="text-align:center;">
									<img src="${endUserForm.imageName}" class="img_pro">
									<div id="drop" style=" margin-top: 15px;">
										
										 <a href="#" id='modal-launcher' data-toggle="modal" data-target="#login-modal" 
										       data-id="${endUserForm.id}" 	
											class="open-dialog btn btn-primary"  id="cancel" ><spring:message code="label.uploadPhoto"/></a> 
										    
		
									</div> 
									
										
								</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message
										code="label.userName" /></b>:</label>
						<div class="col-sm-8"> 
				   			 <form:input path="userName" class="form-control input-sm" id="userName" autocomplete="off" readonly="true"></form:input>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message
										code="label.displayName" /></b>:</label>
						<div class="col-sm-8"> 
				  <form:input path="displayName" placeholder="Enter  Display Name" class="form-control input-sm" id="displayName" autocomplete="off"></form:input>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message
										code="label.email" /></b>:</label>
						<div class="col-sm-8"> 
				   <form:input path="email" class="form-control input-sm" id="email" autocomplete="off"></form:input>
						</div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message
										code="label.contactNumber" /></b>:</label>
						<div class="col-sm-8"> 
				   			<form:input path="contactNo" class="form-control input-sm" id="contactNo" autocomplete="off"></form:input>
						</div>
					</div>
				
				
				
		                <div class="form-group">
		              	   <div class="col-sm-offset-4 col-sm-8">
								<a href="editAdminPWD?id=${model.user.id}" style="color: #f89406;"><spring:message code="label.changePassword"/></a>
								<input type="submit" class="btn btn-success btn-sm" value="Update" style="width: 15%;"/>
								<a href="/spysr-hr-portal/admin/adminPage" class="btn btn-success btn-sm" style="width: 15%;"><spring:message code="label.back"/></a>
							</div>
						</div>
					</div>
				
			
				</div>
				</form:form>
			</div>
			
	</div>		
	</div>
<section>
	<div class="container">
		<div class="modal fade" id="login-modal" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header login_modal_header">
						<button type="button" class="close" data-dismiss="modal"
							aria-hidden="true">&times;</button>
						<h3 class="modal-title" id="myModalLabel">
							<spring:message code="label.uploadImage" />
						</h3>
					</div>
					<div class="modal-body login-modal">
						<div class="clearfix"></div>
						<div id='social-icons-conatainer'>
							<div class='modal-body-left'>
								<form:form action="updateImageForProfile"
									commandName="endUserForm" enctype="multipart/form-data">
									<div class="form-group">
										<form:hidden path="id"  id="id" />
										<form:input path="file" type="file"
											onchange="checkfile(this);" id="imageFile" />
									</div>

									<input type="submit" class="btn btn-primary" value="Upload">
									<button class="btn" data-dismiss="modal" aria-hidden="true">
										<spring:message code="label.close" />
									</button>
								</form:form>
							</div>
						</div>
						<div class="clearfix"></div>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>

<script>
	function checkfile(sender) {
		var validExts = new Array(".bmp", ".dib", ".JPG", ".jpg", ".jpeg",
				".jpe", ".jfif", ".gif", ".tif", ".tiff", ".png");
		var fileExt = sender.value;
		fileExt = fileExt.substring(fileExt.lastIndexOf('.'));
		if (validExts.indexOf(fileExt) < 0) {
			alert("Invalid file selected, valid files are of "
					+ validExts.toString() + " types.");
			var fld = document.getElementById("imageFile");

			fld.form.reset();
			fld.focus();
		}
	}

	$(document).on("click", ".open-dialog", function() {
		$(".modal-body #id").val($(this).data('id'));

	});
</script>
<script>
	function val() {

		var phoneNum = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
		var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

		var altContactNo = document.getElementById('altContactNo').value;
		var contactNo = document.getElementById('contactNo').value;
		var canSubmit = true;

		if (altContactNo
				.match(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/)
				|| (altContactNo == '')) {

			document.getElementById('altContactNoError').style.display = 'none';

		} else {
			document.getElementById('altContactNoError').style.display = 'block';
			canSubmit = false;
		}

		if (altEmail.value
				.match(/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/)
				|| (document.getElementById('altEmail').value == '')) {

			document.getElementById('altEmailError').style.display = 'none';

		} else {
			document.getElementById('altEmailError').style.display = 'block';
			canSubmit = false;
		}

		if (email.value
				.match(/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/)
				&& !(document.getElementById('email').value == '')) {

			document.getElementById('EmailError').style.display = 'none';

		} else {
			document.getElementById('EmailError').style.display = 'block';
			canSubmit = false;
		}

		if (contactNo
				.match(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/)
				&& !(contactNo == '')) {

			document.getElementById('contactNoError').style.display = 'none';

		} else {
			document.getElementById('contactNoError').style.display = 'block';
			canSubmit = false;
		}

		if (canSubmit == false) {
			return false;
		}
	}
</script>
<script>
		$(document).ready(function() {
			var readURL = function(input) {
				if (input.files && input.files[0]) {
					var reader = new FileReader();

					reader.onload = function (e) {
						$('.profile-pic').attr('src', e.target.result);
					}
			
					reader.readAsDataURL(input.files[0]);
				}
			}
			

			$(".file-upload").on('change', function(){
				readURL(this);
			});
			
			$(".upload-button").on('click', function() {
			   $(".file-upload").click();
			});
		});
	</script>