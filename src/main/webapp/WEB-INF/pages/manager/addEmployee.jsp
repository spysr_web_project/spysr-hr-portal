<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<script>
	function val() {

		var name = document.getElementById('name').value;
		
		var fatherName=document.getElementById('fatherName').value;
		
		var employeeId=  document.getElementById('employeeId').value;
		
	/* 	var designation= document.getElementById('designation').value;
		
		var department= document.getElementById('department').value; */
		
		var currentRole = document.getElementById('currentRole').value;
		
		var phoneNum = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
		
		var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		
		 var password            = /^[A-Za-z0-9!@#$%^&*()_]{6,20}$/; 

		var canSubmit = true;

		if (name == '') {

			document.getElementById('name').style.borderColor = "red";
			return false;
		} else {
			document.getElementById('name').style.borderColor = "green";
		}	
		
		if (fatherName == '') {

			document.getElementById('fatherName').style.borderColor = "red";
			return false;
		} else {
			document.getElementById('fatherName').style.borderColor = "green";
		}
		
		
		
		if (employeeId == '') {

			document.getElementById('employeeId').style.borderColor = "red";
			return false;
		} else {
			document.getElementById('employeeId').style.borderColor = "green";
		}

		if (employeeId
				.match('^\\d+$')
				&& !(employeeId == '')) {

			document.getElementById('employeeIdError').style.display = 'none';
		} else {

			document.getElementById('employeeIdError').style.display = 'block';
			canSubmit = false;
		}
		
	/* 	if(designation == ''){
			document.getElementById('designation').style.borderColor = "red";
			return false;
		}else{
			document.getElementById('designation').style.borderColor = "green";
			}
		
	
		if(department ==''){
			document.getElementById('department').style.borderColor="red";
			return false;
		}else{
			document.getElementById('department').style.borderColor="green";	
			}
		 */
	
		if (currentRole == '') {
			document.getElementById('currentRole').style.borderColor = "red";
			return false;
		} else {
			document.getElementById('currentRole').style.borderColor = "green";
		}
	
		
		if (contactNo == '') {

			document.getElementById('contactNo').style.borderColor = "red";
			return false;
		} else {
			document.getElementById('contactNo').style.borderColor = "green";
		}

		if (contactNo
				.match(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/)
				&& !(contactNo == '')) {

			document.getElementById('contactNo1Error').style.display = 'none';
		} else {

			document.getElementById('contactNo1Error').style.display = 'block';
			canSubmit = false;
		}

		if (altContactNo.value
				.match(/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/)
				|| (document.getElementById('altContactNo').value == '')) {

			document.getElementById('contactNum2Error').style.display = 'none';
		} else {

			document.getElementById('contactNum2Error').style.display = 'block';
			canSubmit = false;
		}

		if (email == '') {

			document.getElementById('email').style.borderColor = "red";
			return false;
		} else {
			document.getElementById('email').style.borderColor = "green";
		}
		if (email
				.match(/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/)
				&& !(email == '')) {

			document.getElementById('emailError1').style.display = 'none';

		} else {
			document.getElementById('emailError1').style.display = 'block';
			canSubmit = false;
		}
		if (altEmail.value
				.match(/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/)
				|| (document.getElementById('altEmail').value == '')) {

			document.getElementById('altemailError').style.display = 'none';

		} else {
			document.getElementById('altemailError').style.display = 'block';
			canSubmit = false;
		}

		
		if (canSubmit == false) {
			return false;
		}
	}


	function leave() {
		var r = document.getElementById('noofdays').value;
		if (r <= 1) {
			$("#leaveDateone").show();
		} else if(2>1){
			$("#leaveDatetwo").show();
			$("#leaveDatemore").show();
			$("#leaveDateone").hide();
		}
		else
			{
			$("#leaveDatetwo").hide();
			$("#leaveDatemore").hide()
			}
	}
</script>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

	<form:form action="insertEmployee" method="post" commandName="endUserForm" onsubmit="return val();">		 
	<div class="col-sm-12">
		<div class="modal-header">
				  <h4><b><spring:message code="label.employeedetails"/></b><span style="color: red">*</span></h4>
				</div>
		<div class="add_emp_details_form">
			<div class="Emp_content">
					
				
				<div class="modal-body" style="width: 80%;margin:0px auto;">
				  <div class="form-horizontal" role="form">
					<div class="form-group">
						<label class="control-label col-sm-4" for="email"><spring:message code="label.image" /> :</label>
						<div class="col-sm-8">
						 <img class="profile-pic" src="images/noimg.jpg" />
							<div class="upload-button"><spring:message code="label.uploadImage" /></div>
							<input class="file-upload" type="file" accept="image/*"/>	
						</div>
					</div>
					 <%--  <div class="form-group">
						<label class="control-label col-sm-4" for="email"><b><spring:message code="label.name" /></b><span style="color: red">*</span> :</label>
						<div class="col-sm-8">
					
						  <form:input path="name" class="form-control" placeholder="Enter Name" id="name" />
						</div>
					</div> --%>
						<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.employeeid" /></b><span style="color: red">*</span> :</label>
						<div class="col-sm-8"> 
				  <form:input path="employeeId" class="form-control" placeholder="Employee ID" id="employeeId" />
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.fathername" /></b><span style="color: red">*</span> :</label>
						<div class="col-sm-8"> 
				     <form:input path="fatherName" class="form-control" placeholder="Enter Father's Name" id="fatherName" />
						</div>
					</div>
					
				
						<div class="form-group">
						<label class="control-label col-sm-4" id="DateDemo"><b><spring:message code="label.designation" /></b><span style="color: red">*</span> :</label>
						<div class="col-sm-8"> 
				       <form:select path="designation" class="form-control" id="designation">
						<form:option value="">
							<spring:message code="label.selectValue" />
						</form:option>
						<form:option value="Software Engineer">
							<spring:message code="label.software" />
						</form:option>
						<form:option value="Human Resources">
							<spring:message code="label.humanresources" />
						</form:option>
						<form:option value="TESTING">
							<spring:message code="label.testing" />
						</form:option>
					</form:select>
					</div>
						</div>
					
					
						<div class="form-group">
						<label class="control-label col-sm-4" id="DateDemo"><b><spring:message code="label.department" /></b><span style="color: red">*</span> :</label>
						<div class="col-sm-8"> 
				<form:select path="department" class="form-control" id="department">
						<form:option value="">
							<spring:message code="label.selectValue" />
						</form:option>
						<form:option value="IT">
							<spring:message code="label.IT" />
						</form:option>
						<form:option value="Human Resources">
							<spring:message code="label.humanresources" />
						</form:option>
						<form:option value="TESTING">
							<spring:message code="label.testing" />
						</form:option>
					</form:select>
					</div>
						</div>
					
					
					
					
					<%-- <div class="form-group">
						<label class="control-label col-sm-4" id="DateDemo">Date of birth :</label>
						<div class="col-sm-8"> 
							<div id="sandbox-container">
							<!-- 	<input type="text"  class="form-control" /> -->
								<form:input path="dob" class="myform-control" placeholder="Enter D.O.B" id="dob" />
							</div>
						</div>
					</div> --%>
				
					
					<div class="form-group">
						<label class="control-label col-sm-4" id="DateDemo"><b><spring:message code="label.selectRole" /></b><span style="color: red">*</span> :</label>
						<div class="col-sm-8"> 
				<form:select path="currentRole" class="form-control" id="currentRole">
						<form:option value="">
							<spring:message code="label.selectValue" />
						</form:option>
						<form:option value="ROLE_EMP">
							<spring:message code="label.roleEmployee" />
						</form:option>
						<form:option value="ROLE_MANAGER">
							<spring:message code="label.roleManager" />
						</form:option>
					</form:select>
					</div>
						</div>
			
						<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.password" /></b><span style="color: red">*</span>:</label>
						<div class="col-sm-8"> 
					
						   <form:password path="password" class="form-control" placeholder="Enter Pawword" id="password" />
						</div>
					</div>
				
				</div>
				</div>
				
			</div>
			</div>
	</div>
	<div class="col-sm-6">
	<div class="modal-header">
		<h4><b><spring:message code="label.bankDetails"/></b><span style="color:red">*</span></h4>
	</div>
		<div class="add_emp_details_form">
			<div class="Emp_content">
				
				
				<div class="modal-body" style="width: 100%;margin:0px auto;">
				  <div class="form-horizontal" role="form">
					  <div class="form-group">
						<label class="control-label col-sm-4" for="email"><b><spring:message code="label.bankName"/></b><span style="color:red">*</span>:</label>
						<div class="col-sm-8">
						  <form:input path="bname" type="text" class="form-control" id="bankName" placeholder="Enter Bank Name"/>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.branch"/></b><span style="color:red">*</span>:</label>
						<div class="col-sm-8"> 
						  <form:input path="branch" type="branch" class="form-control" id="pwd" placeholder="Enter branch"/>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-4" id="DateDemo"><b><spring:message code="label.accno"/></b><span style="color:red">*</span> :</label>
						<div class="col-sm-8"> 
							<div id="sandbox-container">
						  <form:input path="accountNo" type="accountNo" class="form-control" id="pwd" placeholder="Enter Account Number"/>
							</div>
						</div>
					</div>
				
				</div>
				</div>
				
			</div>
			</div>
	</div>
	<div class="col-sm-6">
	<div class="modal-header">
		<h4><b><spring:message code="label.personalInfo"/></b><span style="color:red">*</span></h4>
	</div>
		<div class="add_emp_details_form">
			<div class="Emp_content">
				
				
				<div class="modal-body" style="width: 100%;margin:0px auto;">
				  <div class="form-horizontal" role="form">
					  <div class="form-group">
						<label class="control-label col-sm-4" for="email"><b><spring:message code="label.name"/></b><span style="color:red">*</span> :</label>
						<div class="col-sm-8">
						  <form:input path="name" type="text" class="form-control" id="name" placeholder="Enter Name"/>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.email"/></b><span style="color:red">*</span> :</label>
						<div class="col-sm-8"> 
						  <form:input path="email" type="email" class="form-control" id="email" placeholder="Enter Email"/>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.address"/></b><span style="color:red">*</span> :</label>
						<div class="col-sm-8"> 
						  <form:input path="address" type="address" class="form-control" id="address" placeholder="Enter Address"/>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.pan"/></b><span style="color:red">*</span> :</label>
						<div class="col-sm-8"> 
						  <form:input path="pan" type="pan" class="form-control" id="pan" placeholder="Enter Pan"/>
						</div>
					</div>
					<!-- <div class="form-group">
						<label class="control-label col-sm-4" id="DateDemo">Date of birth :</label>
						<div class="col-sm-8"> 
							<div id="sandbox-container">
								<input type="text"  class="form-control" />
							</div>
						</div>
					</div> -->
						<div class="form-group">
						<label class="control-label col-sm-4" id="DateDemo"><b><spring:message code="label.gender" /></b><span style="color: red">*</span> :</label>
						<div class="col-sm-8"> 
						
						
						
				<form:select path="gender" class="form-control" id="currentRole">
						<form:option value="">
							<spring:message code="label.selectValue" />
						</form:option>
						<form:option value="ROLE_EMP">
							<spring:message code="label.male" />
						</form:option>
						<form:option value="ROLE_MANAGER">
							<spring:message code="label.female" />
						</form:option>
					</form:select>
					</div>
						</div>	
						
					<div class="form-group">
						<label class="control-label col-sm-4" id="DateDemo"><b><spring:message code="label.localaddress"/></b><span style="color:red">*</span> :</label>
						<div class="col-sm-8"> 
							<div id="sandbox-container">
								<form:textarea path="localAddress"  class="form-control" rows="5" id="localAddress"></form:textarea>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-4" id="DateDemo"><b><spring:message code="label.permanentaddress"/></b><span style="color:red">*</span>  :</label>
						<div class="col-sm-8"> 
							<div id="sandbox-container">
								<form:textarea path="permanentAddress" class="form-control" rows="5" id="permanentAddress"></form:textarea>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-4" id="DateDemo"><b><spring:message code="label.contactNumber" /></b><span style="color: red">*</span> :</label>
						<div class="col-sm-8"> 
							<div id="sandbox-container">
								<form:input path="contactNo" type="text" id="contactNo" class="form-control" />
							</div>
						</div>
							
						</div>
						 <div class="form-group">
						<label class="control-label col-sm-4" id="DateDemo"><b><spring:message code="label.eligibleleave" /></b><span style="color: red">*</span> :</label>
						<div class="col-sm-8"> 
							
								<form:input path="eligibleLeave" type="text" id="eligibleLeave" class="form-control input-sm" />
							
						</div>
					</div>	
						<div class="form-group">
							<label class="control-label col-sm-4" id="DateDemo"><b><spring:message code="label.totaleave" /></b><span style="color: red">*</span> :</label>
							<div class="col-sm-8"> 
								<div id="sandbox-container">
									<form:input path="totalLeave" type="text" id="totalLeave" class="form-control input-sm" />
								</div>
							</div> 
						</div>
					</div>
				</div>
				</div>
				
			</div>
			</div>
	</div>
	<div class="col-sm-6">
	<div class="modal-header">
		<h4><b><spring:message code="label.salarydetails" /></b><span style="color: red">*</span></h4>
	</div>
		<div class="add_emp_details_form">
			<div class="Emp_content">
				
				
				<div class="modal-body" style="width: 100%;margin:0px auto;">
				  <div class="form-horizontal" role="form">
					  <div class="form-group">
						<label class="control-label col-sm-4" for="email"><b><spring:message code="label.totalpackage" /></b><span style="color: red">*</span> :</label>
						<div class="col-sm-8">
						  <form:input path="totalPackage"  type="text" class="form-control" id="totalPackage" placeholder="Enter Total Package" value="0"/>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.fixedPackage" /></b><span style="color: red">*</span>:</label>
						<div class="col-sm-8"> 
						  <form:input path="fixedPackage" type="fixedPackage" class="form-control" id="pwd" placeholder="Enter fixed Package" value="0"/>
						</div>
					</div>
					
				
					<div class="form-group">
						<label class="control-label col-sm-4" id="DateDemo"><b><spring:message code="label.variablePackage" /></b><span style="color: red">*</span> :</label>
						<div class="col-sm-8"> 
							<div id="sandbox-container">
								<form:input path="variablePackage" type="variablePackage" id="variablePackage"  value="0" class="form-control" />
							</div>
						</div>
					</div>
				</div>
				</div>
				
			</div>
			</div>
	</div>
	<div class="col-sm-6">
	<div class="modal-header">
		<h4><b><spring:message code="label.earningdetails" /></b><span style="color: red">*</span></h4>
	</div>
		<div class="add_emp_details_form">
			<div class="Emp_content">
				
				
				<div class="modal-body" style="width: 100%;margin:0px auto;">
				 <div class="form-horizontal" role="form">
					  <div class="form-group">
						<label class="control-label col-sm-4" for="email"><b><spring:message code="label.basicsalary" /></b><span style="color: red">*</span>:</label>
						<div class="col-sm-8">
						  <form:input path="basicSalary" type="text" class="form-control" id="basicSalary" placeholder="Enter Basic Salary" value="0"/>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-4" for="email"><b><spring:message code="label.hra" /></b><span style="color: red">*</span> :</label>
						<div class="col-sm-8"> 
						  <form:input path="hra" type="hra" class="form-control" id="hra" placeholder="Enter HRA" value="0"/>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.conveyAllowance" /></b><span style="color: red">*</span> :</label>
						<div class="col-sm-8"> 
						  <form:input path="conveyAllowance" type="conveyAllowance" class="form-control" id="conveyAllowance" placeholder="Enter medical Allowance" value="0"/>
						</div>
					</div>
					<!-- <div class="form-group">
						<label class="control-label col-sm-4" id="DateDemo">Date of birth :</label>
						<div class="col-sm-8"> 
							<div id="sandbox-container">
								 <input type="text" value="12-02-2009" id="datepicker-1" class="datepicker form-control" />
							</div>
						</div>
					</div> -->
					<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.medicalAllowance" /></b><span style="color: red">*</span> :</label>
						<div class="col-sm-8"> 
						  <form:input path="medicalAllowance" type="medicalAllowance" class="form-control" id="medicalAllowance" placeholder="Enter medical Allowance" value="0"/>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.specialAllowance" /></b><span style="color: red">*</span> :</label>
						<div class="col-sm-8"> 
						  <form:input path="specialAllowance" type="specialAllowance" class="form-control" id="specialAllowance" placeholder="Enter special Allowance" value="0"/>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-4" id="DateDemo"><b><spring:message code="label.professionalTax" /></b><span style="color: red">*</span> :</label>
						<div class="col-sm-8"> 
							<div id="sandbox-container">
							 <form:input path="professionalTax" type="text" class="form-control" id="professionalTax" placeholder="Enter professional Tax" value="0"/>
							</div>
						</div>
					</div>
				</div>
				</div>
				
			</div>
			</div>
			
	</div> 
	
	<div class="col-sm-6">
	<div class="modal-header">
		<h4><b><spring:message code="label.deductions" /></b><span style="color: red">*</span></h4>
	</div>
		<div class="add_emp_details_form">
			<div class="Emp_content">
				
				
				<div class="modal-body" style="width: 100%;margin:0px auto;">
				 <div class="form-horizontal" role="form">
					  <div class="form-group">
						<label class="control-label col-sm-4" for="email"><b><spring:message code="label.foodamountdeduct" /></b><span style="color: red">*</span>:</label>
						<div class="col-sm-8">
						  <form:input path="foodamountDeduct" type="text" class="form-control" id="foodamountDeduct" placeholder="Enter food amount Deduct" value="0"/>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.professionalTax" /></b><span style="color: red">*</span> :</label>
						<div class="col-sm-8"> 
						  <form:input path="professionalTax" type="professionalTax" class="form-control" id="professionalTax" placeholder="Enter professional Tax" value="0"/>
						</div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-sm-4" id="DateDemo"><b><spring:message code="label.tds" /></b><span style="color: red">*</span> :</label>
						<div class="col-sm-8"> 
							<div id="sandbox-container">
							 <form:input path="tds" type="tds" class="form-control" id="tds" placeholder="Enter TDS" value="0"/>
							</div>
						</div>
					</div>
				</div>
				</div>
				
			</div>
			</div>
			
	</div> 
	
	
	
	
	
	<%-- <div class="col-sm-6">
	<div class="modal-header">
		<h4><b><spring:message code="label.leavedetails" /></b><span style="color: red">*</span></h4>
	</div>
		<div class="add_emp_details_form">
			<div class="Emp_content">
				
				
				<div class="modal-body" style="width: 100%;margin:0px auto;">
				 <div class="form-horizontal" role="form">
					  <div class="form-group">
						<label class="control-label col-sm-4" for="email"><b><spring:message code="label.typesofleave" /></b><span style="color: red">*</span>:</label>
						<div class="col-sm-8">
						  <form:input path="foodamountDeduct" type="text" class="form-control" id="foodamountDeduct" placeholder="Enter food amount Deduct"/>
						</div>
					</div>
					
					
						<div class="form-group">
						<label class="control-label col-sm-4" id="DateDemo"><b><spring:message code="label.typesofleave" /></b><span style="color: red">*</span> :</label>
						<div class="col-sm-8"> 
				<form:select path="typesofLeave" class="form-control" id="typesofLeave">
						<form:option value="">
							<spring:message code="label.selectValue" />
						</form:option>
						<form:option value="ROLE_EMP">
							<spring:message code="label.emergencyleave" />
						</form:option>
						<form:option value="ROLE_MANAGER">
							<spring:message code="label.annunalleave" />
						</form:option>
					</form:select>
					</div>
						</div>
					
					<div class="modal-body" style="width: 100%;margin:0px auto;">
				 <div class="form-horizontal" role="form">
					  <div class="form-group">
						<label class="control-label col-sm-4" for="email"><b><spring:message code="label.reasonforleave" /></b><span style="color: red">*</span>:</label>
						<div class="col-sm-8">
						  <form:input path="reason" type="text" class="form-control" id="reason" placeholder="Enter Reason"/>
						</div>
					</div>
					
					
						<div class="form-group">
						<label class="control-label col-sm-4" id="DateDemo"><b><spring:message code="label.noofdays" /></b><span style="color: red">*</span> :</label>
						<div class="col-sm-8"> 
				    <form:select path="noofdays" class="form-control" id="noofdays"  onchange="leave()">
						<form:option value="">
							<spring:message code="label.selectValue" />
						</form:option>
						<form:option value="1">
							
						</form:option>
						<form:option value="2">
							
						</form:option>
					</form:select>
					</div>
						</div>
					
				
					<div class="form-group" id="leaveDateone" style="display: none;">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.Leavedate" /></b><span style="color: red">*</span> :</label>
						<div class="col-sm-8"> 
						  <form:input path="leaveDate" type="leaveDate" class="form-control" id="leaveDate" placeholder="Enter leave Date"/>
						</div>
					</div>
					
					
					
					<div class="form-group" id="leaveDatetwo" style="display: none;">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.fromdate" /></b><span style="color: red">*</span> :</label>
						<div class="col-sm-8"> 
						  <form:input path="fromDate" type="fromDate" class="form-control" id="fromDate" placeholder="Enter from Date"/>
						</div>
					</div>
					
			
					
					 <div class="form-group" id="leaveDatamore" style="display: none;">
						<label class="control-label col-sm-4" id="DateDemo"><b><spring:message code="label.todate" /></b><span style="color: red">*</span> :</label>
						<div class="col-sm-8"> 
							<div id="sandbox-container">
							 <form:input path="toDate" type="toDate" class="form-control" id="toDate" placeholder="Enter to date"/>
							</div>
						</div>
					</div> 
				</div>
				</div>
				
			</div>
			</div>
			
	</div>  --%>
	

	
	<div class="col-sm-12">
		<div class="text-center">
			<input type="submit" class="btn btn-primary btn-lg" value="Save" style="margin-top: 26px;width: 18%;"/><!-- Save <i class="fa fa-save"></i> -->
			
		</div>
		
		</div>
    </form:form>
	</div>

    </div>
    </div>
    </div>
	 
	<script>
		$('ul.nav li.dropdown').hover(function() {
		  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
		}, function() {
		  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
		});
		
	</script>
	<script>
		$(document).ready(function() {
			var readURL = function(input) {
				if (input.files && input.files[0]) {
					var reader = new FileReader();

					reader.onload = function (e) {
						$('.profile-pic').attr('src', e.target.result);
					}
			
					reader.readAsDataURL(input.files[0]);
				}
			}
			

			$(".file-upload").on('change', function(){
				readURL(this);
			});
			
			$(".upload-button").on('click', function() {
			   $(".file-upload").click();
			});
		});
	</script>
	<script>
		$('#datepicker-1').datepicker();
	</script>
	
  </body>