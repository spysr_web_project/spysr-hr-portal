<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


	<script src="<%=request.getContextPath()%>/resources/js/search.js"></script>
	<style>
		.filter{
			margin: 0px auto;
		    width: 281px;
		    float: right;
		    margin-top: 3px;	
		}
	</style>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				
				<div class="col-sm-12">
				<div class="modal-header">
					<h4><b>Employee Leave Details</b></h4>
				</div>
				<div class="input-group filter"> <span class="input-group-addon">Search</span>
					<input id="filter" type="text" class="form-control" placeholder="Type here...">
				</div>
					<div class="add_emp_details">
						 <table class="table table-bordered table-striped example" id="filter" data-rt-breakpoint="600">
                                        <thead>
                                            <tr>
                                            
                                             <th scope="col" data-rt-column="Project"><spring:message
								code="label.empid" /></th>

						<th scope="col" data-rt-column="Status"><spring:message
								code="label.name" /></th>
						<th scope="col" data-rt-column="Status"><spring:message
								code="label.typesofleave" /></th>

						<th scope="col" data-rt-column="Status">Leaves Remaining</th>
						<%-- <th scope="col" data-rt-column="Status"><spring:message
								code="label.leavedate" /></th> --%>

						<th scope="col" data-rt-column="Status"><spring:message
								code="label.fromdate" /></th>
						<th scope="col" data-rt-column="Status"><spring:message
								code="label.todate" /></th>
<th scope="col" data-rt-column="Status"><spring:message
								code="label.totalnumberleaveDays" /></th>


						<th scope="col" data-rt-column="Status"><spring:message
								code="label.status" /></th>

						<th scope="col" data-rt-column="Progress"><spring:message
								code="label.action" /></th>

					</tr>
				</thead>
				<tbody class="searchable">
					<c:if test="${! empty leaveRequestist}">

						<c:forEach items="${leaveRequestist}" var="leave">
							<tr>

								<td><c:out value="${leave.userName}"></c:out></td>

								<td><c:out value="${leave.name}"></c:out></td>

								<td><c:out value="${leave.typesofLeave}"></c:out></td>

								<td><c:out value="${leave.totalLeave}"></c:out></td>


								<%-- <fmt:formatDate var='date' value="${leave.leaveDate}"
									pattern="dd/MM/yyyy" />
								<td><c:out value="${date}"></c:out>  --%>
								
								<fmt:formatDate
										var='date' value="${leave.fromDate}" pattern="dd/MM/yyyy" />
								<td><c:out value="${date}"></c:out> <fmt:formatDate
										var='date' value="${leave.toDate}" pattern="dd/MM/yyyy" />
								<td><c:out value="${date}"></c:out>
								<td><c:out value="${leave.totalnumberleaveDays}"></c:out></td>
								<td><c:out value="${leave.status}"></c:out></td>
								
						       <td><a href="leavependingemployeeRequest?id=${leave.id}" class="btn btn-primary btn-sm"><spring:message code="label.select" /></a></td>

							

                                            </tr>  
                                            	</c:forEach>
					                         </c:if>                                        
                                        </tbody>
                             </table>
                                    
                      
				</div>
				                
                    <div class="form-group">
                   		<div class="col-sm-offset-5 col-sm-4" style="margin-top:15px;">
						 <a href ="/spysr-hr-portal/managers/manager" class="btn btn-primary btn-sm"><b><spring:message code="label.back"></spring:message></b></a>
						</div>
	 			   </div>
			</div>
		</div>
   
	<script>
		$('ul.nav li.dropdown').hover(function() {
		  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
		}, function() {
		  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
		});
		
	</script>
	
  </body>
</html>
