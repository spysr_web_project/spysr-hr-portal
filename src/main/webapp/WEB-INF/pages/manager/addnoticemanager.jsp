<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

	<!--  datepicker css js links -->
		<link href="<%=request.getContextPath()%>/resources/css/datepicker.min.css" rel="stylesheet">
		<script src="<%=request.getContextPath()%>/resources/js/jquery.min.js"></script>
		<script src="<%=request.getContextPath()%>/resources/js/datepicker.js"></script>
		
		<script src="<%=request.getContextPath()%>/resources/js/datepicker.en.js"></script>
	<!--  End datepicker css js links -->
	
<script>
 $(function() {
  	$( "#datepicker" ).datepicker({format: 'dd/mm/yyyy'});
  });
  var today = new Date();
  today.setHours(0,0,0,0);
	function val() {

		  var noticeDate  = document.getElementById('datepicker').value;
			
			from = noticeDate.split("/");
			var date1 = new Date(from[2], from[1] - 1, from[0]);
		
		 
		var serialNo=document.getElementById('serialNo').value;
		
	     var title= document.getElementById('title').value;
		
		var description= document.getElementById('description').value; 
		
		var canSubmit = true;
		
		if (serialNo == '') {

			document.getElementById('serialNo').style.borderColor = "red";
			return false;
		} else {
			document.getElementById('serialNo').style.borderColor = "green";
		}

		 if (serialNo.match('^\\d+$')  && !(serialNo == '')) {

				document.getElementById('serialNoError').style.display = 'none';

			} else {
				document.getElementById('serialNoError').style.display = 'block';
				
				document.getElementById('serialNo').style.borderColor = "red";
				canSubmit = false;
			}
		
		
		if(title == ''){
			document.getElementById('title').style.borderColor = "red";
			return false;
		}else{
			document.getElementById('title').style.borderColor = "green";
			}
		
		if (document.getElementById('datepicker').value == '') {
			document.getElementById('dateError').style.display = 'block';
			canSubmit = false;
		} else {
			document.getElementById('dateError').style.display = 'none';
		}
		
		if(date1 < today) {
			document.getElementById('datepicker').style.borderColor = "red";
		return false;
		} else {
			document.getElementById('datepicker').style.borderColor = "green";
		
		}
		
		if(description ==''){
			document.getElementById('description').style.borderColor="red";
			return false;
		}else{
			document.getElementById('description').style.borderColor="green";	
			}
	
		
		if (canSubmit == false) {
			return false;
		}
	}
	
</script>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

 
	
	<div class="modal-header">
		<h4><b><spring:message code="label.noticedetails"/></b><span style="color:red">*</span></h4>
	</div>
	 <div class="error">
     <p style="text-align:center;color:red;">${success}</p>
    </div>
		<div class="add_emp_details_form">
			<div class="Emp_content">
				<form:form action="insertnoticeManager" method="post" commandName="noticeDetailsForm" onsubmit="return val();">		
				
				<div class="modal-body" style="width: 80%;">
				  <div class="form-horizontal" role="form">
					  <div class="form-group">
						<label class="control-label col-sm-4" for="email"><b><spring:message code="label.serialNo"/></b><span style="color:red">*</span> :</label>
						<div class="col-sm-8">
						  <form:input path="serialNo" type="text" class="form-control input-sm" id="serialNo" placeholder="Enter serialNo"/>
						</div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"></label>
						<div class="col-sm-8"> 
						 <label id="serialNoError" class="error" style="display:none"><b><spring:message code="label.validation"/></b><span style="color:red">*</span>:</label>
						</div>
						
					</div>
					
					<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.noticetitle"/></b><span style="color:red">*</span> :</label>
						<div class="col-sm-8"> 
						  <form:input path="title"  class="form-control input-sm" id="title" placeholder="Enter title"/>
						</div>
					</div>
					
				
				<div class="form-group" id="e1">
						<label class="control-label col-sm-4" id="DateDemo"><b><spring:message code="label.noticeDate" /></b><span style="color: red">*</span> :</label>
					<div class="col-sm-8"> 
							
				          <form:input path="noticeDate" data-multiple-dates="0" data-multiple-dates-separator=", " data-position="top left" data-language="en" class="form-control input-sm datepicker-here" placeholder="Enter Notice Date" id="datepicker" />
					    	<span id="dateError" style="display: none; color: red;">
						<spring:message code="label.dateIsMandatory"/></span>
						<span id="dateError" style="display: none; color: red;">
						<spring:message code="label.dateIsMandatory"/></span> 					
					</div>
				</div>
				
				
				
					<div class="form-group">
						<label class="control-label col-sm-4"><b><spring:message code="label.description" /></b><span style="color: red">*</span> :</label>
						<div class="col-sm-8"> 
							<div id="sandbox-container">
                       <form:textarea path="description"  class="form-control input-sm" id="description" placeholder="Enter Description"/>
							</div>
						</div>
					</div> 
					<div class="form-group">
						
						<div class="col-sm-offset-4 col-sm-6">
							
								<input type="submit" class="btn btn-primary btn-sm" value="Save" />
		                       <a href ="/spysr-hr-portal/managers/manager" class="btn btn-primary btn-sm"><b><spring:message code="label.back" /></b></a>
						</div>
                     </div>
				</div> 
					
			</div>
					 
					</form:form>
				</div>
				</div>
				
			</div>
			

	 
	<script>
		$('ul.nav li.dropdown').hover(function() {
		  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
		}, function() {
		  $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
		});
		
	</script>
	<script>
		$(document).ready(function() {
			var readURL = function(input) {
				if (input.files && input.files[0]) {
					var reader = new FileReader();

					reader.onload = function (e) {
						$('.profile-pic').attr('src', e.target.result);
					}
			
					reader.readAsDataURL(input.files[0]);
				}
			}
			

			$(".file-upload").on('change', function(){
				readURL(this);
			});
			
			$(".upload-button").on('click', function() {
			   $(".file-upload").click();
			});
		});
	</script>
	<script>
		$('#datepicker-1').datepicker();
	
</script>
</body>