<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
	<!--  datepicker css js links -->
		<link href="<%=request.getContextPath()%>/resources/css/datepicker.min.css" rel="stylesheet">
		<script src="<%=request.getContextPath()%>/resources/js/jquery.min.js"></script>
		<script src="<%=request.getContextPath()%>/resources/js/datepicker.js"></script>
		
		<script src="<%=request.getContextPath()%>/resources/js/datepicker.en.js"></script>
		
  <script src="<%=request.getContextPath()%>/resources/js/bootstrap.min.js"></script>
	<!--  End datepicker css js links -->
<script>
	function val() {

		var serialNo=document.getElementById('serialNo').value;
		
	     var projectName= document.getElementById('projectName').value;
		
		var projectDetails= document.getElementById('projectDetails').value; 
		
		var projectMembers= document.getElementById('projectMembers').value;
		
		var canSubmit = true;
		
		if (serialNo == '') {

			document.getElementById('serialNo').style.borderColor = "red";
			return false;
		} else {
			document.getElementById('serialNo').style.borderColor = "green";
		}

		
		
		if(projectName == ''){
			document.getElementById('projectName').style.borderColor = "red";
			return false;
		}else{
			document.getElementById('projectName').style.borderColor = "green";
			}
		
	
		if(projectDetails ==''){
			document.getElementById('projectDetails').style.borderColor="red";
			return false;
		}else{
			document.getElementById('projectDetails').style.borderColor="green";	
			}
		
		if(projectMembers ==''){
			document.getElementById('projectMembers').style.borderColor="red";
			return false;
		}else{
			document.getElementById('projectMembers').style.borderColor="green";	
			}
		
		if (canSubmit == false) {
			return false;
		}
	}
	
</script>
	
	<style>
		body{
			background: #f2f2f2 !important;
		}
		.company_logo h3{
			color:rgb(45, 175, 240);
			text-align:center;
		}
		.header_title h3{
			color:#666;
			font-family: 'Source Sans Pro SemiBold',sans-serif;
		}
		.header {
			background-color: #fff;
			    box-shadow: rgba(0, 0, 0, 0.298039) 0px 0px 2px;
		}
		.upload-button {
			padding: 4px;
			border: 1px solid rgb(45, 175, 240);
			border-radius: 5px;
			display: block;
			float: left;
			cursor: pointer;
			margin-top: 6px;
			background-color: white;
			font-weight: 600;
			color: #288b8e;
			width: 70%;
		}
		.col-sm-3.sidenav {
			padding: 51px;
    		margin-top: 17px;
		}
		.profile-pic {
			max-width: 211px;
			max-height: 250px;
			display: block;
			border: 5px solid #d6d5d5;
		}

		.file-upload {
			display: none !important;
		}
		label.control-label.col-sm-2 {
			text-align: -webkit-auto;
		}
		hr {
			margin-top: 20px;
			margin-bottom: 20px;
			border: 0;
			border-top: 1px solid #b3afaf;
		}
		label {
			color: #666;
		}
		span.aster {
			margin-left: 6px;
			color: red;
			font-weight: 800;
		}
		footer p{
			text-align:center;
		}
	</style>

<body>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

	<div class="container-fluid">
	  <div class="row content">
			<div class="col-sm-3 sidenav">
				<div class="aside_img">
					<%-- <div class="upload_img">
						<img class="profile-pic img-responsive"  src="<%=request.getContextPath()%>/resources/images/up.jpg" />
						<div class="upload-button"><spring:message code="label.uploadImage" /></div> 
						<input class="file-upload" type="file" accept="image/*"/>
						
						 <form:input path="file" type="file" class="file-upload"
											onchange="checkfile(this);" id="imageFile" /> 
					</div> --%>
				</div>
			</div>
			<div class="col-sm-9" style="border-left: 3px solid rgb(45, 175, 240);">
				<div class="details" style=" margin-top: 44px;margin-bottom: 44px;">
					<div class="modal-header">
					 <h4><b><spring:message code="label.projectList"/></b></h4>
	                 </div>
	                  <div class="error">
                   <p style="text-align:center;color:red;">${success}</p>
                    </div>
					<div class="add_emp_details_form">
			      <div class="Emp_content" style="padding:8px 23px;">
						<form:form action="selectprojectUpdate2" class="form-horizontal" method="post" commandName="projectDetailsForm" onsubmit="return val();">	
					   <form:hidden path="id" />
						<div class="form-group">
						<label class="control-label col-sm-4" for="email"><b><spring:message code="label.serialNo"/></b><span style="color:red">*</span> :</label>
							<div class="col-sm-8">
								<form:input path="serialNo" class="form-control" id="serialNo" placeholder="Enter serialNo"></form:input>
							</div>
						</div>
						<div class="form-group">
					<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.projectName"/></b><span style="color:red">*</span> :</label>
							<div class="col-sm-8">
								<form:input path="projectName" class="form-control" id="projectName" placeholder="Enter projectName"/>
							</div>
						</div>
						<div class="form-group">
					<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.membersInvolved"/></b><span style="color:red">*</span> :</label>
							<div class="col-sm-8">
								<form:select path="membersInvolved" id="membersInvolved" data-multiple-dates="0"
												data-multiple-dates-separator=", "
												data-position="bottom left" data-language="en"
												style="width: 100%;display: inline;"
												class="datepicker-here form-control input-sm"  value="${model.projectDetailsForm.membersInvolved}">
												
												<form:option value="1"></form:option>
												<form:option value="2"></form:option>
												<form:option value="3"></form:option>
												<form:option value="4"></form:option>
												<form:option value="5"></form:option>
												<form:option value="6"></form:option>
												<form:option value="7"></form:option>
												<form:option value="8"></form:option>
												<form:option value="9"></form:option>
												<form:option value="10"></form:option>
												
											</form:select> 
							</div>
						</div>
						<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.projectMembers"/></b><span style="color:red">*</span> :</label>
							<div class="col-sm-8">
								<form:textarea path="projectMembers" class="form-control" rows="5" id="projectMembers" placeholder="Enter Project Members"/>
							</div>
						</div>
						<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.version"/></b><span style="color:red">*</span> :</label>
							<div class="col-sm-8">
								<form:textarea path="version" class="form-control" rows="5" id="version" placeholder="Enter Version"/>
							</div>
						</div>
						<div class="form-group">
						<label class="control-label col-sm-4"><b><spring:message code="label.projectDetails" /></b><span style="color: red">*</span> :</label>
							<div class="col-sm-8">
								<form:textarea path="projectDetails" class="form-control" rows="5" id="projectDetails" placeholder="Enter Project Details"/>
							</div>
						</div>
						
						
						<div class="form-group">
						  <div class="col-sm-offset-4 col-sm-8">
							<button type="submit" onclick="validate();" class="btn btn-primary">Submit</button>
							<button type="submit" onclick="validate();" class="btn btn-success">Back</button>
						  </div>
						</div>
				   </form:form>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
</div>
<!--For form validation script-->
	<script>
		function validate(){
			if(document.getElementById('info').value="") {
                alert("enter something valid");
                return false;
            }
		}
	</script>
<!--End form validation script-->

<!--For upload file script-->
<script>
	$(document).ready(function() {

    
    var readURL = function(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.profile-pic').attr('src', e.target.result);
            }
    
            reader.readAsDataURL(input.files[0]);
        }
    }
    

		$(".file-upload").on('change', function(){
			readURL(this);
		});
		
		$(".upload-button").on('click', function() {
		   $(".file-upload").click();
		});
	});
</script>
<!--End upload file script-->
</body>
</html>

