<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<link href="<%=request.getContextPath()%>/resources/css/HoldOn.css" rel="stylesheet" type="text/css">
<script src="<%=request.getContextPath()%>/resources/js/HoldOn.js"></script>
<script>
function validateForm(themeName) {
    var x = document.forms["myForm"]["recipient"].value;
    var atpos = x.indexOf("@");
    var dotpos = x.lastIndexOf(".");
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length) {
        alert("Enter A Email address");
        return false;
    }
    var y = document.forms["myForm"]["subject"].value;
    
    if(y==null||y==''){

   	 alert("Subject name must be filled out");
   	  return false;
   }
   

   if(x != '' && y != '')

   	{
   	HoldOn.open({
   		theme:themeName,
   	
   	});
   	
   	setTimeout(function(){
   		HoldOn.close();
   	},8000);
   	}
   
   }
</script>
 
       
   
     
     <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        <form method="post" action="mailSender" name="myForm" onsubmit="return validateForm('sk-rect');">
        
	<div class="modal-header">
		<h4><b><spring:message code="label.sendQueryMail"/></b><span style="color:red">*</span></h4>
	</div>
		<div class="add_emp_details_form">
			<div class="Emp_content">
				
				
				 <div class="modal-body" style="width: 100%;margin:0px auto;">
				  <div class="form-horizontal" role="form">
					  <div class="form-group">
						<label class="control-label col-sm-4" for="email"><b><spring:message code="label.to"/></b><span style="color:red">*</span> :</label>
						<div class="col-sm-8">
						 <input type="text" name="recipient" class="form-control input-sm" placeholder="Enter a Email" id="recipient" size="65" />
						</div>
					</div> 
       
       
       	     	    <div class="form-group">
						<label class="control-label col-sm-4" for="pwd"><b><spring:message code="label.subject"/></b><span style="color:red">*</span> :</label>
						<div class="col-sm-8"> 
						<input type="text" name="subject" id="subject" class="form-control input-sm" placeholder="Enter a Subject" size="65" />
						</div>
					</div>
					
					
            		<div class="form-group">
						<label class="control-label col-sm-4" id="DateDemo"><b><spring:message code="label.mesg" /></b><span style="color: red">*</span> :</label>
						<div class="col-sm-8"> 
							<div id="sandbox-container">
							<textarea cols="40" rows="10" name="message" class="form-control input-sm" placeholder="Enter a Message"></textarea>
							</div>
						</div>
                     </div>
                     <div class="form-group">
						
						<div class="col-sm-offset-4 col-sm-5">
							 <input type="submit" value="<spring:message code="label.sendEmailNotification"/>" onclick="testHoldon('sk-rect');" class="btn btn-success btn-sm" />
				      <a href ="/spysr-hr-portal/managers/manager"  class="btn btn-primary btn-sm"><b><spring:message code="label.back"></spring:message></b></a>
						</div>
                     </div>
                     </div>
                     </div>
                     </div>
                     
                  
             
          </form>
    </div>