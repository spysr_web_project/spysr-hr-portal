<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
    <!-- To fetch the request url -->
<c:set var="req" value="${requestScope['javax.servlet.forward.request_uri']}"/>      
<c:set var="baseURL" value="${fn:split(req,'/')}" />

<script src="<%=request.getContextPath()%>/resources/js/search.js"></script>
<style>
.filter {
	margin: 0px auto;
	width: 281px;
	float: right;
	margin-top: 3px;
}
</style>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

	<div class="col-sm-12">
		
		<div class="col-sm-12">
		<div class="modal-header">
			<h4><b><spring:message code="label.leavedetails" /></b><span style="color: red">*</span></h4>
		</div>
		<div class="input-group filter">
			<span class="input-group-addon">search</span> <input id="filter"
				type="text" class="form-control" placeholder="Type here...">
		</div>
		<div class="add_emp_details">
			<table class="table table-bordered table-striped example" id="filter"
				data-rt-breakpoint="600">
				<thead>
					<tr>

						<th scope="col" data-rt-column="Project"><spring:message
								code="label.empid" /></th>

						<th scope="col" data-rt-column="Status"><spring:message
								code="label.name" /></th>
						<th scope="col" data-rt-column="Status"><spring:message
								code="label.typesofleave" /></th>
						<th scope="col" data-rt-column="Status"><spring:message
								code="label.reason" /></th>
						<th scope="col" data-rt-column="Status"><spring:message
								code="label.totalnumberleaveDays" /></th>
					<%-- 	<th scope="col" data-rt-column="Status"><spring:message
								code="label.leavedate" /></th> --%>

						<th scope="col" data-rt-column="Status"><spring:message
								code="label.fromdate" /></th>
						<th scope="col" data-rt-column="Status"><spring:message
								code="label.todate" /></th>
						<%-- <th scope="col" data-rt-column="Status"><spring:message
								code="label.eligibleleave" /></th> --%>
						<th scope="col" data-rt-column="Status"><spring:message
								code="label.totaleave" /></th>


						<th scope="col" data-rt-column="Status"><spring:message
								code="label.status" /></th>
						<th scope="col" data-rt-column="Status"><spring:message
								code="label.comment" /></th>
						<th scope="col" data-rt-column="Progress"><spring:message
								code="label.action" /></th>
						
				</thead>
				<tbody class="searchable">
					<c:if test="${! empty leaveRequestist}">

						<c:forEach items="${leaveRequestist}" var="leave">
							<tr>

								<td><c:out value="${leave.userName}"></c:out></td>

								<td><c:out value="${leave.name}"></c:out></td>

								<td><c:out value="${leave.typesofLeave}"></c:out></td>
								<td><c:out value="${leave.reason}"></c:out></td>
								<td><c:out value="${leave.totalnumberleaveDays}"></c:out></td>



							<%-- 	<fmt:formatDate var='leaveDate' value='${leave.leaveDate}'
									pattern="dd/MM/yyyy" />
								<td><c:out value="${leaveDate}"></c:out></td> --%>


								<fmt:formatDate var='fromDate' value='${leave.fromDate}'
									pattern="dd/MM/yyyy" />
								<td><c:out value="${fromDate}"></c:out></td>

								<fmt:formatDate var='toDate' value='${leave.toDate}'
									pattern="dd/MM/yyyy" />
								<td><c:out value="${toDate}"></c:out></td>


							<%-- 	<td><c:out value="${leave.eligibleLeave}"></c:out></td> --%>
								<td><c:out value="${leave.totalLeave}"></c:out></td>



								<td><c:out value="${leave.status}"></c:out></td>
								<td><c:out value="${leave.comment}"></c:out></td>
								<td><a href="cancelleavesmanagerRequest?id=${leave.id}"
									class="btn btn-primary btn-sm"><spring:message
											code="label.select" /></a></td>



							</tr>
						</c:forEach>
					</c:if>
				</tbody>
			</table>
			 
			
		</div>
		<div class="form-group" style="margin-top:15px;">
                    		<div class="col-sm-offset-5 col-sm-5">
                    		 <c:if test="${baseURL[1] == 'managers'}"><c:set var="back" value="manager"/></c:if>
							 <c:if test="${baseURL[1] == 'officeManagers'}"><c:set var="back" value="officeManager"/></c:if>
					<c:if test="${baseURL[1] == 'employees'}"><c:set var="back" value="employee"/></c:if>
					<a href ="${back}"  class="btn btn-primary btn-sm"><b><spring:message code="label.back"></spring:message></b></a>		     
							</div>
			 </div>
	</div>
</div>
</div>
<script>
	$('ul.nav li.dropdown').hover(
			function() {
				$(this).find('.dropdown-menu').stop(true, true).delay(200)
						.fadeIn(500);
			},
			function() {
				$(this).find('.dropdown-menu').stop(true, true).delay(200)
						.fadeOut(500);
			});
</script>

</body>
</html>
