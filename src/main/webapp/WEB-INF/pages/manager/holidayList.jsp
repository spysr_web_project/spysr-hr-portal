<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<script src="<%=request.getContextPath()%>/resources/js/search.js"></script>
	<style>
		.filter{
			margin: 0px auto;
		    width: 281px;
		    float: right;
		    margin-top: 3px;	
		}
	</style>
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

	<div class="col-sm-12">
		<div class="add_emp">
			<a href="addholidays">
				<button type="button" class="btn btn-info btn-md page-header">
					<b><spring:message code="label.addholiday"></spring:message></b>
				</button>
			</a>
		</div>
	</div>
	<div class="col-sm-12">
		<div class="modal-header">
			<h4>
				<b><spring:message code="label.holidaysList" /></b>
			</h4>
		</div>
	</div>
	<div class="col-sm-12">
	<div class="input-group filter"> <span class="input-group-addon"><spring:message code="label.search"/></span>
					<input id="filter" type="text" class="form-control" placeholder="Type here...">
				</div>
		<div class="add_emp_details">
			<table class="table table-bordered table-striped" id="filter"
				data-rt-breakpoint="600">
				<thead>
					<tr>
						<th scope="col" data-rt-column="ID"><spring:message
								code="label.serialNo" /></th>
						<th scope="col" data-rt-column="Project"><spring:message
								code="label.holidayDate" /></th>
						<th scope="col" data-rt-column="Status"><spring:message
								code="label.holiday" /></th>
						<th scope="col" data-rt-column="Progress"><spring:message
								code="label.action" /></th>
					</tr>
				</thead>
				<tbody class="searchable">
					<c:if test="${! empty holidayList}">

						<c:forEach items="${holidayList}" var="holiday">
							<tr>
								<td><c:out value="${holiday.serialNo}"></c:out></td>

								<fmt:formatDate var='date' value="${holiday.holidayDate}"
									pattern="dd/MM/yyyy" />
								<td><c:out value="${date}"></c:out>
								<td><c:out value="${holiday.holidayName}"></c:out></td>
								<td><a href="selectholidayUpdate?id=${holiday.id}"
									class="btn btn-primary btn-sm"><spring:message
											code="label.edit" /></a></td>

							</tr>
						</c:forEach>
					</c:if>
				</tbody>
			</table>

			<div class="form-group">

				<div class="col-sm-offset-4 col-sm-5">


					<br>
					<a href="/spysr-hr-portal/managers/manager"><button type="button"
							class="btn btn-success btn-sm" aria-hidden="true">
							<spring:message code="label.back" />
						</button></a>
				</div>
			</div>

		</div>
	</div>
</div>


<script>
	$('ul.nav li.dropdown').hover(
			function() {
				$(this).find('.dropdown-menu').stop(true, true).delay(200)
						.fadeIn(500);
			},
			function() {
				$(this).find('.dropdown-menu').stop(true, true).delay(200)
						.fadeOut(500);
			});
</script>

</body>
</html>
