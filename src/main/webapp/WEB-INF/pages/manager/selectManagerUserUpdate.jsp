<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>


<!--  datepicker css js links -->
<link
	href="<%=request.getContextPath()%>/resources/css/datepicker.min.css"
	rel="stylesheet">
<script src="<%=request.getContextPath()%>/resources/js/jquery.min.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/datepicker.js"></script>

<script
	src="<%=request.getContextPath()%>/resources/js/datepicker.en.js"></script>
<!--  End datepicker css js links -->
<script>

	function val() {

		
	
	     var designation= document.getElementById('designation').value;
	    
		
		 var department= document.getElementById('department').value; 

		 var currentRole = document.getElementById('currentRole').value;
		
		var name = document.getElementById('name').value;
	 	
	    var email=document.getElementById('email').value;  
	    
	    var fatherName=document.getElementById('fatherName').value;
	
       var gender=document.getElementById('gender').value; 
	    
	    var permanentAddress=document.getElementById('permanentAddress').value;
	    
	    var contactNo=document.getElementById('contactNo').value;
	    
       var bname=document.getElementById('bname').value;
	    
	    var branch=document.getElementById('branch').value;
	    
	    var accountNo=document.getElementById('accountNo').value;  
	    
	 
	     var fixedPackage=document.getElementById('fixedPackage').value; 
	     
	     var variablePackage=document.getElementById('variablePackage').value; 
	
	     var basicSalary=document.getElementById('basicSalary').value; 
	     
	     var hra=document.getElementById('hra').value; 
	     
	     var conveyAllowance=document.getElementById('conveyAllowance').value; 
	     
	     var medicalAllowance=document.getElementById('medicalAllowance').value; 
	     
	     var specialAllowance=document.getElementById('specialAllowance').value; 
	     
	     var professionalTax=document.getElementById('professionalTax').value; 
	     
	     var foodamountDeduct=document.getElementById('foodamountDeduct').value; 
	    
	      var professionalTax1=document.getElementById('professionalTax1').value;  
	     
	 
		var canSubmit = true;
		
	    
		if(designation == ''){
			document.getElementById('designation').style.borderColor = "red";
			return false;
		}else{
			document.getElementById('designation').style.borderColor = "green";
			}
		 
	
		if(department ==''){
			document.getElementById('department').style.borderColor="red";
			return false;
		}else{
			document.getElementById('department').style.borderColor="green";	
			} 
	

		if (currentRole == '') {
			document.getElementById('currentRole').style.borderColor = "red";
			return false;
		} else {
			document.getElementById('currentRole').style.borderColor = "green";
		}
	
	
	
		if (name == '') {

			document.getElementById('name').style.borderColor = "red";
			return false;
		} else {
			document.getElementById('name').style.borderColor = "green";
		}	 
		
		
		 
		 if (email == '' ) {

			document.getElementById('email').style.borderColor = "red";
			return false;
		} else {
			document.getElementById('email').style.borderColor = "green";
		} 

		 if (email.match(/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/) && !(email == '')) {

				document.getElementById('emailError1').style.display = 'none';

			} else {
				document.getElementById('emailError1').style.display = 'block';
				document.getElementById('email').style.borderColor = "red";
				canSubmit = false;
			} 
		
		
		
			if (fatherName == '') {

				document.getElementById('fatherName').style.borderColor = "red";
				return false;
			} else {
				document.getElementById('fatherName').style.borderColor = "green";
			}
			
		
		
	 	if (gender == '') {
			document.getElementById('gender').style.borderColor = "red";
			return false;
		} else {
			document.getElementById('gender').style.borderColor = "green";
		}
		
		if (permanentAddress == '') {
			document.getElementById('permanentAddress').style.borderColor = "red";
			return false;
		} else {
			document.getElementById('permanentAddress').style.borderColor = "green";
		}
	
		if (contactNo == '') {
			document.getElementById('contactNo').style.borderColor = "red";
			return false;
		} else {
			document.getElementById('contactNo').style.borderColor = "green";
		} 
	
		  /* if (contactNo.match('/^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/') && !(contactNo == '')) {

				document.getElementById('contactNoError').style.display = 'none';

			} else {
				document.getElementById('contactNoError').style.display = 'block';
				document.getElementById('contactNo').style.borderColor = "red";
				canSubmit = false;
			} */
		
		
		if (bname == '') {

			document.getElementById('bname').style.borderColor = "red";
			return false;
		} else {
			document.getElementById('bname').style.borderColor = "green";
		}
		
		if (branch == '') {

			document.getElementById('branch').style.borderColor = "red";
			return false;
		} else {
			document.getElementById('branch').style.borderColor = "green";
		} 
		
		
		if (accountNo == '') {

			document.getElementById('accountNo').style.borderColor = "red";
			return false;
		} else {
			document.getElementById('accountNo').style.borderColor = "green";
		} 
		
		 if (accountNo.match('^\\d+$') && !(accountNo == '')) {

				document.getElementById('accountNoError').style.display = 'none';

			} else {
				document.getElementById('accountNoError').style.display = 'block';
				document.getElementById('accountNo').style.borderColor = "red";
				canSubmit = false;
			} 
		
		 if(fixedPackage==''|| fixedPackage=="0"){
				document.getElementById('fixedPackage').style.borderColor="red";
				return false;
				
			} else {
				document.getElementById('fixedPackage').style.borderColor = "green";
			}   
		 if (fixedPackage.match('^\\d+$')  && !(fixedPackage == '')) {

				document.getElementById('fixedPackageError').style.display = 'none';

			} else {
				document.getElementById('fixedPackageError').style.display = 'block';
				
				document.getElementById('fixedPackage').style.borderColor = "red";
				canSubmit = false;
			} 
		
	   if(variablePackage==''|| variablePackage=="0"){
			document.getElementById('variablePackage').style.borderColor="red";
			return false;
			
		} else {
			document.getElementById('variablePackage').style.borderColor = "green";
		}    
		

		  if (variablePackage.match('^\\d+$')  && !(variablePackage == '')) {

				document.getElementById('variablePackageError').style.display = 'none';

			} else {
				document.getElementById('variablePackageError').style.display = 'block';
				
				document.getElementById('variablePackage').style.borderColor = "red";
				canSubmit = false;
			} 
	
		/*  if(totalPackage==''|| totalPackage=="0"){
				document.getElementById('totalPackage').style.borderColor="red";
				return false;
				
			} else {
				document.getElementById('totalPackage').style.borderColor = "green";
			}   */ 
		 
		 
		 if(basicSalary==''|| basicSalary=="0"){
				document.getElementById('basicSalary').style.borderColor="red";
				return false;
				
			} else {
				document.getElementById('basicSalary').style.borderColor = "green";
			}    
		 
		 
		 	 if (basicSalary.match('^\\d+$')  && !(basicSalary == '')) {

					document.getElementById('basicSalaryError').style.display = 'none';

				} else {
					document.getElementById('basicSalaryError').style.display = 'block';
					
					document.getElementById('basicSalary').style.borderColor = "red";
					canSubmit = false;
				} 
			
		 	 if(hra==''|| hra=="0"){
					document.getElementById('hra').style.borderColor="red";
					return false;
					
				} else {
					document.getElementById('hra').style.borderColor = "green";
				}   
			 
			 
				 if (hra.match('^\\d+$')  && !(hra == '')) {

						document.getElementById('hraError').style.display = 'none';

					} else {
						document.getElementById('hraError').style.display = 'block';
						
						document.getElementById('hra').style.borderColor = "red";
						canSubmit = false;
					} 
			 
			
		 if(conveyAllowance==''|| conveyAllowance=="0"){
				document.getElementById('conveyAllowance').style.borderColor="red";
				return false;
				
			} else {
				document.getElementById('conveyAllowance').style.borderColor = "green";
			}   
		 
		 if (conveyAllowance.match('^\\d+$')  && !(conveyAllowance == '')) {

				document.getElementById('conveyAllowanceError').style.display = 'none';

			} else {
				document.getElementById('conveyAllowanceError').style.display = 'block';
				
				document.getElementById('conveyAllowance').style.borderColor = "red";
				canSubmit = false;
			} 
		  
		 
		 
		 
		 if(medicalAllowance==''|| medicalAllowance=="0"){
				document.getElementById('medicalAllowance').style.borderColor="red";
				return false;
				
			} else {
				document.getElementById('medicalAllowance').style.borderColor = "green";
			}   
		 
		
		 if (medicalAllowance.match('^\\d+$')  && !(medicalAllowance == '')) {

				document.getElementById('medicalAllowanceError').style.display = 'none';

			} else {
				document.getElementById('medicalAllowanceError').style.display = 'block';
				
				document.getElementById('medicalAllowance').style.borderColor = "red";
				canSubmit = false;
			}  
		 
		 
		 
		 if(specialAllowance==''|| specialAllowance=="0"){
				document.getElementById('specialAllowance').style.borderColor="red";
				return false;
				
			} else {
				document.getElementById('specialAllowance').style.borderColor = "green";
			}   
		 
		 if (specialAllowance.match('^\\d+$')  && !(specialAllowance == '')) {

				document.getElementById('specialAllowanceError').style.display = 'none';

			} else {
				document.getElementById('specialAllowanceError').style.display = 'block';
				
				document.getElementById('specialAllowance').style.borderColor = "red";
				canSubmit = false;
			}  
		 
		 
		 
		 
	 	 if(professionalTax==''|| professionalTax=="0"){
				document.getElementById('professionalTax').style.borderColor="red";
				return false;
				
			} else {
				document.getElementById('professionalTax').style.borderColor = "green";
			}   
		 
		 if (professionalTax.match('^\\d+$')  && !(professionalTax == '')) {

				document.getElementById('professionalTaxError').style.display = 'none';

			} else {
				document.getElementById('professionalTaxError').style.display = 'block';
				
				document.getElementById('professionalTax').style.borderColor = "red";
				canSubmit = false;
			} 
		  
		 
		 
		  if(foodamountDeduct==''|| foodamountDeduct=="0"){
				document.getElementById('foodamountDeduct').style.borderColor="red";
				return false;
				
			} else {
				document.getElementById('foodamountDeduct').style.borderColor = "green";
			}   
		 
		 if (foodamountDeduct.match('^\\d+$')  && !(foodamountDeduct == '')) {

				document.getElementById('foodamountDeductError').style.display = 'none';

			} else {
				document.getElementById('foodamountDeductError').style.display = 'block';
				
				document.getElementById('foodamountDeduct').style.borderColor = "red";
				canSubmit = false;
			} 
		  
		 
		 
		 
		   if(professionalTax1==''|| professionalTax1=="0"){
				document.getElementById('professionalTax1').style.borderColor="red";
				return false;
				
			} else {
				document.getElementById('professionalTax1').style.borderColor = "green";
			}   
		 
		  if (professionalTax1.match('^\\d+$')  && !(professionalTax1 == '')) {

				document.getElementById('professionalTaxError1').style.display = 'none';

			} else {
				document.getElementById('professionalTaxError1').style.display = 'block';
				
				document.getElementById('professionalTax1').style.borderColor = "red";
				canSubmit = false;
			} 
		 	
		
		if (canSubmit == false) {
			return false;
		}
	}

	
	
</script>
 <script> 
	$(document).ready(function(){

		//iterate through each textboxes and add keyup
		//handler to trigger sum event
		$(".txt").each(function() {

			$(this).keyup(function(){
				calculateSum();
			});
		});

	});

	function calculateSum() {

		var sum = 0;
		//iterate through each textboxes and add the values
		$(".txt").each(function() {

			//add only if the value is number
			if(!isNaN(this.value) && this.value.length!=0) {
				sum += parseFloat(this.value);
			
				}
			
		});
		
		//.toFixed() method will roundoff the final sum to 2 decimal places
		$("#sum").html(sum.toFixed(2));
		document.getElementById('totalPackage').value= (sum.toFixed(2));
		
	} 
    
    
    
    </script>
   
   <script> 
	$(document).ready(function(){

		//iterate through each textboxes and add keyup
		//handler to trigger sum event
		$(".txt1").each(function() {

			$(this).keyup(function(){
				calculateSum1();
			});
		});

	});

	function calculateSum1() {

		var sum1 = 0;
		//iterate through each textboxes and add the values
		$(".txt1").each(function() {

			//add only if the value is number
			if(!isNaN(this.value) && this.value.length!=0) {
				sum1 += parseFloat(this.value);
			}

		});
		//.toFixed() method will roundoff the final sum to 2 decimal places
		$("#sum1").html(sum1.toFixed(2));
	} 
    
    
    
    </script>  
    
 <script> 
	$(document).ready(function(){

		//iterate through each textboxes and add keyup
		//handler to trigger sum event
		$(".txt2").each(function() {

			$(this).keyup(function(){
				calculateSum2();
			});
		});

	});

	function calculateSum2() {

		var sum2 = 0;
		//iterate through each textboxes and add the values
		$(".txt2").each(function() {

			//add only if the value is number
			if(!isNaN(this.value) && this.value.length!=0) {
				sum2 += parseFloat(this.value);
			}

		});
		//.toFixed() method will roundoff the final sum to 2 decimal places
		$("#sum2").html(sum2.toFixed(2));
	} 
    
    
    
    </script>  


<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

	<form:form action="selectManagerUserUpdate2" method="post"
		commandName="endUserForm" onsubmit="return val();"
		enctype="multipart/form-data">
		<div class="col-sm-12">

			<div class="add_emp_details_form">



				<div class="modal-header">
					<h4>
						<b><spring:message code="label.employeedetails" /></b><span
							style="color: red">*</span>
					</h4>
				</div>
				<div class="add_emp_details_form">
					<div class="Emp_content">
						<div class="modal-body" style="width: 100%; margin: 0px auto;">
							<div class="form-horizontal" role="form">
								<form:hidden path="id" />
								<form:hidden path="role" />
								<form:hidden path="createdby" />
								<form:hidden path="password" />
								<form:hidden path="bname" />
								<form:hidden path="branch" />
								<form:hidden path="accountNo" />
								<form:hidden path="eligibleLeave" />
								<form:hidden path="totalLeave" />
	                            <form:hidden path="dob" />
	                           <form:hidden path="doj" />
	                           <form:hidden path="totalPackage"/>
								<div class="form-group">
									<label class="control-label col-sm-4" for="pwd"><b><spring:message
												code="label.empid" /></b><span style="color: red">*</span>:</label>
									<div class="col-sm-8">

										<form:input path="userName" class="form-control input-sm"
											placeholder="Enter userName"
											value="${model.endUserForm.userName}" readonly="true" />
									</div>
								</div>


								<div class="form-group">
									<label class="control-label col-sm-4" id="DateDemo"><b><spring:message
												code="label.designation" /></b><span style="color: red">*</span>
										:</label>
									<div class="col-sm-8">
										<form:select path="designation" class="form-control input-sm"
											id="designation">
											<form:option value="">
												<spring:message code="label.selectValue" />
											</form:option>
											<form:option value="Software Engineer">
												<spring:message code="label.software" />
											</form:option>
											<form:option value="Human Resources">
												<spring:message code="label.humanresources" />
											</form:option>
											<form:option value="TESTING">
												<spring:message code="label.testing" />
											</form:option>
												<form:option value="SUPPORT">
							                <spring:message code="label.support" />
						                    </form:option>
										</form:select>
									</div>
								</div>


								<div class="form-group">
									<label class="control-label col-sm-4" id="DateDemo"><b><spring:message
												code="label.department" /></b><span style="color: red">*</span>
										:</label>
									<div class="col-sm-8">
										<form:select path="department" class="form-control input-sm"
											id="department">
											<form:option value="">
												<spring:message code="label.selectValue" />
											</form:option>
											<form:option value="IT">
												<spring:message code="label.IT" />
											</form:option>
											<form:option value="Human Resources">
												<spring:message code="label.humanresources" />
											</form:option>
											<form:option value="TESTING">
												<spring:message code="label.testing" />
											</form:option>
											<form:option value="SUPPORT">
							                <spring:message code="label.support" />
						                    </form:option>
				
										</form:select>
									</div>
								</div>
									<div class="form-group">
									<label class="control-label col-sm-4" id="DateDemo"><b><spring:message
												code="label.selectRole" /></b><span style="color: red">*</span>
										:</label>
									<div class="col-sm-8">
										<form:select path="currentRole" class="form-control input-sm"
											id="currentRole">
											<form:option value="">
												<spring:message code="label.selectValue" />
											</form:option>
											<form:option value="ROLE_EMP">
												<spring:message code="label.roleEmployee" />
											</form:option>
											<form:option value="ROLE_MANAGER">
												<spring:message code="label.roleManager" />
											</form:option>
											<form:option value="ROLE_OFFICEMANAGER">
							              <spring:message code="label.roleOfficeManager" />
						                   </form:option>
							               <form:option value="ROLE_SUPPORT">
							               <spring:message code="label.support" />
						                   </form:option>
										</form:select>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="modal-header">
				<h4>
					<b><spring:message code="label.personalInfo" /></b><span
						style="color: red">*</span>
				</h4>
			</div>
			<div class="add_emp_details_form">
				<div class="Emp_content">


					<div class="modal-body" style="width: 100%; margin: 0px auto;">
						<div class="form-horizontal" role="form">
							<div class="form-group">
								<label class="control-label col-sm-4" for="email"><b><spring:message
											code="label.name" /></b><span style="color: red">*</span> :</label>
								<div class="col-sm-8">
									<form:input path="name" type="text"
										class="form-control input-sm" id="name"
										placeholder="Enter Name" />
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-4" for="pwd"><b><spring:message
											code="label.email" /></b><span style="color: red">*</span> :</label>
								<div class="col-sm-8">
									<form:input path="email" type="email"
										class="form-control input-sm" id="email"
										placeholder="Enter Email" />
								</div>
							</div>


							<%-- <div class="form-group">
								<label class="control-label col-sm-4" id="DateDemo"><b><spring:message
											code="label.dob" /></b><span style="color: red">*</span> :</label>
								<div class="col-sm-8">
									<div id="sandbox-container">
										   <form:input path="dob" data-multiple-dates="0" data-multiple-dates-separator=", " data-position="top left" data-language="en" class="form-control input-sm datepicker-here" placeholder="Enter D.O.B" id="datepicker" />
		   <div id="date1Error" style="display: none; color: red;"><spring:message code="label.validation"/></div>
			<div id="date2Error" style="display: none; color: red;">To Date cannot be greater than today's date</div>
			<div id="date3Error" style="display: none; color: red;"><spring:message code="label.dateConfirmation"/></div>	
									</div>
								</div>
							</div>

 --%>

							<div class="form-group">
								<label class="control-label col-sm-4" for="pwd"><b><spring:message
											code="label.fathername" /></b><span style="color: red">*</span>
									:</label>
								<div class="col-sm-8">
									<form:input path="fatherName" class="form-control input-sm"
										placeholder="Enter Father's Name"
										value="${model.endUserForm.fatherName}" id="fatherName" />
								</div>
							</div>

						<%-- 	<div class="form-group">
								<label class="control-label col-sm-4" for="pwd"><b><spring:message
											code="label.address" /></b><span style="color: red">*</span> :</label>
								<div class="col-sm-8">
									<form:input path="address" type="address"
										class="form-control input-sm" id="address"
										placeholder="Enter Address" />
								</div>
							</div> --%>

						<%-- 	<div class="form-group">
								<label class="control-label col-sm-4" id="DateDemo"> <b><spring:message
											code="label.doj" /></b><span style="color: red">*</span>:
								</label>
								<div class="col-sm-8">
									<div id="sandbox-container">
										<form:input path="doj"  data-position="top left" data-language="en" class="form-control input-sm datepicker-here" placeholder="Enter D.O.J" id="datepicker1" />
								<span id="dateError" style="display: none; color: red;">
						<spring:message code="label.dateIsMandatory"/></span>
						<span id="dateError" style="display: none; color: red;">
						<spring:message code="label.dateIsMandatory"/></span> 
									</div>
								</div>
							</div> --%>
							<div class="form-group">
								<label class="control-label col-sm-4" for="pwd"><b><spring:message
											code="label.pan" /></b><span style="color: red">*</span> :</label>
								<div class="col-sm-8">
									<form:input path="pan" type="pan" class="form-control input-sm"
										id="pan" placeholder="Enter Pan"
										value="${model.endUserForm.pan}" readonly="true" />
								</div>
							</div>


							<div class="form-group">
								<label class="control-label col-sm-4" id="DateDemo"><b><spring:message
											code="label.gender" /></b><span style="color: red">*</span> :</label>
								<div class="col-sm-8">



									<form:select path="gender" class="form-control input-sm"
										id="gender">
										<form:option value="">
											<spring:message code="label.selectValue" />
										</form:option>
										<form:option value="male">
											<spring:message code="label.male" />
										</form:option>
										<form:option value="female">
											<spring:message code="label.female" />
										</form:option>
									</form:select>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-4" id="DateDemo"><b><spring:message
											code="label.localaddress" /></b><span style="color: red">*</span>
									:</label>
								<div class="col-sm-8">
									<div id="sandbox-container">
										<form:textarea path="localAddress"
											class="form-control input-sm" rows="5" id="localAddress"
											value="${model.endUserForm.localAddress}"></form:textarea>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-4" id="DateDemo"><b><spring:message
											code="label.permanentaddress" /></b><span style="color: red">*</span>
									:</label>
								<div class="col-sm-8">
									<div id="sandbox-container">
										<form:textarea path="permanentAddress"
											class="form-control input-sm" rows="5" id="permanentAddress"
											value="${model.endUserForm.permanentAddress}"></form:textarea>
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-sm-4" id="DateDemo"><b><spring:message
											code="label.contactNumber" /></b><span style="color: red">*</span>
									:</label>
								<div class="col-sm-8">
									<div id="sandbox-container">
										<form:input path="contactNo" type="text" id="contactNo"
											class="form-control input-sm" />
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>


		<div class="col-sm-6">
			<div class="modal-header">
				<h4>
					<b><spring:message code="label.salarydetails" /></b><span
						style="color: red">*</span>
				</h4>
			</div>
			<div class="add_emp_details_form">
				<div class="Emp_content">


					<div class="modal-body" style="width: 100%; margin: 0px auto;">
						<div class="form-horizontal" role="form">

							<div class="form-group">
								<label class="control-label col-sm-4" for="pwd"><b><spring:message
											code="label.fixedPackage" /></b><span style="color: red">*</span>:</label>
								<div class="col-sm-8">
									<form:input path="fixedPackage" type="text" name="txt"
										class="txt" id="fixedPackage"
										placeholder="Enter fixed Package"
										value="${model.endUserForm.fixedPackage}" />
								</div>
							</div>


	<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"></label>
						<div class="col-sm-8"> 
						 <label id="fixedPackageError" class="error" style="display:none"><b><spring:message code="label.validation"/></b><span style="color:red">*</span>:</label>
						</div>
						</div>


							<div class="form-group">
								<label class="control-label col-sm-4" for="pwd"></label>
								<div class="col-sm-8">
									<label id="fixedPackageError" class="error"
										style="display: none"><b><spring:message
												code="label.validation" /></b><span style="color: red">*</span>:</label>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-4" for="email"><b><spring:message
											code="label.variablePackage" /></b><span style="color: red">*</span>
									:</label>
								<div class="col-sm-8">
									<form:input path="variablePackage" type="text" name="txt"
										class="txt" id="variablePackage"
										value="${model.endUserForm.variablePackage}" />
								</div>
							</div>


							<div class="form-group">
								<label class="control-label col-sm-4" for="pwd"></label>
								<div class="col-sm-8">
									<label id="variablePackageError" class="error"
										style="display: none"><b><spring:message
												code="label.validation" /></b><span style="color: red">*</span>:</label>
								</div>
							</div>



							<div class="form-group">

								<label class="control-label col-sm-4" id="DateDemo"><b><spring:message
											code="label.totalpackage" /></b><span style="color: red">*</span>
									:</label>
								<div class="col-sm-8" id="sum">

									<div id="sandbox-container">

										<form:input path="totalPackage" type="text" name="txt"
											id="totalPackage" placeholder="Enter Total Package"
											value="${model.endUserForm.totalPackage}" class="txt" />

									</div>
								</div>

							</div>
						</div>
					</div>

				</div>
			</div>
		</div>




		<div class="col-sm-6" style="clear: both;">
			<div class="modal-header">
				<h4>
					<b><spring:message code="label.earningdetails" /></b><span
						style="color: red">*</span>
				</h4>
			</div>
			<div class="add_emp_details_form">
				<div class="Emp_content">


					<div class="modal-body" style="width: 100%; margin: 0px auto;">
						<div class="form-horizontal" role="form">
							<div class="form-group">
								<label class="control-label col-sm-4" for="email"><b><spring:message
											code="label.basicsalary" /></b><span style="color: red">*</span>:</label>
								<div class="col-sm-8">
									<form:input path="basicSalary" type="text" 
										class="txt1 form-control input-sm" id="basicSalary"
										placeholder="Enter Basic Salary"
										value="${model.endUserForm.basicSalary}" />
								</div>
							</div>
							
							<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"></label>
						<div class="col-sm-8"> 
						 <label id="basicSalaryError" class="error" style="display:none"><b><spring:message code="label.validation"/></b><span style="color:red">*</span>:</label>
						</div>
						</div>
					
							
							
							
							<div class="form-group">
								<label class="control-label col-sm-4" for="email"><b><spring:message
											code="label.hra" /></b><span style="color: red">*</span> :</label>
								<div class="col-sm-8">
									<form:input path="hra" type="hra" class="txt1 form-control input-sm"
										id="hra" placeholder="Enter HRA"
										value="${model.endUserForm.hra}" />
								</div>
							</div>
							
								<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"></label>
						<div class="col-sm-8"> 
						 <label id="hraError" class="error" style="display:none"><b><spring:message code="label.validation"/></b><span style="color:red">*</span>:</label>
						</div>
						</div>
							
							
							
							<div class="form-group">
								<label class="control-label col-sm-4" for="pwd"><b><spring:message
											code="label.conveyAllowance" /></b><span style="color: red">*</span>
									:</label>
								<div class="col-sm-8">
									<form:input path="conveyAllowance" type="conveyAllowance"
										class="txt1 form-control input-sm" id="conveyAllowance"
										placeholder="Enter medical Allowance"
										value="${model.endUserForm.conveyAllowance}" />
								</div>
							</div>


						<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"></label>
						<div class="col-sm-8"> 
						 <label id="conveyAllowanceError" class="error" style="display:none"><b><spring:message code="label.validation"/></b><span style="color:red">*</span>:</label>
						</div>
						</div>


							<div class="form-group">
								<label class="control-label col-sm-4" for="pwd"><b><spring:message
											code="label.medicalAllowance" /></b><span style="color: red">*</span>
									:</label>
								<div class="col-sm-8">
									<form:input path="medicalAllowance" type="medicalAllowance"
										class="txt1 form-control input-sm" id="medicalAllowance"
										placeholder="Enter medical Allowance"
										value="${model.endUserForm.medicalAllowance}" />
								</div>
							</div>
							
							
								<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"></label>
						<div class="col-sm-8"> 
						 <label id="medicalAllowanceError" class="error" style="display:none"><b><spring:message code="label.validation"/></b><span style="color:red">*</span>:</label>
						</div>
						</div>
					
							<div class="form-group">
								<label class="control-label col-sm-4" for="pwd"><b><spring:message
											code="label.specialAllowance" /></b><span style="color: red">*</span>
									:</label>
								<div class="col-sm-8">
									<form:input path="specialAllowance" type="specialAllowance"
										class="txt1 form-control input-sm" id="specialAllowance"
										placeholder="Enter special Allowance"
										value="${model.endUserForm.specialAllowance}" />
								</div>
							</div>
							
								
					<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"></label>
						<div class="col-sm-8"> 
						 <label id="specialAllowanceError" class="error" style="display:none"><b><spring:message code="label.validation"/></b><span style="color:red">*</span>:</label>
						</div>
						</div>
					
							
							
							<div class="form-group">
								<label class="control-label col-sm-4" id="DateDemo"><b><spring:message
											code="label.professionalTax" /></b><span style="color: red">*</span>
									:</label>
								<div class="col-sm-8">
									<div id="sandbox-container">
										<form:input path="professionalTax" type="text"
											class="txt1 form-control input-sm" id="professionalTax"
											placeholder="Enter professional Tax"
											value="${model.endUserForm.professionalTax}" />
									</div>
								</div>
								
									<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"></label>
						<div class="col-sm-8"> 
						 <label id="professionalTaxError" class="error" style="display:none"><b><spring:message code="label.validation"/></b><span style="color:red">*</span>:</label>
						</div>
						</div>
							 <div class="form-group" >
						<label class="control-label col-sm-4" id="DateDemo"><b><spring:message code="label.total" /></b>:</label>
						<div class="col-sm-8" id="sum1"> 
							<div id="sandbox-container">
							 <form:input path="total" type="text" class="txt1 form-control input-sm" name="txt1" placeholder="Enter Total"/>
							</div>
						</div>		 
							</div>
						</div>
					</div>

				</div>
			</div>

		</div>
</div>
		<div class="col-sm-6">
			<div class="modal-header">
				<h4>
					<b><spring:message code="label.deductions" /></b><span
						style="color: red">*</span>
				</h4>
			</div>
			<div class="add_emp_details_form">
				<div class="Emp_content">


					<div class="modal-body" style="width: 100%; margin: 0px auto;">
						<div class="form-horizontal" role="form">
							<div class="form-group">
								<label class="control-label col-sm-4" for="email"><b><spring:message
											code="label.foodamountdeduct" /></b><span style="color: red">*</span>:</label>
								<div class="col-sm-8">
									<form:input path="foodamountDeduct" type="text"
										class="txt2 form-control input-sm" id="foodamountDeduct"
										placeholder="Enter food amount Deduct"
										value="${model.endUserForm.foodamountDeduct}" />
								</div>
							</div>
							
								<div class="form-group">
						<label class="control-label col-sm-4" for="pwd"></label>
						<div class="col-sm-8"> 
						 <label id="foodamountDeductError" class="error" style="display:none"><b><spring:message code="label.validation"/></b><span style="color:red">*</span>:</label>
						</div>
						</div>
							
							
							<div class="form-group">
								<label class="control-label col-sm-4" for="pwd"><b><spring:message
											code="label.professionalTax" /></b><span style="color: red">*</span>
									:</label>
								<div class="col-sm-8">
									<form:input path="professionalTax" type="professionalTax"
										class="txt2 form-control input-sm" id="professionalTax1"
										placeholder="Enter professional Tax"
										value="${model.endUserForm.professionalTax}" />
								</div>
							</div>

	                    <div class="form-group">
						<label class="control-label col-sm-4" for="pwd"></label>
						<div class="col-sm-8"> 
						 <label id="professionalTaxError1" class="error" style="display:none"><b><spring:message code="label.validation"/></b><span style="color:red">*</span>:</label>
						</div>
						</div>
							<div class="form-group">
								<label class="control-label col-sm-4" id="DateDemo"><b><spring:message
											code="label.tds" /></b><span style="color: red">*</span> :</label>
								<div class="col-sm-8">
									
										<form:input path="tds" type="tds"
											class="txt2 form-control input-sm" id="tds"
											placeholder="Enter TDS" value="${model.endUserForm.tds}" />
									</div>
								</div>
								
						<div class="form-group" >
						<label class="control-label col-sm-4" id="DateDemo"><spring:message code="label.total" />:</label>
						<div class="col-sm-8" id="sum2"> 
							<div id="sandbox-container">
							 <form:input path="totaldeduct"  class="txt2 form-control input-sm " name="txt2" placeholder="Enter Total"/>
							</div>
						</div>	
								
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
		


		<div class="col-sm-12" style="margin-bottom:20px; ">
			<div class="text-center">
				<input type="submit" class="btn btn-primary btn-sm" value="Save"
					style="margin-top: 26px; width: 14%;" /> <a
					href="managerAddEmployeeDetails" class="btn btn-primary btn-sm"
					style="margin-top: 26px; width: 14%;"><b><spring:message
							code="label.back"></spring:message></b></a>
			</div>

		</div>
	</form:form></div>
	<script>
	$('ul.nav li.dropdown').hover(
			function() {
				$(this).find('.dropdown-menu').stop(true, true).delay(200)
						.fadeIn(500);
			},
			function() {
				$(this).find('.dropdown-menu').stop(true, true).delay(200)
						.fadeOut(500);
			});
</script>
<script>
	$(document).ready(function() {
		var readURL = function(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();

				reader.onload = function(e) {
					$('.profile-pic').attr('src', e.target.result);
				}

				reader.readAsDataURL(input.files[0]);
			}
		}

		$(".file-upload").on('change', function() {
			readURL(this);
		});

		$(".upload-button").on('click', function() {
			$(".file-upload").click();
		});
	});
</script>
<script>
	$('#datepicker-1').datepicker();
</script>
