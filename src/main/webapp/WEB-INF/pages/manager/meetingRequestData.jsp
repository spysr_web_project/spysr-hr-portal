<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!--  datepicker css js links -->
<link
	href="<%=request.getContextPath()%>/resources/css/datepicker.min.css"
	rel="stylesheet">
<script src="<%=request.getContextPath()%>/resources/js/jquery.min.js"></script>
<script src="<%=request.getContextPath()%>/resources/js/datepicker.js"></script>

<script
	src="<%=request.getContextPath()%>/resources/js/datepicker.en.js"></script>
<!--  End datepicker css js links -->


<script>
$(function() {
  	$( "#datepicker" ).datepicker({format: 'dd/mm/yyyy'});
  });
  var today = new Date();
  today.setHours(0,0,0,0);
	function val() {

		var meetingDate = document.getElementById('datepicker').value;
		
		from = meetingDate.split("/");
		var date1 = new Date(from[2], from[1] - 1, from[0]);
	
		
        var name=document.getElementById('name').value;
       
		var time = document.getElementById('time').value;

		var description = document.getElementById('description').value;

		
		var canSubmit = true;
		
		
		
		if (document.getElementById('name').checked == false) {
			alert("Please select employees")
			return false;
		} else {
			document.getElementById('name').style.borderColor = "green";
		}

	/* 	if (meetingDate == '') {
			document.getElementById('meetingDate').style.borderColor = "red";
			return false;
		} else {
			document.getElementById('meetingDate').style.borderColor = "green";
		} */
		if (document.getElementById('datepicker').value == '') {
			document.getElementById('dateError').style.display = 'block';
			canSubmit = false;
		} else {
			document.getElementById('dateError').style.display = 'none';
		}
		
		if(date1 < today) {
			document.getElementById('datepicker').style.borderColor = "red";
		return false;
		} else {
			document.getElementById('datepicker').style.borderColor = "green";
		
		}
		if (time == '') {
			document.getElementById('time').style.borderColor = "red";
			return false;
		} else {
			document.getElementById('time').style.borderColor = "green";
		}

		if (description == '') {
			document.getElementById('description').style.borderColor = "red";
			return false;
		} else {
			document.getElementById('description').style.borderColor = "green";
		}

		if (canSubmit == false) {
			return false;
		}
	}
</script>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

	<form:form method="POST" action="submitForm" commandName="endUserForm"
		onsubmit="return val();">

		<div class="col-sm-12">
			<div class="add_emp_details">
				<table class="table table-bordered table-striped" id="tb1"
					data-rt-breakpoint="600">
					<thead>
						<tr>


							<th scope="col" data-rt-column="Status"><spring:message
									code="label.employees" /></th>
					</thead>
					<tbody class="searchable">
						<c:if test="${! empty empList}">
							<c:forEach items="${empList}" var="notification1">
								<tr>



									<td><form:checkbox path="email" id="name"
											items="${notification}" value="${notification1.email}" /> <c:out
											value="${notification1.name}"></c:out></td>

								</tr>

							</c:forEach>

						</c:if>
					</tbody>
				</table>




<div class="form-horizontal">
		<div class="form-group">
			<label class="control-label col-sm-4" id="DateDemo"><b><spring:message
						code="label.meetingdate" /></b><span style="color: red">*</span> :</label>
			<div class="col-sm-8">
				<div id="sandbox-container">
					<form:input path="meetingDate" data-multiple-dates="0" data-multiple-dates-separator=", " data-position="top left" data-language="en" class="form-control input-sm datepicker-here" placeholder="Enter Meeting  Date" id="datepicker" />
				<span id="dateError" style="display: none; color: red;">
						<spring:message code="label.dateIsMandatory"/></span>
						<span id="dateError" style="display: none; color: red;">
						<spring:message code="label.dateIsMandatory"/></span> 		
				</div>
				
			</div>
		</div>

		<div class="form-group">
			<label class="control-label col-sm-4" for="pwd"><b><spring:message
						code="label.time" /></b><span style="color: red">*</span> :</label>
			<div class="col-sm-8">
				<form:input path="time" class="form-control input-sm"
					placeholder="Enter Time" id="time" />
			</div>
		</div>


		<div class="form-group">
			<label class="control-label col-sm-4" for="pwd"><b><spring:message
						code="label.description" /></b><span style="color: red">*</span> :</label>
			<div class="col-sm-8">
				<form:textarea path="description" class="form-control input-sm"
					placeholder="Enter Description" id="description" />
			</div>
		</div>

			
			
			</div>
			<div class="col-sm-offset-4 col-sm-5">
				<input type="submit" class="btn btn-primary btn-md" value="Send"
					style="margin-top: 26px; width: 18%;" /> <a
					href="/spysr-hr-portal/managers/manager" class="btn btn-primary btn-md"
					style="margin-top: 26px; width: 18%;"><b><spring:message
							code="label.back"></spring:message></b></a>
		
			</div>
</div>
</form:form>
</div>
</div>