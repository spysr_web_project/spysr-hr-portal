INSERT INTO hrdbv1.end_user(
   id
  ,accessStatus
  ,accountNo
  ,addbyAdmin
  ,addbyManager
  ,address
  ,basicSalary
  ,bname
  ,branch
  ,comment
  ,contactNo
  ,conveyAllowance
  ,createdby
  ,currentRole
  ,department
  ,description
  ,designation
  ,displayName
  ,dob
  ,doj
  ,eligibleLeave
  ,email
  ,fatherName
  ,fixedPackage
  ,flag
  ,foodamountDeduct
  ,gender
  ,hra
  ,image
  ,imageName
  ,localAddress
  ,lop
  ,managerName
  ,medicalAllowance
  ,meetingDate
  ,name
  ,pan
  ,password
  ,passwordFlag
  ,permanentAddress
  ,prefferedLanguage
  ,professionalTax
  ,reason
  ,role
  ,roleType
  ,specialAllowance
  ,status
  ,tds
  ,theme
  ,`time`
  ,total
  ,totalLeave
  ,totalPack
  ,totalPackage
  ,totaldeduct
  ,userName
  ,variablePackage
) VALUES (
   1 -- id - IN bigint(20)
  ,'1'  -- accessStatus - IN varchar(255)
  ,'1212'  -- accountNo - IN varchar(255)
  ,'Yes'  -- addbyAdmin - IN varchar(255)
  ,'Yes'  -- addbyManager - IN varchar(255)
  ,'delhi'  -- address - IN varchar(255)
  ,2000   -- basicSalary - IN float
  ,'Delhi'  -- bname - IN varchar(255)
  ,'Delhi'  -- branch - IN varchar(255)
  ,'added by me'  -- comment - IN varchar(255)
  ,'9999921219'  -- contactNo - IN varchar(255)
  ,0   -- conveyAllowance - IN float
  ,'yogesh'  -- createdby - IN varchar(255)
  ,'ROLE_ADMIN'  -- currentRole - IN varchar(255)
  ,'CS'  -- department - IN varchar(255)
  ,'CS'  -- description - IN varchar(255)
  ,'Manager'  -- designation - IN varchar(255)
  ,'Ramesh'  -- displayName - IN varchar(255)
  ,'2017-10-01'  -- dob - IN datetime
  ,'2017-10-01'  -- doj - IN datetime
  ,0   -- eligibleLeave - IN int(11)
  ,'yogesh@spysr.in'  -- email - IN varchar(255)
  ,'abc'  -- fatherName - IN varchar(255)
  ,0   -- fixedPackage - IN float
  ,0   -- flag - IN int(11)
  ,0   -- foodamountDeduct - IN float
  ,'Male'  -- gender - IN varchar(255)
  ,0   -- hra - IN float
  ,0   -- image - IN tinyblob
  ,''  -- imageName - IN varchar(255)
  ,''  -- localAddress - IN varchar(255)
  ,0   -- lop - IN int(11)
  ,''  -- managerName - IN varchar(255)
  ,0   -- medicalAllowance - IN float
  ,'2017-10-01'  -- meetingDate - IN datetime
  ,''  -- name - IN varchar(255)
  ,''  -- pan - IN varchar(255)
  ,'password'  -- password - IN varchar(255)
  ,1   -- passwordFlag - IN int(11)
  ,'delhi'  -- permanentAddress - IN varchar(255)
  ,'en'  -- prefferedLanguage - IN varchar(255)
  ,0   -- professionalTax - IN float
  ,''  -- reason - IN varchar(255)
  ,1   -- role - IN int(11)
  ,'ADMIN'  -- roleType - IN varchar(255)
  ,0   -- specialAllowance - IN float
  ,'ACTIVE'  -- status - IN varchar(255)
  ,0   -- tds - IN float
  ,'themeBlue'  -- theme - IN varchar(255)
  ,''  -- time - IN varchar(255)
  ,0   -- total - IN float
  ,0   -- totalLeave - IN int(11)
  ,0   -- totalPack - IN float
  ,0   -- totalPackage - IN float
  ,0   -- totaldeduct - IN float
  ,'yogesh@spysr.in'  -- userName - IN varchar(255)
  ,0   -- variablePackage - IN float
)
