package com.spysr.hr.portal.utility;

public class Constants {

	public static final float busPass=2250;
	public static final float veg=74;
	public static final float noVeg=94;
	public static final String DRIVE = "E:\\";
	public static final String BLOCK = "Block";
	public static final String APPROVED = "Approved";
	public static final String REJECTED = "Rejected";
	public static final String BLOCKED = "Blocked";
	public static final String UNBLOCK = "Unblock";
	public static final String UNBLOCKED = "Unblocked";
	public static final String PENDING = "Pending";
	public static final String DONE = "Done";
	public static final String RENEWED = "Renewed";
	public static final String CLOSE = "Close";
	public static final String CANCEL = "Cancel";
	
	public static final String SUCCESS = "success";
	public static final String ERROR = "error";
	public static final String ROLE = "User Name already Exists";
	public static final String SAVEDROLE = "Saved Successfully";
	public static final String ROLEADDED = "Role status saved successfully";
	public static final String MODULEROLE = "Role";
	public static final String NOTIFICATION = "Notification Sent";
	public static final String TIMESHEET = "Time sheet already submitted";
	public static final String FUTUREDATE = "You Cannot Select future date";
	public static final String NORECORDS = "No records found";
	public static final String ADMINMAIl = "navya.nara89@gmail.com";
	public static final String RECORDEXISTING="Record Already Existing";
	public static final String INVALID = "Invalid User Name and Email";
    public static final String DATEINVALID = "Past date cannot be selected";
    
    
}
