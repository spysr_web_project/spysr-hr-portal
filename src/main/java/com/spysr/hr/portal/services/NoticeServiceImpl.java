/**
 * 
 */
package com.spysr.hr.portal.services;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailParseException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.spysr.hr.portal.dao.EndUserDAO;
import com.spysr.hr.portal.dao.NoticeDetailsDAO;
import com.spysr.hr.portal.entity.EndUser;
import com.spysr.hr.portal.entity.NoticeDetails;
import com.spysr.hr.portal.entity.ProjectDetails;
import com.spysr.hr.portal.form.NoticeDetailsForm;
import com.spysr.hr.portal.form.ProjectDetailsForm;



/**
 * @author Sudhir
 *
 */
@Service
public class NoticeServiceImpl implements NoticeService {

	@Autowired
	NoticeDetailsDAO  noticeDetailsDAO;
	
	@Autowired
	NoticeDetailsForm  noticeDetailsForm;
	
	@Autowired
	EndUserDAO endUserDAO;
	
	@Autowired
	JavaMailSender mailSender;
	
	
	
	public void Save(NoticeDetailsForm  noticeDetailsForm) {
		
		   
		NoticeDetails noticeDetails=new NoticeDetails();
		
	   noticeDetails.setId(noticeDetailsForm.getId());
	  noticeDetails.setSerialNo(noticeDetailsForm.getSerialNo());
	   noticeDetails.setNoticeDate(noticeDetailsForm.getNoticeDate());
	   noticeDetails.setDescription(noticeDetailsForm.getDescription());
	   noticeDetails.setTitle(noticeDetailsForm.getTitle());
	   noticeDetailsDAO.createNotice(noticeDetails);
	  /* mail(noticeDetailsForm);*/
	}
	
	
	public void Update(NoticeDetailsForm noticeDetailsForm) {

		NoticeDetails noticeDetails=new NoticeDetails();

		    noticeDetails.setId(noticeDetailsForm.getId());
		   noticeDetails.setSerialNo(noticeDetailsForm.getSerialNo());
		   noticeDetails.setNoticeDate(noticeDetailsForm.getNoticeDate());
		   noticeDetails.setDescription(noticeDetailsForm.getDescription());
		   noticeDetails.setTitle(noticeDetailsForm.getTitle());
		   noticeDetailsDAO.Update(noticeDetails);
		  /* mail(noticeDetailsForm);*/
	}
	
	
	
	public void mail(NoticeDetailsForm  noticeDetailsForm) {
		
		
		Collection<EndUser> endUsers = endUserDAO.getByEmpList().getResultList();
		if (endUsers != null && endUsers.size() > 0) {
			for (EndUser endUser : endUsers) {
		
	
		
		
		String title=noticeDetailsForm.getTitle();
		String Description=noticeDetailsForm.getDescription();
		Date noticeDate=noticeDetailsForm.getNoticeDate();
		MimeMessage message = mailSender.createMimeMessage();

		try {
			MimeMessageHelper helper = new MimeMessageHelper(message, true);

			helper.setTo(endUser.getEmail());
			helper.setSubject(title);
			helper.setText(String.format(endUser.getName() + ",\n\n Hi,New Notice Details From SpYsR HR  On " + noticeDate.toString() + "\n\n\n"+Description
					+ "\n\n\nAdmin\nSpYsR HR"));
     
		} catch (MessagingException e) {
			throw new MailParseException(e);
		}
		mailSender.send(message);
	     
	}
			
		}
	}
}
	
	