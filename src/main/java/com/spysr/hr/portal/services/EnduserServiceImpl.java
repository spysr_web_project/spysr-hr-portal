package com.spysr.hr.portal.services;

import java.text.Format;
import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spysr.hr.portal.dao.EndUserDAO;
import com.spysr.hr.portal.entity.EndUser;
import com.spysr.hr.portal.form.EndUserForm;

@Service
public class EnduserServiceImpl implements EnduserService {

	@Autowired
	EndUserDAO endUserDAO;

	public void Save(EndUserForm endUserForm) {
		EndUser endUser = new EndUser();
		try {
			endUser.setImage(endUserForm.getFile().getBytes());
			endUser.setImageName(endUserForm.getFile().getOriginalFilename());
		} catch (Exception e) {
			e.getMessage();
		}
		endUser.setEmail(endUserForm.getEmail());
		endUser.setContactNo(endUserForm.getContactNo());
		if (endUserForm.getCurrentRole().equals("ROLE_EMP")) {
			endUser.setRole(3);
		} else if(endUserForm.getCurrentRole().equals("ROLE_MANAGER")){
			endUser.setRole(2);
		}
		else
		{
			endUser.setRole(4);
		}

		endUser.setUserName(endUserForm.getUserName());
		endUser.setCurrentRole(endUserForm.getCurrentRole());
		endUser.setAccountNo(endUserForm.getAccountNo());
		endUser.setAddress(endUserForm.getAddress());
        endUser.setBasicSalary(endUserForm.getBasicSalary());
		endUser.setBname(endUserForm.getBname());
		endUser.setBranch(endUserForm.getBranch());
		endUser.setConveyAllowance(endUserForm.getConveyAllowance());
		endUser.setDepartment(endUserForm.getDepartment());
		endUser.setDesignation(endUserForm.getDesignation());
		endUser.setDisplayName(endUserForm.getUserName());
		endUser.setDob(endUserForm.getDob());
		endUser.setDoj(endUserForm.getDoj());
		endUser.setAccessStatus("Unblocked");
       
		// Leave Logic
		Format formatter = new SimpleDateFormat("MMMM");
		String s = formatter.format(endUserForm.getDoj());

		Format formatter1 = new SimpleDateFormat("d");
		String s1 = formatter1.format(endUserForm.getDoj());


		/*if (s.equals("April")) {
			int day = Integer.parseInt(s1);

			if (day < 15) {
				endUser.setEligibleLeave(2);
				endUser.setTotalLeave(22);

			} else if (day >= 15 && day < 25) {
				endUser.setEligibleLeave(21);
				endUser.setTotalLeave(14);
			}
			else{
				endUser.setEligibleLeave(0);
				endUser.setTotalLeave(20);
			}
			
		}
			

		if (s.equals("May")) {
			int day = Integer.parseInt(s1);

			if (day < 15) {
				endUser.setEligibleLeave(2);
				endUser.setTotalLeave(20);

			} else if (day >= 15 && day < 25) {
				endUser.setEligibleLeave(1);
				endUser.setTotalLeave(19);
			}
			else{
				endUser.setEligibleLeave(0);
				endUser.setTotalLeave(18);
			}
			
		}

		if (s.equals("June")) {
			int day = Integer.parseInt(s1);

			if (day < 15) {
				endUser.setEligibleLeave(2);
				endUser.setTotalLeave(18);

			} else if (day >= 15 && day < 25) {
				endUser.setEligibleLeave(1);
				endUser.setTotalLeave(17);
			}
			else{
				endUser.setEligibleLeave(0);
				endUser.setTotalLeave(16);
			}
			
		}
		if (s.equals("July")) {
			int day = Integer.parseInt(s1);

			if (day < 15) {
				endUser.setEligibleLeave(2);
				endUser.setTotalLeave(16);

			} else if (day >= 15 && day < 25) {
				endUser.setEligibleLeave(1);
				endUser.setTotalLeave(15);
			}
			else{
				endUser.setEligibleLeave(0);
				endUser.setTotalLeave(14);
			}
			
		}
		if (s.equals("August")) {
			int day = Integer.parseInt(s1);

			if (day < 15) {
				endUser.setEligibleLeave(2);
				endUser.setTotalLeave(14);

			} else if (day >= 15 && day < 25) {
				endUser.setEligibleLeave(1);
				endUser.setTotalLeave(13);
			}
			else{
				endUser.setEligibleLeave(0);
				endUser.setTotalLeave(12);
			}
			
		}
		if (s.equals("September")) {
			int day = Integer.parseInt(s1);

			if (day < 15) {
				endUser.setEligibleLeave(2);
				endUser.setTotalLeave(12);

			} else if (day >=15 && day < 25) {
				endUser.setEligibleLeave(1);
				endUser.setTotalLeave(11);
			}
			else{
				endUser.setEligibleLeave(0);
				endUser.setTotalLeave(10);
			}
			
		}

		if (s.equals("October")) {
			int day = Integer.parseInt(s1);

			if (day < 15) {
				endUser.setEligibleLeave(2);
				endUser.setTotalLeave(10);

			} else if (day >=15 && day < 25) {
				endUser.setEligibleLeave(1);
				endUser.setTotalLeave(9);
			}
			else{
				endUser.setEligibleLeave(0);
				endUser.setTotalLeave(8);
			}
			
		}

		if (s.equals("November")) {
			int day = Integer.parseInt(s1);

			if (day < 15) {
				endUser.setEligibleLeave(2);
				endUser.setTotalLeave(9);

			} else if (day >= 15 && day < 25) {
				endUser.setEligibleLeave(1);
				endUser.setTotalLeave(8);
			}
			else{
				endUser.setEligibleLeave(0);
				endUser.setTotalLeave(7);
			}
			
		}

		if (s.equals("December")) {
			int day = Integer.parseInt(s1);

			if (day < 15) {
				endUser.setEligibleLeave(2);
				endUser.setTotalLeave(7);

			} else if (day >= 15 && day < 25) {
				endUser.setEligibleLeave(1);
				endUser.setTotalLeave(6);
			}
			else{
				endUser.setEligibleLeave(0);
				endUser.setTotalLeave(5);
			}
			
		}
		
		if (s.equals("January")) {
			int day = Integer.parseInt(s1);

			if (day < 15) {
				endUser.setEligibleLeave(2);
				endUser.setTotalLeave(5);

			} else if (day >= 15 && day < 25) {
				endUser.setEligibleLeave(1);
				endUser.setTotalLeave(4);
			}
			else{
				endUser.setEligibleLeave(0);
				endUser.setTotalLeave(3);
			}
			
		}
		
		if (s.equals("February")) {
			int day = Integer.parseInt(s1);

			if (day < 15) {
				endUser.setEligibleLeave(2);
				endUser.setTotalLeave(3);

			} else if (day >= 15 && day < 25) {
				endUser.setEligibleLeave(1);
				endUser.setTotalLeave(2);
			}
			else{
				endUser.setEligibleLeave(0);
				endUser.setTotalLeave(1);
			}
			
		}
		
		if (s.equals("March")) {
			int day = Integer.parseInt(s1);

			if (day < 15) {
				endUser.setEligibleLeave(2);
				endUser.setTotalLeave(2);

			} else if (day >= 15 && day < 25) {
				endUser.setEligibleLeave(1);
				endUser.setTotalLeave(1);
			}
			else{
				endUser.setEligibleLeave(0);
				endUser.setTotalLeave(0);
			}
			
		}
		*/
		
endUser.setEligibleLeave(endUserForm.getEligibleLeave());
endUser.setTotalLeave(endUserForm.getTotalLeave());
		endUser.setFatherName(endUserForm.getFatherName());
		endUser.setFixedPackage(endUserForm.getFixedPackage());
		endUser.setFoodamountDeduct(endUserForm.getFoodamountDeduct());

		endUser.setLocalAddress(endUserForm.getLocalAddress());
		endUser.setPermanentAddress(endUserForm.getPermanentAddress());

		endUser.setGender(endUserForm.getGender());
		endUser.setHra(endUserForm.getHra());

		endUser.setMedicalAllowance(endUserForm.getMedicalAllowance());
		endUser.setSpecialAllowance(endUserForm.getSpecialAllowance());
		endUser.setName(endUserForm.getName());
		

		/*
		 * try { endUser.setEmployee(endUserForm.getFile().getBytes()); } catch
		 * (IOException e) {
		 * 
		 * e.printStackTrace(); }
		 * endUser.setEmployeeimageName(endUserForm.getFile().
		 * getOriginalFilename());
		 */

		endUser.setPan(endUserForm.getPan());
		endUser.setProfessionalTax(endUserForm.getProfessionalTax());
		endUser.setLop(0);
		endUser.setTds(endUserForm.getTds());
		endUser.setTotalPackage(endUserForm.getTotalPackage());
		
		endUser.setTotalPack(endUserForm.getTotalPack());
		
		endUser.setTotaldeduct(endUserForm.getTotaldeduct());
		
		endUser.setPrefferedLanguage("en");
		endUser.setTheme("themeBlue");
		
		endUser.setStatus("Pending");
		if (endUserForm.getCreatedby().equals("ROLE_MANAGER")) {
			endUser.setCreatedby("MANAGER");
		} else {
			endUser.setCreatedby("ADMIN");
		}
		endUser.setAddbyManager(endUserForm.getAddbyManager());
		endUser.setPassword(endUserForm.getPassword());
		endUser.setVariablePackage(endUserForm.getVariablePackage());

		endUser.setPasswordFlag(0);

		endUserDAO.createUser(endUser);

	}



	public void Update(EndUserForm endUserForm) {
		EndUser endUser = new EndUser();

		endUser.setId(endUserForm.getId());
		endUser.setEmail(endUserForm.getEmail());
		endUser.setUserName(endUserForm.getUserName());
		endUser.setLocalAddress(endUserForm.getLocalAddress());
		endUser.setPermanentAddress(endUserForm.getPermanentAddress());
		endUser.setDob(endUserForm.getDob());
		endUser.setDoj(endUserForm.getDoj());
		endUser.setContactNo(endUserForm.getContactNo());
		endUser.setCurrentRole(endUserForm.getCurrentRole());
		endUser.setFatherName(endUserForm.getFatherName());
		endUser.setAccountNo(endUserForm.getAccountNo());
		endUser.setAddress(endUserForm.getAddress());
	    endUser.setAddbyManager(endUserForm.getUserName());
		endUser.setBasicSalary(endUserForm.getBasicSalary());
		endUser.setBname(endUserForm.getBname());
		endUser.setBranch(endUserForm.getBranch());
		endUser.setConveyAllowance(endUserForm.getConveyAllowance());
		endUser.setDepartment(endUserForm.getDepartment());
		endUser.setDesignation(endUserForm.getDesignation());
		endUser.setDisplayName(endUserForm.getUserName());
		endUser.setEmail(endUserForm.getEmail());
		endUser.setFixedPackage(endUserForm.getFixedPackage());
		endUser.setFoodamountDeduct(endUserForm.getFoodamountDeduct());
		endUser.setLop(endUserForm.getLop());
		endUser.setGender(endUserForm.getGender());
		endUser.setHra(endUserForm.getHra());
		endUser.setCreatedby(endUserForm.getCreatedby());
		endUser.setMedicalAllowance(endUserForm.getMedicalAllowance());
		endUser.setName(endUserForm.getName());
        endUser.setEligibleLeave(endUserForm.getEligibleLeave());
        endUser.setTotalLeave(endUserForm.getTotalLeave());
		endUser.setSpecialAllowance(endUserForm.getSpecialAllowance());
		endUser.setProfessionalTax(endUserForm.getProfessionalTax());
		endUser.setPan(endUserForm.getPan());
		endUser.setRole(endUserForm.getRole());
		try {
			endUser.setImage(endUserForm.getFile().getBytes());
			endUser.setImageName(endUserForm.getFile().getOriginalFilename());
		} catch (Exception e) {
			e.getMessage();
		}
		endUser.setTds(endUserForm.getTds());
		endUser.setTotalPackage(endUserForm.getTotalPackage());
		endUser.setTotalPack(endUserForm.getTotalPack());
		endUser.setTotaldeduct(endUserForm.getTotaldeduct());
		endUser.setStatus(endUserForm.getStatus());
		endUser.setComment(endUserForm.getComment());
		endUser.setPrefferedLanguage("en");
		endUser.setTheme("themeBlue");
		endUser.setPasswordFlag(0);

		endUser.setPassword(endUserForm.getPassword());
		endUser.setVariablePackage(endUserForm.getVariablePackage());

		endUserDAO.update(endUser);

	}

}
