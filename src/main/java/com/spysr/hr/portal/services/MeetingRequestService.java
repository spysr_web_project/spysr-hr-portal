/**
 * 
 */
package com.spysr.hr.portal.services;

import com.spysr.hr.portal.form.MeetingRequestForm;

/**
 * @author Sudhir
 *
 */
public interface MeetingRequestService {
	
	public void Save(MeetingRequestForm meetingRequestForm);
}
