package com.spysr.hr.portal.services;

import com.spysr.hr.portal.form.JobOffersForm;

/**
 * @author spysr_dev
 *
 */
public interface JobOffersService {
	
	/**
	 * Method for adding job offers details
	 * @param jobOffersForm
	 */
	public void addDetails(JobOffersForm jobOffersForm);

}
