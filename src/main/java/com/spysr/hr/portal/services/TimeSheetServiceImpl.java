package com.spysr.hr.portal.services;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailParseException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import com.spysr.hr.portal.dao.EndUserDAO;
import com.spysr.hr.portal.dao.FoodBillDAO;
import com.spysr.hr.portal.dao.TimeSheetDAO;
import com.spysr.hr.portal.entity.EndUser;
import com.spysr.hr.portal.entity.FoodBill;
import com.spysr.hr.portal.entity.TimeSheet;
import com.spysr.hr.portal.form.FoodBillForm;
import com.spysr.hr.portal.form.TimeSheetForm;
import com.spysr.hr.portal.utility.Constants;

/**
 * @author spysr_dev
 *
 */
@Service
public class TimeSheetServiceImpl implements TimeSheetService {

	@Autowired
	TimeSheetDAO timeSheetDAO;
	
	@Autowired
	TimeSheetForm timeSheetForm;
	
	@Autowired
	JavaMailSender mailSender;
	
	@Autowired
	EndUserDAO endUserDAO;
	
	@Autowired
	FoodBillDAO foodBillDAO;

	public void add(TimeSheetForm timeSheetForm) {
		
		TimeSheet timeSheet = new TimeSheet();
		
		float zero = 0;
		
		timeSheet.setRole(timeSheetForm.getRole());
		timeSheet.setUserName(timeSheetForm.getUserName());
		timeSheet.setName(timeSheetForm.getName());
		timeSheet.setEmail(timeSheetForm.getEmail());
		timeSheet.setMonday(timeSheetForm.getMonday());
		timeSheet.setMondayStatus(timeSheetForm.getMondayStatus());
		timeSheet.setMondayWorkDescription(timeSheetForm.getMondayWorkDescription());
		timeSheet.setTuesday(timeSheetForm.getTuesday());
		timeSheet.setTuesdayStatus(timeSheetForm.getTuesdayStatus());
		timeSheet.setTuesdayWorkDescription(timeSheetForm.getTuesdayWorkDescription());
		timeSheet.setWednesday(timeSheetForm.getWednesday());
		timeSheet.setWednesdayStatus(timeSheetForm.getWednesdayStatus());
		timeSheet.setWednesdayWorkDescription(timeSheetForm.getWednesdayWorkDescription());
		timeSheet.setThursday(timeSheetForm.getThursday());
		timeSheet.setThursdayStatus(timeSheetForm.getThursdayStatus());
		timeSheet.setThursdayWorkDescription(timeSheetForm.getThursdayWorkDescription());
		timeSheet.setFriday(timeSheetForm.getFriday());
		timeSheet.setFridayStatus(timeSheetForm.getFridayStatus());
		timeSheet.setFridayWorkDescription(timeSheetForm.getFridayWorkDescription());
		timeSheet.setStatus(Constants.PENDING);
		
		if (timeSheetForm.getMondayStatus().equals("Working")) {
			timeSheet.setMondayBill(Constants.veg);
		} else {
			timeSheet.setMondayBill(zero);
		}
		if (timeSheetForm.getTuesdayStatus().equals("Working")) {
			timeSheet.setTuesdayBill(Constants.veg);
		} else {
			timeSheet.setTuesdayBill(zero);
		}
		if (timeSheetForm.getWednesdayStatus().equals("Working")) {
			timeSheet.setWednesdayBill(Constants.veg);
		} else {
			timeSheet.setWednesdayBill(zero);
		}
		if (timeSheetForm.getThursdayStatus().equals("Working")) {
			timeSheet.setThursdayBill(Constants.veg);
		} else {
			timeSheet.setThursdayBill(zero);
		}
		if (timeSheetForm.getFridayStatus().equals("Working")) {
			timeSheet.setFridayBill(Constants.noVeg);
		} else {
			timeSheet.setFridayBill(zero);
		}
		
		timeSheetDAO.add(timeSheet);
	}

	@Override
	public TimeSheetForm getById(Long Id) {
		
		TimeSheet timeSheet = timeSheetDAO.getById(Id);
		
		timeSheetForm.setId(timeSheet.getId());
		timeSheetForm.setName(timeSheet.getName());
		timeSheetForm.setUserName(timeSheet.getUserName());
		timeSheetForm.setEmail(timeSheet.getEmail());
		timeSheetForm.setRole(timeSheet.getRole());
		timeSheetForm.setMonday(timeSheet.getMonday());
		timeSheetForm.setMondayStatus(timeSheet.getMondayStatus());
		timeSheetForm.setMondayWorkDescription(timeSheet.getMondayWorkDescription());
		timeSheetForm.setTuesday(timeSheet.getTuesday());
		timeSheetForm.setTuesdayStatus(timeSheet.getTuesdayStatus());
		timeSheetForm.setTuesdayWorkDescription(timeSheet.getTuesdayWorkDescription());
		timeSheetForm.setWednesday(timeSheet.getWednesday());
		timeSheetForm.setWednesdayStatus(timeSheet.getWednesdayStatus());
		timeSheetForm.setWednesdayWorkDescription(timeSheet.getWednesdayWorkDescription());
		timeSheetForm.setThursday(timeSheet.getThursday());
		timeSheetForm.setThursdayStatus(timeSheet.getThursdayStatus());
		timeSheetForm.setThursdayWorkDescription(timeSheet.getThursdayWorkDescription());
		timeSheetForm.setFriday(timeSheet.getFriday());
		timeSheetForm.setFridayStatus(timeSheet.getFridayStatus());
		timeSheetForm.setFridayWorkDescription(timeSheet.getFridayWorkDescription());
		timeSheetForm.setStatus(timeSheet.getStatus());
		timeSheetForm.setMondayBill(timeSheet.getMondayBill());
		timeSheetForm.setTuesdayBill(timeSheet.getTuesdayBill());
		timeSheetForm.setWednesdayBill(timeSheet.getWednesdayBill());
		timeSheetForm.setThursdayBill(timeSheet.getThursdayBill());
		timeSheetForm.setFridayBill(timeSheet.getFridayBill());
		return timeSheetForm;
	}

	public float getWeekData(Long Id) {
		
		TimeSheet timeSheet = timeSheetDAO.getById(Id);
		
		float weekBill = timeSheet.getMondayBill() + timeSheet.getTuesdayBill()+timeSheet.getWednesdayBill()+timeSheet.getThursdayBill()+timeSheet.getFridayBill();
		
		return weekBill;
	}
	
	
	public void updateByAdmin(TimeSheetForm timeSheetForm) {
		TimeSheet timeSheet = timeSheetDAO.getById(timeSheetForm.getId());		
		
		timeSheet.setStatus(timeSheetForm.getStatus());
		if(timeSheetForm.getStatus().equals(Constants.APPROVED))
		{
			MimeMessage message = mailSender.createMimeMessage();

			try {
				MimeMessageHelper helper = new MimeMessageHelper(message, true);

				helper.setTo(timeSheetForm.getEmail());
				helper.setSubject("Approval Notification!!!!");
				helper.setText(String.format(timeSheetForm.getName() + ",\n\n Your time sheet has been approved. " 
						+ "\n\n\nPartho H. Chakraborty\nSpYsR HR"));

			} catch (MessagingException e) {
				throw new MailParseException(e);
			}
			mailSender.send(message);
		}
		else
		{
			MimeMessage message = mailSender.createMimeMessage();

			try {
				MimeMessageHelper helper = new MimeMessageHelper(message, true);

				helper.setTo(timeSheetForm.getEmail());
				helper.setSubject("Rejection Notification!!!!");
				helper.setText(String.format(timeSheetForm.getName() + ",\n\nYour time sheet has been rejected. \n\n Check your portal." 
						+ "\n\n\nPartho H. Chakraborty\nSpYsR HR"));
				

			} catch (MessagingException e) {
				throw new MailParseException(e);
			}
			mailSender.send(message);
		}
		timeSheet.setComment(timeSheetForm.getComment());
		timeSheetDAO.update(timeSheet);
	}

	
	public void update(TimeSheetForm timeSheetForm) {
		TimeSheet timeSheet = timeSheetDAO.getById(timeSheetForm.getId());
		
		timeSheet.setMondayStatus(timeSheetForm.getMondayStatus());
		timeSheet.setMondayWorkDescription(timeSheetForm.getMondayWorkDescription());
		timeSheet.setTuesdayStatus(timeSheetForm.getTuesdayStatus());
		timeSheet.setTuesdayWorkDescription(timeSheetForm.getTuesdayWorkDescription());
		timeSheet.setWednesdayStatus(timeSheetForm.getWednesdayStatus());
		timeSheet.setWednesdayWorkDescription(timeSheetForm.getWednesdayWorkDescription());
		timeSheet.setThursdayStatus(timeSheetForm.getThursdayStatus());
		timeSheet.setThursdayWorkDescription(timeSheetForm.getThursdayWorkDescription());
		timeSheet.setFridayStatus(timeSheetForm.getFridayStatus());
		timeSheet.setFridayWorkDescription(timeSheetForm.getFridayWorkDescription());
		timeSheet.setStatus(Constants.PENDING);
		timeSheet.setComment("");
		
		timeSheetDAO.update(timeSheet);
		
		
	}

	@Override
	public void sendWeekFoodBill(TimeSheetForm timeSheetForm) {
		
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		String monday = format.format(timeSheetForm.getMonday());
		String friday = format.format(timeSheetForm.getFriday());
		SimpleMailMessage email = new SimpleMailMessage();
		email.setTo(Constants.ADMINMAIl);
		email.setSubject("Food Bill from "+monday+" to "+friday+"!!!!");
		email.setText("Partho, " +
				 "\n\nFood Bill is "+timeSheetForm.getMondayBill()+" from "+monday+" to "+friday+" ."
				+ "\n\n\nRegards,\nSpYsR HR");

		mailSender.send(email);
	}

	
	

	public void foodBill(String month,int year) {

		Collection<EndUser> endUsers = endUserDAO.getEmployees().getResultList();

		if (endUsers.size() != 0) {
			for (EndUser endUser : endUsers) {
				int temp = 0;
				int veg = 0;
				int nonVeg = 0;
				Collection<TimeSheet> timeSheets = timeSheetDAO.getIndividualDetails(endUser.getUserName(), month,
						year);

				if (timeSheets.size() != 0) {
					Integer month1 = Integer.parseInt(month);
					Calendar calForDays = new GregorianCalendar(year, month1 - 1, 1);
					int noOfDays = calForDays.getActualMaximum(Calendar.DAY_OF_MONTH);
					

					for (int i = 1; i <= noOfDays; i++) {

						Calendar cal = new GregorianCalendar(year, month1 - 1, i);
						Date checkDate = cal.getTime();
						
						SimpleDateFormat fmt = new SimpleDateFormat("EEEE");
						String day = fmt.format(checkDate);
						

						Collection<TimeSheet> timeSheet1;
						if (day.equals("Monday")) {

							timeSheet1 = timeSheetDAO.monday(endUser.getUserName(), checkDate);

							if (timeSheet1.size() != 0) {
								for (TimeSheet timeSheet : timeSheet1) {
									if (timeSheet.getMondayStatus().equals("Working")) {

										temp += 1;
										veg += 1;
									}
								}
							}

						} else if (day.equals("Tuesday")) {
							timeSheet1 = timeSheetDAO.tuesday(endUser.getUserName(), checkDate);
							if (timeSheet1.size() != 0) {
								for (TimeSheet timeSheet : timeSheet1) {
									if (timeSheet.getTuesdayStatus().equals("Working")) {
										temp += 1;
										veg += 1;
									}
								}
							}

						} else if (day.equals("Wednesday")) {
							timeSheet1 = timeSheetDAO.wednesday(endUser.getUserName(), checkDate);
							if (timeSheet1.size() != 0) {
								for (TimeSheet timeSheet : timeSheet1) {
									if (timeSheet.getWednesdayStatus().equals("Working")) {
										temp += 1;
										veg += 1;
									}
								}
							}
						} else if (day.equals("Thursday")) {
							timeSheet1 = timeSheetDAO.thursday(endUser.getUserName(), checkDate);
							if (timeSheet1.size() != 0) {
								for (TimeSheet timeSheet : timeSheet1) {
									if (timeSheet.getThursdayStatus().equals("Working")) {
										temp += 1;
										veg += 1;
									}
								}
							}
						} else if (day.equals("Friday")) {
							timeSheet1 = timeSheetDAO.friday(endUser.getUserName(), checkDate);
							if (timeSheet1.size() != 0) {
								for (TimeSheet timeSheet : timeSheet1) {
									if (timeSheet.getFridayStatus().equals("Working")) {
										temp += 1;
										nonVeg += 1;
									}

								}
							}
						}

					}

					System.out.println(endUser.getName() + " is attended " + temp + " days");
					
					float amount = Constants.busPass- (temp*10);
					float foodAmount = (veg*Constants.veg) + (nonVeg*Constants.noVeg);
					
					FoodBill foodBill = new FoodBill();     //For saving individual record
					 
					foodBill.setUserName(endUser.getUserName());
					foodBill.setName(endUser.getName());
					foodBill.setDate(new Date());
					foodBill.setVeg(veg);
					foodBill.setNonVeg(nonVeg);
					foodBill.setMonth(month);
					foodBill.setYear(year);
					foodBill.setPresents(temp);
					foodBill.setMoney(amount);
					foodBill.setFoodBill(foodAmount);
					
					foodBillDAO.save(foodBill);
				}

			}
		}
	}

	@Override
	public void sendingBusPassMail(TimeSheetForm timeSheetForm) {
		
		Collection<FoodBill> foodBills = foodBillDAO.getData(timeSheetForm.getMonth(),timeSheetForm.getYear());
		
		int year = Calendar.getInstance().get(Calendar.YEAR);
		Integer month = Integer.parseInt(timeSheetForm.getMonth());
		Calendar cal = new GregorianCalendar(year, month - 1, 1);
		Date checkDate = cal.getTime();
		
		SimpleDateFormat fmt = new SimpleDateFormat("MMMM");
		String m = fmt.format(checkDate);
		
		
		MimeMessage message = mailSender.createMimeMessage();

		try {
			MimeMessageHelper helper = new MimeMessageHelper(message, true);

			helper.setTo(Constants.ADMINMAIl);
			helper.setSubject("Bus Pass Bill for "+m+"!!!!");
			
			
			String a = "Partho,\n\nPlease find the details below.";

			
			String b = "";

			for (FoodBill foodBill : foodBills) {
				b = b + "\n\n" + foodBill.getName() + "- Bus pass Bill: " + foodBill.getMoney() + "  Food Bill: "+foodBill.getFoodBill();
			}
			String c = "\n\n\nRegards,\nSpYsR HR";

			String msg = a + b + c;

			helper.setText(msg);
			

		} catch (MessagingException e) {
			throw new MailParseException(e);
		}
		mailSender.send(message);
		
	}

	@Override
	public void getUpdate(FoodBillForm foodBillForm) {
		
		FoodBill foodBill = foodBillDAO.findId(foodBillForm.getId());
		
		
		foodBill.setMoney(Constants.busPass- (foodBillForm.getPresents()*10));
		foodBill.setFoodBill((foodBillForm.getVeg()*Constants.veg)+(foodBillForm.getNonVeg()*Constants.noVeg));
		foodBill.setPresents(foodBillForm.getPresents());
		foodBill.setVeg(foodBillForm.getVeg());
		foodBill.setNonVeg(foodBillForm.getNonVeg());
		
		foodBillDAO.update(foodBill);
	}

	@Override
	public float monthBill(TimeSheetForm timeSheetForm) {
		Integer month = Integer.parseInt(timeSheetForm.getMonth());
		Calendar calForDays = new GregorianCalendar(timeSheetForm.getYear(), month - 1, 1);
		int noOfDays = calForDays.getActualMaximum(Calendar.DAY_OF_MONTH);
		

		float temp = 0;
		for (int i = 1; i <= noOfDays; i++) {

			Calendar cal = new GregorianCalendar(timeSheetForm.getYear(), month - 1, i);
			Date checkDate = cal.getTime();
			
			SimpleDateFormat fmt = new SimpleDateFormat("EEEE");
			String day = fmt.format(checkDate);
			

			Collection<TimeSheet> timeSheet1;
			if (day.equals("Monday")) {

				timeSheet1 = timeSheetDAO.getDetails(checkDate);

				if (timeSheet1.size() != 0) {
					for (TimeSheet timeSheet : timeSheet1) {
						if (timeSheet.getMondayStatus().equals("Working")) {
                         temp += timeSheet.getMondayBill();
						}
					}
				}

			} else if (day.equals("Tuesday")) {
				timeSheet1 = timeSheetDAO.getDetails(checkDate);
				if (timeSheet1.size() != 0) {
					for (TimeSheet timeSheet : timeSheet1) {
						if (timeSheet.getTuesdayStatus().equals("Working")) {
							temp += timeSheet.getTuesdayBill();
							
						}
					}
				}

			} else if (day.equals("Wednesday")) {
				timeSheet1 = timeSheetDAO.getDetails(checkDate);
				if (timeSheet1.size() != 0) {
					for (TimeSheet timeSheet : timeSheet1) {
						if (timeSheet.getWednesdayStatus().equals("Working")) {
							temp += timeSheet.getWednesdayBill();
						}
					}
				}
			} else if (day.equals("Thursday")) {
				timeSheet1 = timeSheetDAO.getDetails(checkDate);
				if (timeSheet1.size() != 0) {
					for (TimeSheet timeSheet : timeSheet1) {
						if (timeSheet.getThursdayStatus().equals("Working")) {
							temp += timeSheet.getThursdayBill();
						}
					}
				}
			} else if (day.equals("Friday")) {
				timeSheet1 = timeSheetDAO.getDetails(checkDate);
				if (timeSheet1.size() != 0) {
					for (TimeSheet timeSheet : timeSheet1) {
						if (timeSheet.getFridayStatus().equals("Working")) {
							temp += timeSheet.getFridayBill();
						}

					}
				}
			}

		}

		return temp;
	}

	@Override
	public void sendingMonthData(TimeSheetForm timeSheetForm) {
		Integer month = Integer.parseInt(timeSheetForm.getMonth());
		Calendar cal = new GregorianCalendar(timeSheetForm.getYear(), month - 1, 1);
		Date checkDate = cal.getTime();
		
		SimpleDateFormat fmt = new SimpleDateFormat("MMMM");
		String month1 = fmt.format(checkDate);
		SimpleMailMessage email = new SimpleMailMessage();
		email.setTo(Constants.ADMINMAIl);
		email.setSubject("Food Bill for "+month1+"!!!!");
		email.setText("Partho, " +
				 "\n\nFood Bill is "+timeSheetForm.getMondayBill()+" for "+month1+ "\n\n\nRegards,\nSpYsR HR");

		mailSender.send(email);
	}
		
	}



