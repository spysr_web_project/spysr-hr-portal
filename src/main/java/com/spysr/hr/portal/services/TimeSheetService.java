package com.spysr.hr.portal.services;

import com.spysr.hr.portal.form.FoodBillForm;
import com.spysr.hr.portal.form.TimeSheetForm;

/**
 * @author spysr_dev
 *
 */
public interface TimeSheetService {
	
	/**
	 * Method for adding time sheets
	 * @param timeSheet
	 */
	public void add(TimeSheetForm timeSheetForm);
	
	
	/**
	 * Method for getting Id details
	 * @param Id
	 * @return
	 */
	public TimeSheetForm getById(Long Id);
	
	/**
	 * Method for getting individual data
	 * @param id
	 * @return
	 */
	public float getWeekData(Long id);
	
	
	/**
	 * Method to update by admin
	 * @param timeSheet
	 */
	public void updateByAdmin(TimeSheetForm timeSheetForm);
	
	/**
	 * Method to update time sheet
	 * @param timeSheetForm
	 */
	public void update(TimeSheetForm timeSheetForm);
	
	
	/**
	 * Method for sending week food bill
	 * @param timeSheetForm
	 */
	public void sendWeekFoodBill(TimeSheetForm timeSheetForm);
	
	
	/**
	 * Method for food bill aclculations
	 * @param month
	 */
	public void foodBill(String month,int year);
	
	/**
	 * method for sending buspass money
	 * @param timeSheetForm
	 */
	public void sendingBusPassMail(TimeSheetForm timeSheetForm);
	
	
	
	/**
	 * Method for food bill update
	 * @param foodBill
	 */
	public void getUpdate(FoodBillForm foodBillForm);
	
	/**
	 * Method for month bill
	 * @param timeSheetForm
	 * @return
	 */
	public float monthBill(TimeSheetForm timeSheetForm);
	
	/**
	 * Method for month bill
	 * @param timeSheetForm
	 */
	public void sendingMonthData(TimeSheetForm timeSheetForm);
	
	
	
	

}
