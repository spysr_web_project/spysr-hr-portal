package com.spysr.hr.portal.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spysr.hr.portal.dao.EndUserDAO;
import com.spysr.hr.portal.dao.HolidayDAO;
import com.spysr.hr.portal.entity.EndUser;
import com.spysr.hr.portal.entity.HolidayList;
import com.spysr.hr.portal.form.EndUserForm;
import com.spysr.hr.portal.form.HolidayListForm;

@Service
public class HolidayServiceImpl implements HolidayService {

	/**
	 * save data
	 */
	@Autowired
	HolidayDAO holidayDAO;

	public void Save(HolidayListForm holidayListForm) {

		HolidayList holidayList = new HolidayList();

		holidayList.setSerialNo(holidayListForm.getSerialNo());
		holidayList.setHolidayName(holidayListForm.getHolidayName());
		holidayList.setHolidayDate(holidayListForm.getHolidayDate());
		holidayDAO.createUser(holidayList);

	}

	public void Update(HolidayListForm holidayListForm) {
		HolidayList holidayList = new HolidayList();
		holidayList.setId(holidayListForm.getId());
		holidayList.setSerialNo(holidayListForm.getSerialNo());
		holidayList.setHolidayName(holidayListForm.getHolidayName());
		holidayList.setHolidayDate(holidayListForm.getHolidayDate());
		holidayDAO.Update(holidayList);

	}
}
