/**
 * 
 */
package com.spysr.hr.portal.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spysr.hr.portal.dao.LeaveRequestDAO;
import com.spysr.hr.portal.entity.EndUser;
import com.spysr.hr.portal.entity.LeaveRequest;
import com.spysr.hr.portal.form.EndUserForm;
import com.spysr.hr.portal.form.LeaveRequestForm;

/**
 * @author Sudhir
 *
 */
@Service
public class LeaveRequestServiceImpl implements LeaveRequestService {
	
	@Autowired
	LeaveRequestDAO leaveRequestDAO;
	
	

	/* (non-Javadoc)
	 * @see com.spysr.hr.portal.services.LeaveRequestService#Save(com.spysr.hr.portal.form.LeaveRequestForm)
	 */
	@Override
	public void Save(LeaveRequestForm leaveRequestForm) {
		
		LeaveRequest leaveRequest=new LeaveRequest();
		leaveRequest.setId(leaveRequestForm.getId());
		leaveRequest.setUserName(leaveRequestForm.getUserName());
		leaveRequest.setEmail(leaveRequestForm.getEmail());
		leaveRequest.setDepartment(leaveRequestForm.getDepartment());
		leaveRequest.setDesignation(leaveRequestForm.getDesignation());
		leaveRequest.setName(leaveRequestForm.getName());
		leaveRequest.setStatus("Pending");
		leaveRequest.setLeaveStatus("Not Cancel");
		leaveRequest.setDoj(leaveRequestForm.getDoj());
		leaveRequest.setEligibleLeave(leaveRequestForm.getEligibleLeave());
		leaveRequest.setTotalLeave(leaveRequestForm.getTotalLeave());
		leaveRequest.setTypesofLeave(leaveRequestForm.getTypesofLeave());
		leaveRequest.setReason(leaveRequestForm.getReason());
	    leaveRequest.setLeaveLength(leaveRequestForm.getLeaveLength());
		leaveRequest.setCurrentRole(leaveRequestForm.getCurrentRole());
		leaveRequest.setLeaveDate(leaveRequestForm.getLeaveDate());
		leaveRequest.setFromDate(leaveRequestForm.getFromDate());
		leaveRequest.setToDate(leaveRequestForm.getToDate());
		
		if( leaveRequest.getFromDate() != null)
		{
			if( leaveRequest.getToDate() != null)
			{
			int totalnumberofDays = DateService.getDaysBetweenTwoDatesExcludingWeekend(leaveRequest.getFromDate(), leaveRequest.getToDate());
			leaveRequest.setTotalnumberleaveDays(totalnumberofDays);
			}
		}
		else
		{
			int totalnumberofDays =1;
			leaveRequest.setTotalnumberleaveDays(totalnumberofDays);
		}
		
		leaveRequestDAO.insert(leaveRequest);
	}
	
	
	
	


}

