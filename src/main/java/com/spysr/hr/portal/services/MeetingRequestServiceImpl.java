/**
 * 
 */
package com.spysr.hr.portal.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spysr.hr.portal.dao.MeetingRequestDAO;
import com.spysr.hr.portal.entity.MeetingRequest;
import com.spysr.hr.portal.form.MeetingRequestForm;




/**
 * @author Sudhir
 *
 */
@Service
public class MeetingRequestServiceImpl implements MeetingRequestService{
	
	
	@Autowired
	MeetingRequestDAO meetingRequestDAO;
	
	
	@Autowired
    MeetingRequestForm  meetingRequestForm;
	
	public void Save(MeetingRequestForm  meetingRequestForm) {
	
	
	MeetingRequest meetingRequest=new MeetingRequest();
	
	meetingRequest.setDescription(meetingRequestForm.getDescription());
	
	meetingRequest.setTime(meetingRequestForm.getTime());
	}
	   
	}


