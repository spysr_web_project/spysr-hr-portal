package com.spysr.hr.portal.services;

import com.spysr.hr.portal.form.HolidayListForm;

public interface HolidayService {
	
	
	/**
	 * Method to save data
	 * @param holidayListForm
	 */
	public void Save(HolidayListForm holidayListForm);
  
	
	public void Update(HolidayListForm holidayListForm);
	
	}
