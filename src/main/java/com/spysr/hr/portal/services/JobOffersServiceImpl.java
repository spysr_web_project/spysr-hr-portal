package com.spysr.hr.portal.services;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spysr.hr.portal.dao.JobOffersDAO;
import com.spysr.hr.portal.entity.JobOffers;
import com.spysr.hr.portal.form.JobOffersForm;

/**
 * @author spysr_dev
 *
 */
@Service
public class JobOffersServiceImpl implements JobOffersService {
	
	@Autowired
	JobOffersDAO jobOffersDAO;

	@Override
	public void addDetails(JobOffersForm jobOffersForm) {
		
		JobOffers jobOffers = new JobOffers();
		Date date = new Date();
		
		jobOffers.setJobTiltle(jobOffersForm.getJobTiltle());
		jobOffers.setExp(jobOffersForm.getExp());
		jobOffers.setTechnology(jobOffersForm.getTechnology());
		jobOffers.setLastDate(jobOffersForm.getLastDate());
		jobOffers.setPositions(jobOffersForm.getPositions());
		jobOffers.setJobDescription(jobOffersForm.getJobDescription());
		jobOffers.setPostedDate(date);
		
		jobOffersDAO.addDetails(jobOffers);
		
		
		
		
	}

}
