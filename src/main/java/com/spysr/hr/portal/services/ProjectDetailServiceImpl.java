/**
 * 
 */
package com.spysr.hr.portal.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spysr.hr.portal.dao.ProjectDetailsDAO;
import com.spysr.hr.portal.entity.ProjectDetails;
import com.spysr.hr.portal.form.ProjectDetailsForm;

/**
 * @author Sudhir
 *
 */
@Service
public class ProjectDetailServiceImpl implements ProjectDetailService {

	@Autowired
	ProjectDetailsDAO projectDetailsDAO;

	@Autowired
	ProjectDetailsForm projectDetailsForm;

	public void Save(ProjectDetailsForm projectDetailsForm) {

		ProjectDetails projectDetails = new ProjectDetails();
		
		projectDetails.setId(projectDetailsForm.getId());
		try {
			projectDetails.setImage(projectDetailsForm.getFile().getBytes());
			projectDetails.setImageName(projectDetailsForm.getFile().getOriginalFilename());
		} catch (Exception e) {
			e.getMessage();
		}
		projectDetails.setSerialNo(projectDetailsForm.getSerialNo());
		projectDetails.setProjectDetails(projectDetailsForm.getProjectDetails());
		projectDetails.setVersion(projectDetailsForm.getVersion());
		projectDetails.setMembersInvolved(projectDetailsForm.getMembersInvolved());
		projectDetails.setProjectMembers(projectDetailsForm.getProjectMembers());
		projectDetails.setProjectName(projectDetailsForm.getProjectName());

		projectDetailsDAO.createUser(projectDetails);
	}

	public void Update(ProjectDetailsForm projectDetailsForm) {

		ProjectDetails projectDetails = new ProjectDetails();

		projectDetails.setId(projectDetailsForm.getId());
		projectDetails.setSerialNo(projectDetailsForm.getSerialNo());
		projectDetails.setProjectDetails(projectDetailsForm.getProjectDetails());
		projectDetails.setMembersInvolved(projectDetailsForm.getMembersInvolved());
		projectDetails.setVersion(projectDetailsForm.getVersion());
		projectDetails.setProjectMembers(projectDetailsForm.getProjectMembers());
		projectDetails.setProjectName(projectDetailsForm.getProjectName());

		projectDetailsDAO.Update(projectDetails);
	}

}