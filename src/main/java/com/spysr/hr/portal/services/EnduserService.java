package com.spysr.hr.portal.services;

import com.spysr.hr.portal.form.EndUserForm;

public interface EnduserService {
	
	
	/**
	 * Method to save data
	 * @param endUserForm
	 */
	public void Save(EndUserForm endUserForm);
	/**
	 * Method to update data
	 * @param endUserForm
	 */
	
	public void Update(EndUserForm  endUserForm);
	
	


}
