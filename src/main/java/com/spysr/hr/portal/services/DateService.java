package com.spysr.hr.portal.services;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.springframework.expression.ParseException;
import org.springframework.stereotype.Service;
/**
 * Class to manipulate Dates
 */
@Service
public class DateService {
	
	/**
	 * Method to add four days to current date
	 */
	public static Date currentDateFourDaysAhead() {
		Calendar currentDate = Calendar.getInstance();
		currentDate.add(Calendar.DAY_OF_MONTH,4);
		currentDate.set(Calendar.HOUR_OF_DAY, 0);
		currentDate.set(Calendar.MINUTE, 0);
		currentDate.set(Calendar.SECOND, 0);
		currentDate.set(Calendar.MILLISECOND, 0);
		
		return currentDate.getTime();
	}
	
	/**
	 * Method to generate Date for FD.
	 * Add Days to current Date.
	 * @return Date
	 */
	public static Date generateDaysDate(Date date,int t) {
		Calendar currentDate = Calendar.getInstance();
		currentDate.setTime(date);
		currentDate.add(Calendar.DAY_OF_MONTH,t);
		currentDate.set(Calendar.HOUR_OF_DAY, 0);
		currentDate.set(Calendar.MINUTE, 0);
		currentDate.set(Calendar.SECOND, 0);
		currentDate.set(Calendar.MILLISECOND, 0);
		
		return currentDate.getTime();
	}
	
	
	/**
	 * Method to generate Date for FD.
	 * Add Months to current Date.
	 * @return Date
	 */
	public static Date generateMonthsDate(Date date,int t) {
		Calendar currentDate = Calendar.getInstance();
		currentDate.setTime(date);
		currentDate.add(Calendar.MONTH,t);
		currentDate.set(Calendar.HOUR_OF_DAY, 0);
		currentDate.set(Calendar.MINUTE, 0);
		currentDate.set(Calendar.SECOND, 0);
		currentDate.set(Calendar.MILLISECOND, 0);
		
		
		return currentDate.getTime();
	}
	
	/**
	 * Method to generate Date for FD.
	 * Add Years to current Date.
	 * @return Date
	 */
	public static Date generateYearsDate(Date date,int t) {
		Calendar currentDate = Calendar.getInstance();
		currentDate.setTime(date);
		currentDate.add(Calendar.YEAR,t);
		currentDate.set(Calendar.HOUR_OF_DAY, 0);
		currentDate.set(Calendar.MINUTE, 0);
		currentDate.set(Calendar.SECOND, 0);
		currentDate.set(Calendar.MILLISECOND, 0);
		
		
		return currentDate.getTime();
	}
	/**
	 * Method to get only date truncating time
	 */
	public static Date getDate(Date givenDate) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(givenDate);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		
		return cal.getTime();
	}	
	
	/**
	 * Method to get no of days between two dates
	 */
	public static int getDaysBetweenTwoDates(Date fromDate, Date toDate) {
		fromDate = DateService.getDate(fromDate);
		toDate = DateService.getDate(toDate);		
		long diff = toDate.getTime() - fromDate.getTime();
		int days = (int)(diff/(1000 * 60 * 60 * 24));
		
		return days;
	}
	
	
	
	public static int getDaysBetweenTwoDatesExcludingWeekend(Date fromDate, Date toDate){
	
        fromDate = DateService.getDate(fromDate);
		toDate = DateService.getDate(toDate);	
		long diff = toDate.getTime() - fromDate.getTime();
		int days = (int)(diff/(1000 * 60 * 60 * 24));
        int workDays=getdays(fromDate,toDate);
        System.out.println("Totaling Days = "+workDays);
        return workDays;
    }
	
	
	
public static int getdays(Date fromDate, Date toDate) {
	   Calendar startCal;
	    Calendar endCal;
	    startCal = Calendar.getInstance();
	    startCal.setTime(fromDate);
	    endCal = Calendar.getInstance();
	    endCal.setTime(toDate);
	    int workingDays = 0;
	    try
	    {
	      
	    
	      while(!startCal.after(endCal))
	      {
	       
	        int day = startCal.get(Calendar.DAY_OF_WEEK);
	       
	        if ((day != Calendar.SATURDAY) && (day != Calendar.SUNDAY))
	        workingDays++;
	      
	        startCal.add(Calendar.DATE, 1);
	      }
	      System.out.println(workingDays);
	    }
	    catch(Exception e)
	    {
	      e.printStackTrace();
	    }
		return workingDays;
	  }

	
	
	
	
	
	/**
	 * Method to generate Current Date without time
	 * @return
	 */
	public static Date getCurrentDate() {		
		Calendar currentDate = Calendar.getInstance();
		
		currentDate.set(Calendar.HOUR_OF_DAY, 0);
		currentDate.set(Calendar.MINUTE, 0);
		currentDate.set(Calendar.SECOND, 0);
		currentDate.set(Calendar.MILLISECOND, 0);
		
		return currentDate.getTime();
	}


}
