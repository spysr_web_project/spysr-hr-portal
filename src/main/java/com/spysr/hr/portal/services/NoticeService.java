/**
 * 
 */
package com.spysr.hr.portal.services;


import com.spysr.hr.portal.form.NoticeDetailsForm;


/**
 * @author Sudhir
 *
 */
public interface NoticeService {
	
	public void Save(NoticeDetailsForm  noticeDetailsForm);
	
	public void Update(NoticeDetailsForm NoticeDetailsForm);

}
