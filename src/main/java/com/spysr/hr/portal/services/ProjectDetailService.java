/**
 * 
 */
package com.spysr.hr.portal.services;


import com.spysr.hr.portal.form.ProjectDetailsForm;

/**
 * @author Sudhir
 *
 */
public interface ProjectDetailService {
	
	public void Save(ProjectDetailsForm projectDetailsForm);
	
	public void Update(ProjectDetailsForm projectDetailsForm);

}
