package com.spysr.hr.portal.scheduler;

import java.text.ParseException;

import javax.servlet.http.HttpServletRequest;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 * @author spysr_dev
 *
 */
public class NotificationsJob extends QuartzJobBean {
	
	private NotificationsScheduler notificationsScheduler;
	
	
	
	public void setNotificationsScheduler(NotificationsScheduler notificationsScheduler) {
		this.notificationsScheduler = notificationsScheduler;
	}

	@Override
	protected void executeInternal(JobExecutionContext arg0) throws JobExecutionException {
		
		HttpServletRequest request = null;
		
		  notificationsScheduler.sendPaySlip(request); // Sending Pay slip every month
		 

		notificationsScheduler.timeSheetAlert(); //Sending email notification
		

		 notificationsScheduler.sendForwardEligbleLeave(); 

		
	  try { notificationsScheduler.leaveCalculationDetails(); } catch
	 (ParseException e) { // TODO Auto-generated catch block
		 e.printStackTrace(); }
		 
	

	}

		
	}


