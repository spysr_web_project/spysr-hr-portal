package com.spysr.hr.portal.scheduler;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailParseException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.spysr.hr.portal.dao.BankTransferDetailsDAO;
import com.spysr.hr.portal.dao.EndUserDAO;
import com.spysr.hr.portal.dao.FoodBillDAO;
import com.spysr.hr.portal.dao.LeaveRequestDAO;
import com.spysr.hr.portal.dao.TimeSheetDAO;
import com.spysr.hr.portal.entity.BankTransferDetails;
import com.spysr.hr.portal.entity.EndUser;
import com.spysr.hr.portal.entity.FoodBill;
import com.spysr.hr.portal.entity.LeaveRequest;
import com.spysr.hr.portal.form.LeaveRequestForm;
import com.spysr.hr.portal.services.DateService;
import com.spysr.hr.portal.services.NumberToWord;
import com.spysr.hr.portal.utility.Constants;

public class NotificationsScheduler {

	@Autowired
	EndUserDAO endUserDAO;

	@Autowired
	JavaMailSender mailSender;

	@Autowired

	LeaveRequestForm leaveRequestForm;

	@Autowired
	LeaveRequestDAO leaveRequestDAO;

	@Autowired
	TimeSheetDAO timeSheetDAO;
	
	@Autowired
	BankTransferDetailsDAO bankTransferDetailsDAO;
	
	@Autowired
	FoodBillDAO foodBillDAO;

	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}

	/**
	 * @author spysr_dev Method for sending pay slip every month
	 */
	public void sendPaySlip(HttpServletRequest request) {
		Format formatter2 = new SimpleDateFormat("d");
		Format monthFormat = new SimpleDateFormat("MM");
		String s2 = formatter2.format(new Date());
		if (s2.equals("5")) {
			Collection<EndUser> endUsers = endUserDAO.getEmployees().getResultList();
			if (endUsers != null && endUsers.size() > 0) {
				for (EndUser endUser : endUsers) {


					Format formatter = new SimpleDateFormat("MMMM");
					
					Date date = DateService.generateMonthsDate(new Date(), -1);
					String s = formatter.format(date);
					Format formatter1 = new SimpleDateFormat("MMMM YYYY");
					
					String s1 = formatter1.format(date);
									
					String doJoin = formatter1.format(endUser.getDoj());
					String currentMonth = formatter1.format(new Date());
					
					float foodAmt=0;
					
					if(currentMonth.compareTo(doJoin) < 0)
					{
					
					try {
						
						if (endUser.getFoodamountDeduct() != 0 || endUser.getFoodamountDeduct() != null) {
							List<FoodBill> foodBills = foodBillDAO
									.getIndivualData(endUser.getUserName(), monthFormat.format(date)).getResultList();
							if (foodBills.size() == 0) {
								foodAmt = 0;

							} else {
								FoodBill foodBill = foodBills.get(0);
								foodAmt = foodBill.getPresents() * 10;

							}

						} else {
							List<FoodBill> foodBills = foodBillDAO
									.getIndivualData(endUser.getUserName(), monthFormat.format(date)).getResultList();
							if (foodBills.size() == 0) {
								foodAmt = 0;

							} else {
								FoodBill foodBill = foodBills.get(0);
								foodAmt = foodBill.getPresents() * 10;

							}
						}
						OutputStream file = new FileOutputStream(
								new File(Constants.DRIVE+ endUser.getName() + "_" + s + ".pdf"));
						Font bold = new Font(Font.FontFamily.HELVETICA, 8f, Font.BOLD);
						Font normal = new Font(Font.FontFamily.HELVETICA, 8f, Font.NORMAL);
						float total = 0, deduction = 0, netPay = 0;
						Document document = new Document();
						PdfWriter.getInstance(document, file);
                         
						Image image = Image.getInstance(request.getSession().getServletContext().getRealPath("/") + "resources/images/logo1.jpg");
						image.scaleAbsolute(70f, 70f);
						
						
						
						
						
						
						if (s1.equals(doJoin)) {

							float pfTaxData = 0;

							Date dojData = DateService.getDate(endUser.getDoj());
							Calendar cal = Calendar.getInstance();
							cal.add(Calendar.MONTH, -1);
							int noOfDays = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
							cal.set(Calendar.DAY_OF_MONTH, noOfDays);
							Date monthDate = DateService.getDate(cal.getTime());
							int workingDays = DateService.getDaysBetweenTwoDates(dojData, monthDate);

							float basic = endUser.getBasicSalary() / noOfDays;
							float basicsalary1 = (float) Math.ceil(workingDays * basic);

							float hra = endUser.getHra() / noOfDays;
							float HRA1 = (float) Math.ceil(workingDays * hra);

							float convey = endUser.getConveyAllowance() / noOfDays;
							float conveyAllowance1 = (float) Math.ceil(workingDays * convey);

							float medical = endUser.getMedicalAllowance() / noOfDays;
							float medicalAllowance1 = (float) Math.ceil(workingDays * medical);

							float special = endUser.getSpecialAllowance() / noOfDays;
							float specialAllowance1 = (float) Math.ceil(workingDays * special);

							float TDS = endUser.getTds() / noOfDays;
							float tds1 = (float) Math.ceil(workingDays * TDS);

							if (s.equals("November") || s.equals("April")) {
								total = basicsalary1 + HRA1 + conveyAllowance1 + medicalAllowance1 + specialAllowance1
										+ +(endUser.getVariablePackage() / 2);
							} else {
								total = basicsalary1 + HRA1 + conveyAllowance1 + medicalAllowance1 + specialAllowance1;
							}
							if (total < 15000) {
								pfTaxData = 0;

							} else {
								pfTaxData = 200;

							}

							if (endUser.getLop() != 0 || endUser.getLop() != null) {

								float eachday = (float) Math.ceil(total / workingDays);
							
								deduction = tds1 + foodAmt + pfTaxData + endUser.getLop() * eachday;
							} else {

								deduction = tds1 + foodAmt + pfTaxData;
							}
							total += pfTaxData;
							netPay = total- deduction;
						}  //If joining date is same month 
						
						else{

						
						if (s.equals("November") || s.equals("April")) {
							total = endUser.getBasicSalary() + endUser.getHra() + endUser.getConveyAllowance()
									+ endUser.getMedicalAllowance() + endUser.getSpecialAllowance()
									+ endUser.getProfessionalTax() + (endUser.getVariablePackage() / 2);
							deduction = endUser.getTds() +foodAmt+ endUser.getProfessionalTax();
							netPay = total - deduction;
						} else {
							total = endUser.getBasicSalary() + endUser.getHra() + endUser.getConveyAllowance()
									+ endUser.getMedicalAllowance() + endUser.getSpecialAllowance()
									+ endUser.getProfessionalTax();
							deduction = endUser.getTds() + foodAmt + endUser.getProfessionalTax();
							netPay = total - deduction;
						}
						}
						
						


					Calendar cal = Calendar.getInstance();
					cal.add(Calendar.MONTH, -1);
					int day =0;
					int pay = 0;
							if (doJoin.equals(s1)) {
								Date dojData = DateService.getDate(endUser.getDoj());
								
								int noOfdays = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
								cal.set(Calendar.DAY_OF_MONTH, noOfdays);
								Date monthDate = DateService.getDate(cal.getTime());
								int workingDays = DateService.getDaysBetweenTwoDates(dojData, monthDate);
							
									pay = (int) (total -deduction);
									day = workingDays;
								
							} else {
								
									int lopDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
								
									pay = (int) (total -deduction);
									day =lopDay ;
								}
							
					String days =Integer.toString(day);

						NumberToWord numberToWord = new NumberToWord(); // Converting
																		// Number
																		// to
																		// word
																		// format
						String inWords = numberToWord.convert(pay);
						String netPayMoney = Float.toString(pay);

						
						Chunk chunk = new Chunk("                   Net Pay:", bold);
						Chunk chunk1 = new Chunk(netPayMoney, bold);
						Chunk chunk2 = new Chunk("                   Amount in Words:Rupees ", bold);
						Chunk chunk3 = new Chunk(inWords + " only", bold);
						Chunk chunk4 = new Chunk("                   Mode of Payment: Bank Transfer", bold);
						Chunk chunk5 = new Chunk(
								"                   This payslip is computer generated, hence no signature is required",
								normal);
						document.open();

						Paragraph ph1 = new Paragraph("                   SpYsR HR", bold);
						Paragraph ph2 = new Paragraph(
								"                   No1, “AKHITAAN”,2nd floor,Kundanahalli, ITPL Main Road,", bold);
						Paragraph ph3 = new Paragraph(
								"                   Next to Knight Bridge Apartment, Brookefield,", bold);
						Paragraph ph4 = new Paragraph("                   Bangalore - 560 037", bold);
						Paragraph ph5 = new Paragraph(
								"                                                                                                                                                                                   PaySlip for "
										+ s1,
								bold);


					Paragraph p = new Paragraph();
					p.add(image);
					p.add(ph1);
					p.add(ph2);
					p.add(ph3);									
					p.add(ph4);
					p.add(ph5);
					document.add(p);
					document.add(Chunk.NEWLINE);
					
					Chunk ename = new Chunk(endUser.getName(),bold);
					
					PdfPTable table1 = new PdfPTable(5);
					table1.setWidthPercentage(85);
			        table1.getDefaultCell().setBorder(PdfPCell.NO_BORDER);
			        
			        String lop_days = endUser.getLop().toString();
			        
			        Chunk name = new Chunk("Employee Name:",normal);
					Phrase eName = new Phrase(name);
					
					Chunk nameData1 = new Chunk(ename);
					Phrase nameData = new Phrase(nameData1);
					
					Chunk wLocation1 = new Chunk("Work Location:",normal);
					Phrase wLocation = new Phrase(wLocation1);
					
					Chunk bLore = new Chunk("Bangalore",normal);
					Phrase bangalore = new Phrase(bLore);
					
					Chunk eId = new Chunk("Employee Id:",normal);
					Phrase employeeId = new Phrase(eId);
					
					Chunk userName1 = new Chunk(endUser.getUserName(),normal);
					Phrase userName = new Phrase(userName1);
					
					Chunk lop1 = new Chunk("LOP Days:",normal);
					Phrase lop = new Phrase(lop1);
					
					Chunk lopDays = new Chunk(lop_days,normal);
					Phrase lopDay = new Phrase(lopDays);
					
					Chunk des = new Chunk("Designation:",normal);
					Phrase designation = new Phrase(des);
					
					Chunk design = new Chunk(endUser.getDesignation(),normal);
					Phrase designData = new Phrase(design);
					
					Chunk wDays = new Chunk("Worked Days:",normal);
					Phrase wDay = new Phrase(wDays);
					
					Chunk wDayData1 = new Chunk(days,normal);
					Phrase wDayData = new Phrase(wDayData1);
					
					Chunk dept = new Chunk("Department:",normal);
					Phrase department = new Phrase(dept);
					
					Chunk deptData1 = new Chunk(endUser.getDepartment(),normal);
					Phrase deptData = new Phrase(deptData1);
					
					Chunk bank1 = new Chunk("Bank:",normal);
					Phrase bank = new Phrase(bank1);
					
					Chunk bankData1 = new Chunk(endUser.getBname() + "," + endUser.getBranch(),normal);
					Phrase bankData = new Phrase(bankData1);
					
					Chunk doj = new Chunk("Date of Joining:",normal);
					Phrase DOJ = new Phrase(doj);
					
					Chunk dojData1 = new Chunk(new SimpleDateFormat("dd/MM/yyyy").format(endUser.getDoj()),normal);
					Phrase dojData = new Phrase(dojData1);
					
					Chunk bNo = new Chunk("Bank A/C No:",normal);
					Phrase bankNo = new Phrase(bNo);
					
					Chunk acNo = new Chunk(endUser.getAccountNo(),normal);
					Phrase account = new Phrase(acNo);
			        
					table1.addCell(eName);
					table1.addCell(nameData);
					table1.addCell("");
					table1.addCell(wLocation);
					table1.addCell(bangalore);
					table1.addCell(employeeId);
					table1.addCell(userName);
					table1.addCell("");
					table1.addCell(lop);
					table1.addCell(lopDay);
					table1.addCell(designation);
					table1.addCell(designData);
					table1.addCell("");
					table1.addCell(wDay);
					table1.addCell(wDayData);
					table1.addCell(department);
					table1.addCell(deptData);
					table1.addCell("");
					table1.addCell(bank);
					table1.addCell(bankData);
					table1.addCell(DOJ);
					table1.addCell(dojData);
					table1.addCell("");
					table1.addCell(bankNo);
					table1.addCell(account);
					
					document.add(table1);
					
					PdfPTable table = Table(endUser,foodAmt);   //Table for Payslip
					document.add(table);
					document.add(chunk);
					document.add(chunk1);
					document.add(Chunk.NEWLINE);
					document.add(chunk2);
					document.add(chunk3);
					document.add(Chunk.NEWLINE);
					document.add(chunk4);
					document.add(Chunk.NEWLINE);
					document.add(Chunk.NEWLINE);
					document.add(chunk5);

						

						document.close();
						file.close();
						System.out.println("Payslip generated successfully..");
						
						BankTransferDetails bankTransferDetails = new BankTransferDetails();   //Individual details saving
						
						bankTransferDetails.setName(endUser.getName());
						bankTransferDetails.setAccountNO(endUser.getAccountNo());
						bankTransferDetails.setAmount(pay);
						Date bankDate = DateService.getCurrentDate();
						bankTransferDetails.setDate(bankDate);
						
						bankTransferDetailsDAO.save(bankTransferDetails);
						                        
						mailNotification(endUser); // Method for sending pay
													// slip to respective email
					}

					catch (Exception e) {
						e.printStackTrace();
					}
				}
				
				}//If doj is current month
			} // for loop close
			Date bankDate = DateService.getCurrentDate();
			generateExcelForCalulation(bankDate);     //Method for generating Excel and sending it to manager
		} // close check 5th date
	}// Main if close

	/**
	 * @author spysr_dev Method for Pay Slip mail notification
	 * @param endUser
	 */
	public void mailNotification(EndUser endUser) {
		Format formatter = new SimpleDateFormat("MMMM");
		Date date = DateService.generateMonthsDate(new Date(), -1);
		String s = formatter.format(date);
		Format formatter1 = new SimpleDateFormat("MMMM YYYY");
		String s1 = formatter1.format(date);
		MimeMessage message = mailSender.createMimeMessage();

		try {
			MimeMessageHelper helper = new MimeMessageHelper(message, true);

			helper.setTo(endUser.getEmail());
			helper.setSubject("Pay Slip for " + s1);
			helper.setText(String.format(endUser.getName() + ",\n\n Enclosed, please find your Pay Slip for " + s1
					+ "\n\n\nRegards,\nSpYsR HR"));

			FileSystemResource attachment = new FileSystemResource(Constants.DRIVE + endUser.getName() + "_" + s + ".pdf");
			helper.addAttachment(attachment.getFilename(), attachment);

		} catch (MessagingException e) {
			throw new MailParseException(e);
		}
		mailSender.send(message);
	}

	/**
	 * @author spysr_dev Method for Pay slip table
	 * @param endUser
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public PdfPTable Table(EndUser endUser,float foodAmt) throws UnsupportedEncodingException {
		Format formatter = new SimpleDateFormat("MMMM");
		String s = formatter.format(new Date());
		Font bold = new Font(Font.FontFamily.HELVETICA, 8f, Font.BOLD);
		Font normal = new Font(Font.FontFamily.HELVETICA, 8f, Font.NORMAL);
		PdfPTable table = new PdfPTable(5);
		table.setWidthPercentage(85);
		
		Date date = DateService.generateMonthsDate(new Date(), -1);
		Format formatter1 = new SimpleDateFormat("MMMM YYYY");
		String s1 = formatter1.format(date);				
		String doJoin = formatter1.format(endUser.getDoj());
		
		String basicSalary,HRA,conveyAllowance,medicalAllowance,specialAllowance,tds,professionalTax,food,vPay,totalSalary,deductions;
		float pfTaxData=0;
		Chunk lop,lopData ;
		Phrase ph28 = null,ph29 = null;
		if (s1.equals(doJoin)) {
			Date dojData = DateService.getDate(endUser.getDoj());
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.MONTH, -1);
			int noOfDays = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
			cal.set(Calendar.DAY_OF_MONTH, noOfDays);
			Date monthDate = DateService.getDate(cal.getTime());
			int workingDays = DateService.getDaysBetweenTwoDates(dojData, monthDate);

			float basic = endUser.getBasicSalary() / noOfDays;
			float basicsalary1 = (float) Math.ceil(workingDays * basic);

			float hra = endUser.getHra() / noOfDays;
			float HRA1 =  (float) Math.ceil(workingDays * hra);

			float convey = endUser.getConveyAllowance() / noOfDays;
			float conveyAllowance1 =  (float) Math.ceil(workingDays * convey);

			float medical = endUser.getMedicalAllowance() / noOfDays;
			float medicalAllowance1 =  (float) Math.ceil(workingDays * medical);

			float special = endUser.getSpecialAllowance() / noOfDays;
			float specialAllowance1 = (float) Math.ceil( workingDays * special);

			float TDS = endUser.getTds() / noOfDays;
			float tds1 =  (float) Math.ceil(workingDays * TDS);
			
			basicSalary = Float.toString(basicsalary1);
			HRA = Float.toString(HRA1);

			conveyAllowance = Float.toString(conveyAllowance1);
			medicalAllowance = Float.toString(medicalAllowance1);
			tds = Float.toString(tds1);
			specialAllowance = Float.toString(specialAllowance1);

			float total = 0;

			if (s.equals("November") || s.equals("April")) {
				total = basicsalary1 + HRA1 + conveyAllowance1 + medicalAllowance1 + specialAllowance1
						+ +(endUser.getVariablePackage() / 2);
			} else {
				total = basicsalary1 + HRA1 + conveyAllowance1 + medicalAllowance1 + specialAllowance1;
			}
			if (total < 15000) {
				pfTaxData = 0;
				 professionalTax =Float.toString(pfTaxData);
				 totalSalary = Float.toString(total);
			} else {
				pfTaxData = 200;
				 professionalTax =Float.toString(pfTaxData);
				 totalSalary = Float.toString(total + 200);
			}

			float variablePay = endUser.getVariablePackage() / 2;
			 vPay = Float.toString(variablePay);

			 float deduction;
				
			if(endUser.getLop() !=0 || endUser.getLop() != null)
			{
				 lop = new Chunk("loss Of Pay", normal);
				 ph28 = new Phrase(lop);

				 float eachday = (float) Math.ceil(total/workingDays);
				 String lopCalc = Float.toString(endUser.getLop() *eachday);
				 
				 lopData = new Chunk(lopCalc, normal);
				 ph29 = new Phrase(lopData);
				 
				 deduction = tds1 + foodAmt + pfTaxData+endUser.getLop() *eachday;
			}
			else
			{
				lop = new Chunk("", normal);
				 ph28 = new Phrase(lop);

				 lopData = new Chunk("", normal);
				 ph29 = new Phrase(lopData);
				 deduction = tds1 + foodAmt + pfTaxData;
			}
			deductions = Float.toString(deduction);
		}
		
		else{
		 basicSalary = endUser.getBasicSalary().toString();
		 HRA = endUser.getHra().toString();
		 conveyAllowance = endUser.getConveyAllowance().toString();
		 medicalAllowance = endUser.getMedicalAllowance().toString();
		 specialAllowance = endUser.getSpecialAllowance().toString();
		 tds = endUser.getTds().toString();
		 professionalTax = endUser.getProfessionalTax().toString();
		 food = endUser.getFoodamountDeduct().toString();
		float variablePay = endUser.getVariablePackage() / 2;
		vPay = Float.toString(variablePay);
		float total = 0;
		if (s.equals("November") || s.equals("April")) {
			total = endUser.getBasicSalary() + endUser.getHra() + endUser.getConveyAllowance()
					+ endUser.getMedicalAllowance() + endUser.getSpecialAllowance() + endUser.getProfessionalTax()
					+ (endUser.getVariablePackage() / 2);
		} else {
			total = endUser.getBasicSalary() + endUser.getHra() + endUser.getConveyAllowance()
					+ endUser.getMedicalAllowance() + endUser.getSpecialAllowance() + endUser.getProfessionalTax();
		}
		totalSalary = Float.toString(total);

		float deduction = endUser.getTds() + foodAmt + endUser.getProfessionalTax();
		deductions = Float.toString(deduction);
		
		}
		
		Chunk earnings = new Chunk("Earnings", bold);
		Phrase ph1 = new Phrase(earnings);

		String rupee = "\u20B9";
		byte[] utf8 = rupee.getBytes("UTF-8");

		rupee = new String(utf8, "UTF-8");

		Chunk amount = new Chunk("Amount" + (rupee), bold);
		Phrase ph2 = new Phrase(amount);

		Chunk deduct = new Chunk("Deductions", bold);
		Phrase ph3 = new Phrase(deduct);

		Chunk salary = new Chunk("Total Salary", bold);
		Phrase ph4 = new Phrase(salary);

		Chunk salary1 = new Chunk(totalSalary, bold);
		Phrase ph5 = new Phrase(salary1);

		Chunk ded = new Chunk("Total Deductions", bold);
		Phrase ph6 = new Phrase(ded);

		Chunk ded1 = new Chunk(deductions, bold);
		Phrase ph7 = new Phrase(ded1);

		Chunk bSalary = new Chunk("Basic Salary", normal);
		Phrase ph8 = new Phrase(bSalary);

		Chunk bSalary1 = new Chunk(basicSalary, normal);
		Phrase ph9 = new Phrase(bSalary1);

		Chunk hra = new Chunk("HRA", normal);
		Phrase ph10 = new Phrase(hra);

		Chunk hra1 = new Chunk(HRA, normal);
		Phrase ph11 = new Phrase(hra1);

		Chunk cAllowance = new Chunk("Conveyance Allowance", normal);
		Phrase ph12 = new Phrase(cAllowance);

		Chunk cAllowance1 = new Chunk(conveyAllowance, normal);
		Phrase ph13 = new Phrase(cAllowance1);

		Chunk mAllowance = new Chunk("Medical Allowance", normal);
		Phrase ph14 = new Phrase(mAllowance);

		Chunk mAllowance1 = new Chunk(medicalAllowance, normal);
		Phrase ph15 = new Phrase(mAllowance1);

		Chunk sAllowance = new Chunk("Special Allowance", normal);
		Phrase ph16 = new Phrase(sAllowance);

		Chunk sAllowance1 = new Chunk(specialAllowance, normal);
		Phrase ph17 = new Phrase(sAllowance1);

		Chunk tds1 = new Chunk("Tax Deducted at Source", normal);
		Phrase ph18 = new Phrase(tds1);

		Chunk tds2 = new Chunk(tds, normal);
		Phrase ph19 = new Phrase(tds2);

		Chunk pTax = new Chunk("Professional tax", normal);
		Phrase ph20 = new Phrase(pTax);

		Chunk pTax1 = new Chunk(professionalTax, normal);
		Phrase ph21 = new Phrase(pTax1);

		Chunk ADeduct = new Chunk("Amount Deducted for Food", normal);
		Phrase ph22 = new Phrase(ADeduct);

		food = Float.toString(foodAmt);
		
		Chunk ADeduct1 = new Chunk(food, normal);
		Phrase ph23 = new Phrase(ADeduct1);

		Chunk vSalary = new Chunk("Variable Salary", normal);
		Phrase ph24 = new Phrase(vSalary);

		Phrase ph27 = new Phrase();
		if (s.equals("November") || s.equals("April")) {
			Chunk vSalary1 = new Chunk(vPay, normal);
			ph27 = new Phrase(vSalary1);

		} else {
			Chunk vSalary1 = new Chunk("-", normal);
			ph27 = new Phrase(vSalary1);

		}

		Chunk pfTax = new Chunk("Profession Tax", normal);
		Phrase ph25 = new Phrase(pfTax);

		Chunk pfTax1 = new Chunk(professionalTax, normal);
		Phrase ph26 = new Phrase(pfTax1);

		table.addCell(ph1);
		table.addCell(ph2);
		table.addCell("");
		table.addCell(ph3);
		table.addCell(ph2);
		table.addCell(ph8);
		table.addCell(ph9);
		table.addCell("");
		table.addCell("");
		table.addCell("");
		table.addCell(ph10);
		table.addCell(ph11);
		table.addCell("");
		table.addCell("");
		table.addCell("");
		table.addCell(ph12);
		table.addCell(ph13);
		table.addCell("");
		table.addCell("");
		table.addCell("");
		table.addCell(ph14);
		table.addCell(ph15);
		table.addCell("");
		table.addCell(ph28);
		table.addCell(ph29);
		table.addCell(ph16);
		table.addCell(ph17);
		table.addCell("");
		table.addCell(ph18);
		table.addCell(ph19);
		table.addCell(ph20);
		table.addCell(ph21);
		table.addCell("");
		table.addCell(ph22);
		table.addCell(ph23);
		table.addCell(ph24);
		table.addCell(ph27);
		table.addCell("");
		table.addCell(ph25);
		table.addCell(ph26);
		table.addCell(ph4);
		table.addCell(ph5);
		table.addCell("");
		table.addCell(ph6);
		table.addCell(ph7);

		table.setSpacingBefore(30.0f);
		table.setSpacingAfter(30.0f);

		return table;
	}

	/**
	 * @author spysr_dev Method for sending mail notification for time sheet
	 * 
	 */
	public void timeSheetAlert() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY) {
			Collection<EndUser> endUsers = endUserDAO.getEmployees().getResultList();
			if (endUsers != null && endUsers.size() > 0) {
				for (EndUser endUser : endUsers) {

					MimeMessage message = mailSender.createMimeMessage();

					try {
						MimeMessageHelper helper = new MimeMessageHelper(message, true);

						helper.setTo(endUser.getEmail());
						helper.setSubject("Time Sheet Reminder");
						helper.setText(
								String.format(endUser.getName() + ",\n\n Please fill the Time Sheet for this week "
										+ "\n\n\nRegards,\nSpYsR HR"));

					} catch (MessagingException e) {
						throw new MailParseException(e);
					}
					mailSender.send(message);
				}
			}

		}
	}
	
	
	/**
	 * @author spysr_dev
	 * Method for generating excel and sending it to manager
	 * @param bankDate
	 */
	public void generateExcelForCalulation(Date bankDate) {
		
		Format formatter = new SimpleDateFormat("MMMM");
		Date date = DateService.generateMonthsDate(new Date(), -1);
		String s = formatter.format(date);
		Format formatter1 = new SimpleDateFormat("MMMM-YYYY");
		String s1 = formatter1.format(date);
		Collection<BankTransferDetails> bankTransferDetails = bankTransferDetailsDAO.getDetails(bankDate);
		if(bankTransferDetails.size() != 0)
		{
			
			HSSFWorkbook workbook = new HSSFWorkbook();
			HSSFSheet sheet = workbook.createSheet("Sample sheet");
			int i=1;
			int j=1;
			Map<Integer, Object[]> data = new HashMap<Integer, Object[]>();
			
			data.put(i, new Object[]{"S.No", "Name", "Account No","Amount"});
			for(BankTransferDetails bDetails : bankTransferDetails)
			{
				++i;
				data.put(i, new Object[]{j,bDetails.getName(), bDetails.getAccountNO(),bDetails.getAmount()});
				++j;
			}
			Set<Integer> keyset = data.keySet();
			int rownum = 3;
			Row row1 = sheet.createRow(1);
			Cell cell1 = row1.createCell(4);
			cell1.setCellValue("Bank Transfer Details");
			
			for (Integer key : keyset) {
				Row row = sheet.createRow(rownum++);
				Object [] objArr = data.get(key);
				int cellnum = 2;
				for (Object obj : objArr) {
					Cell cell = row.createCell(cellnum++);
					if(obj instanceof Date) 
						cell.setCellValue((Date)obj);
					else if(obj instanceof Integer)
						cell.setCellValue((Integer)obj);
					else if(obj instanceof String)
						cell.setCellValue((String)obj);
					else if(obj instanceof Float)
						cell.setCellValue((Float)obj);
				}
			}
			Row row = sheet.createRow(rownum++);
			Cell cell = row.createCell(4);
			cell.setCellValue("Grand Total");
			
			Double amount = bankTransferDetailsDAO.getTotal(bankDate);
			Cell cell2 = row.createCell(5);
			cell2.setCellValue(amount);
			
			try {
				
				FileOutputStream out = 
						new FileOutputStream(new File(Constants.DRIVE+"Bank_Detail_"+s1+".xls"));
				workbook.write(out);
				out.close();
				System.out.println("Excel written successfully..");
				
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			MimeMessage message = mailSender.createMimeMessage();

			try {
				MimeMessageHelper helper = new MimeMessageHelper(message, true);

				helper.setTo(Constants.ADMINMAIl);
				helper.setSubject("Bank Transfer Details for " + s);
				helper.setText(String.format("Partho"+ ",\n\n Enclosed, please find details for " + s1
						+ "\n\n\nRegards,\nSpYsR HR"));

				FileSystemResource attachment = new FileSystemResource(Constants.DRIVE+"Bank_Detail_"+s1+".xls");
				helper.addAttachment(attachment.getFilename(), attachment);

			} catch (MessagingException e) {
				throw new MailParseException(e);
			}
			mailSender.send(message);
		}
	}
	

	/**
	 * @author sudhir Method for leave Calculation
	 * @throws ParseException 
	 * 
	 */
	public void leaveCalculationDetails() throws ParseException {

		Collection<EndUser> endUsers = endUserDAO.getEmployees().getResultList();
		if (endUsers != null && endUsers.size() > 0) {
			for (EndUser endUser : endUsers) {

				Date doj = endUser.getDoj();

				int eligibleLeave = endUser.getEligibleLeave();
				int totalLeave = endUser.getTotalLeave();

				Collection<LeaveRequest> leaveRequest = leaveRequestDAO.getLeaveList(endUser.getUserName())
						.getResultList();
				if (leaveRequest != null && leaveRequest.size() > 0) {
					for (LeaveRequest leave : leaveRequest) {
						
						if(leave.getDeducted().equals(" "))
						{

						if (leave.getStatus().equals("Approved")) {

							if (!leave.getStatus().equals("Cancel")) {

								DateFormat dateFormat = new SimpleDateFormat("dd/M/yyyy");
								Date date = new Date();
								String sysdat = dateFormat.format(date);

								Date end = new SimpleDateFormat("dd/M/yyyy").parse(sysdat);
								Date date2 = leave.getToDate();
								if (date2 != null) {
								
									if (date2.compareTo(end) < 0) {
										if (endUser.getEligibleLeave() >= leave.getTotalnumberleaveDays()) {
											endUser.setEligibleLeave(
													endUser.getEligibleLeave() - leave.getTotalnumberleaveDays());
											endUser.setTotalLeave(
													endUser.getTotalLeave() - leave.getTotalnumberleaveDays());
											endUser.setLop(0);
										} else {

											int lop = leave.getTotalnumberleaveDays() - endUser.getEligibleLeave();
											totalLeave = endUser.getTotalLeave() - leave.getTotalnumberleaveDays();
											endUser.setLop(lop);
											endUser.setEligibleLeave(0);
											endUser.setTotalLeave(totalLeave);

										}
										
										leave.setDeducted("Deducted");
										
										leaveRequestDAO.update(leave);
										
										endUserDAO.update(endUser);

										MimeMessage message = mailSender.createMimeMessage();

										try {
											MimeMessageHelper helper = new MimeMessageHelper(message, true);
											int lop = endUser.getLop();
											helper.setTo(leave.getEmail());
											helper.setSubject("Leave Calculation in a Year");
											helper.setText("Hello" + "\n\n"
													+ String.format(leave.getName() + ",\n\n Your Date of Joining is: "
															+ doj + ",\n\n Eligible Leave for current Month: "
															+ eligibleLeave + ",\n\n Lop: " + lop
															+ ",\n\n Total Available Leave for Current Calender Year: "
															+ totalLeave
															+ "\n\n\nAdmin,\nSpYsR HR"));

										} catch (MessagingException e) {
											throw new MailParseException(e);
										}
										mailSender.send(message);
									}

								}

							}
						}

						}

					}

				}
			}
		}

	}

	public void sendForwardEligbleLeave() {

		MimeMessage message = mailSender.createMimeMessage();
		Format formatter2 = new SimpleDateFormat("d");
		String s2 = formatter2.format(new Date());
		
		if (s2.equals("1")) {

			
			SimpleDateFormat fmt = new SimpleDateFormat("MMMM");
			String month = fmt.format(new Date());
			
			Collection<EndUser> endUsers = endUserDAO.getEmployees().getResultList();

			if (endUsers != null && endUsers.size() > 0) {
				for (EndUser endUser : endUsers) {
					int eligibleLeave = endUser.getEligibleLeave();
					if (!month.equals("February") || !month.equals("October")) {
						endUser.setEligibleLeave(eligibleLeave + 2);

					} else {
						endUser.setEligibleLeave(eligibleLeave + 1);
					}
					endUserDAO.update(endUser);

					try {
						MimeMessageHelper helper = new MimeMessageHelper(message, true);

						helper.setTo(endUser.getEmail());
						helper.setSubject("Leave Calculation in a Year");
						helper.setText("Hello" + "\n\n"
								+ String.format(endUser.getName() + ",\n\n Eligible Leave for current Month: "
										+ eligibleLeave + "\n\n\nAdmin,\nSpYsR HR"));

					} catch (MessagingException e) {
						throw new MailParseException(e);
					}
					mailSender.send(message);
				}

			}

		}

	}

	

}
