package com.spysr.hr.portal.web;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailParseException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.theme.CookieThemeResolver;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfWriter;
import com.spysr.hr.portal.dao.EndUserDAO;
import com.spysr.hr.portal.dao.HolidayDAO;
import com.spysr.hr.portal.dao.JobOffersDAO;
import com.spysr.hr.portal.dao.LeaveRequestDAO;
import com.spysr.hr.portal.dao.ProjectDetailsDAO;
import com.spysr.hr.portal.dao.ReferJobDAO;
import com.spysr.hr.portal.dao.TimeSheetDAO;
import com.spysr.hr.portal.entity.EndUser;
import com.spysr.hr.portal.entity.HolidayList;
import com.spysr.hr.portal.entity.JobOffers;
import com.spysr.hr.portal.entity.LeaveRequest;
import com.spysr.hr.portal.entity.ProjectDetails;
import com.spysr.hr.portal.entity.ReferJob;
import com.spysr.hr.portal.entity.TimeSheet;
import com.spysr.hr.portal.form.EndUserForm;
import com.spysr.hr.portal.form.JobOffersForm;
import com.spysr.hr.portal.form.LeaveRequestForm;
import com.spysr.hr.portal.form.ProjectDetailsForm;
import com.spysr.hr.portal.form.RequestedDocsForm;
import com.spysr.hr.portal.form.TimeSheetForm;
import com.spysr.hr.portal.services.ImageService;
import com.spysr.hr.portal.services.LeaveRequestService;
import com.spysr.hr.portal.services.ProjectDetailService;
import com.spysr.hr.portal.services.TimeSheetService;
import com.spysr.hr.portal.utility.Constants;



/*import spysr.hr.portal.dao.ReferJobDAO;*/

/*import com.spysr.hr.portal.entity.ReferJob;*/
@Controller
@RequestMapping("/employees")
public class EmployeeController {

	@Autowired
	EndUserDAO endUserDAOImpl;

	@Autowired
	EndUserForm endUserForm;
	@Autowired
	LeaveRequestForm leaveRequestForm;
	
	@Autowired
	LeaveRequestDAO leaveRequestDAO;
	
	@Autowired
	LeaveRequestService leaveRequestService;
	
	@Autowired
	JavaMailSender mailSender;
	
	@Autowired
	HolidayDAO holidayDAO;
	
	@Autowired
	ProjectDetailsDAO projectDetailsDAO;
	
	@Autowired
	ProjectDetailsForm projectDetailsForm;
	@Autowired
	ProjectDetailService projectDetailService;
	
	@Autowired
	TimeSheetForm timeSheetForm;
	
	@Autowired
	TimeSheetService timeSheetService;
	
	@Autowired
	TimeSheetDAO timeSheetDAO;
	
	@Autowired
	RequestedDocsForm requestedDocsForm;
	
	@Autowired
	JobOffersForm jobOffersForm;
	
	@Autowired
	JobOffersDAO jobOffersDAO;
	
	@Autowired
	ReferJobDAO referJobDAO;
	
/*	@Autowired
	ReferJobDAO referJobDAO;*/
	
	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}
	
	CookieThemeResolver themeResolver = new CookieThemeResolver();

	static Logger log = Logger.getLogger(AdminController.class.getName());

	private String getCurrentLoggedUserName() {
		return SecurityContextHolder.getContext().getAuthentication().getName();

	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		CustomDateEditor editor = new CustomDateEditor(dateFormat, true);
		binder.registerCustomEditor(Date.class, editor);
	}

	/**
	 * @author spysr_dev
	 * Method to get current user details
	 */
	private EndUser getCurrentLoggedUserDetails() {

		EndUser endUser = endUserDAOImpl.findByUsername(getCurrentLoggedUserName()).getSingleResult();

		return endUser;

	}
	
	/**
	 * @author spysr_dev
	 * Method to calling current user details
	 * 
	 * @param model
	 * @return
	 */
	@ModelAttribute("requestCurrentUser")
	public EndUser getUserDetails(ModelMap model) {
		EndUser user = getCurrentLoggedUserDetails();
		if (user.getImageName() != null) {
			String type = ImageService.getImageType(user.getImageName());

			String url = "data:image/" + type + ";base64," + Base64.encodeBase64String(user.getImage());
			user.setImageName(url);
		}
		
		model.put("user", user);
		return user;
	}

	 

	/**
	 * @author spysr_dev
	 * Method to editing profile
	 * 
	 * @param model,
	 *            id
	 * @return view
	 */
	@RequestMapping(value = "/editEmployeeProfile", method = RequestMethod.GET)
	public ModelAndView editAdminProfile(ModelMap model, @RequestParam("id") Long id, RedirectAttributes attributes) {

		EndUser userProfile = endUserDAOImpl.findId(id);

		if (userProfile.getImageName() != null) {
			String type = ImageService.getImageType(userProfile.getImageName());

			String url = "data:image/" + type + ";base64," + Base64.encodeBase64String(userProfile.getImage());
			userProfile.setImageName(url);

			endUserForm.setImageName(url);
		}
		endUserForm.setId(userProfile.getId());
		endUserForm.setDisplayName(userProfile.getDisplayName());
		endUserForm.setUserName(userProfile.getUserName());
		
		endUserForm.setContactNo(userProfile.getContactNo());
		endUserForm.setEmail(userProfile.getEmail());

		endUserForm.setPassword(userProfile.getPassword());
		

		model.put("endUserForm", endUserForm);

		return new ModelAndView("editEmployeeProfile", "model", model);

	}
	
	/**
	 * @author spysr_dev
	 * Method to updating profile image
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/updateImageForProfile", method = RequestMethod.POST)
	public String updateImageForProfile(ModelMap model, @ModelAttribute EndUserForm endUserForm) {

		try {
			EndUser userProfile = endUserDAOImpl.findId(endUserForm.getId());
			userProfile.setImage(endUserForm.getFile().getBytes());
			userProfile.setImageName(endUserForm.getFile().getOriginalFilename());
			endUserDAOImpl.update(userProfile);

		} catch (Exception e) {
			e.getMessage();
		}
		return "redirect:editEmployeeProfile?id=" + endUserForm.getId();
	}

	/**
	 * @author spysr_dev
	 * Method to confirm edit profile
	 * 
	 * @param model
	 * @return
	 */
	/*@RequestMapping(value = "/confirmEditEmployeeProfile", method = RequestMethod.POST)
	public ModelAndView confirmEditAdminProfile(ModelMap model, @ModelAttribute EndUserForm endUserForm) {

		model.put("endUserForm", endUserForm);

		return new ModelAndView("confirmEditEmployeeProfile", "model", model);

	}*/
    
	/**
	 * @author spysr_dev
	 * Method to updating profile details
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/updateEmployeeDetails", method = RequestMethod.POST)
	public String updateAdminDetails(ModelMap model, @ModelAttribute EndUserForm endUserForm,
			BindingResult result, RedirectAttributes attributes) {

		EndUser endUser = endUserDAOImpl.findId(endUserForm.getId());

		endUser.setId(endUserForm.getId());

		endUser.setDisplayName(endUserForm.getDisplayName());
		endUser.setContactNo(endUserForm.getContactNo());
	
		endUser.setEmail(endUserForm.getEmail());
	
		endUser.setUserName(endUserForm.getUserName());
		
		endUserDAOImpl.update(endUser);

		model.put("endUserForm", endUserForm);

	return "redirect:updateEmployeeSuccess";

	}
	
	@RequestMapping(value="updateEmployeeSuccess",method=RequestMethod.GET)
	public ModelAndView update(ModelMap model,@ModelAttribute EndUserForm endUserForm){
		model.put("endUserForm", endUserForm);
		return new ModelAndView("updateEmployeeSuccess","model",model);
	}
	
	
	/**
	 * @author spysr_dev
	 * Method to edit password details
	 * 
	 * @param model,id
	 * @return
	 */
	@RequestMapping(value = "/editEmployeePWD", method = RequestMethod.GET)
	public ModelAndView editAdminPWD(ModelMap model, @RequestParam("id") Long id, RedirectAttributes attributes) {

		EndUser userProfile = endUserDAOImpl.findId(id);

		endUserForm.setId(userProfile.getId());

		

		model.put("endUserForm", endUserForm);

		return new ModelAndView("editEmployeePWD", "model", model);

	}

	/**
	 * @author spysr_dev
	 * Method to updating password details
	 * 
	 * @param model,
	 *            enduserForm
	 * @return
	 */
	@RequestMapping(value = "/updateEditEmployeePWD", method = RequestMethod.POST)
	public ModelAndView updateEditAdminPWD(ModelMap model, @ModelAttribute EndUserForm endUserForm,
			BindingResult result, RedirectAttributes attributes) {

		EndUser endUser = endUserDAOImpl.findId(endUserForm.getId());

		endUser.setId(endUserForm.getId());

		endUser.setPassword(endUserForm.getConfirmNewPassword());
	
		endUserDAOImpl.update(endUser);

		model.put("endUserForm", endUserForm);

		return new ModelAndView("updateEmployeeSuccess", "model", model);

	}
    
	/**
	 * @author spysr_dev
	 * Method to change theme
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/themeChange", method = RequestMethod.GET)
	public String getThemeChange(ModelMap model, HttpServletRequest request, HttpServletResponse response) {

		log.info("Received request for theme change");

		EndUser endUser = endUserDAOImpl.findByUsername(getCurrentLoggedUserName()).getSingleResult();
		if (endUser.getTheme() == null)
			themeResolver.setThemeName(request, response, "themeBlue");
		if (!endUser.getTheme().equalsIgnoreCase(request.getParameter("theme"))) {

			if (request.getParameter("theme").equalsIgnoreCase("themeBlue")) {
				themeResolver.setThemeName(request, response, "themeBlue");
			} else if (request.getParameter("theme").equalsIgnoreCase("themeGreen")) {
				themeResolver.setThemeName(request, response, "themeGreen");
			} else if (request.getParameter("theme").equalsIgnoreCase("themeOrange")) {
				themeResolver.setThemeName(request, response, "themeOrange");
			} else if (request.getParameter("theme").equalsIgnoreCase("themeRed")) {
				themeResolver.setThemeName(request, response, "themeRed");
			}

			endUser.setTheme(request.getParameter("theme"));
			endUserDAOImpl.update(endUser);
		} else
			themeResolver.setThemeName(request, response, endUser.getTheme());

		return "redirect:employee";
	}

	/**
	 * @author spysr_dev
	 * Method to change language
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/getLocaleLang", method = RequestMethod.GET)
	public String getLocaleLang(ModelMap model, HttpServletRequest request, HttpServletResponse response) {

		log.info("Received request for locale change");
		EndUser user = endUserDAOImpl.findByUsername(getCurrentLoggedUserName()).getSingleResult();
		LocaleResolver localeResolver = new CookieLocaleResolver();
		Locale locale = new Locale(request.getParameter("lang"));
		localeResolver.setLocale(request, response, locale);
		user.setPrefferedLanguage(request.getParameter("lang"));

		endUserDAOImpl.update(user);
		return "redirect:employee";
	}
	
	
	@RequestMapping(value = "/employee", method = RequestMethod.GET)
	public ModelAndView showadminDashBoard(ModelMap model, HttpServletRequest request, HttpServletResponse response) {

		return new ModelAndView("employee", "model", model);
	}

	@RequestMapping(value="/employeeLogindetails",method=RequestMethod.GET)
	public ModelAndView employeeDetails(ModelMap model,
			RedirectAttributes attributes) {
		EndUser user = getCurrentLoggedUserDetails();
		model.put("user", user);
		
        return new ModelAndView("employeeLogindetails", "model", model);
       }
   
	@RequestMapping(value="/leaveRequest",method=RequestMethod.GET)
	public ModelAndView LeaveRequest(ModelMap model,
			RedirectAttributes attributes) {
		EndUser user = endUserDAOImpl
				.findByUsername(getCurrentLoggedUserName()).getSingleResult();
		model.put("user", user);
	
		leaveRequestForm.setUserName(user.getUserName());
		leaveRequestForm.setEligibleLeave(user.getEligibleLeave());
		leaveRequestForm.setTotalLeave(user.getTotalLeave());
	    leaveRequestForm.setDepartment(user.getDepartment());
	    leaveRequestForm.setDesignation(user.getDesignation());
	    leaveRequestForm.setCurrentRole(user.getCurrentRole());
	    leaveRequestForm.setDoj(user.getDoj());
	    leaveRequestForm.setEmail(user.getEmail());
	    leaveRequestForm.setName(user.getName());
	    leaveRequestForm.setEmail(user.getEmail());
		model.put("leaveRequestForm", leaveRequestForm);
        return new ModelAndView("leaveRequestDetails", "model", model);
       }
	
	
	@RequestMapping(value="/insertLeaveApplied", method=RequestMethod.POST)
	public String insertLeaveAppied(@ModelAttribute ModelMap map,LeaveRequestForm leaveRequestForm){
		EndUser user=getCurrentLoggedUserDetails();
		leaveRequestForm.setCurrentRole(user.getCurrentRole());
	     leaveRequestService.Save(leaveRequestForm);
		map.put("user", user);
		map.put("leaveRequestForm", leaveRequestForm);
		return "redirect:insertLeaveSuccess";
		
	}
	
	
	
	
	  //Leave Cancel Logic
	
		@RequestMapping(value = "/employeeleavecancelDetails", method = RequestMethod.GET)
		public ModelAndView employeeLeaveAppliedDetails(ModelMap model, HttpServletRequest request,
				HttpServletResponse response) {

			EndUser user = getCurrentLoggedUserDetails();
			model.put("user", user);
			List<LeaveRequest> leaveRequestist = leaveRequestDAO.getAppliedLeaves(user.getUserName()).getResultList();

			model.put("user", user);
			if(leaveRequestist != null && leaveRequestist.size() > 0)
			{
			model.put("leaveRequestist", leaveRequestist);
			return new ModelAndView("employeeleavecancelDetails", "model", model);
			}
			else
			{
				return new ModelAndView("noDataFoundEmployee", "model", model);
			}

		}
		
		
		@RequestMapping(value = "/cancelleavesemployeeRequest", method = RequestMethod.GET)
		public ModelAndView leavePendingemployeeRequest(@RequestParam Long id, ModelMap model,
				RedirectAttributes attributes) {
			EndUser user = getCurrentLoggedUserDetails();

		
			LeaveRequest leaveRequest=leaveRequestDAO.findId(id);

			leaveRequestForm.setId(leaveRequest.getId());
		    leaveRequestForm.setUserName(leaveRequest.getUserName());
		    leaveRequestForm.setDepartment(leaveRequest.getDepartment());
		    leaveRequestForm.setDesignation(leaveRequest.getDesignation());
		    leaveRequestForm.setName(leaveRequest.getName());
		    leaveRequestForm.setDoj(leaveRequest.getDoj());
		    leaveRequestForm.setEligibleLeave(leaveRequest.getEligibleLeave());
		    leaveRequestForm.setTotalLeave(leaveRequest.getTotalLeave());
		    leaveRequestForm.setTypesofLeave(leaveRequest.getTypesofLeave());
		    leaveRequestForm.setReason(leaveRequest.getReason());
		    leaveRequestForm.setStatus(leaveRequest.getStatus());
		    leaveRequestForm.setLeaveStatus(leaveRequest.getLeaveStatus());
		    leaveRequestForm.setComment(leaveRequest.getComment());
		    leaveRequestForm.setToDate(leaveRequest.getToDate());
		    leaveRequestForm.setFromDate(leaveRequest.getFromDate());
		    leaveRequestForm.setEmail(leaveRequest.getEmail());
		
		    leaveRequestForm.setTotalnumberleaveDays(leaveRequest.getTotalnumberleaveDays());
		    leaveRequestForm.setLeaveDate(leaveRequest.getLeaveDate());
		   
			
			model.put("user", user);

			model.put("leaveRequestForm", leaveRequestForm);

			return new ModelAndView("cancelleavesemployeeRequest", "model", model);
		}

		@RequestMapping(value = "/leavecancelupdate", method = RequestMethod.POST)
		public String leavePendingemployeeRequesthowConfrim(
				@ModelAttribute LeaveRequestForm leaveRequestForm, ModelMap model,
				RedirectAttributes attributes) {
		
			LeaveRequest leaveRequest=leaveRequestDAO.findId(leaveRequestForm.getId());
			leaveRequest.setLeaveStatus(leaveRequestForm.getLeaveStatus());
			/*leaveRequest.setLeaveStatus("Cancel");*/
			leaveRequestDAO.update(leaveRequest);
			
			 model.put("leaveRequest", leaveRequest);
			 
            model.put("leaveRequestForm", leaveRequestForm);

			return "redirect:leavecancelupdate";
		}
		
		
		@RequestMapping(value="/leavecancelupdate",method=RequestMethod.GET)
		public ModelAndView leavePendingemployeeRequesthowConfrim(ModelMap model)
		{
			return new ModelAndView("leavecancelupdate","model",model);
		}	
		
	
	
	@RequestMapping(value="/insertLeaveSuccess",method=RequestMethod.GET)
	public ModelAndView insertsuccess(@ModelAttribute ModelMap model ){
		EndUser user=getCurrentLoggedUserDetails();
		model.put("user",user);
		return new ModelAndView("insertLeaveSuccess","model",model);
	}
	
	@RequestMapping(value = "/querymailEmployee", method = RequestMethod.GET)
	public ModelAndView doSendEmail1(@ModelAttribute ModelMap model) {
		EndUser user = getCurrentLoggedUserDetails();

		model.put("user", user);

		return new ModelAndView("querymailEmployee", "model", model);
	}

	/**
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/mailSender", method = RequestMethod.POST)
	public ModelAndView doSendEmail(@ModelAttribute ModelMap model,
			HttpServletRequest request) {
		EndUser user = getCurrentLoggedUserDetails();
     try{
		String recipientAddress = request.getParameter("recipient");
		String subject = request.getParameter("subject");
		String message = request.getParameter("message");

		System.out.println("To: " + recipientAddress);
		System.out.println("Subject: " + subject);
		System.out.println("Message: " + message);

		// creates a simple e-mail object
		SimpleMailMessage email = new SimpleMailMessage();
		email.setTo(recipientAddress);
		email.setSubject(subject);
		email.setText(message);
		// sends the e-mail
		mailSender.send(email);

		model.put("user", user);
     }catch(Exception e){
    	 e.getMessage();
     }
		// forwards to the view named "Result"
		return new ModelAndView("querymailSuccess", "model", model);
	}
	/**
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/listofholiDays", method = RequestMethod.GET)
	public ModelAndView listofholiDays(ModelMap model) {

		EndUser user = getCurrentLoggedUserDetails();

		List<HolidayList> holidayList = holidayDAO.holidayList().getResultList();
		if (holidayList != null && holidayList.size() > 0) {
			
			model.put("holidayList", holidayList);
			model.put("user", user);
			return new ModelAndView("listofholiDays", "model", model);

		}

		else {
			return new ModelAndView("noDataFoundEmployee", "model", model);
		}
	}
	
	@RequestMapping(value = "/projectDetailsList", method = RequestMethod.GET)
	public ModelAndView dispalyProjectDetails(ModelMap model) {

		EndUser user = getCurrentLoggedUserDetails();

		List<ProjectDetails> projectList = projectDetailsDAO.productList().getResultList();
		if (projectList != null && projectList.size() > 0) {
			
			model.put("projectList", projectList);
			model.put("user", user);
			return new ModelAndView("projectDetailsList", "model", model);

		}
       else {
			return new ModelAndView("noDataFoundEmployee", "model", model);
		}
	}
	
	
	/**
	 * @author spysr_dev
	 * Method for getting Time sheet
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/timeSheet", method = RequestMethod.GET)
	public ModelAndView timeSheet(ModelMap model) {

		EndUser user = getCurrentLoggedUserDetails();
		timeSheetForm.setUserName(user.getUserName());
		timeSheetForm.setName(user.getName());
		timeSheetForm.setRole(user.getCurrentRole());
		timeSheetForm.setEmail(user.getEmail());
		
		model.put("timeSheetForm", timeSheetForm);
		return new ModelAndView("timeSheet", "model", model);

	}
	/**
	 * @author spysr_dev
	 * Method for saving time sheet data
	 * @param model
	 * @param timeSheetForm
	 * @return
	 */
	@RequestMapping(value = "/timeSheetData", method = RequestMethod.POST)
	public String timeSheetData(ModelMap model,@ModelAttribute TimeSheetForm timeSheetForm,RedirectAttributes attributes) {
		
		Collection<TimeSheet> timeSheet = timeSheetDAO.checkData(timeSheetForm.getUserName(), timeSheetForm.getMonday());
		if ( timeSheet.size() !=0) {
			attributes.addFlashAttribute(Constants.SUCCESS, Constants.TIMESHEET);
			return "redirect:timeSheet";
		} 
		
	timeSheetService.add(timeSheetForm);
	return "redirect:dataSaved";
	}
	
	/**
	 * @author spysr_dev
	 * Method for saved
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/dataSaved", method = RequestMethod.GET)
	public ModelAndView dataSaved(ModelMap model) {
	return new ModelAndView("dataSaved","moodel",model);
	}
	
	
	/**
	 * @author spysr_dev
	 * Method for getting time sheet data
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/rejectedTimeSheet", method = RequestMethod.GET)
	public ModelAndView rejectedTimeSheet(ModelMap model) {
		
		EndUser user = getCurrentLoggedUserDetails();
		
		Collection<TimeSheet> timeSheets = timeSheetDAO.rejectedData(user.getUserName());
		 if(timeSheets != null && timeSheets.size()  > 0) {
			 model.put("timeSheets", timeSheets);
			 return new ModelAndView("rejectTimeSheetData", "model", model);
	     } else {
	    	 return new ModelAndView("noDataFoundEmployee", "model", model);
	       }
	}
	
	/**
	 * @author spysr_dev
	 * Method to get individual time sheet data
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/getData", method = RequestMethod.GET)
	public ModelAndView getData(ModelMap model, @RequestParam("id") Long id) {
	
		TimeSheetForm timeSheetForm = timeSheetService.getById(id);
		
		model.put("timeSheetForm", timeSheetForm);
		return new ModelAndView("getRejectedData","model",model);
		
	}	
	
	/**
	 * @author spysr_dev
	 * Method for updating
	 * @param model
	 * @param timeSheetForm
	 * @return
	 */
	@RequestMapping(value = "/updateData", method = RequestMethod.POST)
	public ModelAndView updateData(ModelMap model, @ModelAttribute TimeSheetForm timeSheetForm) {
         timeSheetService.updateByAdmin(timeSheetForm);
		
		return new ModelAndView("updateTS");
	}
	
	/**
	 * @author spysr_dev
	 * Method for requesting Documents
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/requestedDocs", method = RequestMethod.GET)
	public ModelAndView requestedDocs(ModelMap model) {
		
		EndUser user = getCurrentLoggedUserDetails();
		
		requestedDocsForm.setName(user.getName());
		requestedDocsForm.setUserName(user.getUserName());
		requestedDocsForm.setAddress(user.getAddress());
		requestedDocsForm.setEmail(user.getEmail());
		
		model.put("requestedDocsForm", requestedDocsForm);
		return new ModelAndView("requestedDocsForm","model",model);
		
	}
	/**
	 * Method for confirmation
	 * @param model
	 * @param requestedDocsForm
	 * @return
	 */
	@RequestMapping(value = "/confirmDoc", method = RequestMethod.POST)
  	public ModelAndView confirmDoc(ModelMap model,@ModelAttribute RequestedDocsForm requestedDocsForm) {
		model.put("requestedDocsForm", requestedDocsForm);
  		return new ModelAndView("confirmForm","model",model);
  		
		
	}
	
	/**
	 * @author spysr_dev
	 * @param model
	 * @param requestedDocsForm
	 */
	@RequestMapping(value = "/getDoc", method = RequestMethod.POST)
	public String getDoc(ModelMap model,@ModelAttribute RequestedDocsForm requestedDocsForm,HttpServletRequest request) {
		
		EndUser user = getCurrentLoggedUserDetails();
		
		Format formatter = new SimpleDateFormat("dd-MM-YYYY");
		String s = formatter.format(new Date());
		try{
			OutputStream file;
		 file = new FileOutputStream(new File(Constants.DRIVE + requestedDocsForm.getName() + "_" + requestedDocsForm.getDocName() + ".pdf"));
		Font bold = new Font(Font.FontFamily.HELVETICA, 12f, Font.BOLD);
		Font normal = new Font(Font.FontFamily.HELVETICA, 12f, Font.NORMAL);
		Document document = new Document(PageSize.A4, 36, 36, 154, 54);
		PdfWriter writer = PdfWriter.getInstance(document, file);
		
        /*HeaderFooter event = new HeaderFooter();
		writer.setPageEvent(event);*/
		document.open();
		
		Image header = Image.getInstance(request.getSession().getServletContext().getRealPath("/") + "resources/images/header.jpg");
		
		header.scalePercent(200f);
		header.setAbsolutePosition(0,(float) (PageSize.A4.getHeight() - 105.0));
		
		document.add(header);
		header.scaleToFit(600f, 500f);
        document.add(header);

		
		Paragraph ph= new Paragraph("                                                                                                                       Date: "+ s,bold);
		Paragraph ph1= new Paragraph("                                                                                                                       Bangalore,",bold);
		
		document.add(ph);
		document.add(ph1);
		
		
		String name = user.getName();
		
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		Date date = user.getDoj();
		String doj = format.format(date);
		
		if(requestedDocsForm.getDocName().equals("Home Loan"))
				{
		Chunk ch = new Chunk("KOTAK Bank,MGRoad branch,Banglore",normal);
		Phrase p = new Phrase(ch);
				
		
		float salary = user.getTotalPackage();
		
		
		
		document.add(new Paragraph("To :",bold));
		document.add(p);
		
		document.add(new Paragraph(
				"Re :",bold));
		document.add(new Paragraph(name));

		
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		
		document.add(new Paragraph(
				"We confirm the following details regarding "+name+"'s employment with SpYsR HR:",normal));
		document.add(Chunk.NEWLINE);
		document.add(new Paragraph(
				"His salary is Rs."+salary+" per annum gross.",normal));
		document.add(new Paragraph("He is employed on a permanent full time basis."));
		
		document.add(new Paragraph("He started work with us on the "+doj));
		document.add(new Paragraph("He is not on probation."));
		document.add(Chunk.NEWLINE);
		document.add(new Paragraph("Should you require any additional information, please do not hesitate to contact +91 8065777004."));
		
				}
				else
				{
					
					
					String desg = user.getDesignation();
					document.add(Chunk.NEWLINE);
					document.add(Chunk.NEWLINE);
					document.add(Chunk.NEWLINE);
					document.add(new Paragraph("This is to certify that "+name +" is working with SpYsR HR since "+doj+" and he/she holding a designation "+desg+". "));
					document.add(Chunk.NEWLINE);
					document.add(Chunk.NEWLINE);
					document.add(new Paragraph("This certificate is issued to her/his for personal use"));
					document.add(Chunk.NEWLINE);
					document.add(Chunk.NEWLINE);
					document.add(Chunk.NEWLINE);
					document.add(new Paragraph("For(SpYsR HR)"));
					document.add(Chunk.NEWLINE);
					document.add(Chunk.NEWLINE);
					document.add(Chunk.NEWLINE);
					
				}
		document.add(Chunk.NEWLINE);
		document.add(new Paragraph("Regards,"));
		document.add(Chunk.NEWLINE);
		document.add(new Paragraph("(SIGN)"));
		document.add(Chunk.NEWLINE);
		document.add(new Paragraph("Partho,"));
		document.add(new Paragraph("Manager,"));
		document.add(new Paragraph("SpYsR HR"));
				
		
		document.add(Chunk.NEWLINE);		
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);		
		document.add(Chunk.NEWLINE);
		
		
		Image footer = Image.getInstance(request.getSession().getServletContext().getRealPath("/") + "resources/images/footer.jpg");
		footer.scaleToFit(500f,500f);
       document.add(footer);
		
		document.close();
		
			MimeMessage message = mailSender.createMimeMessage();
			
				MimeMessageHelper helper = new MimeMessageHelper(message, true);

				helper.setTo(requestedDocsForm.getEmail());
				helper.setSubject("Document For " + requestedDocsForm.getDocName());
				helper.setText(String
						.format(requestedDocsForm.getName() + ",\n\n Enclosed, please find your requested document " + s
								+ "\n\n\nPartho H. Chakraborty\nSpYsR HR"));

				FileSystemResource attachment = new FileSystemResource(
						Constants.DRIVE + requestedDocsForm.getName() + "_" + requestedDocsForm.getDocName() + ".pdf");
				helper.addAttachment(attachment.getFilename(), attachment);
			
		mailSender.send(message);

		}
	   catch (MessagingException e) {
			throw new MailParseException(e);
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	return "redirect:docSend";
	}
	
	/**
	 * @author spysr_dev
	 * Method for saved
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/docSend", method = RequestMethod.GET)
	public ModelAndView docSend(ModelMap model) {
		
		return new ModelAndView("docSend","model",model);
		
	}
	
	/**
	 * @author spysr_dev
	 * Method for job offers data
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/jobOffers", method = RequestMethod.GET)
	public ModelAndView jobOffers(ModelMap model) {
		Collection<JobOffers> jobOffers = jobOffersDAO.getAll();
		if(jobOffers!=null &&jobOffers.size()>0){
			model.put("jobOffers", jobOffers);
			return new ModelAndView("jobOffersdata","model",model);
		}
		   else {
				return new ModelAndView("noDataFoundEmployee", "model", model);
			}
	}
	
	/**
	 * @author spysr_dev
	 * Method for selecting job offer
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/selectJobOffer", method = RequestMethod.GET)
	public ModelAndView selectJobOffers(ModelMap model,@RequestParam("id") long id) {
		
		JobOffers jobOffers = jobOffersDAO.getById(id);
		
		jobOffersForm.setExp(jobOffers.getExp());
		jobOffersForm.setJobTiltle(jobOffers.getJobTiltle());
		jobOffersForm.setTechnology(jobOffers.getTechnology());
		
		model.put("jobOffersForm", jobOffersForm);
		
		return new ModelAndView("selectJobOffer","model",model);
	}
	
	/**
	 * @author spysr_dev
	 * Method for uploaded
	 * @param model
	 * @param jobOffersForm
	 * @return
	 * @throws IOException
	 */
	@RequestMapping(value = "/updateOffer", method = RequestMethod.POST)
	public String updateOffer(ModelMap model,@ModelAttribute JobOffersForm jobOffersForm,BindingResult result) throws IOException{
		
		EndUser user = getCurrentLoggedUserDetails();
		
		ReferJob referJob = new ReferJob();
		referJob.setNameOfCandidate(jobOffersForm.getNameOfCandidate());
		referJob.setExp(jobOffersForm.getCandidateExp());
		referJob.setCompanyName(jobOffersForm.getCompanyName());
		
		referJob.setReferredBy(user.getName());
		
		referJob.setFormFile(jobOffersForm.getFile().getBytes());
		referJob.setForm(jobOffersForm.getFile().getOriginalFilename());
		
		referJob.setDate(new Date());
		
		referJobDAO.save(referJob);
		
		return "redirect:uploaded";
		
		
		
	}
	
	/**
	 * Method to saved display
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/uploaded", method = RequestMethod.GET)
	public ModelAndView updateOffer(ModelMap model) {
		
			return new ModelAndView("uploaded","model",model);
				
		}
	
	/**
	 * @author spysr_dev
	 * Method for getting  timesheet data
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/statusTimeSheet", method = RequestMethod.GET)
	public ModelAndView statusTimeSheet(ModelMap model) {
		EndUser user = getCurrentLoggedUserDetails();
		Collection<TimeSheet> timeSheets= timeSheetDAO.gettingOwnData(user.getUserName());
		if(timeSheets != null && timeSheets.size()  > 0) {
			 model.put("timeSheets", timeSheets);
			 return new ModelAndView("empTimeSheetData", "model", model);
	     } else {
	    	 return new ModelAndView("noDataFoundEmployee", "model", model);
	       }
	}
	
	/**
	 * @author spysr_dev
	 * Method to get individual time sheet data
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/getTimeSheet", method = RequestMethod.GET)
	public ModelAndView getTimeSheet(ModelMap model, @RequestParam("id") Long id) {
	
		TimeSheetForm timeSheetForm = timeSheetService.getById(id);
		
		model.put("timeSheetForm", timeSheetForm);
		return new ModelAndView("getEmpTimeSheetData","model",model);
		
	}
  

	
	}