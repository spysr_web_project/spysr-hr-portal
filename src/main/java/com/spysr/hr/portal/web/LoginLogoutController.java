package com.spysr.hr.portal.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.spysr.hr.portal.dao.EndUserDAO;
import com.spysr.hr.portal.entity.EndUser;
import com.spysr.hr.portal.form.EndUserForm;
import com.spysr.hr.portal.form.LoginForm;
import com.spysr.hr.portal.utility.Constants;


@Controller
@RequestMapping("/auth")
public class LoginLogoutController {

	@Autowired
	EndUserDAO endUserDAOImpl;



	protected static Logger logger = Logger.getLogger("controller");

	@RequestMapping("/login")
	public String login(
			@RequestParam(value = "error", defaultValue = "0") Short error,
			ModelMap model, HttpServletRequest request,
			@ModelAttribute LoginForm loginForm, RedirectAttributes attributes) {
		logger.debug("Received request to show login page");
		/*if(error == null) {
			error = 0;
		}*/
		if (error == 1) {
			model.put("error",
					"You have entered an invalid username or password!");
		}else if(error == 2) {
			model.put("error",
					"Your account is blocked!!! Please contact Admin");
		}else if(error == 3) {
			model.put("error","Your account is expired!!! Please contact Admin");
		}

		return "loginPage";
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String getlogout() {
		logger.debug("Received request to show denied page");

		// This will resolve to /WEB-INF/jsp/deniedpage.jsp
		return "loginPage";
	}

	@RequestMapping(value = "/denied")
	public String denied() {
		logger.debug("Received request to show Denied page");
		return "access/denied";
	}

	@RequestMapping(value = "/logout")
	public String logoutSuccess() {

		return "loginPage";
	}

	@RequestMapping(value = "/login-alert", method = RequestMethod.GET)
	public String getLoginALertPage(ModelMap model) {

		logger.debug("Received request to show login alert page");
		model.put("error",
				"You can only log-in once! Either you wait for you session to expire "
						+ "or clear your browser's cache "
						+ "or manually log-out!");

		return "login-alert-page";
	}

	@RequestMapping(value = "/forgotPassword", method = RequestMethod.GET)
	public ModelAndView forgotPassword(ModelMap model,
			@ModelAttribute EndUserForm endUserForm) {

		model.put("endUserForm", endUserForm);

		return new ModelAndView("forgotPassword", "model", model);

	}

	@RequestMapping(value = "/checkForgotPassword", method = RequestMethod.POST)
	public ModelAndView checkForgotPassword(
			@ModelAttribute EndUserForm endUserForm,
			RedirectAttributes attributes, ModelMap model) {

		ModelAndView mav = new ModelAndView();

		List<EndUser> endUser = endUserDAOImpl.findByUsernameAndEmail(
				endUserForm.getUserName(), endUserForm.getEmail())
				.getResultList();

		if (endUser.size() == 0)

		{

			/*attributes
					.addFlashAttribute("success", "Incorrect details entered");*/
			 String success=Constants.INVALID;
	            model.put("success", success);
	            attributes.addFlashAttribute(Constants.SUCCESS,Constants.INVALID);

			mav = new ModelAndView("redirect:forgotPassword");

		} else {

			endUserForm.setId(endUser.get(0).getId());

			

			model.put("endUserForm", endUserForm);

			mav = new ModelAndView("newPassword", "model", model);

		}

		return mav;
	}

	@RequestMapping(value = "/newForgotPassword", method = RequestMethod.POST)
	public String newForgotPassword(ModelMap model,
			@ModelAttribute EndUserForm endUserForm, BindingResult result,
			RedirectAttributes attributes) {

		EndUser endUser = endUserDAOImpl.findId(endUserForm.getId());

		endUser.setId(endUserForm.getId());

		endUser.setPassword(endUserForm.getNewPassword());
	

		endUserDAOImpl.update(endUser);

		model.put("endUserForm", endUserForm);

	/*	return new ModelAndView("paswwordChanged", "model", model);*/
		
		return "redirect:paswwordChanged";

	}

	
	@RequestMapping(value="/paswwordChanged", method=RequestMethod.GET)
	public ModelAndView paswwordChnaged(ModelMap model){
	return new ModelAndView("paswwordChanged", "model", model);
	}

	/**
	 * Method to display Change Password screen
	 * @param model
	 * @param endUserForm
	 * @return
	 */
	@RequestMapping(value = "/loginChangePassword", method = RequestMethod.GET)
	public ModelAndView getLoginChangePassword(ModelMap model,
											   @RequestParam Long id,
			                                   @ModelAttribute EndUserForm endUserForm) {		
		endUserForm.setId(id);
		model.put("endUserForm", endUserForm);
		return new ModelAndView("loginChangePassword", "model", model);
	}
	
	/**
	 * Method to update EndUser table with Password expiry date and New password
	 * @param model
	 * @param endUserForm
	 * @param result
	 * @param attributes
	 * @return redirect to Login Page
	 */
	@RequestMapping(value = "/updateLoginChangePwd", method = RequestMethod.POST)
	public String updateLoginChangePwd(ModelMap model,
			@ModelAttribute EndUserForm endUserForm, BindingResult result,
			RedirectAttributes attributes) {

		EndUser endUser = endUserDAOImpl.findId(endUserForm.getId());		
		if(!endUser.getPassword().equals(endUserForm.getPassword())) {
			attributes.addFlashAttribute("error","Old Password Incorrect");
			return "redirect:/auth/loginChangePassword?id="+endUser.getId();
		}else if(endUser.getPassword().equals(endUserForm.getNewPassword())) {
			attributes.addFlashAttribute("error","Old Password and New Password cannot be same");
			return "redirect:/auth/loginChangePassword?id="+endUser.getId();
		}
		endUser.setPassword(endUserForm.getNewPassword());		
	
		endUser.setPasswordFlag(1);
		
		endUserDAOImpl.update(endUser);	

		return "redirect:login";

	}

}
