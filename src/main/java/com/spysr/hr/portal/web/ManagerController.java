package com.spysr.hr.portal.web;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.ImageIcon;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailParseException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.theme.CookieThemeResolver;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfWriter;
import com.spysr.hr.portal.dao.EndUserDAO;
import com.spysr.hr.portal.dao.HolidayDAO;
import com.spysr.hr.portal.dao.JobOffersDAO;
import com.spysr.hr.portal.dao.LeaveRequestDAO;
import com.spysr.hr.portal.dao.MeetingRequestDAO;
import com.spysr.hr.portal.dao.NoticeDetailsDAO;
import com.spysr.hr.portal.dao.ProjectDetailsDAO;
import com.spysr.hr.portal.dao.ReferJobDAO;
import com.spysr.hr.portal.dao.TimeSheetDAO;
import com.spysr.hr.portal.entity.EndUser;
import com.spysr.hr.portal.entity.HolidayList;
import com.spysr.hr.portal.entity.JobOffers;
import com.spysr.hr.portal.entity.LeaveRequest;
import com.spysr.hr.portal.entity.NoticeDetails;
import com.spysr.hr.portal.entity.ProjectDetails;
import com.spysr.hr.portal.entity.ReferJob;
import com.spysr.hr.portal.entity.TimeSheet;
import com.spysr.hr.portal.form.EndUserForm;
import com.spysr.hr.portal.form.HolidayListForm;
import com.spysr.hr.portal.form.JobOffersForm;
import com.spysr.hr.portal.form.LeaveRequestForm;
import com.spysr.hr.portal.form.MeetingRequestForm;
import com.spysr.hr.portal.form.NoticeDetailsForm;
import com.spysr.hr.portal.form.ProjectDetailsForm;
import com.spysr.hr.portal.form.RequestedDocsForm;
import com.spysr.hr.portal.form.TimeSheetForm;
import com.spysr.hr.portal.services.DateService;
import com.spysr.hr.portal.services.EnduserService;
import com.spysr.hr.portal.services.HolidayService;
import com.spysr.hr.portal.services.ImageService;
import com.spysr.hr.portal.services.LeaveRequestServiceImpl;
import com.spysr.hr.portal.services.MeetingRequestService;
import com.spysr.hr.portal.services.NoticeService;
import com.spysr.hr.portal.services.ProjectDetailService;
import com.spysr.hr.portal.services.TimeSheetService;
import com.spysr.hr.portal.utility.Constants;



/*import spysr.hr.portal.dao.ReferJobDAO;*/

/*import com.spysr.hr.portal.entity.ReferJob;*/
@Controller
@RequestMapping("/managers")
public class ManagerController {

	@Autowired
	EndUserDAO endUserDAOImpl;

	@Autowired
	EndUserForm endUserForm;
	
	@Autowired
	EnduserService enduserService;
	
	@Autowired
	HolidayDAO holidayDAO;
	@Autowired
	HolidayService holidayService;
	
	@Autowired
	NoticeDetailsDAO  noticeDetailsDAO;
	
	@Autowired
	NoticeDetailsForm  noticeDetailsForm;
	
	@Autowired
	NoticeService noticeService;
	
	@Autowired
	ProjectDetailsDAO projectDetailsDAO;
	
	@Autowired
	ProjectDetailsForm projectDetailsForm;
	@Autowired
	ProjectDetailService projectDetailService;
	
	@Autowired
	MeetingRequestService meetingRequestService;
	@Autowired
	JavaMailSender mailSender;
	
	@Autowired
	LeaveRequestForm leaveRequestForm;
	
	@Autowired
	LeaveRequestDAO leaveRequestDAO;
	
	@Autowired
	LeaveRequestServiceImpl leaveRequestService;
	@Autowired	 
	HolidayListForm  holidayListForm;
	
	@Autowired
	TimeSheetForm timeSheetForm;
	
	@Autowired
	TimeSheetService timeSheetService;
	
	@Autowired
	TimeSheetDAO timeSheetDAO;
	
	@Autowired
	
	MeetingRequestForm meetingRequestForm;
	
	@Autowired
	MeetingRequestDAO notificationDAO;
	
	@Autowired
	RequestedDocsForm requestedDocsForm;
	
	@Autowired
	JobOffersDAO jobOffersDAO;
	
	@Autowired
	JobOffersForm jobOffersForm;	
	
	@Autowired
    ReferJobDAO referJobDAO;
	
	CookieThemeResolver themeResolver = new CookieThemeResolver();

	static Logger log = Logger.getLogger(AdminController.class.getName());

	private String getCurrentLoggedUserName() {
		return SecurityContextHolder.getContext().getAuthentication().getName();

	}
   /**
    * @author spysr_dev
     * Method to get current user details
     */
	private EndUser getCurrentLoggedUserDetails() {

		EndUser endUser = endUserDAOImpl.findByUsername(getCurrentLoggedUserName()).getSingleResult();

		return endUser;

	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		CustomDateEditor editor = new CustomDateEditor(dateFormat, true);
		binder.registerCustomEditor(Date.class, editor);
	}
	
	/**
	 * @author spysr_dev 
	 * Method to calling current user details
	 * @param model
	 * @return
	 */
	@ModelAttribute("requestCurrentUser")
	public EndUser getUserDetails(ModelMap model) {
		EndUser user = getCurrentLoggedUserDetails();
		if (user.getImageName() != null) {
			String type = ImageService.getImageType(user.getImageName());

			String url = "data:image/" + type + ";base64," + Base64.encodeBase64String(user.getImage());
			user.setImageName(url);
		}
		
		model.put("user", user);
		return user;
	}

	 
	@RequestMapping(value = "/manager", method = RequestMethod.GET)
	public ModelAndView showadminDashBoard(ModelMap model, HttpServletRequest request, HttpServletResponse response) {

		return new ModelAndView("manager", "model", model);
	}

	
	
	
	@RequestMapping(value = "/managerAddEmployee", method = RequestMethod.GET)
	public ModelAndView addEmployee(ModelMap model) {
		EndUser user = getCurrentLoggedUserDetails();
		model.put("user", user);
		model.put("endUserForm", endUserForm);
		return new ModelAndView("managerAddEmployee", "model", model);
		
	}
	
	@RequestMapping(value = "/insertEmployeeDetails", method = RequestMethod.POST)
	public String roleApprovalListUpdate(ModelMap model,
			@ModelAttribute EndUserForm endUserForm,
			RedirectAttributes attributes) {
		List<EndUser> endUser = endUserDAOImpl.findByUsername(endUserForm.getUserName()).getResultList();
		
		if (endUser.size() == 0) {
			EndUser user = getCurrentLoggedUserDetails();

		    endUserForm.setCreatedby(user.getCurrentRole());
		    endUserForm.setAddbyManager(user.getUserName());
		    System.out.println("total Package"+ endUserForm.getTotalPackage());
		   enduserService.Save(endUserForm); // calling service class for saving data		
      
			model.put("user", user);
			
			try {
				String email = endUserForm.getEmail();
				String name = endUserForm.getName();
				String password = endUserForm.getPassword();

				String currentRole = endUserForm.getCurrentRole();
				String tex = "Login Credentials";

				SimpleMailMessage emails = new SimpleMailMessage();
				emails.setTo(email);
				emails.setSubject(tex);
				emails.setText("Hello " + name
						+ ",\n\n An official account has been created for You. "
						+ "\n" + "\n" + "\n\n Your Login credentials are: "
						+ "\n\nUser Name:" + endUserForm.getUserName() + "\n\nPassword: " + password
					    + "\n\n\nCurrent Role: " + currentRole
						+ "\n\n\nRegards,\nManager");
				System.out.println("" + email + name);
				mailSender.send(emails);
				SimpleMailMessage emails1 = new SimpleMailMessage();
				emails1.setTo(email);
				emails1.setSubject(tex);
				emails1.setText("Hello " + name
						+ "\n\n An Official account has been created for You.\n"
						+ "\n\nRegards,\nManager");

			} catch (Exception e) {
				System.out.println(e.getMessage() + e);
			}

		
		    model.put("endUserForm", endUserForm);

		return "redirect:insertEmployeeDetails";
		
		}else{
			EndUser user = getCurrentLoggedUserDetails();
            String success=Constants.RECORDEXISTING;
            attributes.addFlashAttribute(Constants.SUCCESS, Constants.RECORDEXISTING);
            model.put("success", success);
			model.put("user", user);
			return "redirect:insertEmployeeDetails";
			
		}

	

	}
	
	
	/**
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/insertEmployeeDetails",method=RequestMethod.GET)
	public ModelAndView selectUserUpdate2(ModelMap model)
	{
		return new ModelAndView("insertEmployeeDetails","model",model);
	}	
	
	
	
	@RequestMapping(value="/managerAddEmployeeDetails",method=RequestMethod.GET)
	public ModelAndView employeeDetails(ModelMap model,
			RedirectAttributes attributes) {
		EndUser user = getCurrentLoggedUserDetails();
		model.put("user", user);
		
	  List<EndUser> userList = endUserDAOImpl.getByRoleList().getResultList();
       if(userList != null && userList.size()  > 0)	
       {
		model.put("userList", userList);
		return new ModelAndView("managerAddEmployeeDetails", "model", model);
       }

       else{
    	   return new ModelAndView("noDataFoundManager","model",model);
       }
	}
	
	@RequestMapping(value = "/selectManagerUserUpdate", method = RequestMethod.GET)
	public ModelAndView selectBuyerUpdate(ModelMap model,
			@RequestParam("id") Long id, RedirectAttributes attributes) {

		EndUser user = getCurrentLoggedUserDetails();

		EndUser enduser = endUserDAOImpl.findId(id);
	
	 /*   enduserService.Show(endUserForm);*/
		endUserForm.setId(enduser.getId());
		endUserForm.setEmail(enduser.getEmail());
		endUserForm.setContactNo(enduser.getContactNo());
		endUserForm.setUserName(enduser.getUserName());	
		endUserForm.setFatherName(enduser.getFatherName());
		endUserForm.setCurrentRole(enduser.getCurrentRole());	
		endUserForm.setRole(enduser.getRole());
		endUserForm.setAccountNo(enduser.getAccountNo());
		endUserForm.setAddress(enduser.getAddress());
		endUserForm.setCreatedby(enduser.getCreatedby());
		endUserForm.setEligibleLeave(enduser.getEligibleLeave());
		endUserForm.setTotalLeave(enduser.getTotalLeave());
		endUserForm.setBasicSalary(enduser.getBasicSalary());
		endUserForm.setDob(enduser.getDob());
		endUserForm.setDoj(enduser.getDoj());
		endUserForm.setBname(enduser.getBname());
		endUserForm.setBranch(enduser.getBranch());
		endUserForm.setConveyAllowance(enduser.getConveyAllowance());
		endUserForm.setDepartment(enduser.getDepartment());
		endUserForm.setDesignation(enduser.getDesignation());
		endUserForm.setDisplayName(enduser.getDisplayName());
		endUserForm.setSpecialAllowance(enduser.getSpecialAllowance());
		endUserForm.setFixedPackage(enduser.getFixedPackage());
		endUserForm.setFoodamountDeduct(enduser.getFoodamountDeduct());
	    endUserForm.setLocalAddress(enduser.getLocalAddress());
	    endUserForm.setPermanentAddress(enduser.getPermanentAddress());
		endUserForm.setGender(enduser.getGender());
		endUserForm.setHra(enduser.getHra());
	
		endUserForm.setMedicalAllowance(enduser.getMedicalAllowance());
		endUserForm.setName(enduser.getName());

		endUserForm.setPan(enduser.getPan());
		endUserForm.setProfessionalTax(enduser.getProfessionalTax());
	
		endUserForm.setTds(enduser.getTds());
		endUserForm.setTotalPackage(enduser.getTotalPackage());
		endUserForm.setStatus(enduser.getStatus());	
		endUserForm.setPassword(enduser.getPassword());
		endUserForm.setVariablePackage(enduser.getVariablePackage());
	    
		model.put("user", user);
        model.put("enduser", enduser);
		model.put("endUserForm", endUserForm);

		return new ModelAndView("selectManagerUserUpdate", "model", model);

	}



	@RequestMapping(value = "/selectManagerUserUpdate2", method = RequestMethod.POST)
	public String selectuserUpdate2(
			@ModelAttribute EndUserForm endUserForm, ModelMap model,
			RedirectAttributes attribute) {

		EndUser user = getCurrentLoggedUserDetails();

		EndUser enduser = endUserDAOImpl.findId(endUserForm.getId());

	    enduserService.Update(endUserForm); // calling service class for saving data		
		
		model.put("endUserForm", endUserForm);

	
	    model.put("user", user);
	    
	    model.put("enduser", enduser);
	    
		return "redirect:selectManagerUserUpdate2";
	}

	
	@RequestMapping(value="/selectManagerUserUpdate2", method=RequestMethod.GET)
		public ModelAndView selectManagerUserUpdate2(ModelMap model)
		{
			return new ModelAndView("selectManagerUserUpdate2","model",model);
		}	
	
	
	
	/**
	 * @author spysr_dev 
	 * Method to editing profile
	 * @param model,
	 *            id
	 * @return view
	 */
	@RequestMapping(value = "/editManagerProfile", method = RequestMethod.GET)
	public ModelAndView editAdminProfile(ModelMap model, @RequestParam("id") Long id, RedirectAttributes attributes) {

		EndUser userProfile = endUserDAOImpl.findId(id);

		if (userProfile.getImageName() != null) {
			String type = ImageService.getImageType(userProfile.getImageName());

			String url = "data:image/" + type + ";base64," + Base64.encodeBase64String(userProfile.getImage());
			userProfile.setImageName(url);

			endUserForm.setImageName(url);
		}
		endUserForm.setId(userProfile.getId());
		endUserForm.setDisplayName(userProfile.getDisplayName());
		endUserForm.setUserName(userProfile.getUserName());
	
		endUserForm.setContactNo(userProfile.getContactNo());
		endUserForm.setEmail(userProfile.getEmail());

		endUserForm.setPassword(userProfile.getPassword());
		

		model.put("endUserForm", endUserForm);

		return new ModelAndView("editManagerProfile", "model", model);

	}
	
	/**
	 * @author spysr_dev 
	 * Method to updating profile image
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/updateImageForProfile", method = RequestMethod.POST)
	public String updateImageForProfile(ModelMap model, @ModelAttribute EndUserForm endUserForm) {

		try {
			EndUser userProfile = endUserDAOImpl.findId(endUserForm.getId());
			userProfile.setImage(endUserForm.getFile().getBytes());
			userProfile.setImageName(endUserForm.getFile().getOriginalFilename());
			endUserDAOImpl.update(userProfile);

		} catch (Exception e) {
			e.getMessage();
		}
		return "redirect:editManagerProfile?id=" + endUserForm.getId();
	}

	/**
	 * @author spysr_dev
	 * Method to confirm edit profile
	 * 
	 * @param model
	 * @return
	 */
	/*@RequestMapping(value = "/confirmEditManagerProfile", method = RequestMethod.POST)
	public ModelAndView confirmEditAdminProfile(ModelMap model, @ModelAttribute EndUserForm endUserForm) {

		model.put("endUserForm", endUserForm);

		return new ModelAndView("confirmEditManagerProfile", "model", model);

	}*/
    
	/**
	 * @author spysr_dev
	 * Method to updating profile details
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/updateManagerDetails", method = RequestMethod.POST)
	public String updateAdminDetails(ModelMap model, @ModelAttribute EndUserForm endUserForm,
			BindingResult result, RedirectAttributes attributes) {

		EndUser endUser = endUserDAOImpl.findId(endUserForm.getId());

		endUser.setId(endUserForm.getId());

		endUser.setDisplayName(endUserForm.getDisplayName());
		endUser.setContactNo(endUserForm.getContactNo());
		
		endUser.setEmail(endUserForm.getEmail());
	
		endUser.setUserName(endUserForm.getUserName());
		

		endUserDAOImpl.update(endUser);

		model.put("endUserForm", endUserForm);

		return "redirect:updateManagerSuccess";

	}
	@RequestMapping(value="/updateManagerSuccess",method=RequestMethod.GET)
	public ModelAndView updateManagerSuccess(ModelMap model)
	{
		return new ModelAndView("updateManagerSuccess","model",model);
	}	
	
	/**
	 * @author spysr_dev
	 * Method to edit password details
	 * 
	 * @param model,id
	 * @return
	 */
	@RequestMapping(value = "/editEManagerPWD", method = RequestMethod.GET)
	public ModelAndView editAdminPWD(ModelMap model, @RequestParam("id") Long id, RedirectAttributes attributes) {

		EndUser userProfile = endUserDAOImpl.findId(id);

		endUserForm.setId(userProfile.getId());

	

		model.put("endUserForm", endUserForm);

		return new ModelAndView("editManagerPWD", "model", model);

	}

	/**
	 * @author spysr_dev
	 * Method to updating password details
	 * 
	 * @param model,
	 *            enduserForm
	 * @return
	 */
	@RequestMapping(value = "/updateEditManagerPWD", method = RequestMethod.POST)
	public ModelAndView updateEditAdminPWD(ModelMap model, @ModelAttribute EndUserForm endUserForm,
			BindingResult result, RedirectAttributes attributes) {

		EndUser endUser = endUserDAOImpl.findId(endUserForm.getId());

		endUser.setId(endUserForm.getId());

		endUser.setPassword(endUserForm.getNewPassword());
	

		endUserDAOImpl.update(endUser);

		model.put("endUserForm", endUserForm);

		return new ModelAndView("updateManagerSuccess", "model", model);

	}
    
	/**
	 * @author spysr_dev
	 * Method to change theme
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/themeChange", method = RequestMethod.GET)
	public String getThemeChange(ModelMap model, HttpServletRequest request, HttpServletResponse response) {

		log.info("Received request for theme change");

		EndUser endUser = endUserDAOImpl.findByUsername(getCurrentLoggedUserName()).getSingleResult();
		if (endUser.getTheme() == null)
			themeResolver.setThemeName(request, response, "themeBlue");
		if (!endUser.getTheme().equalsIgnoreCase(request.getParameter("theme"))) {

			if (request.getParameter("theme").equalsIgnoreCase("themeBlue")) {
				themeResolver.setThemeName(request, response, "themeBlue");
			} else if (request.getParameter("theme").equalsIgnoreCase("themeGreen")) {
				themeResolver.setThemeName(request, response, "themeGreen");
			} else if (request.getParameter("theme").equalsIgnoreCase("themeOrange")) {
				themeResolver.setThemeName(request, response, "themeOrange");
			} else if (request.getParameter("theme").equalsIgnoreCase("themeRed")) {
				themeResolver.setThemeName(request, response, "themeRed");
			}

			endUser.setTheme(request.getParameter("theme"));
			endUserDAOImpl.update(endUser);
		} else
			themeResolver.setThemeName(request, response, endUser.getTheme());

		return "redirect:manager";
	}

	/**
	 * @author spysr_dev
	 * Method to change language
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/getLocaleLang", method = RequestMethod.GET)
	public String getLocaleLang(ModelMap model, HttpServletRequest request, HttpServletResponse response) {

		log.info("Received request for locale change");
		EndUser user = endUserDAOImpl.findByUsername(getCurrentLoggedUserName()).getSingleResult();
		LocaleResolver localeResolver = new CookieLocaleResolver();
		Locale locale = new Locale(request.getParameter("lang"));
		localeResolver.setLocale(request, response, locale);
		user.setPrefferedLanguage(request.getParameter("lang"));

		endUserDAOImpl.update(user);
		return "redirect:manager";
	}
	
	
	
	
	@RequestMapping(value = "/showMail", method = RequestMethod.GET)
	public ModelAndView doSendEmail1(@ModelAttribute ModelMap model) {
		EndUser user = getCurrentLoggedUserDetails();

		model.put("user", user);

		return new ModelAndView("queryMail", "model", model);
	}

	/**
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/mailSender", method = RequestMethod.POST)
	public ModelAndView doSendEmail(@ModelAttribute ModelMap model,
			HttpServletRequest request) {
		EndUser user = getCurrentLoggedUserDetails();
       try{
		String recipientAddress = request.getParameter("recipient");
		String subject = request.getParameter("subject");
		String message = request.getParameter("message");

		System.out.println("To: " + recipientAddress);
		System.out.println("Subject: " + subject);
		System.out.println("Message: " + message);

		// creates a simple e-mail object
		SimpleMailMessage email = new SimpleMailMessage();
		email.setTo(recipientAddress);
		email.setSubject(subject);
		email.setText(message);
		// sends the e-mail
		mailSender.send(email);

		model.put("user", user);
       } catch (Exception e) {
			e.getMessage();
		}

		// forwards to the view named "Result"
		return new ModelAndView("result", "model", model);
	}
	
	/**
	 * @param model
	 * @return
	 * @throws ParseException 
	 */
	@RequestMapping(value = "/holidayList", method = RequestMethod.GET)
	public ModelAndView holidayList(ModelMap model) throws ParseException {

		EndUser user = getCurrentLoggedUserDetails();

		List<HolidayList> holidayList = holidayDAO.holidayList().getResultList();
		if (holidayList != null && holidayList.size() > 0) {
			
		  for (HolidayList holiday : holidayList) {
				Date date1 = holiday.getHolidayDate();
		DateFormat dateFormat = new SimpleDateFormat("d/M/yyyy");
	
		Date date = new Date();
		String sysdat = dateFormat.format(date);

		Date end = new SimpleDateFormat("d/M/yyyy").parse(sysdat);
	
		if (date1 != null) {
			String dat1 = dateFormat.format(date1);
			Date bdate1 = new SimpleDateFormat("d/M/yyyy").parse(dat1);
			if (bdate1.compareTo(end) < 0) {
				String colour1 = "green";
				model.put("colour1", colour1);

			} else {
				String colour1 = "red";
				model.put("colour1", colour1);
			}
		}
		 }
			model.put("user", user);
			model.put("holidayList", holidayList);
			
		}
		return new ModelAndView("holidayList", "model", model);
	}
		
		
	

	/**
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/addholidays", method = RequestMethod.GET)
	public ModelAndView addHoliday(ModelMap model) {
		EndUser user = getCurrentLoggedUserDetails();
		model.put("user", user);
		model.put("holidayListForm", holidayListForm);
		return new ModelAndView("addholidays", "model", model);
		
	}
	
	/**
	 * @param model
	 * @param holidayListForm
	 * @param attributes
	 * @return
	 */
	@RequestMapping(value = "/insertholiDays", method = RequestMethod.POST)
	public String insertHoliday(ModelMap model,
			@ModelAttribute HolidayListForm holidayListForm,
			RedirectAttributes attributes) {
		List<HolidayList> holidayList = holidayDAO.findByHolidayName(holidayListForm.getSerialNo()).getResultList();
		
		if (holidayList.size() == 0) {
			EndUser user = getCurrentLoggedUserDetails();
		
			holidayService.Save(holidayListForm);// calling service class for saving data		

			model.put("user", user);
		    model.put("holidayListForm", holidayListForm);

		return "redirect:insertholiDays";
		
		}else{
			EndUser user = getCurrentLoggedUserDetails();

			   String success=Constants.RECORDEXISTING;
			    model.addAttribute("success", success);
			    attributes.addFlashAttribute(Constants.SUCCESS,Constants.RECORDEXISTING);

			model.put("holidayList", holidayList);
			model.put("user", user);
			model.put("holidayListForm", holidayListForm);
			return "redirect:addholidays";
			
		}

	}
	
	@RequestMapping(value="/insertholiDays",method=RequestMethod.GET)
	public ModelAndView insertholiDays(ModelMap model,RedirectAttributes attributes) {
		EndUser user = getCurrentLoggedUserDetails();
		model.put("user", user);
		
        return new ModelAndView("insertholiDays", "model", model);
       }
	
	
	
	

	@RequestMapping(value = "/selectholidayUpdate", method = RequestMethod.GET)
	public ModelAndView selectholidayUpdate(ModelMap model,
			@RequestParam("id") Long id, RedirectAttributes attributes) {

		HolidayList holiday =holidayDAO.findId(id);
	   holidayListForm.setId(holiday.getId());
	   holidayListForm.setSerialNo(holiday.getSerialNo());
	   holidayListForm.setHolidayName(holiday.getHolidayName());
	   holidayListForm.setHolidayDate(holiday.getHolidayDate());
        model.put("holiday", holiday);
		model.put("holidayListForm", holidayListForm);

		return new ModelAndView("selectholidayUpdate", "model", model);

	}



	
	@RequestMapping(value = "/selectholidayUpdate2", method = RequestMethod.POST)
	public String selectholidayUpdate2(
			@ModelAttribute HolidayListForm holidayListForm, ModelMap model,
			RedirectAttributes attribute) {

		HolidayList holiday =holidayDAO.findId(holidayListForm.getId());

		 holidayService.Update(holidayListForm);	
		
		model.put("holidayListForm", holidayListForm);

	
	  
	    model.put("holiday", holiday);
	    
	    return "redirect:selectholidayUpdate2";
	}

	/**
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/selectholidayUpdate2",method=RequestMethod.GET)
	public ModelAndView selectholidayUpdate2(ModelMap model)
	{
		return new ModelAndView("selectholidayUpdate2","model",model);
	}	
	
	
	
	
	
	@RequestMapping(value = "/dispalyProjectDetails", method = RequestMethod.GET)
	public ModelAndView dispalyProjectDetails(ModelMap model) {

		EndUser user = getCurrentLoggedUserDetails();

		List<ProjectDetails> projectList = projectDetailsDAO.productList().getResultList();
		if (projectList != null && projectList.size() > 0) {
			
			model.put("projectList", projectList);
			model.put("user", user);
		
		}
		return new ModelAndView("dispalyProjectDetails", "model", model);
		
	}
	
	//Edit
	@RequestMapping(value = "/selectprojectUpdate", method = RequestMethod.GET)
	public ModelAndView selectprojectUpdate(ModelMap model,
			@RequestParam("id") Long id, RedirectAttributes attributes) {

	    ProjectDetails project=projectDetailsDAO.findId(id);
	   projectDetailsForm.setId(project.getId());
	   projectDetailsForm.setSerialNo(project.getSerialNo());
	   projectDetailsForm.setProjectDetails(project.getProjectDetails());
	   projectDetailsForm.setMembersInvolved(project.getMembersInvolved());
	   projectDetailsForm.setProjectMembers(project.getProjectMembers());
	   projectDetailsForm.setVersion(project.getVersion());
	   projectDetailsForm.setProjectName(project.getProjectName());
        model.put("project", project);
		model.put("projectDetailsForm", projectDetailsForm);

		return new ModelAndView("selectprojectUpdate", "model", model);

	}



	
	@RequestMapping(value = "/selectprojectUpdate2", method = RequestMethod.POST)
	public String selectprojectUpdate2(@ModelAttribute ProjectDetailsForm projectDetailsForm, ModelMap model,
			RedirectAttributes attribute) {

		ProjectDetails project = projectDetailsDAO.findId(projectDetailsForm.getId());

		EndUser user = getCurrentLoggedUserDetails();

		projectDetailService.Update(projectDetailsForm);
		model.put("project", project);
		model.put("user", user);
		model.put("projectDetailsForm", projectDetailsForm);

		return "redirect:selectprojectUpdate2";

	}
	

	/**
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/selectprojectUpdate2",method=RequestMethod.GET)
	public ModelAndView selectprojectUpdate2(ModelMap model)
	{
		return new ModelAndView("selectprojectUpdate2","model",model);
	}	
	
	

	
	@RequestMapping(value = "/addmanagerProjects", method = RequestMethod.GET)
	public ModelAndView addmanagerProjects(ModelMap model) {
		EndUser user = getCurrentLoggedUserDetails();
		
		List<EndUser> empList = endUserDAOImpl.getEmployees().getResultList();
		
		 if(empList != null && empList.size()  > 0) {
			 model.put("empList", empList);
	     } 
		
		model.put("user", user);
		
		model.put("projectDetailsForm", projectDetailsForm);
		
		return new ModelAndView("addmanagerProjects", "model", model);
		
	}
	
	
	@RequestMapping(value = "/insertManagerProjects", method = RequestMethod.POST)
	public String insertHoliday(ModelMap model,
			@ModelAttribute ProjectDetailsForm projectDetailsForm,
			RedirectAttributes attributes) {
		List<ProjectDetails> productList = projectDetailsDAO.findByProductName(projectDetailsForm.getSerialNo()).getResultList();
		
		if (productList.size() == 0) {
			EndUser user = getCurrentLoggedUserDetails();
		
			projectDetailService.Save(projectDetailsForm);// calling service class for saving data		

			model.put("user", user);
		    model.put("projectDetailsForm", projectDetailsForm);

		return "redirect:insertManagerProjects";
		
		}else{
			EndUser user = getCurrentLoggedUserDetails();
            String success=Constants.RECORDEXISTING;
            model.put("success", success);
            attributes.addFlashAttribute(Constants.SUCCESS, Constants.RECORDEXISTING);

			model.put("productList", productList);
			model.put("user", user);
			model.put("projectDetailsForm", projectDetailsForm);
			return "redirect:addmanagerProjects";
			
		}

	}
	
	@RequestMapping(value="/insertManagerProjects",method=RequestMethod.GET)
	public ModelAndView insertManagerProjects(ModelMap model,RedirectAttributes attributes) {
		EndUser user = getCurrentLoggedUserDetails();
		model.put("user", user);
		
        return new ModelAndView("insertManagerProjects", "model", model);
       }
	
	/**Notice Details
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/managerNotice", method = RequestMethod.GET)
	public ModelAndView noticeDetails(ModelMap model) {

		EndUser user = getCurrentLoggedUserDetails();

		List<NoticeDetails> noticeList =noticeDetailsDAO.noticeList().getResultList();
		if (noticeList != null && noticeList.size() > 0) {
			
			model.put("noticeList", noticeList);

		}
		model.put("user", user);
		return new ModelAndView("managerNotice", "model", model);

	}


	/**add notice Details
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/addnoticemanager", method = RequestMethod.GET)
	public ModelAndView addnoticeDetails(ModelMap model) {
		EndUser user = getCurrentLoggedUserDetails();
		
		model.put("user", user);
		
		model.put("noticeDetailsForm", noticeDetailsForm);
		
		return new ModelAndView("addnoticemanager", "model", model);
		
	}
	
	
	/**save data for Notice Details
	 * @param model
	 * @param projectDetailsForm
	 * @param attributes
	 * @return
	 */
	@RequestMapping(value = "/insertnoticeManager", method = RequestMethod.POST)
	public String insertnoticeDetails(ModelMap model,
			@ModelAttribute NoticeDetailsForm  noticeDetailsForm,
			RedirectAttributes attributes) {
		List<NoticeDetails> noticeList = noticeDetailsDAO.findByNoticeDetailsName(noticeDetailsForm.getSerialNo()).getResultList();
		
		if (noticeList.size() == 0) {
			EndUser user = getCurrentLoggedUserDetails();
			
		   noticeService.Save(noticeDetailsForm);
		
            model.put("user", user);
		    model.put("noticeDetailsForm", noticeDetailsForm);

		return "redirect:insertnoticeManager";
		
		
		
		}else{
			EndUser user = getCurrentLoggedUserDetails();
          String success=Constants.RECORDEXISTING;
          model.addAttribute("success", success);
      	   attributes.addFlashAttribute(Constants.SUCCESS,Constants.RECORDEXISTING);

			model.put("noticeList", noticeList);
			model.put("user", user);
			model.put("noticeDetailsForm", noticeDetailsForm);
			return "redirect:addnoticemanager";
			
		}

	}
	
	/**Redirect Page
	 * @param model
	 * @param attributes
	 * @return
	 */
	@RequestMapping(value="/insertnoticeManager",method=RequestMethod.GET)
	public ModelAndView insertnoticeDetails(ModelMap model,RedirectAttributes attributes) {
		EndUser user = getCurrentLoggedUserDetails();
		model.put("user", user);
		
        return new ModelAndView("insertnoticeManager", "model", model);
       }
	
	//Edit Notice Details
	
	@RequestMapping(value = "/selectnoticemanagerUpdate", method = RequestMethod.GET)
		public ModelAndView selectnoticeUpdate(ModelMap model,
				@RequestParam("id") Long id, RedirectAttributes attributes) {

		   NoticeDetails noticeDetails=noticeDetailsDAO.findId(id);
		   noticeDetailsForm.setId(noticeDetails.getId());
		   noticeDetailsForm.setSerialNo(noticeDetails.getSerialNo());
		   noticeDetailsForm.setDescription(noticeDetails.getDescription());
		   noticeDetailsForm.setTitle(noticeDetails.getTitle());
		   noticeDetailsForm.setNoticeDate(noticeDetails.getNoticeDate());
	        model.put("noticeDetails", noticeDetails);
			model.put("noticeDetailsForm", noticeDetailsForm);

			return new ModelAndView("selectnoticemanagerUpdate", "model", model);

		}



		
	@RequestMapping(value = "/selectnoticemanagerUpdate2", method = RequestMethod.POST)
	public String selectprojectUpdate2(@ModelAttribute NoticeDetailsForm noticeDetailsForm, ModelMap model,
			RedirectAttributes attribute) {

		NoticeDetails noticeDetails = noticeDetailsDAO.findId(noticeDetailsForm.getId());

		noticeService.Update(noticeDetailsForm);

		model.put("projectDetailsForm", projectDetailsForm);

		model.put("noticeDetails", noticeDetails);

		return "redirect:selectnoticemanagerUpdate2";
	}

		/**
		 * @param model
		 * @return
		 */
		@RequestMapping(value="/selectnoticemanagerUpdate2",method=RequestMethod.GET)
		public ModelAndView selectnoticemanagerUpdate2(ModelMap model)
		{
			return new ModelAndView("selectnoticemanagerUpdate2","model",model);
		}	

	
	/**	Applied Leave
	 * @param model
	 * @param attributes
	 * @return
	 */
	@RequestMapping(value="/leaverequestManager",method=RequestMethod.GET)
	public ModelAndView LeaveRequest(ModelMap model,
			RedirectAttributes attributes) {
		EndUser user = endUserDAOImpl
				.findByUsername(getCurrentLoggedUserName()).getSingleResult();
		model.put("user", user);
	
		leaveRequestForm.setUserName(user.getUserName());
		leaveRequestForm.setEligibleLeave(user.getEligibleLeave());
		leaveRequestForm.setTotalLeave(user.getTotalLeave());
	    leaveRequestForm.setDepartment(user.getDepartment());
	    leaveRequestForm.setDesignation(user.getDesignation());
	    leaveRequestForm.setCurrentRole(user.getCurrentRole());
	    leaveRequestForm.setDoj(user.getDoj());
	    leaveRequestForm.setEmail(user.getEmail());
	    leaveRequestForm.setName(user.getName());
	    leaveRequestForm.setEmail(user.getEmail());
		model.put("leaveRequestForm", leaveRequestForm);
        return new ModelAndView("leaverequestManager", "model", model);
       }
	
	
	@RequestMapping(value="/insertleaveAppliedManager", method=RequestMethod.POST)
	public String insertLeaveAppied(@ModelAttribute ModelMap map,LeaveRequestForm leaveRequestForm){
		EndUser user=getCurrentLoggedUserDetails();
		leaveRequestForm.setCurrentRole(user.getCurrentRole());
	     leaveRequestService.Save(leaveRequestForm);
		map.put("user", user);
		map.put("leaveRequestForm", leaveRequestForm);
		return "redirect:insertLeaveManagerSuccess";
		
	}
	
	@RequestMapping(value="/insertLeaveManagerSuccess", method=RequestMethod.GET)
	public ModelAndView insertLeaveManagerSuccess(@ModelAttribute ModelMap model,LeaveRequestForm leaveRequestForm){
		EndUser user=getCurrentLoggedUserDetails();
		model.put("user", user);
		model.put("leaveRequestForm", leaveRequestForm);
		return new ModelAndView("insertLeaveManagerSuccess", "model", model);
		
	}
	
	
	  //Leave Approved/Rejected Logic
	
		@RequestMapping(value = "/employeeleaveDetails", method = RequestMethod.GET)
		public ModelAndView employeeLeaveAppliedDetails(ModelMap model, HttpServletRequest request,
				HttpServletResponse response) {

			EndUser user = getCurrentLoggedUserDetails();
			model.put("user", user);
			/*List<LeaveRequest> leaveRequestist = leaveRequestDAO.getAppliedLeaves(user.getUserName()).getResultList();*/
			List<LeaveRequest> leaveRequestist = leaveRequestDAO.getLeaveRequestDetailsmanager().getResultList();
			/*getLeaveRequestDetails*/
			model.put("user", user);
			if(leaveRequestist != null && leaveRequestist.size() > 0)
			{
			model.put("leaveRequestist", leaveRequestist);
			return new ModelAndView("employeeleaveDetails", "model", model);
			}
			else
			{
				return new ModelAndView("noDataFoundManager", "model", model);
			}

		}
		
		
	@RequestMapping(value = "/leavependingemployeeRequest", method = RequestMethod.GET)
	public ModelAndView leavePendingemployeeRequest(@RequestParam Long id, ModelMap model,
			RedirectAttributes attributes) {
		EndUser user = getCurrentLoggedUserDetails();

		LeaveRequest leaveRequest = leaveRequestDAO.findId(id);

		leaveRequestForm.setId(leaveRequest.getId());
		leaveRequestForm.setUserName(leaveRequest.getUserName());
		leaveRequestForm.setDepartment(leaveRequest.getDepartment());
		leaveRequestForm.setDesignation(leaveRequest.getDesignation());
		leaveRequestForm.setName(leaveRequest.getName());
		leaveRequestForm.setDoj(leaveRequest.getDoj());
		leaveRequestForm.setEligibleLeave(leaveRequest.getEligibleLeave());
		leaveRequestForm.setTotalLeave(leaveRequest.getTotalLeave());
		leaveRequestForm.setTypesofLeave(leaveRequest.getTypesofLeave());
		leaveRequestForm.setReason(leaveRequest.getReason());
		leaveRequestForm.setStatus(leaveRequest.getStatus());
		leaveRequestForm.setLeaveStatus(leaveRequest.getLeaveStatus());
		leaveRequestForm.setComment(leaveRequest.getComment());
		leaveRequestForm.setToDate(leaveRequest.getToDate());
		leaveRequestForm.setFromDate(leaveRequest.getFromDate());
		leaveRequestForm.setEmail(leaveRequest.getEmail());
		leaveRequestForm.setLeaveLength(leaveRequest.getLeaveLength());
		/* leaveRequestForm.setNoofdays(leaveRequest.getNoofdays()); */
		leaveRequestForm.setTotalnumberleaveDays(leaveRequest.getTotalnumberleaveDays());
		leaveRequestForm.setLeaveDate(leaveRequest.getLeaveDate());

		model.put("user", user);

		model.put("leaveRequestForm", leaveRequestForm);

		return new ModelAndView("leavependingemployeeRequest", "model", model);
	}

	@RequestMapping(value = "/leavependingRequesthowConfrim", method = RequestMethod.POST)
	public String leavePendingemployeeRequesthowConfrim(@ModelAttribute LeaveRequestForm leaveRequestForm,
			ModelMap model, RedirectAttributes attributes) {

		LeaveRequest leaveRequest = leaveRequestDAO.findId(leaveRequestForm.getId());

		leaveRequest.setStatus(leaveRequestForm.getStatus());
		leaveRequest.setComment(leaveRequestForm.getComment());
		leaveRequestDAO.update(leaveRequest);

		try {
			String status = leaveRequestForm.getStatus();
			String username = leaveRequestForm.getUserName();
			String email = leaveRequestForm.getEmail();
			/* int noofdays=leaveRequestForm.getNoofdays(); */
			Date LeaveDate = leaveRequestForm.getLeaveDate();
			String comment = leaveRequestForm.getComment();

			if (status.equals("Approved")) {
				String tex = "Leave Details Approved  Details Notification";
				SimpleMailMessage emails = new SimpleMailMessage();
				emails.setTo(email);
				emails.setSubject(tex);
				emails.setText("Hello " + username + ",\n\n Your Leave Applied Request has been Created. "
						+ ",\n\n Your Details are as follows. " + "\n" + "\n"

						+ "\n\nUserName:" + username + "\n\nStatus:" + status + "\n\nLeave Date:" + LeaveDate
						+ "\n\n\nRegards,\n Manager");
				System.out.println("" + email + username);
				mailSender.send(emails);
				SimpleMailMessage emails1 = new SimpleMailMessage();
				emails1.setTo(email);

			} else if (status.equals("Rejected")) {
				String tex = "Leave Details Rejection notification";
				SimpleMailMessage emails = new SimpleMailMessage();
				emails.setTo(email);
				emails.setSubject(tex);
				emails.setText("Hello " + username + ",\n\n Your Account has been Rejected  " + "\n\n Comment:"
						+ comment + "\n" + "\n" + "\n\n\nRegards,\nAdmin");
				mailSender.send(emails);
			} else {
				String tex = "Leave Details Pending notification";
				SimpleMailMessage emails = new SimpleMailMessage();
				emails.setTo(email);
				emails.setSubject(tex);
				emails.setText("Hello " + username + ",\n\n  Buyer Details has been Pending " + "\n" + "\n"
						+ "\n\n\nRegards,\nAdmin");
				mailSender.send(emails);
			}

			model.put("leaveRequestForm", leaveRequestForm);

		} catch (Exception e) {
			System.out.println(e.getMessage() + e);
		}

		return "redirect:leavependingRequesthowConfrim";
	}
		
		
		@RequestMapping(value="/leavependingRequesthowConfrim",method=RequestMethod.GET)
		public ModelAndView leavePendingemployeeRequesthowConfrim(ModelMap model)
		{
			return new ModelAndView("leavependingRequesthowConfrim","model",model);
		}	
		
		
		
		  //Leave Cancel Logic
		
			@RequestMapping(value = "/managerleavecancelDetails", method = RequestMethod.GET)
			public ModelAndView employeeleavecancelDetails(ModelMap model, HttpServletRequest request,
					HttpServletResponse response) {

				EndUser user = getCurrentLoggedUserDetails();
				model.put("user", user);
				List<LeaveRequest> leaveRequestist = leaveRequestDAO.getAppliedLeaves(user.getUserName()).getResultList();

				model.put("user", user);
				if(leaveRequestist != null && leaveRequestist.size() > 0)
				{
				model.put("leaveRequestist", leaveRequestist);
				return new ModelAndView("managerleavecancelDetails", "model", model);
				}
				else
				{
					return new ModelAndView("noDataFoundManager", "model", model);
				}

			}
			
			
			@RequestMapping(value = "/cancelleavesmanagerRequest", method = RequestMethod.GET)
			public ModelAndView cancelleavesemployeeRequest(@RequestParam Long id, ModelMap model,
					RedirectAttributes attributes) {
				EndUser user = getCurrentLoggedUserDetails();

			
				LeaveRequest leaveRequest=leaveRequestDAO.findId(id);

				leaveRequestForm.setId(leaveRequest.getId());
			    leaveRequestForm.setUserName(leaveRequest.getUserName());
			    leaveRequestForm.setDepartment(leaveRequest.getDepartment());
			    leaveRequestForm.setDesignation(leaveRequest.getDesignation());
			    leaveRequestForm.setName(leaveRequest.getName());
			    leaveRequestForm.setDoj(leaveRequest.getDoj());
			    leaveRequestForm.setEligibleLeave(leaveRequest.getEligibleLeave());
			    leaveRequestForm.setTotalLeave(leaveRequest.getTotalLeave());
			    leaveRequestForm.setTypesofLeave(leaveRequest.getTypesofLeave());
			    leaveRequestForm.setReason(leaveRequest.getReason());
			    leaveRequestForm.setStatus(leaveRequest.getStatus());
			    leaveRequestForm.setLeaveStatus(leaveRequest.getLeaveStatus());
			    leaveRequestForm.setComment(leaveRequest.getComment());
			    leaveRequestForm.setToDate(leaveRequest.getToDate());
			    leaveRequestForm.setFromDate(leaveRequest.getFromDate());
			    leaveRequestForm.setEmail(leaveRequest.getEmail());
			
			    leaveRequestForm.setTotalnumberleaveDays(leaveRequest.getTotalnumberleaveDays());
			    leaveRequestForm.setLeaveDate(leaveRequest.getLeaveDate());
			   
				
				model.put("user", user);

				model.put("leaveRequestForm", leaveRequestForm);

				return new ModelAndView("cancelleavesmanagerRequest", "model", model);
			}

			@RequestMapping(value = "/leavecancelmanagerupdate", method = RequestMethod.POST)
			public String leavecancelupdate(
					@ModelAttribute LeaveRequestForm leaveRequestForm, ModelMap model,
					RedirectAttributes attributes) {
			
				LeaveRequest leaveRequest=leaveRequestDAO.findId(leaveRequestForm.getId());
				
				leaveRequest.setLeaveStatus(leaveRequestForm.getLeaveStatus());
				
				/*leaveRequest.setLeaveStatus("Cancel");*/
				leaveRequestDAO.update(leaveRequest);
				
				 model.put("leaveRequest", leaveRequest);
				 
	            model.put("leaveRequestForm", leaveRequestForm);

				return "redirect:leavecancelmanagerupdate";
			}
			
			
			@RequestMapping(value="/leavecancelmanagerupdate",method=RequestMethod.GET)
			public ModelAndView leavecancelupdate(ModelMap model)
			{
				return new ModelAndView("leavecancelmanagerupdate","model",model);
			}	
			
		
		
		@RequestMapping(value="/insertLeaveSuccess",method=RequestMethod.GET)
		public ModelAndView insertsuccess(@ModelAttribute ModelMap model ){
			EndUser user=getCurrentLoggedUserDetails();
			model.put("user",user);
			return new ModelAndView("insertLeaveSuccess","model",model);
		}
	
	
	/**
	 * @author spysr_dev
	 * Method for getting Time sheet
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/timeSheet", method = RequestMethod.GET)
	public ModelAndView timeSheet(ModelMap model) {

		EndUser user = getCurrentLoggedUserDetails();
		timeSheetForm.setUserName(user.getUserName());
		timeSheetForm.setName(user.getName());
		timeSheetForm.setRole(user.getCurrentRole());
		timeSheetForm.setEmail(user.getEmail());
		
		model.put("timeSheetForm", timeSheetForm);
		return new ModelAndView("managerTimeSheet", "model", model);

	}
	
	/**
	 * @author spysr_dev
	 * Method for saving time sheet data
	 * @param model
	 * @param timeSheetForm
	 * @return
	 */
	@RequestMapping(value = "/timeSheetData", method = RequestMethod.POST)
	public String timeSheetData(ModelMap model,@ModelAttribute TimeSheetForm timeSheetForm) {
		
	timeSheetService.add(timeSheetForm);
	return "redirect:dataSaved";
	}
	
	/**
	 * @author spysr_dev
	 * Method for saved
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/dataSaved", method = RequestMethod.GET)
	public ModelAndView dataSaved(ModelMap model) {
	return new ModelAndView("mDataSaved","moodel",model);
	}
	
	/**
	 * @author spysr_dev
	 * Method for getting time sheet data
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/rejectedMngTimeSheet", method = RequestMethod.GET)
	public ModelAndView rejectedTimeSheet(ModelMap model) {
		
		EndUser user = getCurrentLoggedUserDetails();
		
		Collection<TimeSheet> timeSheets = timeSheetDAO.rejectedData(user.getUserName());
		 if(timeSheets != null && timeSheets.size()  > 0) {
			 model.put("timeSheets", timeSheets);
			 return new ModelAndView("rejectMngTimeSheetData", "model", model);
	     } else {
	    	 return new ModelAndView("noDataFoundManager", "model", model);
	       }
	}
	
	/**
	 * @author spysr_dev
	 * Method to get individual time sheet data
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/getData", method = RequestMethod.GET)
	public ModelAndView getData(ModelMap model, @RequestParam("id") Long id) {
	
		TimeSheetForm timeSheetForm = timeSheetService.getById(id);
		
		model.put("timeSheetForm", timeSheetForm);
		return new ModelAndView("getMngRejectedData","model",model);
		
	}
	
	
	/**
	 * @author spysr_dev
	 * Method for updating
	 * @param model
	 * @param timeSheetForm
	 * @return
	 */
	@RequestMapping(value = "/updateData", method = RequestMethod.POST)
	public ModelAndView updateData(ModelMap model, @ModelAttribute TimeSheetForm timeSheetForm) {
        timeSheetService.updateByAdmin(timeSheetForm);
		
		return new ModelAndView("updateMngTS");
	}
	
	
	
	@RequestMapping(value = "/meetingRequest", method = RequestMethod.GET)
	public ModelAndView notification(ModelMap model,@ModelAttribute EndUserForm endUserForm) {
		
		EndUser user = getCurrentLoggedUserDetails();
		
	
		List<EndUser> empList = endUserDAOImpl.findByAddManager(user.getUserName()).getResultList();
	
		 if(empList != null && empList.size()  > 0) {
			 model.put("empList", empList);
			model.put("endUserForm", endUserForm);
		
         return new ModelAndView("meetingRequestData", "model", model);
	     } else {
	    	 return new ModelAndView("noDataFoundManager", "model", model);
	       }
	}
	
	      @RequestMapping( value="/submitForm" ,method = RequestMethod.POST)
	  
	      public String submitForm(ModelMap model,@ModelAttribute MeetingRequestForm meetingRequestForm, @ModelAttribute EndUserForm endUserForm,HttpServletRequest request) {
	    	  
	    	EndUser user = getCurrentLoggedUserDetails();
	  		  meetingRequestService.Save(meetingRequestForm);
	    	  model.put("meetingRequestForm", meetingRequestForm);
	    	  System.out.println(endUserForm.getEmail());
	   	 	     String email1 = endUserForm.getEmail();
	  				String[] emails2 = email1.split(",");

	  			for(String mailId : emails2)
	  			{
	  				 try {
	  		  			String email = mailId;
	  		  		    String time=  meetingRequestForm.getTime();
	  		  		    String description=meetingRequestForm.getDescription();
	  		  		    Date meetingDate=meetingRequestForm.getMeetingDate();
	  		  		
	  		  			String tex = "Meeting Request By Manager";

	  		  			SimpleMailMessage emails = new SimpleMailMessage();
	  		  			emails.setTo(email);
	  		  			emails.setSubject(tex);
	  		  			emails.setText("Hello "
	  		  					+ ",\n\n A Meeting Requested by Manager "+user.getName()+". "
	  		  					+ "\n" + "\n\n Your Meeting Details are: "
	  		  					+ "\n\nTime:" + time + "\n\nDescription:" + description + "\n\nMeeting Date:" + meetingDate 
	  		  					+ "\n\n\nRegards,\nManager");
	  		  		
	  		  			mailSender.send(emails);
	  		  			
	  		  		} catch (Exception e) {
	  		  			System.out.println(e.getMessage() + e);
	  		  		}
	  			
	  			}
	  		
	
	  
	          return "submitForm";
	  
	      }
	      
	      /**
	  	 * @author spysr_dev
	  	 * Method for requesting Documents
	  	 * @param model
	  	 * @return
	  	 */
	  	@RequestMapping(value = "/requestedDocs", method = RequestMethod.GET)
	  	public ModelAndView requestedDocs(ModelMap model) {
	  		
	  		EndUser user = getCurrentLoggedUserDetails();
	  		
	  		requestedDocsForm.setName(user.getName());
	  		requestedDocsForm.setUserName(user.getUserName());
	  		requestedDocsForm.setAddress(user.getAddress());
	  		requestedDocsForm.setEmail(user.getEmail());
	  		
	  		model.put("requestedDocsForm", requestedDocsForm);
	  		return new ModelAndView("mngRequestedDocsForm","model",model);
	  		
	  	}
	  	
		/**
		 * Method for confirmation
		 * @param model
		 * @param requestedDocsForm
		 * @return
		 */
		@RequestMapping(value = "/confirmDoc", method = RequestMethod.POST)
	  	public ModelAndView confirmDoc(ModelMap model,@ModelAttribute RequestedDocsForm requestedDocsForm) {
			model.put("requestedDocsForm", requestedDocsForm);
	  		return new ModelAndView("mngConfirmForm","model",model);
	  		
			
		}
	  		

	  	
	  	
		/**
		 * @author spysr_dev
		 * @param model
		 * @param requestedDocsForm
		 */
		@RequestMapping(value = "/getDoc", method = RequestMethod.POST)
		public String getDoc(ModelMap model,@ModelAttribute RequestedDocsForm requestedDocsForm,HttpServletRequest request) {
			EndUser user = getCurrentLoggedUserDetails();
			
			Format formatter = new SimpleDateFormat("dd-MM-YYYY");
			String s = formatter.format(new Date());
			try{
				OutputStream file;
			 file = new FileOutputStream(new File(Constants.DRIVE + requestedDocsForm.getName() + "_" + requestedDocsForm.getDocName() + ".pdf"));
			Font bold = new Font(Font.FontFamily.HELVETICA, 12f, Font.BOLD);
			Font normal = new Font(Font.FontFamily.HELVETICA, 12f, Font.NORMAL);
			Document document = new Document(PageSize.A4, 36, 36, 154, 54);
			PdfWriter writer = PdfWriter.getInstance(document, file);
			
	        /*HeaderFooter event = new HeaderFooter();
			writer.setPageEvent(event);*/
			document.open();
			//URL url1 = getClass().getClassLoader().getResource("/images/header.jpg");
			String img =request.getSession().getServletContext().getRealPath("/") + "resources/images/header.jpg";
			Image header = Image.getInstance(img);
			
			header.scalePercent(200f);
			header.setAbsolutePosition(0,(float) (PageSize.A4.getHeight() - 105.0));
			
			document.add(header);
			header.scaleToFit(600f, 500f);
	        document.add(header);

			
			Paragraph ph= new Paragraph("                                                                                                                       Date: "+ s,bold);
			Paragraph ph1= new Paragraph("                                                                                                                       Bangalore,",bold);
			
			document.add(ph);
			document.add(ph1);
			
			
			String name = user.getName();
			
			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
			Date date = user.getDoj();
			String doj = format.format(date);
			
			if(requestedDocsForm.getDocName().equals("Home Loan"))
					{
			Chunk ch = new Chunk("KOTAK Bank,MGRoad branch,Banglore",normal);
			Phrase p = new Phrase(ch);
					
			
			float salary = user.getTotalPackage();
			
			
			
			document.add(new Paragraph("To :",bold));
			document.add(p);
			
			document.add(new Paragraph(
					"Re :",bold));
			document.add(new Paragraph(name));

			
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);
			
			document.add(new Paragraph(
					"We confirm the following details regarding "+name+"'s employment with SpYsR HR:",normal));
			document.add(Chunk.NEWLINE);
			document.add(new Paragraph(
					"His salary is Rs."+salary+" per annum gross.",normal));
			document.add(new Paragraph("He is employed on a permanent full time basis."));
			
			document.add(new Paragraph("He started work with us on the "+doj));
			document.add(new Paragraph("He is not on probation."));
			document.add(Chunk.NEWLINE);
			document.add(new Paragraph("Should you require any additional information, please do not hesitate to contact +91 8065777004."));
			
					}
					else
					{
						
						
						String desg = user.getDesignation();
						document.add(Chunk.NEWLINE);
						document.add(Chunk.NEWLINE);
						document.add(Chunk.NEWLINE);
						document.add(new Paragraph("This is to certify that "+name +" is working with SpYsR HR since "+doj+" and he/she holding a designation "+desg+". "));
						document.add(Chunk.NEWLINE);
						document.add(Chunk.NEWLINE);
						document.add(new Paragraph("This certificate is issued to her/his for personal use"));
						document.add(Chunk.NEWLINE);
						document.add(Chunk.NEWLINE);
						document.add(Chunk.NEWLINE);
						document.add(new Paragraph("For(SpYsR HR)"));
						document.add(Chunk.NEWLINE);
						document.add(Chunk.NEWLINE);
						document.add(Chunk.NEWLINE);
						
					}
			document.add(Chunk.NEWLINE);
			document.add(new Paragraph("Regards,"));
			document.add(Chunk.NEWLINE);
			document.add(new Paragraph("(SIGN)"));
			document.add(Chunk.NEWLINE);
			document.add(new Paragraph("Partho,"));
			document.add(new Paragraph("Manager,"));
			document.add(new Paragraph("SpYsR HR"));
					
			
			document.add(Chunk.NEWLINE);		
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);
			document.add(Chunk.NEWLINE);		
			document.add(Chunk.NEWLINE);	
			//URL url = getClass().getResource("src/footer.jpg");
			Image footer = Image.getInstance(request.getSession().getServletContext().getRealPath("/") + "resources/images/footer.jpg");				
			footer.scaleToFit(500f,500f);
	       document.add(footer);
			
			document.close();
			
				MimeMessage message = mailSender.createMimeMessage();
				
					MimeMessageHelper helper = new MimeMessageHelper(message, true);

					helper.setTo(requestedDocsForm.getEmail());
					helper.setSubject("Document For " + requestedDocsForm.getDocName());
					helper.setText(String
							.format(requestedDocsForm.getName() + ",\n\n Enclosed, please find your requested document " + s
									+ "\n\n\nPartho H. Chakraborty\nSpYsR HR"));

					FileSystemResource attachment = new FileSystemResource(
							Constants.DRIVE + requestedDocsForm.getName() + "_" + requestedDocsForm.getDocName() + ".pdf");
					helper.addAttachment(attachment.getFilename(), attachment);
				
			mailSender.send(message);

			}
		   catch (MessagingException e) {
				throw new MailParseException(e);
			}
			
			catch(Exception e)
			{
				e.printStackTrace();
			}
	
			return "redirect:docSend";
		}
		
		/**
		 * @author spysr_dev
		 * Method for saved
		 * @param model
		 * @return
		 */
		@RequestMapping(value = "/docSend", method = RequestMethod.GET)
		public ModelAndView docSend(ModelMap model) {
			
			return new ModelAndView("mngDocSend","model",model);
			
		}
		
		/**
		 * @author spysr_dev
		 * Method for job offers data
		 * @param model
		 * @return
		 */
		@RequestMapping(value = "/jobOffers", method = RequestMethod.GET)
		public ModelAndView jobOffers(ModelMap model) {
			Collection<JobOffers> jobOffers = jobOffersDAO.getAll();
			model.put("jobOffers", jobOffers);
			return new ModelAndView("mngJobOffersdata","model",model);
		}
		
		/**
		 * @author spysr_dev
		 * Method for selecting job offer
		 * @param model
		 * @param id
		 * @return
		 */
		@RequestMapping(value = "/selectJobOffer", method = RequestMethod.GET)
		public ModelAndView selectJobOffers(ModelMap model,@RequestParam("id") long id) {
			
			JobOffers jobOffers = jobOffersDAO.getById(id);
			
			jobOffersForm.setExp(jobOffers.getExp());
			jobOffersForm.setJobTiltle(jobOffers.getJobTiltle());
			jobOffersForm.setTechnology(jobOffers.getTechnology());
			
			model.put("jobOffersForm", jobOffersForm);
			
			return new ModelAndView("mngSelectJobOffer","model",model);
		}
		
		/**
		 * @author spysr_dev
		 * Method for uploaded
		 * @param model
		 * @param jobOffersForm
		 * @return
		 * @throws IOException
		 */
		@RequestMapping(value = "/updateOffer", method = RequestMethod.POST)
		public String updateOffer(ModelMap model,@ModelAttribute JobOffersForm jobOffersForm,BindingResult result) throws IOException{
			
			EndUser user = getCurrentLoggedUserDetails();
			
			ReferJob referJob = new ReferJob();
			
			referJob.setNameOfCandidate(jobOffersForm.getNameOfCandidate());
			referJob.setExp(jobOffersForm.getCandidateExp());
			referJob.setCompanyName(jobOffersForm.getCompanyName());
			
			referJob.setReferredBy(user.getName());
			
			referJob.setFormFile(jobOffersForm.getFile().getBytes());
			referJob.setForm(jobOffersForm.getFile().getOriginalFilename());
			
			referJob.setDate(new Date());
			
			referJobDAO.save(referJob);
			
			return "redirect:mngUploaded";
			
			
			
		}
		
		/**
		 * Method to saved display
		 * @param model
		 * @return
		 */
		@RequestMapping(value = "/mngUploaded", method = RequestMethod.GET)
		public ModelAndView updateOffer(ModelMap model) {
			
				return new ModelAndView("mngUploaded","model",model);
					
			}
		
		
		/**
		 * @author spysr_dev
		 * Method for getting employee timesheet data
		 * @param model
		 * @return
		 */
		@RequestMapping(value = "/employeeTimeSheet", method = RequestMethod.GET)
		public ModelAndView employeeTimeSheet(ModelMap model) {
			Collection<TimeSheet> timeSheets= timeSheetDAO.gettingEmployeeData();
			if(timeSheets != null && timeSheets.size()  > 0) {
				 model.put("timeSheets", timeSheets);
				 return new ModelAndView("mngEmpTimeSheetData", "model", model);
		     } else {
		    	 return new ModelAndView("noDataFoundManager", "model", model);
		       }
		}
		
		/**
		 * @author spysr_dev
		 * Method for getting  timesheet data
		 * @param model
		 * @return
		 */
		@RequestMapping(value = "/statusTimeSheet", method = RequestMethod.GET)
		public ModelAndView statusTimeSheet(ModelMap model) {
			EndUser user = getCurrentLoggedUserDetails();
			Collection<TimeSheet> timeSheets= timeSheetDAO.gettingOwnData(user.getUserName());
			if(timeSheets != null && timeSheets.size()  > 0) {
				 model.put("timeSheets", timeSheets);
				 return new ModelAndView("mngEmpTimeSheetData", "model", model);
		     } else {
		    	 return new ModelAndView("noDataFoundManager", "model", model);
		       }
		}
		
		/**
		 * @author spysr_dev
		 * Method to get individual time sheet data
		 * @param model
		 * @param id
		 * @return
		 */
		@RequestMapping(value = "/getTimeSheet", method = RequestMethod.GET)
		public ModelAndView getTimeSheet(ModelMap model, @RequestParam("id") Long id) {
		
			TimeSheetForm timeSheetForm = timeSheetService.getById(id);
			
			model.put("timeSheetForm", timeSheetForm);
			return new ModelAndView("getMngEmpTimeSheetData","model",model);
			
		}
	  
	
	  }


	    

	

		

