package com.spysr.hr.portal.web;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.theme.CookieThemeResolver;

import com.spysr.hr.portal.dao.EndUserDAO;
import com.spysr.hr.portal.dao.FoodBillDAO;
import com.spysr.hr.portal.dao.LeaveRequestDAO;
import com.spysr.hr.portal.dao.NoticeDetailsDAO;
import com.spysr.hr.portal.dao.ProjectDetailsDAO;
import com.spysr.hr.portal.dao.TimeSheetDAO;
import com.spysr.hr.portal.entity.EndUser;
import com.spysr.hr.portal.entity.FoodBill;
import com.spysr.hr.portal.entity.LeaveRequest;
import com.spysr.hr.portal.entity.LeaveType;
import com.spysr.hr.portal.entity.NoticeDetails;
import com.spysr.hr.portal.entity.ProjectDetails;
import com.spysr.hr.portal.entity.TimeSheet;
import com.spysr.hr.portal.form.EndUserForm;
import com.spysr.hr.portal.form.FoodBillForm;
import com.spysr.hr.portal.form.JobOffersForm;
import com.spysr.hr.portal.form.LeaveRequestForm;
import com.spysr.hr.portal.form.LeaveTypeForm;
import com.spysr.hr.portal.form.NoticeDetailsForm;
import com.spysr.hr.portal.form.ProjectDetailsForm;
import com.spysr.hr.portal.form.TimeSheetForm;
import com.spysr.hr.portal.services.DateService;
import com.spysr.hr.portal.services.EnduserService;
import com.spysr.hr.portal.services.ImageService;
import com.spysr.hr.portal.services.JobOffersService;
import com.spysr.hr.portal.services.LeaveRequestServiceImpl;
import com.spysr.hr.portal.services.NoticeService;
import com.spysr.hr.portal.services.ProjectDetailService;
import com.spysr.hr.portal.services.TimeSheetService;
import com.spysr.hr.portal.utility.Constants;
@Controller
@RequestMapping("/admin")
public class AdminController {

	@Autowired
	EndUserDAO endUserDAOImpl;

	@Autowired
	EndUserForm endUserForm;
	@Autowired
	EnduserService enduserService;
	
	@Autowired
	ProjectDetailsDAO projectDetailsDAO;
	
	@Autowired
	ProjectDetailsForm projectDetailsForm;
	@Autowired
	ProjectDetailService projectDetailService;
	
	@Autowired
	NoticeDetailsDAO  noticeDetailsDAO;
	
	@Autowired
	NoticeDetailsForm  noticeDetailsForm;
	
	@Autowired
	NoticeService noticeService;
	
	@Autowired
	LeaveRequestForm leaveRequestForm;
	
	@Autowired
	LeaveRequestDAO leaveRequestDAO;
	
	@Autowired
	LeaveRequestServiceImpl leaveRequestService;
	
	@Autowired
	private JavaMailSender mailSender;
	
	@Autowired
	TimeSheetDAO timeSheetDAO;
	
	@Autowired
	TimeSheetForm timeSheetForm;
	
	@Autowired
	TimeSheetService timeSheetService;
	
	@Autowired
	JobOffersService jobOffersService;
	
	@Autowired
	JobOffersForm jobOffersForm;
	
	@Autowired
	FoodBillForm foodBillForm;
	
	@Autowired
	FoodBillDAO foodBillDAO;

	CookieThemeResolver themeResolver = new CookieThemeResolver();

	static Logger log = Logger.getLogger(AdminController.class.getName());

	private String getCurrentLoggedUserName() {
		return SecurityContextHolder.getContext().getAuthentication().getName();

	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		CustomDateEditor editor = new CustomDateEditor(dateFormat, true);
		binder.registerCustomEditor(Date.class, editor);
	}
   /**
     * Method to get current user details
     */
	private EndUser getCurrentLoggedUserDetails() {

		EndUser endUser = endUserDAOImpl.findByUsername(getCurrentLoggedUserName()).getSingleResult();

		return endUser;

	}
	
	 /**
	    * Method to calling current user details
	    * @param model 
	    * @return
	    */
	@ModelAttribute("requestCurrentUser")
	public EndUser getUserDetails(ModelMap model) {
		EndUser user = getCurrentLoggedUserDetails();
		if (user.getImageName() != null) {
			String type = ImageService.getImageType(user.getImageName());

			String url = "data:image/" + type + ";base64," + Base64.encodeBase64String(user.getImage());
			user.setImageName(url);
		}
		
		model.put("user", user);
		return user;
	}

	 
	@RequestMapping(value = "/adminPage", method = RequestMethod.GET)
	public ModelAndView showadminDashBoard(ModelMap model, HttpServletRequest request, HttpServletResponse response) {

		return new ModelAndView("admin", "model", model);
	}

	/**
	    * Method to editing profile
	    * @param model, id 
	    * @return view
	    */
	@RequestMapping(value = "/editAdminProfile", method = RequestMethod.GET)
	public ModelAndView editAdminProfile(ModelMap model, @RequestParam("id") Long id, RedirectAttributes attributes) {

		EndUser userProfile = endUserDAOImpl.findId(id);

		if (userProfile.getImageName() != null) {
			String type = ImageService.getImageType(userProfile.getImageName());

			String url = "data:image/" + type + ";base64," + Base64.encodeBase64String(userProfile.getImage());
			userProfile.setImageName(url);

			endUserForm.setImageName(url);
		}
		endUserForm.setId(userProfile.getId());
		endUserForm.setDisplayName(userProfile.getDisplayName());
		endUserForm.setUserName(userProfile.getUserName());
	
		endUserForm.setContactNo(userProfile.getContactNo());
		endUserForm.setEmail(userProfile.getEmail());

		endUserForm.setPassword(userProfile.getPassword());
		

		model.put("endUserForm", endUserForm);

		return new ModelAndView("editAdminProfile", "model", model);

	}
	
	/**
	    * Method to updating profile image
	    * @param model 
	    * @return
	    */
	@RequestMapping(value = "/updateImageForProfile", method = RequestMethod.POST)
	public String updateImageForProfile(ModelMap model, @ModelAttribute EndUserForm endUserForm) {

		try {
			EndUser userProfile = endUserDAOImpl.findId(endUserForm.getId());
			userProfile.setImage(endUserForm.getFile().getBytes());
			userProfile.setImageName(endUserForm.getFile().getOriginalFilename());
			endUserDAOImpl.update(userProfile);

		} catch (Exception e) {
			e.getMessage();
		}
		return "redirect:editAdminProfile?id=" + endUserForm.getId();
	}
	/**
	    * Method to confirm edit profile
	    * @param model
	    * @return
	    */
	/*@RequestMapping(value = "/confirmEditAdminProfile", method = RequestMethod.POST)
	public ModelAndView confirmEditAdminProfile(ModelMap model, @ModelAttribute EndUserForm endUserForm) {

		model.put("endUserForm", endUserForm);

		return new ModelAndView("confirmEditAdminProfile", "model", model);

	}
    */
	/**
	    * Method to updating profile details
	    * @param model
	    * @return
	    */
	@RequestMapping(value = "/updateAdminDetails", method = RequestMethod.POST)
	public String updateAdminDetails(ModelMap model, @ModelAttribute EndUserForm endUserForm,
			BindingResult result, RedirectAttributes attributes) {

		EndUser endUser = endUserDAOImpl.findId(endUserForm.getId());

		endUser.setId(endUserForm.getId());

		endUser.setDisplayName(endUserForm.getDisplayName());
		endUser.setContactNo(endUserForm.getContactNo());
	
		endUser.setEmail(endUserForm.getEmail());

		endUser.setUserName(endUserForm.getUserName());
	

		endUserDAOImpl.update(endUser);

		model.put("endUserForm", endUserForm);
		
		return "redirect:updateAdminSuccess";

		

	}
	
	@RequestMapping(value="/updateAdminSuccess",method=RequestMethod.GET)
	public ModelAndView updateAdminSuccess(ModelMap model)
	{
		return new ModelAndView("updateAdminSuccess","model",model);
	}	
	
	
	
	@RequestMapping(value="/testDemo",method=RequestMethod.GET)
	public ModelAndView testDemo(ModelMap model)
	{
		return new ModelAndView("testDemo","model",model);
	}	
	
	
	/**
	    * Method to edit password details
	    * @param model,id
	    * @return
	    */
	@RequestMapping(value = "/editAdminPWD", method = RequestMethod.GET)
	public ModelAndView editAdminPWD(ModelMap model, @RequestParam("id") Long id, RedirectAttributes attributes) {

		EndUser userProfile = endUserDAOImpl.findId(id);

		endUserForm.setId(userProfile.getId());

	

		model.put("endUserForm", endUserForm);

		return new ModelAndView("editAdminPWD", "model", model);

	}

	/**
	    * Method to updating password details
	    * @param model, enduserForm
	    * @return
	    */
	@RequestMapping(value = "/updateEditAdminPWD", method = RequestMethod.POST)
	public ModelAndView updateEditAdminPWD(ModelMap model, @ModelAttribute EndUserForm endUserForm,
			BindingResult result, RedirectAttributes attributes) {

		EndUser endUser = endUserDAOImpl.findId(endUserForm.getId());

		endUser.setId(endUserForm.getId());

		endUser.setPassword(endUserForm.getConfirmNewPassword());
	

		endUserDAOImpl.update(endUser);

		model.put("endUserForm", endUserForm);

		return new ModelAndView("updateAdminSuccess", "model", model);

	}
    
	/**
	    * Method to change theme
	    * @param model
	    * @return
	    */
	@RequestMapping(value = "/themeChange", method = RequestMethod.GET)
	public String getThemeChange(ModelMap model, HttpServletRequest request, HttpServletResponse response) {

		log.info("Received request for theme change");

		EndUser endUser = endUserDAOImpl.findByUsername(getCurrentLoggedUserName()).getSingleResult();
		if (endUser.getTheme() == null)
			themeResolver.setThemeName(request, response, "themeBlue");
		if (!endUser.getTheme().equalsIgnoreCase(request.getParameter("theme"))) {

			if (request.getParameter("theme").equalsIgnoreCase("themeBlue")) {
				themeResolver.setThemeName(request, response, "themeBlue");
			} else if (request.getParameter("theme").equalsIgnoreCase("themeGreen")) {
				themeResolver.setThemeName(request, response, "themeGreen");
			} else if (request.getParameter("theme").equalsIgnoreCase("themeOrange")) {
				themeResolver.setThemeName(request, response, "themeOrange");
			} else if (request.getParameter("theme").equalsIgnoreCase("themeRed")) {
				themeResolver.setThemeName(request, response, "themeRed");
			}

			endUser.setTheme(request.getParameter("theme"));
			endUserDAOImpl.update(endUser);
		} else
			themeResolver.setThemeName(request, response, endUser.getTheme());

		return "redirect:adminPage";
	}

	/**
	 * Method to change language
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/getLocaleLang", method = RequestMethod.GET)
	public String getLocaleLang(ModelMap model, HttpServletRequest request, HttpServletResponse response) {

		log.info("Received request for locale change");
		EndUser user = endUserDAOImpl.findByUsername(getCurrentLoggedUserName()).getSingleResult();
		LocaleResolver localeResolver = new CookieLocaleResolver();
		Locale locale = new Locale(request.getParameter("lang"));
		localeResolver.setLocale(request, response, locale);
		user.setPrefferedLanguage(request.getParameter("lang"));

		endUserDAOImpl.update(user);
		return "redirect:adminPage";
	}
	
	
	
	/**
	 * @author sudhir
	 * Method to get employee details
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/addEmployee", method = RequestMethod.GET)
	public ModelAndView addEmployee(ModelMap model) {
		EndUser user = getCurrentLoggedUserDetails();
		model.put("user", user);
		model.put("endUserForm", endUserForm);
		return new ModelAndView("addEmployee", "model", model);
		
	}
	
	/**
	 * @param model
	 * @param endUserForm
	 * @param attributes
	 * @return
	 */
	@RequestMapping(value = "/insertEmployee", method = RequestMethod.POST)
	public String roleApprovalListUpdate(ModelMap model,
			@ModelAttribute EndUserForm endUserForm,
			RedirectAttributes attributes) {
		EndUser user = getCurrentLoggedUserDetails();
		
		List<EndUser> endUser = endUserDAOImpl.findByUsername(endUserForm.getUserName()).getResultList();
		model.put("endUser", endUser);
		
		
		if (endUser.size() == 0) {
		

			model.put("user", user);
			model.put("endUserForm", endUserForm);
			
	    System.out.println("total Package"+ endUserForm.getTotalPackage());
	    endUserForm.setCreatedby(user.getCurrentRole());
	    enduserService.Save(endUserForm); // calling service class for saving data		
		
		try {
			String email = endUserForm.getEmail();
			String name = endUserForm.getName();
			String password = endUserForm.getPassword();

			String currentRole = endUserForm.getCurrentRole();
			String tex = "Login Credentials";

			SimpleMailMessage emails = new SimpleMailMessage();
			emails.setTo(email);
			emails.setSubject(tex);
			emails.setText("Hello " + name
					+ ",\n\n An official account has been created for You. "
					+ "\n" + "\n" + "\n\n Your Login credentials are: "
					+ "\n\nUser Name:" + endUserForm.getUserName() + "\n\nPassword: " + password
				    + "\n\n\nCurrent Role: " + currentRole
					+ "\n\n\nRegards,\nAdmin");
			System.out.println("" + email + name);
			mailSender.send(emails);
			SimpleMailMessage emails1 = new SimpleMailMessage();
			emails1.setTo(email);
			emails1.setSubject(tex);
			emails1.setText("Hello " + name
					+ "\n\n An Official account has been created for You.\n"
					+ "\n\nRegards,\nAdmin");

		} catch (Exception e) {
			System.out.println(e.getMessage() + e);
		}

		

		model.put("endUserForm", endUserForm);

		return "redirect:insertEmployee";
		
		}else{
		
           String success=Constants.RECORDEXISTING;
            model.put("success", success);
            attributes.addFlashAttribute(Constants.SUCCESS,Constants.RECORDEXISTING);
			model.put("user", user);

			return "redirect:addEmployee";
		}

	}
	
	/**
	 * @author Sudheer
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/insertEmployee",method=RequestMethod.GET)
	public ModelAndView insertEmployee(ModelMap model)
	{
		return new ModelAndView("insertEmployee","model",model);
	}
		
	
	
	@RequestMapping(value="/employeeDetails",method=RequestMethod.GET)
	public ModelAndView employeeDetails(ModelMap model,
			RedirectAttributes attributes) {
		EndUser user = getCurrentLoggedUserDetails();
		model.put("user", user);
	
		
	  List<EndUser> userList = endUserDAOImpl.getEmployees().getResultList();
	 
	  model.put("user", user);
       if(userList != null && userList.size()  > 0)	
       {
		model.put("userList", userList);
		
      }
       return new ModelAndView("employeeDetails", "model", model);
	}
	
	
	@RequestMapping(value = "/selectUserUpdate", method = RequestMethod.GET)
	public ModelAndView selectBuyerUpdate(ModelMap model,
			@RequestParam("id") Long id, RedirectAttributes attributes) {

		EndUser user = getCurrentLoggedUserDetails();

		EndUser enduser = endUserDAOImpl.findId(id);
		
		endUserForm.setId(enduser.getId());
		endUserForm.setEmail(enduser.getEmail());
		endUserForm.setContactNo(enduser.getContactNo());
		endUserForm.setUserName(enduser.getUserName());	
		endUserForm.setFatherName(enduser.getFatherName());
		endUserForm.setCurrentRole(enduser.getCurrentRole());	
		endUserForm.setAccountNo(enduser.getAccountNo());
		endUserForm.setAddress(enduser.getAddress());
	    endUserForm.setCreatedby(enduser.getCreatedby());
		endUserForm.setBasicSalary(enduser.getBasicSalary());
		endUserForm.setBname(enduser.getBname());
		endUserForm.setBranch(enduser.getBranch());
		endUserForm.setConveyAllowance(enduser.getConveyAllowance());
		endUserForm.setDepartment(enduser.getDepartment());
		endUserForm.setDesignation(enduser.getDesignation());
		endUserForm.setDisplayName(enduser.getDisplayName());
		endUserForm.setLocalAddress(enduser.getLocalAddress());
		endUserForm.setPermanentAddress(enduser.getPermanentAddress());
		endUserForm.setDob(enduser.getDob());
		endUserForm.setDoj(enduser.getDoj());
	     endUserForm.setRole(enduser.getRole());
		endUserForm.setFixedPackage(enduser.getFixedPackage());
		endUserForm.setFoodamountDeduct(enduser.getFoodamountDeduct());
		endUserForm.setEligibleLeave(enduser.getEligibleLeave());
		endUserForm.setTotalLeave(enduser.getTotalLeave());
	
		endUserForm.setGender(enduser.getGender());
		endUserForm.setHra(enduser.getHra());

		endUserForm.setMedicalAllowance(enduser.getMedicalAllowance());
		endUserForm.setSpecialAllowance(enduser.getSpecialAllowance());
		endUserForm.setName(enduser.getName());
	
		endUserForm.setPan(enduser.getPan());
		endUserForm.setProfessionalTax(enduser.getProfessionalTax());
	    endUserForm.setLop(enduser.getLop());
		endUserForm.setTds(enduser.getTds());
		endUserForm.setTotalPackage(enduser.getTotalPackage());
		
		endUserForm.setPassword(enduser.getPassword());
		endUserForm.setVariablePackage(enduser.getVariablePackage());
	/*	enduserService.Show(endUserForm);*/
		model.put("user", user);
        model.put("enduser", enduser);
		model.put("endUserForm", endUserForm);

		return new ModelAndView("selectuserUpdate", "model", model);

	}



	/**
	 * @param endUserForm
	 * @param model
	 * @param attribute
	 * @return
	 */
	@RequestMapping(value = "/selectUserUpdate2", method = RequestMethod.POST)
	public String selectuserUpdate2(
			@ModelAttribute EndUserForm endUserForm, ModelMap model,
			RedirectAttributes attribute) {

		EndUser user = getCurrentLoggedUserDetails();

		EndUser enduser = endUserDAOImpl.findId(endUserForm.getId());

	    enduserService.Update(endUserForm); // calling service class for saving data		
	    System.out.println("total Package"+ endUserForm.getTotalPackage());	
		model.put("endUserForm", endUserForm);

	
	    model.put("user", user);
	    
	    model.put("enduser", enduser);
	    
	    return "redirect:selectUserUpdate2";
	}

	/**
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/selectUserUpdate2",method=RequestMethod.GET)
	public ModelAndView selectUserUpdate2(ModelMap model)
	{
		return new ModelAndView("selectUserUpdate2","model",model);
	}	
	
	
	
	
	
	
	@RequestMapping(value = "/approvalPendingManager", method = RequestMethod.GET)
	public ModelAndView getAllBuyer(ModelMap model, HttpServletRequest request,
			HttpServletResponse response) {

		EndUser user = getCurrentLoggedUserDetails();
		model.put("user", user);
		List<EndUser> userList = endUserDAOImpl.getByRole().getResultList();

		model.put("user", user);
		if(userList != null && userList.size() > 0)
		{
		model.put("userList", userList);
		return new ModelAndView("approvalPendingManager", "model", model);
		}
		else
		{
			return new ModelAndView("noDataFoundAdmin", "model", model);
		}

	}

	@RequestMapping(value = "/approvalPendingManagershow", method = RequestMethod.GET)
	public ModelAndView buyerPageShow(@RequestParam Long id, ModelMap model,
			RedirectAttributes attributes) {
		EndUser user = getCurrentLoggedUserDetails();

		EndUser endUser = endUserDAOImpl.findId(id);

		endUserForm.setId(endUser.getId());
		endUserForm.setUserName(endUser.getUserName());
		endUserForm.setName(endUser.getName());
		endUserForm.setBank(endUser.getBname());
		endUserForm.setBranch(endUser.getBranch());
		endUserForm.setFatherName(endUser.getFatherName());
		endUserForm.setContactNo(endUser.getContactNo());
		endUserForm.setEmail(endUser.getEmail());
		endUserForm.setCreatedby(endUser.getCreatedby());
		endUserForm.setEligibleLeave(endUser.getEligibleLeave());
		endUserForm.setTotalLeave(endUser.getTotalLeave());
		
		endUserForm.setCurrentRole(endUser.getCurrentRole());
		endUserForm.setAccountNo(endUser.getAccountNo());
		endUserForm.setAddress(endUser.getAddress());
		
		endUserForm.setBasicSalary(endUser.getBasicSalary());
		endUserForm.setBname(endUser.getBname());
		
		endUserForm.setConveyAllowance(endUser.getConveyAllowance());
		endUserForm.setDepartment(endUser.getDepartment());
		endUserForm.setDesignation(endUser.getDesignation());
		endUserForm.setDisplayName(endUser.getDisplayName());
		endUserForm.setDob(endUser.getDob());
		endUserForm.setDoj(endUser.getDoj());
		endUserForm.setRole(endUser.getRole());
		endUserForm.setFixedPackage(endUser.getFixedPackage());
		endUserForm.setFoodamountDeduct(endUser.getFoodamountDeduct());
		
		endUserForm.setLocalAddress(endUser.getLocalAddress());
		endUserForm.setPermanentAddress(endUser.getPermanentAddress());
	
		endUserForm.setGender(endUser.getGender());
		endUserForm.setHra(endUser.getHra());
	
		endUserForm.setMedicalAllowance(endUser.getMedicalAllowance());
		endUserForm.setSpecialAllowance(endUser.getSpecialAllowance());
	
	
		endUserForm.setPan(endUser.getPan());
		endUserForm.setProfessionalTax(endUser.getProfessionalTax());
	
		endUserForm.setTds(endUser.getTds());
		endUserForm.setTotalPackage(endUser.getTotalPackage());
		
	
		endUserForm.setPassword(endUser.getPassword());
		endUserForm.setVariablePackage(endUser.getVariablePackage());
	
		
		
		model.put("user", user);

		model.put("endUserForm", endUserForm);

		return new ModelAndView("approvalPendingManagershow", "model", model);
	}

	@RequestMapping(value = "/approvalPendingManagershowConfrim", method = RequestMethod.POST)
	public String approvalPendingManagershow(
			@ModelAttribute EndUserForm endUserForm, ModelMap model,
			RedirectAttributes attributes) {
		EndUser user = getCurrentLoggedUserDetails();
		model.put("user", user);

		EndUser endUser = endUserDAOImpl.findId(endUserForm.getId());
		
	/*	endUserForm.setId(endUser.getId());
		endUserForm.setName(endUser.getName());
		endUserForm.setBank(endUser.getBname());
		endUserForm.setBranch(endUser.getBranch());
		endUserForm.setFatherName(endUser.getFatherName());
		endUserForm.setContactNo(endUser.getContactNo());
		endUserForm.setEmail(endUser.getEmail());*/
		
		/*Date approvDate = new Date();
		endUser.setApproveDate(approvDate);*/
		
	    enduserService.Update(endUserForm);

		try {
			String status = endUserForm.getStatus();
			String username = endUserForm.getUserName();
			String email = endUserForm.getEmail();
			String password = endUserForm.getPassword();
			String comment = endUserForm.getComment();

			if (status.equals("Approved")) {
				String tex = "Login Credentials";
				SimpleMailMessage emails = new SimpleMailMessage();
				emails.setTo(email);
				emails.setSubject(tex);
				emails.setText("Hello " + username
						+ ",\n\n Your Account has been Created. "
						+ ",\n\n Your Credentials Details are as follows. "
						+ "\n" + "\n"

						+ "\n\nUserName:" + username + "\n\nPassword:"
						+ password +

						"\n\n\nRegards,\n Manager");
				System.out.println("" + email + username);
				mailSender.send(emails);
				SimpleMailMessage emails1 = new SimpleMailMessage();
				emails1.setTo(email);

			} else if (status.equals("Rejected")) {
				String tex = "Login Credentials Rejection notification";
				SimpleMailMessage emails = new SimpleMailMessage();
				emails.setTo(email);
				emails.setSubject(tex);
				emails.setText("Hello " + username
						+ ",\n\n Your Account has been Rejected  "
						+ "\n\n Comment:" + comment + "\n" + "\n"
						+ "\n\n\nRegards,\nAdmin");
				mailSender.send(emails);
			} else {
				String tex = "Login Credentials Pending notification";
				SimpleMailMessage emails = new SimpleMailMessage();
				emails.setTo(email);
				emails.setSubject(tex);
				emails.setText("Hello " + username
						+ ",\n\n  Login Credentials Details has been Pending " + "\n"
						+ "\n" + "\n\n\nRegards,\nAdmin");
				mailSender.send(emails);
			}

			

			model.put("endUserForm", endUserForm);

		} catch (Exception e) {
			System.out.println(e.getMessage() + e);
		}
		return "redirect:approvalPendingManagershowConfrim";
	}
	
	
	@RequestMapping(value="/approvalPendingManagershowConfrim",method=RequestMethod.GET)
	public ModelAndView approvalPendingManagershowConfrim(ModelMap model)
	{
		return new ModelAndView("approvalPendingManagershowConfrim","model",model);
	}	
	
	
	
	
	@RequestMapping(value = "/projectDetails", method = RequestMethod.GET)
	public ModelAndView productDetails(ModelMap model) {

		EndUser user = getCurrentLoggedUserDetails();

		List<ProjectDetails> productList =projectDetailsDAO.productList().getResultList();
		if (productList != null && productList.size() > 0) {
			
			
			model.put("productList", productList);
			
			
		}
		model.put("user", user);
		return new ModelAndView("projectDetails", "model", model);
	}
	@RequestMapping(value = "/addProjects", method = RequestMethod.GET)
	public ModelAndView addHoliday(ModelMap model) {
		EndUser user = getCurrentLoggedUserDetails();
		
		List<EndUser> empList = endUserDAOImpl.getEmployees().getResultList();
		
		 if(empList != null && empList.size()  > 0) {
			 model.put("empList", empList);
	     } 
		
		model.put("user", user);
		
		model.put("projectDetailsForm", projectDetailsForm);
		
		return new ModelAndView("addProjects", "model", model);
		
	}
	
	
	@RequestMapping(value = "/insertProjects", method = RequestMethod.POST)
	public String insertHoliday(ModelMap model,
			@ModelAttribute ProjectDetailsForm projectDetailsForm,
			RedirectAttributes attributes) {
		List<ProjectDetails> productList = projectDetailsDAO.findByProductName(projectDetailsForm.getSerialNo()).getResultList();
		
		if (productList.size() == 0) {
			EndUser user = getCurrentLoggedUserDetails();
		
			projectDetailService.Save(projectDetailsForm);// calling service class for saving data		

			model.put("user", user);
		    model.put("projectDetailsForm", projectDetailsForm);

		return "redirect:insertProjects";
		
		}else{
			EndUser user = getCurrentLoggedUserDetails();
            String success=Constants.RECORDEXISTING;
            model.put("success", success);
            attributes.addFlashAttribute(Constants.SUCCESS, Constants.RECORDEXISTING);

			model.put("productList", productList);
			model.put("user", user);
			model.put("projectDetailsForm", projectDetailsForm);
			return "redirect:addProjects";
			
		}

	}
	
	@RequestMapping(value="/insertProjects",method=RequestMethod.GET)
	public ModelAndView insertholiDays(ModelMap model,RedirectAttributes attributes) {
		EndUser user = getCurrentLoggedUserDetails();
		model.put("user", user);
		
        return new ModelAndView("insertProjects", "model", model);
       }
	
	//Edit
	@RequestMapping(value = "/selectprojectadminUpdate", method = RequestMethod.GET)
	public ModelAndView selectprojectUpdate(ModelMap model,
			@RequestParam("id") Long id, RedirectAttributes attributes) {
		
		List<EndUser> empList = endUserDAOImpl.getEmployees().getResultList();
		
		 if(empList != null && empList.size()  > 0) {
			 model.put("empList", empList);
	     } 

	   ProjectDetails project=projectDetailsDAO.findId(id);
	   projectDetailsForm.setId(project.getId());
	   projectDetailsForm.setSerialNo(project.getSerialNo());
	   projectDetailsForm.setProjectDetails(project.getProjectDetails());
	   projectDetailsForm.setMembersInvolved(project.getMembersInvolved());
	   projectDetailsForm.setProjectMembers(project.getProjectMembers());
	   projectDetailsForm.setVersion(project.getVersion());
	   projectDetailsForm.setProjectName(project.getProjectName());
        model.put("project", project);
		model.put("projectDetailsForm", projectDetailsForm);

		return new ModelAndView("selectprojectadminUpdate", "model", model);

	}



	
	@RequestMapping(value = "/selectprojectadminUpdate2", method = RequestMethod.POST)
	public String selectprojectUpdate2(@ModelAttribute ProjectDetailsForm projectDetailsForm, ModelMap model,
			RedirectAttributes attribute) {

		ProjectDetails project = projectDetailsDAO.findId(projectDetailsForm.getId());

		projectDetailService.Update(projectDetailsForm);

		model.put("projectDetailsForm", projectDetailsForm);

		model.put("project", project);

		return "redirect:selectprojectadminUpdate2";
	}

	/**
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/selectprojectadminUpdate2",method=RequestMethod.GET)
	public ModelAndView selectprojectadminUpdate2(ModelMap model)
	{
		return new ModelAndView("selectprojectadminUpdate2","model",model);
	}	
	//Notice Details
	
	/**method for all Notice Details
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/noticeDetails", method = RequestMethod.GET)
	public ModelAndView noticeDetails(ModelMap model) {

		EndUser user = getCurrentLoggedUserDetails();

		List<NoticeDetails> noticeList = noticeDetailsDAO.noticeList().getResultList();
		if (noticeList != null && noticeList.size() > 0) {

			model.put("noticeList", noticeList);
			model.put("user", user);
			
		} 
		return new ModelAndView("noticeDetails", "model", model);
	}


	/**add notice Details
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/addnoticeDetails", method = RequestMethod.GET)
	public ModelAndView addnoticeDetails(ModelMap model) {
		EndUser user = getCurrentLoggedUserDetails();

		model.put("user", user);

		model.put("noticeDetailsForm", noticeDetailsForm);

		return new ModelAndView("addnoticeDetails", "model", model);

	}
	
	
	/**save data for Notice Details
	 * @param model
	 * @param projectDetailsForm
	 * @param attributes
	 * @return
	 */
	@RequestMapping(value = "/insertnoticeDetails", method = RequestMethod.POST) 
	public String insertnoticeDetails(ModelMap model, @ModelAttribute NoticeDetailsForm noticeDetailsForm,
			RedirectAttributes attributes) {
		List<NoticeDetails> noticeList = noticeDetailsDAO.findByNoticeDetailsName(noticeDetailsForm.getSerialNo())
				.getResultList();

		if (noticeList.size() == 0) {
			EndUser user = getCurrentLoggedUserDetails();

			noticeService.Save(noticeDetailsForm);

			model.put("user", user);
			model.put("noticeDetailsForm", noticeDetailsForm);

			return "redirect:insertnoticeDetails";

		} else {
			EndUser user = getCurrentLoggedUserDetails();
			String success = Constants.RECORDEXISTING;
			model.addAttribute("success", success);

			attributes.addFlashAttribute(Constants.SUCCESS, Constants.RECORDEXISTING);
			model.put("noticeList", noticeList);
			model.put("user", user);
			model.put("noticeDetailsForm", noticeDetailsForm);
			return "redirect:addnoticeDetails";

		}

	}
	
	/**Redirect Page
	 * @param model
	 * @param attributes
	 * @return
	 */
	@RequestMapping(value = "/insertnoticeDetails", method = RequestMethod.GET)
	public ModelAndView insertnoticeDetails(ModelMap model, RedirectAttributes attributes) {
		EndUser user = getCurrentLoggedUserDetails();
		model.put("user", user);

		return new ModelAndView("insertnoticeDetails", "model", model);
	}

	// Edit Notice Details

	@RequestMapping(value = "/selectnoticeUpdate", method = RequestMethod.GET)
	public ModelAndView selectnoticeUpdate(ModelMap model, @RequestParam("id") Long id, RedirectAttributes attributes) {

		NoticeDetails noticeDetails = noticeDetailsDAO.findId(id);
		noticeDetailsForm.setId(noticeDetails.getId());
		noticeDetailsForm.setSerialNo(noticeDetails.getSerialNo());
		noticeDetailsForm.setDescription(noticeDetails.getDescription());
		noticeDetailsForm.setTitle(noticeDetails.getTitle());
		noticeDetailsForm.setNoticeDate(noticeDetails.getNoticeDate());
		model.put("noticeDetails", noticeDetails);
		model.put("noticeDetailsForm", noticeDetailsForm);

		return new ModelAndView("selectnoticeUpdate", "model", model);

	}



		
		@RequestMapping(value = "/selectnoticeUpdate2", method = RequestMethod.POST)
		public String selectprojectUpdate2(
				@ModelAttribute NoticeDetailsForm  noticeDetailsForm, ModelMap model,
				RedirectAttributes attribute) {

			  NoticeDetails noticeDetails=noticeDetailsDAO.findId(noticeDetailsForm.getId());

			 noticeService.Update(noticeDetailsForm);	
			
			model.put("projectDetailsForm", projectDetailsForm);

		
		  
		    model.put("noticeDetails", noticeDetails);
		    
		    return "redirect:selectnoticeUpdate2";
		}

		/**
		 * @param model
		 * @return
		 */
		@RequestMapping(value="/selectnoticeUpdate2",method=RequestMethod.GET)
		public ModelAndView selectprojectUpdate2(ModelMap model)
		{
			return new ModelAndView("selectnoticeUpdate2","model",model);
		}	
		
	
  //Leave Approved/Rejected Logic
	
	@RequestMapping(value = "/employeeLeaveAppliedDetails", method = RequestMethod.GET)
	public ModelAndView employeeLeaveAppliedDetails(ModelMap model, HttpServletRequest request,
			HttpServletResponse response) {

		EndUser user = getCurrentLoggedUserDetails();
		model.put("user", user);
		List<LeaveRequest> leaveRequestist = leaveRequestDAO.getLeaveRequestDetails().getResultList();

		model.put("user", user);
		if(leaveRequestist != null && leaveRequestist.size() > 0)
		{
		model.put("leaveRequestist", leaveRequestist);
		return new ModelAndView("employeeLeaveAppliedDetails", "model", model);
		}
		else
		{
			return new ModelAndView("noDataFoundAdmin", "model", model);
		}

	}
	
	
	@RequestMapping(value = "/leavePendingemployeeRequest", method = RequestMethod.GET)
	public ModelAndView leavePendingemployeeRequest(@RequestParam Long id, ModelMap model,
			RedirectAttributes attributes) {
		EndUser user = getCurrentLoggedUserDetails();

	
		LeaveRequest leaveRequest=leaveRequestDAO.findId(id);

		leaveRequestForm.setId(leaveRequest.getId());
	    leaveRequestForm.setUserName(leaveRequest.getUserName());
	    leaveRequestForm.setDepartment(leaveRequest.getDepartment());
	    leaveRequestForm.setDesignation(leaveRequest.getDesignation());
	    leaveRequestForm.setName(leaveRequest.getName());
	    leaveRequestForm.setDoj(leaveRequest.getDoj());
	    leaveRequestForm.setEligibleLeave(leaveRequest.getEligibleLeave());
	    leaveRequestForm.setTotalLeave(leaveRequest.getTotalLeave());
	    leaveRequestForm.setTypesofLeave(leaveRequest.getTypesofLeave());
	    leaveRequestForm.setReason(leaveRequest.getReason());
	    leaveRequestForm.setStatus(leaveRequest.getStatus());
	    leaveRequestForm.setLeaveStatus(leaveRequest.getLeaveStatus());
	    leaveRequestForm.setComment(leaveRequest.getComment());
	    leaveRequestForm.setToDate(leaveRequest.getToDate());
	    leaveRequestForm.setFromDate(leaveRequest.getFromDate());
	    leaveRequestForm.setEmail(leaveRequest.getEmail());
	    leaveRequestForm.setLeaveLength(leaveRequest.getLeaveLength());
	/*    leaveRequestForm.setNoofdays(leaveRequest.getNoofdays());*/
	    leaveRequestForm.setTotalnumberleaveDays(leaveRequest.getTotalnumberleaveDays());
	    leaveRequestForm.setLeaveDate(leaveRequest.getLeaveDate());
	   
		
		model.put("user", user);

		model.put("leaveRequestForm", leaveRequestForm);

		return new ModelAndView("leavePendingemployeeRequest", "model", model);
	}

	@RequestMapping(value = "/leavePendingemployeeRequesthowConfrim", method = RequestMethod.POST)
	public String leavePendingemployeeRequesthowConfrim(
			@ModelAttribute LeaveRequestForm leaveRequestForm, ModelMap model,
			RedirectAttributes attributes) {
	
		LeaveRequest leaveRequest=leaveRequestDAO.findId(leaveRequestForm.getId());
		leaveRequest.setStatus(leaveRequestForm.getStatus());
		leaveRequest.setComment(leaveRequestForm.getComment());
		model.put("leaveRequest", leaveRequest);
		model.put("leaveRequestForm", leaveRequestForm);
		leaveRequestDAO.update(leaveRequest);
		 

		try {
			String status = leaveRequestForm.getStatus();
			String username = leaveRequestForm.getUserName();
			String email = leaveRequestForm.getEmail();
	        int totalNumberofDays=leaveRequestForm.getTotalnumberleaveDays();
		    Date fromdate=leaveRequestForm.getFromDate();
		    Date todate=leaveRequestForm.getToDate();
			String comment = leaveRequestForm.getComment();

			if (status.equals("Approved")) {
				String tex = "Leave Details Approved  Details Notification";
				SimpleMailMessage emails = new SimpleMailMessage();
				emails.setTo(email);
				emails.setSubject(tex);
				emails.setText("Hello " + username
						+ ",\n\n Your Leave Applied Request has been Created. "
						+ ",\n\n Your Details are as follows. "
						+ "\n" + "\n"

						+ "\n\nUserName:" + username + "\n\nStatus:"+status+ "\n\nFrom Date:"+fromdate+"\n\nTo Date Date:"+todate+"\n\nTotal Number in Days:"+totalNumberofDays+"\n\n\nRegards,\n Manager");
				System.out.println("" + email + username);
				mailSender.send(emails);
				SimpleMailMessage emails1 = new SimpleMailMessage();
				emails1.setTo(email);

			} else if (status.equals("Rejected")) {
				String tex = "Leave Details Rejection notification";
				SimpleMailMessage emails = new SimpleMailMessage();
				emails.setTo(email);
				emails.setSubject(tex);
				emails.setText("Hello " + username
						+ ",\n\n Your Account has been Rejected  "
						+ "\n\n Comment:" + comment + "\n" + "\n"
						+ "\n\n\nRegards,\nAdmin");
				mailSender.send(emails);
			} else {
				String tex = "Leave Details Pending notification";
				SimpleMailMessage emails = new SimpleMailMessage();
				emails.setTo(email);
				emails.setSubject(tex);
				emails.setText("Hello " + username
						+ ",\n\n  Buyer Details has been Pending " + "\n"
						+ "\n" + "\n\n\nRegards,\nAdmin");
				mailSender.send(emails);
			}

			

			model.put("leaveRequestForm", leaveRequestForm);

		} catch (Exception e) {
			System.out.println(e.getMessage() + e);
		}
	
		
			return "redirect:leavePendingemployeeRequesthowConfrim";
	}
	
	
	@RequestMapping(value="/leavePendingemployeeRequesthowConfrim",method=RequestMethod.GET)
	public ModelAndView leavePendingemployeeRequesthowConfrim(ModelMap model)
	{
		return new ModelAndView("leavePendingemployeeRequesthowConfrim","model",model);
	}	
	
	

	
	/**
	 * Method to display List of (Emp,Manager) to Block/Unblock or Renew
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/getUsersForBlockUnblockRenew", method = RequestMethod.GET)
	public ModelAndView getUsersForBlockUnblockRenew(ModelMap model,@ModelAttribute EndUserForm endUserForm) {
		
		List<EndUser> userList = endUserDAOImpl.getUsersForBlockUnblockRenew();
		 if(userList != null && userList.size()  > 0) {
			 model.put("userList", userList);
			 model.put("endUserForm", endUserForm);
			 return new ModelAndView("getUsersForBlockUnblockRenew", "model", model);
	     } else {
	    	 return new ModelAndView("noDataFoundAdmin", "model", model);
	       }

	}

	

	/**
	 * Method to update EndUser with Block/Unblock status
	 * 
	 * @param endUserForm
	 * @param model
	 * @param attributes
	 * @return
	 */
	@RequestMapping(value = "/blockUnblockAccount", method = RequestMethod.POST)
	public String blockUnblockAccount(ModelMap model,
			RedirectAttributes attributes,@ModelAttribute EndUserForm endUserForm) {
		EndUser user = getCurrentLoggedUserDetails();
		model.put("user", user);
		EndUser endUser = endUserDAOImpl.findId(endUserForm.getId());
		endUser.setAccessStatus(endUserForm.getAccessStatus());
		endUser.setReason(endUserForm.getReason());
	
		endUserDAOImpl.update(endUser);

		model.put("endUserForm", endUserForm);
		
		return "redirect:adminBlockRenewTransaction";
	}

	
	
	@RequestMapping(value = "/adminBlockRenewTransaction", method = RequestMethod.GET)
	public ModelAndView adminBlockRenewTransaction(ModelMap model,@ModelAttribute JobOffersForm jobOffersForm) {
	
		EndUser user = getCurrentLoggedUserDetails();

		model.put("user", user);
		model.put("endUserForm", endUserForm);
		return new ModelAndView("adminBlockRenewTransaction", "model", model);
	}
	
	
	/**
	 * @author spysr_dev
	 * Method for getting time sheet data
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/approveTimeSheetDetails", method = RequestMethod.GET)
	public ModelAndView approveTimeSheetDetails(ModelMap model) {
		Collection<TimeSheet> timeSheets = timeSheetDAO.gettingApproveData();
		 if(timeSheets != null && timeSheets.size()  > 0) {
			 model.put("timeSheets", timeSheets);
			 return new ModelAndView("approveTimeSheetDetails", "model", model);
	     } else {
	    	 return new ModelAndView("noDataFoundAdmin", "model", model);
	       }
	}
	
	
	/**
	 * @author spysr_dev
	 * Method to get individual time sheet data
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/getData", method = RequestMethod.GET)
	public ModelAndView getData(ModelMap model, @RequestParam("id") Long id) {
	
		TimeSheetForm timeSheetForm = timeSheetService.getById(id);
		
		model.put("timeSheetForm", timeSheetForm);
		return new ModelAndView("getData","model",model);
		
	}
	
	/**
	 * @author spysr_dev
	 * Method to update data
	 * @param model
	 * @param timeSheetForm
	 * @return
	 */
	@RequestMapping(value = "/updateTimeSheet", method = RequestMethod.POST)
	public ModelAndView updateTimeSheet(ModelMap model, @ModelAttribute TimeSheetForm timeSheetForm) {
		
		timeSheetService.updateByAdmin(timeSheetForm);
		
		return new ModelAndView("updatedTimeSheet");
	}
	
	
	
	/**
	 * @author spysr_dev
	 * Method for getting time sheet data
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/timeSheetDetails", method = RequestMethod.GET)
	public ModelAndView timeSheetDetails(ModelMap model) {
		Collection<TimeSheet> timeSheets = timeSheetDAO.gettingData();
		 if(timeSheets != null && timeSheets.size()  > 0) {
			 model.put("timeSheets", timeSheets);
			 return new ModelAndView("timeSheetDetails", "model", model);
	     } else {
	    	 return new ModelAndView("noDataFoundAdmin", "model", model);
	       }
	}
	
	
	/**
	 * @author spysr_dev
	 * Method to get individual time sheet data
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/getTimeSheet", method = RequestMethod.GET)
	public ModelAndView getTimeSheet(ModelMap model, @RequestParam("id") Long id) {
	
		TimeSheetForm timeSheetForm = timeSheetService.getById(id);
		
		model.put("timeSheetForm", timeSheetForm);
		return new ModelAndView("getTimeSheetData","model",model);
		
	}
	
	
	/**
	 * @author spysr_dev
	 * Method for getting job offers form
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/jobOffers", method = RequestMethod.GET)
	public ModelAndView jobOffers(ModelMap model) {
	
		model.put("jobOffersForm", jobOffersForm);
		return new ModelAndView("jobOffersForm","model",model);
		
	}
	
	/**
	 * @author spysr_dev
	 * Method for adding details
	 * @param model
	 * @param jobOffersForm
	 * @return
	 */
	@RequestMapping(value = "/addJobOffers", method = RequestMethod.POST)
	public String addJobOffers(ModelMap model,@ModelAttribute JobOffersForm jobOffersForm, RedirectAttributes attributes) {
		Date date = DateService.getCurrentDate();
		if(date.compareTo(jobOffersForm.getLastDate())<0)
		{
			
		jobOffersService.addDetails(jobOffersForm);
		
		attributes.addFlashAttribute("jobOffersForm", jobOffersForm);
		return "redirect:jobOffersSaved";
		}
		else
		{
			attributes.addFlashAttribute(Constants.SUCCESS, Constants.DATEINVALID);
			return "redirect:jobOffers";	
		}
	}
	
	/**
	 * @author spysr_dev
	 * Method for saved details
	 * @param model
	 * @param jobOffersForm
	 * @return
	 */
	@RequestMapping(value = "/jobOffersSaved", method = RequestMethod.GET)
	public ModelAndView jobOffersSaved(ModelMap model,@ModelAttribute JobOffersForm jobOffersForm) {
	
		model.put("jobOffersForm", jobOffersForm);
		return new ModelAndView("jobOffersFormSaved","model",model);
		
	}

		
	@RequestMapping(value = "/foodbillCalculation", method = RequestMethod.GET)
	public ModelAndView foodbillCalculation(ModelMap model,@ModelAttribute TimeSheetForm timeSheetForm) {
		
		EndUser user = getCurrentLoggedUserDetails();
		model.put("user", user);
	
		
	  List<TimeSheet> timeList = timeSheetDAO.gettingTimesheetlist().getResultList();
	 
	 
		if(timeList != null && timeList.size() > 0)
		{
		model.put("timeList", timeList);
		model.put("timeSheetForm", timeSheetForm);
		return new ModelAndView("foodbillCalculation", "model", model);
		}
		else
		{
			return new ModelAndView("noDataFoundAdmin", "model", model);
		}

	}
	
	
	@RequestMapping(value = "/viewTimeSheet", method = RequestMethod.GET)
	public ModelAndView viewTimeSheet(ModelMap model, @RequestParam("id") Long id) {

	TimeSheetForm timeSheetForm = timeSheetService.getById(id);
		
		model.put("timeSheetForm", timeSheetForm);
		return new ModelAndView("viewTimeSheet","model",model);
	
	}
	
	
	/**
	 * method for bus pass bill data
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/busPassBill", method = RequestMethod.GET)
	public ModelAndView busPassBill(ModelMap model) {
		
		Collection<FoodBill> foodBills = foodBillDAO.getAllData();
		
		if(foodBills.size() != 0)
		{
			Set<FoodBill> set = new TreeSet<FoodBill>(new Comparator<FoodBill>() {

				@Override
				public int compare(FoodBill o1, FoodBill o2) {
					if (o1.getUserName().equals(o2.getUserName())) {
						return 0;
					} else {
						return 1;
					}
				}
			});
			set.addAll(foodBills);
			foodBills.clear();
			foodBills.addAll(set);
			model.put("foodBills", foodBills);
			foodBillForm.setFoodBills(foodBills);
			model.put("foodBillForm", foodBillForm);
			return new ModelAndView("busPassBillAdmin","model",model);
		}
		else
		{
			return new ModelAndView("noDataFoundAdmin", "model", model);
		}
		
	}
	
	
	/**
	 * Method for individual bus pass bill
	 * @param model
	 * @param foodBillForm
	 * @return
	 */
	@RequestMapping(value = "/getBusPassBill", method = RequestMethod.POST)
	public ModelAndView getBusPassBill(ModelMap model,@ModelAttribute FoodBillForm foodBillForm) {

		Collection<FoodBill> foodBills = foodBillDAO.getIndividualData(foodBillForm.getUserName());
		
		model.put("foodBills", foodBills);
		
		return new ModelAndView("getBusPassBill","model",model);
	}
		
	
	/**
	 * Method for individual leaveTypeList
	 * @param model
	 * @param leaveTypeForm
	 * @return
	 */
	@RequestMapping(value = "/leaveTypeList", method = RequestMethod.GET)
	public ModelAndView leaveTypeList(ModelMap model) {
		LeaveTypeForm leaveTypeForm = new LeaveTypeForm();
		Collection<LeaveType> leaveTypeList = leaveRequestDAO.getLeaveTypeList(leaveTypeForm.getName());
		model.put("leaveTypeList", leaveTypeList);
		leaveRequestForm = new LeaveRequestForm();
		model.put("leaveRequestForm", leaveRequestForm);
		return new ModelAndView("leaveTypeList","model",model);
	}
		
	
		
	
}
