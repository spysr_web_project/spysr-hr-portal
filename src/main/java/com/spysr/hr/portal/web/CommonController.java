package com.spysr.hr.portal.web;

import java.util.Calendar;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.LocaleUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.theme.CookieThemeResolver;

import com.spysr.hr.portal.dao.EndUserDAO;
import com.spysr.hr.portal.entity.EndUser;
import com.spysr.hr.portal.utility.Constants;

@Controller
@RequestMapping("/main")
public class CommonController {

	@Autowired
	EndUserDAO endUserDAO;

	private CookieLocaleResolver localeResolver;
	private CookieThemeResolver themeResolver;

	static Logger log = Logger.getLogger(AdminController.class.getName());

	@RequestMapping(value = "/default", method = RequestMethod.GET)
	public String getDefaultPage(HttpServletRequest request, HttpServletResponse response,
			RedirectAttributes attributes) {

		EndUser endUser = endUserDAO.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName())
				.getSingleResult();
		HttpSession session = request.getSession();
		if (!request.isUserInRole("ROLE_ADMIN")) {
			if (null != endUser.getAccessStatus() && endUser.getAccessStatus().equals(Constants.BLOCKED)) {

				return "redirect:/auth/login?error=2";

			}
		}

		if (endUser.getTheme() != null) {
			themeResolver = new CookieThemeResolver();
			themeResolver.setThemeName(request, response, endUser.getTheme());
		}

		if (endUser.getPrefferedLanguage() != null) {

			log.info("Changing to default language " + endUser.getPrefferedLanguage());
			localeResolver = new CookieLocaleResolver();
			String defaultLocale = endUser.getPrefferedLanguage();
			Locale currentLanguage = LocaleUtils.toLocale(defaultLocale);
			localeResolver.setLocale(request, response, currentLanguage);
		}
		if ((endUser.getPasswordFlag() == 0)) {

			attributes.addFlashAttribute("error", "Password expired!!Please change the password");
			return "redirect:/auth/loginChangePassword?id=" + endUser.getId();

		} else {
			session.setAttribute("played", false);
			if (request.isUserInRole("ROLE_ADMIN")) {
				return "redirect:/admin/adminPage";
			} else if (request.isUserInRole("ROLE_MANAGER")) {
				return "redirect:/managers/manager";
			} else if (request.isUserInRole("ROLE_EMP")) {
				return "redirect:/employees/employee";
			} else {
				return "redirect:/officeManagers/officeManager";
			}
		}
	}

}
