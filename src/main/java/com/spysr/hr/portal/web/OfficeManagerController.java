package com.spysr.hr.portal.web;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.binary.Base64;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailParseException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.theme.CookieThemeResolver;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfWriter;
import com.spysr.hr.portal.dao.EndUserDAO;
import com.spysr.hr.portal.dao.FoodBillDAO;
import com.spysr.hr.portal.dao.HolidayDAO;
import com.spysr.hr.portal.dao.LeaveRequestDAO;
import com.spysr.hr.portal.dao.TimeSheetDAO;
import com.spysr.hr.portal.entity.EndUser;
import com.spysr.hr.portal.entity.FoodBill;
import com.spysr.hr.portal.entity.HolidayList;
import com.spysr.hr.portal.entity.LeaveRequest;
import com.spysr.hr.portal.entity.TimeSheet;
import com.spysr.hr.portal.form.EndUserForm;
import com.spysr.hr.portal.form.FoodBillForm;
import com.spysr.hr.portal.form.HolidayListForm;
import com.spysr.hr.portal.form.LeaveRequestForm;
import com.spysr.hr.portal.form.RequestedDocsForm;
import com.spysr.hr.portal.form.TimeSheetForm;
import com.spysr.hr.portal.services.DateService;
import com.spysr.hr.portal.services.EnduserService;
import com.spysr.hr.portal.services.HolidayService;
import com.spysr.hr.portal.services.ImageService;
import com.spysr.hr.portal.services.LeaveRequestServiceImpl;
import com.spysr.hr.portal.services.TimeSheetService;
import com.spysr.hr.portal.utility.Constants;

/**
 * @author spysr_dev
 *
 */
@Controller
@RequestMapping("/officeManagers")
public class OfficeManagerController {
	
	@Autowired
	EndUserDAO endUserDAOImpl;

	@Autowired
	EndUserForm endUserForm;
	
	@Autowired
	EnduserService enduserService;
	
	@Autowired
	HolidayDAO holidayDAO;
	@Autowired
	HolidayService holidayService;
	
	
	@Autowired
	JavaMailSender mailSender;
	
	@Autowired
	LeaveRequestForm leaveRequestForm;
	
	@Autowired
	LeaveRequestDAO leaveRequestDAO;
	
	@Autowired
	LeaveRequestServiceImpl leaveRequestService;
	@Autowired	 
	HolidayListForm  holidayListForm;
	
	@Autowired
	TimeSheetForm timeSheetForm;
	
	@Autowired
	TimeSheetService timeSheetService;
	
	@Autowired
	TimeSheetDAO timeSheetDAO;
	
	@Autowired
	RequestedDocsForm requestedDocsForm;
	
	@Autowired
	FoodBillDAO foodBillDAO;
	
	@Autowired
	FoodBillForm foodBillForm;
	
	public void setMailSender(JavaMailSender mailSender) {
		this.mailSender = mailSender;
	}
	
	CookieThemeResolver themeResolver = new CookieThemeResolver();

	static Logger log = Logger.getLogger(AdminController.class.getName());

	private String getCurrentLoggedUserName() {
		return SecurityContextHolder.getContext().getAuthentication().getName();

	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		CustomDateEditor editor = new CustomDateEditor(dateFormat, true);
		binder.registerCustomEditor(Date.class, editor);
	}

	/**
	 * @author spysr_dev
	 * Method to get current user details
	 */
	private EndUser getCurrentLoggedUserDetails() {

		EndUser endUser = endUserDAOImpl.findByUsername(getCurrentLoggedUserName()).getSingleResult();

		return endUser;

	}
	
	/**
	 * @author spysr_dev
	 * Method to calling current user details
	 * 
	 * @param model
	 * @return
	 */
	@ModelAttribute("requestCurrentUser")
	public EndUser getUserDetails(ModelMap model) {
		EndUser user = getCurrentLoggedUserDetails();
		if (user.getImageName() != null) {
			String type = ImageService.getImageType(user.getImageName());

			String url = "data:image/" + type + ";base64," + Base64.encodeBase64String(user.getImage());
			user.setImageName(url);
		}
		
		model.put("user", user);
		return user;
	}

	 

	/**
	 * @author spysr_dev
	 * Method to editing profile
	 * 
	 * @param model,
	 *            id
	 * @return view
	 */
	@RequestMapping(value = "/editOfficeManagerProfile", method = RequestMethod.GET)
	public ModelAndView editAdminProfile(ModelMap model, @RequestParam("id") Long id, RedirectAttributes attributes) {

		EndUser userProfile = endUserDAOImpl.findId(id);

		if (userProfile.getImageName() != null) {
			String type = ImageService.getImageType(userProfile.getImageName());

			String url = "data:image/" + type + ";base64," + Base64.encodeBase64String(userProfile.getImage());
			userProfile.setImageName(url);

			endUserForm.setImageName(url);
		}
		endUserForm.setId(userProfile.getId());
		endUserForm.setDisplayName(userProfile.getDisplayName());
		endUserForm.setUserName(userProfile.getUserName());
		
		endUserForm.setContactNo(userProfile.getContactNo());
		endUserForm.setEmail(userProfile.getEmail());

		endUserForm.setPassword(userProfile.getPassword());
		

		model.put("endUserForm", endUserForm);

		return new ModelAndView("editOfficeManagerProfile", "model", model);

	}
	
	/**
	 * @author spysr_dev
	 * Method to updating profile image
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/updateImageForProfile", method = RequestMethod.POST)
	public String updateImageForProfile(ModelMap model, @ModelAttribute EndUserForm endUserForm) {

		try {
			EndUser userProfile = endUserDAOImpl.findId(endUserForm.getId());
			userProfile.setImage(endUserForm.getFile().getBytes());
			userProfile.setImageName(endUserForm.getFile().getOriginalFilename());
			endUserDAOImpl.update(userProfile);

		} catch (Exception e) {
			e.getMessage();
		}
		return "redirect:editOfficeManagerProfile?id=" + endUserForm.getId();
	}

	/**
	 * @author spysr_dev
	 * Method to confirm edit profile
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/confirmEditEmployeeProfile", method = RequestMethod.POST)
	public ModelAndView confirmEditAdminProfile(ModelMap model, @ModelAttribute EndUserForm endUserForm) {

		model.put("endUserForm", endUserForm);

		return new ModelAndView("confirmEditOfficeManagerProfile", "model", model);

	}
    
	/**
	 * @author spysr_dev
	 * Method to updating profile details
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/updateEmployeeDetails", method = RequestMethod.POST)
	public ModelAndView updateAdminDetails(ModelMap model, @ModelAttribute EndUserForm endUserForm,
			BindingResult result, RedirectAttributes attributes) {

		EndUser endUser = endUserDAOImpl.findId(endUserForm.getId());

		endUser.setId(endUserForm.getId());

		endUser.setDisplayName(endUserForm.getDisplayName());
		endUser.setContactNo(endUserForm.getContactNo());
	
		endUser.setEmail(endUserForm.getEmail());
	
		endUser.setUserName(endUserForm.getUserName());
		
		endUserDAOImpl.update(endUser);

		model.put("endUserForm", endUserForm);

		return new ModelAndView("updateOfficeManagerSuccess", "model", model);

	}
	
	/**
	 * @author spysr_dev
	 * Method to edit password details
	 * 
	 * @param model,id
	 * @return
	 */
	@RequestMapping(value = "/editEmployeePWD", method = RequestMethod.GET)
	public ModelAndView editAdminPWD(ModelMap model, @RequestParam("id") Long id, RedirectAttributes attributes) {

		EndUser userProfile = endUserDAOImpl.findId(id);

		endUserForm.setId(userProfile.getId());

		

		model.put("endUserForm", endUserForm);

		return new ModelAndView("editOfficeManagerPWD", "model", model);

	}

	/**
	 * @author spysr_dev
	 * Method to updating password details
	 * 
	 * @param model,
	 *            enduserForm
	 * @return
	 */
	@RequestMapping(value = "/updateEditEmployeePWD", method = RequestMethod.POST)
	public ModelAndView updateEditAdminPWD(ModelMap model, @ModelAttribute EndUserForm endUserForm,
			BindingResult result, RedirectAttributes attributes) {

		EndUser endUser = endUserDAOImpl.findId(endUserForm.getId());

		endUser.setId(endUserForm.getId());

		endUser.setPassword(endUserForm.getPassword());
	
		endUserDAOImpl.update(endUser);

		model.put("endUserForm", endUserForm);

		return new ModelAndView("updateOfficeManagerSuccess", "model", model);

	}
    
	/**
	 * @author spysr_dev
	 * Method to change theme
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/themeChange", method = RequestMethod.GET)
	public String getThemeChange(ModelMap model, HttpServletRequest request, HttpServletResponse response) {

		log.info("Received request for theme change");

		EndUser endUser = endUserDAOImpl.findByUsername(getCurrentLoggedUserName()).getSingleResult();
		if (endUser.getTheme() == null)
			themeResolver.setThemeName(request, response, "themeBlue");
		if (!endUser.getTheme().equalsIgnoreCase(request.getParameter("theme"))) {

			if (request.getParameter("theme").equalsIgnoreCase("themeBlue")) {
				themeResolver.setThemeName(request, response, "themeBlue");
			} else if (request.getParameter("theme").equalsIgnoreCase("themeGreen")) {
				themeResolver.setThemeName(request, response, "themeGreen");
			} else if (request.getParameter("theme").equalsIgnoreCase("themeOrange")) {
				themeResolver.setThemeName(request, response, "themeOrange");
			} else if (request.getParameter("theme").equalsIgnoreCase("themeRed")) {
				themeResolver.setThemeName(request, response, "themeRed");
			}

			endUser.setTheme(request.getParameter("theme"));
			endUserDAOImpl.update(endUser);
		} else
			themeResolver.setThemeName(request, response, endUser.getTheme());

		return "redirect:officeManager";
	}

	/**
	 * @author spysr_dev
	 * Method to change language
	 * 
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/getLocaleLang", method = RequestMethod.GET)
	public String getLocaleLang(ModelMap model, HttpServletRequest request, HttpServletResponse response) {

		log.info("Received request for locale change");
		EndUser user = endUserDAOImpl.findByUsername(getCurrentLoggedUserName()).getSingleResult();
		LocaleResolver localeResolver = new CookieLocaleResolver();
		Locale locale = new Locale(request.getParameter("lang"));
		localeResolver.setLocale(request, response, locale);
		user.setPrefferedLanguage(request.getParameter("lang"));

		endUserDAOImpl.update(user);
		return "redirect:officeManager";
	}
	
	
	@RequestMapping(value = "/officeManager", method = RequestMethod.GET)
	public ModelAndView showadminDashBoard(ModelMap model, HttpServletRequest request, HttpServletResponse response) {

		return new ModelAndView("officeManager", "model", model);
	}
	
	
	@RequestMapping(value="/officeMngLeaveRequest",method=RequestMethod.GET)
	public ModelAndView LeaveRequest(ModelMap model,
			RedirectAttributes attributes) {
		EndUser user = endUserDAOImpl
				.findByUsername(getCurrentLoggedUserName()).getSingleResult();
		model.put("user", user);
	
		leaveRequestForm.setUserName(user.getUserName());
		leaveRequestForm.setEligibleLeave(user.getEligibleLeave());
		leaveRequestForm.setTotalLeave(user.getTotalLeave());
	    leaveRequestForm.setDepartment(user.getDepartment());
	    leaveRequestForm.setDesignation(user.getDesignation());
	    leaveRequestForm.setDoj(user.getDoj());
	    leaveRequestForm.setEmail(user.getEmail());
	    leaveRequestForm.setName(user.getName());
	    leaveRequestForm.setEmail(user.getEmail());
		model.put("leaveRequestForm", leaveRequestForm);
        return new ModelAndView("officeMngLeaveRequestDetails", "model", model);
       }
	
	
	@RequestMapping(value="/insertLeaveApplied", method=RequestMethod.POST)
	public String insertLeaveAppied(@ModelAttribute ModelMap map,LeaveRequestForm leaveRequestForm){
		EndUser user=getCurrentLoggedUserDetails();
		
	     leaveRequestService.Save(leaveRequestForm);
		map.put("user", user);
		map.put("leaveRequestForm", leaveRequestForm);
		return "redirect:insertLeaveSuccess";
		
	}
	
	@RequestMapping(value="/insertLeaveSuccess",method=RequestMethod.GET)
	public ModelAndView insertsuccess(@ModelAttribute ModelMap model ){
		EndUser user=getCurrentLoggedUserDetails();
		model.put("user",user);
		return new ModelAndView("insertOfficeMngLeaveSuccess","model",model);
	}
	
	
	@RequestMapping(value = "/officeMngleavecancelDetails", method = RequestMethod.GET)
	public ModelAndView employeeLeaveAppliedDetails(ModelMap model, HttpServletRequest request,
			HttpServletResponse response) {

		EndUser user = getCurrentLoggedUserDetails();
		model.put("user", user);
		List<LeaveRequest> leaveRequestist = leaveRequestDAO.getAppliedLeaves(user.getUserName()).getResultList();

		model.put("user", user);
		if(leaveRequestist != null && leaveRequestist.size() > 0)
		{
		model.put("leaveRequestist", leaveRequestist);
		return new ModelAndView("OfficeMngleavecancelDetails", "model", model);
		}
		else
		{
			return new ModelAndView("noDataFoundOfficeManager", "model", model);
		}

	}
	
	
	@RequestMapping(value = "/cancelleavesemployeeRequest", method = RequestMethod.GET)
	public ModelAndView leavePendingemployeeRequest(@RequestParam Long id, ModelMap model,
			RedirectAttributes attributes) {
		EndUser user = getCurrentLoggedUserDetails();

	
		LeaveRequest leaveRequest=leaveRequestDAO.findId(id);

		leaveRequestForm.setId(leaveRequest.getId());
	    leaveRequestForm.setUserName(leaveRequest.getUserName());
	    leaveRequestForm.setDepartment(leaveRequest.getDepartment());
	    leaveRequestForm.setDesignation(leaveRequest.getDesignation());
	    leaveRequestForm.setName(leaveRequest.getName());
	    leaveRequestForm.setDoj(leaveRequest.getDoj());
	    leaveRequestForm.setEligibleLeave(leaveRequest.getEligibleLeave());
	    leaveRequestForm.setTotalLeave(leaveRequest.getTotalLeave());
	    leaveRequestForm.setTypesofLeave(leaveRequest.getTypesofLeave());
	    leaveRequestForm.setReason(leaveRequest.getReason());
	    leaveRequestForm.setStatus(leaveRequest.getStatus());
	    leaveRequestForm.setLeaveStatus(leaveRequest.getLeaveStatus());
	    leaveRequestForm.setComment(leaveRequest.getComment());
	    leaveRequestForm.setToDate(leaveRequest.getToDate());
	    leaveRequestForm.setFromDate(leaveRequest.getFromDate());
	    leaveRequestForm.setEmail(leaveRequest.getEmail());
	
	    leaveRequestForm.setTotalnumberleaveDays(leaveRequest.getTotalnumberleaveDays());
	    leaveRequestForm.setLeaveDate(leaveRequest.getLeaveDate());
	   
		
		model.put("user", user);

		model.put("leaveRequestForm", leaveRequestForm);

		return new ModelAndView("cancelleavesOfficeMngRequest", "model", model);
	}

	@RequestMapping(value = "/leavecancelupdate", method = RequestMethod.POST)
	public String leavePendingemployeeRequesthowConfrim(
			@ModelAttribute LeaveRequestForm leaveRequestForm, ModelMap model,
			RedirectAttributes attributes) {
	
		LeaveRequest leaveRequest=leaveRequestDAO.findId(leaveRequestForm.getId());

		leaveRequest.setLeaveStatus(leaveRequestForm.getLeaveStatus());
		leaveRequestDAO.update(leaveRequest);

        model.put("leaveRequestForm", leaveRequestForm);

		return "redirect:leavecancelupdate";
	}
	
	
	@RequestMapping(value="/leavecancelupdate",method=RequestMethod.GET)
	public ModelAndView leavePendingemployeeRequesthowConfrim(ModelMap model)
	{
		return new ModelAndView("officeMngLeavecancelupdate","model",model);
	}	
	
	
	@RequestMapping(value = "/querymailOfficeMng", method = RequestMethod.GET)
	public ModelAndView doSendEmail1(@ModelAttribute ModelMap model) {
		EndUser user = getCurrentLoggedUserDetails();

		model.put("user", user);

		return new ModelAndView("querymailOfficeMng", "model", model);
	}

	/**
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/mailSender", method = RequestMethod.POST)
	public ModelAndView doSendEmail(@ModelAttribute ModelMap model,
			HttpServletRequest request) {
		EndUser user = getCurrentLoggedUserDetails();
     try{
		String recipientAddress = request.getParameter("recipient");
		String subject = request.getParameter("subject");
		String message = request.getParameter("message");

		System.out.println("To: " + recipientAddress);
		System.out.println("Subject: " + subject);
		System.out.println("Message: " + message);

		// creates a simple e-mail object
		SimpleMailMessage email = new SimpleMailMessage();
		email.setTo(recipientAddress);
		email.setSubject(subject);
		email.setText(message);
		// sends the e-mail
		mailSender.send(email);

		model.put("user", user);
     }catch(Exception e){
    	 e.getMessage();
     }
		// forwards to the view named "Result"
		return new ModelAndView("officeMngQuerymailSuccess", "model", model);
	}
	
	
	@RequestMapping(value = "/listofholiDays", method = RequestMethod.GET)
	public ModelAndView listofholiDays(ModelMap model) {

		EndUser user = getCurrentLoggedUserDetails();

		List<HolidayList> holidayList = holidayDAO.holidayList().getResultList();
		if (holidayList != null && holidayList.size() > 0) {
			
			model.put("holidayList", holidayList);
			model.put("user", user);
			return new ModelAndView("officeMngListofholiDays", "model", model);
		}
		else
		{
			return new ModelAndView("noDataFoundOfficeManager", "model", model);
		}

	}
	
	/**
	 * @author spysr_dev
	 * Method for getting Time sheet
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/timeSheet", method = RequestMethod.GET)
	public ModelAndView timeSheet(ModelMap model) {

		EndUser user = getCurrentLoggedUserDetails();
		timeSheetForm.setUserName(user.getUserName());
		timeSheetForm.setName(user.getName());
		timeSheetForm.setRole(user.getCurrentRole());
		timeSheetForm.setEmail(user.getEmail());
		
		model.put("timeSheetForm", timeSheetForm);
		return new ModelAndView("officeMngTimeSheet", "model", model);

	}
	/**
	 * @author spysr_dev
	 * Method for saving time sheet data
	 * @param model
	 * @param timeSheetForm
	 * @return
	 */
	@RequestMapping(value = "/timeSheetData", method = RequestMethod.POST)
	public String timeSheetData(ModelMap model,@ModelAttribute TimeSheetForm timeSheetForm,RedirectAttributes attributes) {
		
		Collection<TimeSheet> timeSheet = timeSheetDAO.checkData(timeSheetForm.getUserName(), timeSheetForm.getMonday());
		if ( timeSheet.size() !=0) {
			attributes.addFlashAttribute(Constants.SUCCESS, Constants.TIMESHEET);
			return "redirect:timeSheet";
		} 
		
	timeSheetService.add(timeSheetForm);
	return "redirect:dataSaved";
	}
	
	/**
	 * @author spysr_dev
	 * Method for saved
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/dataSaved", method = RequestMethod.GET)
	public ModelAndView dataSaved(ModelMap model) {
	return new ModelAndView("officeMngDataSaved","moodel",model);
	}

	/**
	 * @author spysr_dev
	 * Method for getting time sheet data
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/rejectedTimeSheet", method = RequestMethod.GET)
	public ModelAndView rejectedTimeSheet(ModelMap model) {
		
		EndUser user = getCurrentLoggedUserDetails();
		
		Collection<TimeSheet> timeSheets = timeSheetDAO.rejectedData(user.getUserName());
		 if(timeSheets != null && timeSheets.size()  > 0) {
			 model.put("timeSheets", timeSheets);
			 return new ModelAndView("rejectOfficeMngTimeSheetData", "model", model);
	     } else {
	    	 return new ModelAndView("noDataFoundOfficeManager", "model", model);
	       }
	}
	
	/**
	 * @author spysr_dev
	 * Method to get individual time sheet data
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/getData", method = RequestMethod.GET)
	public ModelAndView getData(ModelMap model, @RequestParam("id") Long id) {
	
		TimeSheetForm timeSheetForm = timeSheetService.getById(id);
		
		model.put("timeSheetForm", timeSheetForm);
		return new ModelAndView("getOfficeMngRejectedData","model",model);
		
	}	
	
	/**
	 * @author spysr_dev
	 * Method for updating
	 * @param model
	 * @param timeSheetForm
	 * @return
	 */
	@RequestMapping(value = "/updateData", method = RequestMethod.POST)
	public ModelAndView updateData(ModelMap model, @ModelAttribute TimeSheetForm timeSheetForm) {
         timeSheetService.updateByAdmin(timeSheetForm);
		
		return new ModelAndView("updateOfficeMngTS");
	}
	
	/**
	 * @author spysr_dev
	 * Method for getting  timesheet data
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/statusTimeSheet", method = RequestMethod.GET)
	public ModelAndView statusTimeSheet(ModelMap model) {
		EndUser user = getCurrentLoggedUserDetails();
		Collection<TimeSheet> timeSheets= timeSheetDAO.gettingOwnData(user.getUserName());
		if(timeSheets != null && timeSheets.size()  > 0) {
			 model.put("timeSheets", timeSheets);
			 return new ModelAndView("officeMngTimeSheetData", "model", model);
	     } else {
	    	 return new ModelAndView("noDataFoundOfficeManager", "model", model);
	       }
	}
	
	/**
	 * @author spysr_dev
	 * Method to get individual time sheet data
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/getTimeSheet", method = RequestMethod.GET)
	public ModelAndView getTimeSheet(ModelMap model, @RequestParam("id") Long id) {
	
		TimeSheetForm timeSheetForm = timeSheetService.getById(id);
		
		model.put("timeSheetForm", timeSheetForm);
		return new ModelAndView("getofficeMngTimeSheetData","model",model);
		
	}
	
	/**
	 * @author spysr_dev
	 * Method for requesting Documents
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/requestedDocs", method = RequestMethod.GET)
	public ModelAndView requestedDocs(ModelMap model) {
		
		EndUser user = getCurrentLoggedUserDetails();
		
		requestedDocsForm.setName(user.getName());
		requestedDocsForm.setUserName(user.getUserName());
		requestedDocsForm.setAddress(user.getAddress());
		requestedDocsForm.setEmail(user.getEmail());
		
		model.put("requestedDocsForm", requestedDocsForm);
		return new ModelAndView("requestedOfficeMngDocsForm","model",model);
		
	}
	/**
	 * Method for confirmation
	 * @param model
	 * @param requestedDocsForm
	 * @return
	 */
	@RequestMapping(value = "/confirmDoc", method = RequestMethod.POST)
  	public ModelAndView confirmDoc(ModelMap model,@ModelAttribute RequestedDocsForm requestedDocsForm) {
		model.put("requestedDocsForm", requestedDocsForm);
  		return new ModelAndView("confirmOfficeMngForm","model",model);
  		
		
	}
	
	/**
	 * @author spysr_dev
	 * @param model
	 * @param requestedDocsForm
	 */
	@RequestMapping(value = "/getDoc", method = RequestMethod.POST)
	public String getDoc(ModelMap model,@ModelAttribute RequestedDocsForm requestedDocsForm,HttpServletRequest request) {
		
		EndUser user = getCurrentLoggedUserDetails();
		
		Format formatter = new SimpleDateFormat("dd-MM-YYYY");
		String s = formatter.format(new Date());
		try{
			OutputStream file;
		 file = new FileOutputStream(new File(Constants.DRIVE + requestedDocsForm.getName() + "_" + requestedDocsForm.getDocName() + ".pdf"));
		Font bold = new Font(Font.FontFamily.HELVETICA, 12f, Font.BOLD);
		Font normal = new Font(Font.FontFamily.HELVETICA, 12f, Font.NORMAL);
		Document document = new Document(PageSize.A4, 36, 36, 154, 54);
		PdfWriter writer = PdfWriter.getInstance(document, file);
		
        /*HeaderFooter event = new HeaderFooter();
		writer.setPageEvent(event);*/
		document.open();
		
		Image header = Image.getInstance(request.getSession().getServletContext().getRealPath("/") + "resources/images/header.jpg");
		
		header.scalePercent(200f);
		header.setAbsolutePosition(0,(float) (PageSize.A4.getHeight() - 105.0));
		
		document.add(header);
		header.scaleToFit(600f, 500f);
        document.add(header);

		
		Paragraph ph= new Paragraph("                                                                                                                       Date: "+ s,bold);
		Paragraph ph1= new Paragraph("                                                                                                                       Bangalore,",bold);
		
		document.add(ph);
		document.add(ph1);
		
		
		String name = user.getName();
		
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		Date date = user.getDoj();
		String doj = format.format(date);
		
		if(requestedDocsForm.getDocName().equals("Home Loan"))
				{
		Chunk ch = new Chunk("KOTAK Bank,MGRoad branch,Banglore",normal);
		Phrase p = new Phrase(ch);
				
		
		float salary = user.getTotalPackage();
		
		
		
		document.add(new Paragraph("To :",bold));
		document.add(p);
		
		document.add(new Paragraph(
				"Re :",bold));
		document.add(new Paragraph(name));

		
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		
		document.add(new Paragraph(
				"We confirm the following details regarding "+name+"'s employment with SpYsR HR:",normal));
		document.add(Chunk.NEWLINE);
		document.add(new Paragraph(
				"His salary is Rs."+salary+" per annum gross.",normal));
		document.add(new Paragraph("He is employed on a permanent full time basis."));
		
		document.add(new Paragraph("He started work with us on the "+doj));
		document.add(new Paragraph("He is not on probation."));
		document.add(Chunk.NEWLINE);
		document.add(new Paragraph("Should you require any additional information, please do not hesitate to contact +91 8065777004."));
		
				}
				else
				{
					
					
					String desg = user.getDesignation();
					document.add(Chunk.NEWLINE);
					document.add(Chunk.NEWLINE);
					document.add(Chunk.NEWLINE);
					document.add(new Paragraph("This is to certify that "+name +" is working with SpYsR HR since "+doj+" and he/she holding a designation "+desg+". "));
					document.add(Chunk.NEWLINE);
					document.add(Chunk.NEWLINE);
					document.add(new Paragraph("This certificate is issued to her/his for personal use"));
					document.add(Chunk.NEWLINE);
					document.add(Chunk.NEWLINE);
					document.add(Chunk.NEWLINE);
					document.add(new Paragraph("For(SpYsR HR)"));
					document.add(Chunk.NEWLINE);
					document.add(Chunk.NEWLINE);
					document.add(Chunk.NEWLINE);
					
				}
		document.add(Chunk.NEWLINE);
		document.add(new Paragraph("Regards,"));
		document.add(Chunk.NEWLINE);
		document.add(new Paragraph("(SIGN)"));
		document.add(Chunk.NEWLINE);
		document.add(new Paragraph("Partho,"));
		document.add(new Paragraph("Manager,"));
		document.add(new Paragraph("SpYsR HR"));
				
		
		document.add(Chunk.NEWLINE);		
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);
		document.add(Chunk.NEWLINE);		
		document.add(Chunk.NEWLINE);
		
		
		Image footer = Image.getInstance(request.getSession().getServletContext().getRealPath("/") + "resources/images/footer.jpg");
		footer.scaleToFit(500f,500f);
       document.add(footer);
		
		document.close();
		
			MimeMessage message = mailSender.createMimeMessage();
			
				MimeMessageHelper helper = new MimeMessageHelper(message, true);

				helper.setTo(requestedDocsForm.getEmail());
				helper.setSubject("Document For " + requestedDocsForm.getDocName());
				helper.setText(String
						.format(requestedDocsForm.getName() + ",\n\n Enclosed, please find your requested document " + s
								+ "\n\n\nPartho H. Chakraborty\nSpYsR HR"));

				FileSystemResource attachment = new FileSystemResource(
						Constants.DRIVE + requestedDocsForm.getName() + "_" + requestedDocsForm.getDocName() + ".pdf");
				helper.addAttachment(attachment.getFilename(), attachment);
			
		mailSender.send(message);

		}
	   catch (MessagingException e) {
			throw new MailParseException(e);
		}
		
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	return "redirect:docSend";
	}
	
	/**
	 * @author spysr_dev
	 * Method for saved
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/docSend", method = RequestMethod.GET)
	public ModelAndView docSend(ModelMap model) {
		
		return new ModelAndView("officeMngDocSend","model",model);
		
	}
	
	/**
	 * Method for weekly bill
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/weeklyBill", method = RequestMethod.GET)
	public ModelAndView weeklyBill(ModelMap model,@ModelAttribute TimeSheetForm timeSheetForm) {

		model.put("timeSheetForm", timeSheetForm);
		return new ModelAndView("datePage","model",model);
	}
	
	/**
	 * Method for getting weekly details
	 * @param model
	 * @param timeSheetForm
	 * @param attributes
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/getWeekBill", method = RequestMethod.POST)
	public ModelAndView getWeekBill(ModelMap model,@ModelAttribute TimeSheetForm timeSheetForm,RedirectAttributes attributes) {
		Date currentDate = DateService.getCurrentDate();
		Date weekDate = DateService.getDate(timeSheetForm.getMonday());
		Date monday = null;
		Date friday = null;
		Collection<TimeSheet> timeSheets = timeSheetDAO.getDetails(timeSheetForm.getMonday());
		if (weekDate.compareTo(currentDate) >= 0) {

			String success = Constants.FUTUREDATE;
			model.addAttribute("success", success);
			model.put("timeSheetForm", timeSheetForm);
			return new ModelAndView("datePage", "model", model); // "redirect:weeklyBill";
		} else if (timeSheets.size() == 0) {

			String success = Constants.NORECORDS;
			model.addAttribute("success", success);
			model.put("timeSheetForm", timeSheetForm);
			return new ModelAndView("datePage", "model", model);
		} else {
			
			for (TimeSheet timeSheet : timeSheets) {
				monday = timeSheet.getMonday();
				friday = timeSheet.getFriday();
				break;
			}

			Collection<EndUser> endUsers = endUserDAOImpl.getEmployees().getResultList();
			HashMap<String, String> map = new HashMap<String, String>();
			
			for (EndUser endUser : endUsers) {
				Collection<TimeSheet> timeSheets1 = timeSheetDAO.monday(endUser.getUserName(),
						timeSheetForm.getMonday());
				Collection<TimeSheet> timeSheets2 = timeSheetDAO.tuesday(endUser.getUserName(),
						timeSheetForm.getMonday());
				Collection<TimeSheet> timeSheets3 = timeSheetDAO.wednesday(endUser.getUserName(),
						timeSheetForm.getMonday());
				Collection<TimeSheet> timeSheets4 = timeSheetDAO.thursday(endUser.getUserName(),
						timeSheetForm.getMonday());
				Collection<TimeSheet> timeSheets5 = timeSheetDAO.friday(endUser.getUserName(),
						timeSheetForm.getMonday());
				if (timeSheets1.size() == 0 && timeSheets2.size() == 0 && timeSheets3.size() == 0 && timeSheets4.size() == 0 && timeSheets5.size() == 0) {
					map.put(endUser.getUserName(), endUser.getName());
				}				
				
			}			
			Double weekAmount = timeSheetDAO.getMondayBill(timeSheetForm.getMonday())
					+ timeSheetDAO.getTuesdayBill(timeSheetForm.getMonday())
					+ timeSheetDAO.getWednesdayBill(timeSheetForm.getMonday())
					+ timeSheetDAO.getThursdayBill(timeSheetForm.getMonday())
					+ timeSheetDAO.getFridayBill(timeSheetForm.getMonday());
			 
			timeSheetForm.setMonday(monday);
			timeSheetForm.setFriday(friday);
            model.addAttribute("weekAmount", weekAmount);
            model.put("map", map);
    		model.put("timeSheets", timeSheets);
    		model.put("timeSheetForm",timeSheetForm);
    		return new ModelAndView("weekBillDataDetails","model",model);
		}
	}
	
	/**
	 * @author spysr_dev
	 * Method to get individual time sheet data
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/getEachFoodBill", method = RequestMethod.GET)
	public ModelAndView getEachFoodBill(ModelMap model, @RequestParam("id") Long id) {
	
		TimeSheetForm timeSheetForm = timeSheetService.getById(id);
		float amount = timeSheetService.getWeekData(id);
		model.addAttribute("amount", amount);
		model.put("timeSheetForm", timeSheetForm);
		return new ModelAndView("getEachTimeSheetData","model",model);
		
	}
	
	/**
	 * Method for sent mail
	 * @param model
	 * @param timeSheetForm
	 * @return
	 */
	@RequestMapping(value = "/sendingData", method = RequestMethod.POST)
	public ModelAndView sendingData(ModelMap model, @ModelAttribute TimeSheetForm timeSheetForm) {
		
		timeSheetService.sendWeekFoodBill(timeSheetForm);
		
		return new ModelAndView("mailSend");
		
	}
	
	
	/**
	 * Method for getting bus pass bill first page
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/busPassBill", method = RequestMethod.GET)
	public ModelAndView busPassBill(ModelMap model) {
		
		model.put("timeSheetForm", timeSheetForm);
		return new ModelAndView("bussPassBill1","model",model);
	}
	
	/**
	 * Method for bus pass bill calulation
	 * @param model
	 * @param timeSheetForm
	 * @return
	 */
	@RequestMapping(value = "/getCalc", method = RequestMethod.POST)
	public ModelAndView getCalc(ModelMap model,@ModelAttribute TimeSheetForm timeSheetForm) {
		
		Collection<FoodBill> foodBills = foodBillDAO.getData(timeSheetForm.getMonth(),timeSheetForm.getYear());
		
		Collection<TimeSheet> timeSheets = timeSheetDAO.getForFoodBill(timeSheetForm.getMonth(), timeSheetForm.getYear());
		if(foodBills.size() != 0)
		{
			model.put("foodBills", foodBills);
			model.put("timeSheetForm", timeSheetForm);
			return new ModelAndView("getCalc","model",model);
		}
		else if(timeSheets.size() != 0)
		{		
		timeSheetService.foodBill(timeSheetForm.getMonth(),timeSheetForm.getYear());
		
		Collection<FoodBill> foodBill = foodBillDAO.getData(timeSheetForm.getMonth(),timeSheetForm.getYear());
		
		model.put("foodBills", foodBill);
		model.put("timeSheetForm", timeSheetForm);
		return new ModelAndView("getCalc","model",model);
		
		}
		else
		{
			String success = Constants.NORECORDS;
			model.addAttribute("success", success);
			model.put("timeSheetForm", timeSheetForm);
			return new ModelAndView("bussPassBill1", "model", model);
		}
	
	}
	
	/**
	 * Method for bus pass bill
	 * @param model
	 * @param timeSheetForm
	 * @return
	 */
	@RequestMapping(value = "/sendingFoodBillData", method = RequestMethod.POST)
	public ModelAndView sendingFoodBillData(ModelMap model,@ModelAttribute TimeSheetForm timeSheetForm) {
		
		timeSheetService.sendingBusPassMail(timeSheetForm);
		
		return new ModelAndView("mailSend");
	}
		
	/**
	 * Method for get Seperate
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/getSeperate", method = RequestMethod.GET)
	public ModelAndView getSeperate(ModelMap model,@RequestParam("id") long id) {
		
		FoodBill foodBill = foodBillDAO.findId(id);
		
		foodBillForm.setDate(foodBill.getDate());
		foodBillForm.setId(foodBill.getId());
		foodBillForm.setMoney(foodBill.getMoney());
		foodBillForm.setMonth(foodBill.getMonth());
		foodBillForm.setYear(foodBill.getYear());
		foodBillForm.setName(foodBill.getName());
		foodBillForm.setVeg(foodBill.getVeg());
		foodBillForm.setNonVeg(foodBill.getNonVeg());
		foodBillForm.setFoodBill(foodBill.getFoodBill());
		foodBillForm.setPresents(foodBill.getPresents());
		foodBillForm.setUserName(foodBill.getUserName());
		
		model.put("foodBillForm", foodBillForm);
		return new ModelAndView("getSeperate","model",model);
	}
	
	/**
	 * Method for getting update
	 * @param model
	 * @param foodBillForm
	 * @return
	 */
	@RequestMapping(value = "/getUpdate", method = RequestMethod.POST)
	public ModelAndView getUpdate(ModelMap model,@ModelAttribute FoodBillForm foodBillForm) {
		
		timeSheetService.getUpdate(foodBillForm);
		
        Collection<FoodBill> foodBill = foodBillDAO.getData(foodBillForm.getMonth(),foodBillForm.getYear());
		
		model.put("foodBills", foodBill);
		timeSheetForm.setMonth(foodBillForm.getMonth());
		timeSheetForm.setYear(foodBillForm.getYear());
		model.put("timeSheetForm", timeSheetForm);
		return new ModelAndView("getCalc","model",model);
	}
		
	
	/**
	 * Method for monthly bill
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/monthlyBill", method = RequestMethod.GET)
	public ModelAndView monthlyBill(ModelMap model,@ModelAttribute TimeSheetForm timeSheetForm) {

		model.put("timeSheetForm", timeSheetForm);
		return new ModelAndView("monthPage","model",model);
	}
	
	
	/**
	 * Method for monthly bill
	 * @param model
	 * @param timeSheetForm
	 * @return
	 */
	@RequestMapping(value = "/getMonthBill", method = RequestMethod.POST)
	public ModelAndView getMonthBill(ModelMap model,@ModelAttribute TimeSheetForm timeSheetForm) {
		
		Collection<TimeSheet> timeSheets = timeSheetDAO.getForFoodBill(timeSheetForm.getMonth(), timeSheetForm.getYear());
	    if(timeSheets.size() != 0)
		{		
		float amount = timeSheetService.monthBill(timeSheetForm);
		model.put("timeSheets", timeSheets);
		timeSheetForm.setMondayBill(amount);
		model.put("timeSheetForm", timeSheetForm);
		return new ModelAndView("getMonthBill","model",model);
		
		}
		else
		{
			String success = Constants.NORECORDS;
			model.addAttribute("success", success);
			model.put("timeSheetForm", timeSheetForm);
			return new ModelAndView("monthPage", "model", model);
		}
	}

	/**
	 * @author spysr_dev
	 * Method to get individual time sheet data
	 * @param model
	 * @param id
	 * @return
	 */
	@RequestMapping(value = "/getEachWeekFoodBill", method = RequestMethod.GET)
	public ModelAndView getEachWeekFoodBill(ModelMap model, @RequestParam("id") Long id) {
	
		TimeSheetForm timeSheetForm = timeSheetService.getById(id);
		float amount = timeSheetService.getWeekData(id);
		model.addAttribute("amount", amount);
		model.put("timeSheetForm", timeSheetForm);
		return new ModelAndView("getEachEmpTimeSheetData","model",model);
		
	}
	
	/**
	 * Method for monthly bill
	 * @param model
	 * @param timeSheetForm
	 * @return
	 */
	@RequestMapping(value = "/sendingMonthData", method = RequestMethod.POST)
	public ModelAndView sendingMonthData(ModelMap model,@ModelAttribute TimeSheetForm timeSheetForm) {
		
		timeSheetService.sendingMonthData(timeSheetForm);
		
		return new ModelAndView("mailSend");
	}
	

}
