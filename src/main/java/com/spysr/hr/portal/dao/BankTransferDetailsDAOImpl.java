package com.spysr.hr.portal.dao;

import java.util.Collection;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spysr.hr.portal.entity.BankTransferDetails;

@Repository
public class BankTransferDetailsDAOImpl implements BankTransferDetailsDAO{
	
	@PersistenceContext
	EntityManager em;

	@Transactional
	public void save(BankTransferDetails bankTransferDetails) {
		em.persist(bankTransferDetails);
	}

	
	@SuppressWarnings("unchecked")
	public Collection<BankTransferDetails> getDetails(Date date) {
		Query query = em
				.createQuery("SELECT o FROM BankTransferDetails o where o.date = '"+date+"'");

		return (Collection<BankTransferDetails>) query.getResultList();
	}

	
	public Double getTotal(Date date) {
		Double total = (Double) em
				.createQuery("SELECT sum(amount) FROM BankTransferDetails o where o.date =:date")
				.setParameter("date", date).getSingleResult();

		return total;
	}

}
