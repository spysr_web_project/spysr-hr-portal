/**
 * 
 */
package com.spysr.hr.portal.dao;

import java.util.Collection;

import javax.persistence.TypedQuery;

import com.spysr.hr.portal.entity.EndUser;
import com.spysr.hr.portal.entity.FoodBill;
import com.spysr.hr.portal.entity.LeaveRequest;
import com.spysr.hr.portal.entity.LeaveType;

/**
 * @author Sudhir
 *
 */
public interface LeaveRequestDAO {

	public LeaveRequest insert(LeaveRequest leaveRequest);
	
	public TypedQuery<LeaveRequest> getLeaveRequestDetails();
	
	public LeaveRequest findId(Long id);
	
	public void update(LeaveRequest leaveRequest);
	
	public TypedQuery<LeaveRequest> getLeaveList(String userName);
	
	
	public TypedQuery<LeaveRequest> getAppliedLeaves(String userName);
	
	public TypedQuery<LeaveRequest> getLeaveRequestDetailsmanager();

	public Collection<LeaveType> getLeaveTypeList(String userName);
}
