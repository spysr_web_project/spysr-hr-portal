package com.spysr.hr.portal.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spysr.hr.portal.entity.ReferJob;

@Repository
public class ReferJobDAOImpl implements ReferJobDAO{
	
	@PersistenceContext
	EntityManager em;

	
    @Transactional
	public void save(ReferJob referJob) {
		em.persist(referJob);
	}
	
	

}
