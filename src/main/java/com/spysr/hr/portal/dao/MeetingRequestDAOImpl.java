/**
 * 
 */
package com.spysr.hr.portal.dao;

import java.util.Collection;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spysr.hr.portal.entity.MeetingRequest;
import com.spysr.hr.portal.entity.NoticeDetails;


/**
 * @author Sudhir
 *
 */
@Repository
public class MeetingRequestDAOImpl implements MeetingRequestDAO {
	@PersistenceContext
	EntityManager em;

	
	@SuppressWarnings("unchecked")
	public Collection<MeetingRequest> gettingData() {
		Query query = em.createQuery("SELECT o FROM Notification o");

		return (Collection<MeetingRequest>) query.getResultList();
	}

	

	
	@SuppressWarnings("unchecked")
	public Collection<MeetingRequest> employeeNotificationsend(String userName) {
		Query query = em.createQuery("SELECT o FROM Notification o where o.userName ='"+userName+"'");

		return (Collection<MeetingRequest>) query.getResultList();
	}


	@Transactional
	public MeetingRequest add(MeetingRequest meetingRequest) {

		em.persist(meetingRequest);
		
		return meetingRequest;
	}

	



}
