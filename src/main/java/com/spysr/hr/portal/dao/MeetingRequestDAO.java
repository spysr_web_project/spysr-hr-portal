/**
 * 
 */
package com.spysr.hr.portal.dao;

import java.util.Collection;
import java.util.Date;

import com.spysr.hr.portal.entity.MeetingRequest;
import com.spysr.hr.portal.entity.NoticeDetails;
import com.spysr.hr.portal.entity.TimeSheet;



/**
 * @author Sudhir
 *
 */
public interface MeetingRequestDAO {
	
	
	
	/**
	 * Method for get data
	 * @param date
	 * @return
	 */
	public Collection<MeetingRequest> employeeNotificationsend(String userName);
	
	
	
	/**
	 * Method for getting employees time sheets data
	 * @return
	 */
	public Collection<MeetingRequest> gettingData();
	
	
	public MeetingRequest add(MeetingRequest meetingRequest) ;
	
	
	

}
