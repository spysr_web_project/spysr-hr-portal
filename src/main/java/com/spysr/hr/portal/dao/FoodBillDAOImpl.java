package com.spysr.hr.portal.dao;

import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spysr.hr.portal.entity.FoodBill;

@Repository
public class FoodBillDAOImpl implements FoodBillDAO{
	
	@PersistenceContext
	EntityManager em;

	@Transactional
	public void save(FoodBill foodBill) {
		em.persist(foodBill);
	}

	
	@SuppressWarnings("unchecked")
	public Collection<FoodBill> getData(String month,int year) {
		Query query = em
				.createQuery("SELECT o FROM FoodBill o where o.month = '"+month+"' and o.year ="+year+" ORDER BY o.id ASC");

		return (Collection<FoodBill>) query.getResultList();
	}

	
	@SuppressWarnings("unchecked")
	public Collection<FoodBill> getIndividualData(String userName) {
		Query query = em
				.createQuery("SELECT o FROM FoodBill o where o.userName = '"+userName+"' ORDER BY o.id ASC");

		return (Collection<FoodBill>) query.getResultList();
	}


	
	public FoodBill findId(long id) {
		
		return em.find(FoodBill.class, id);
	}


	@Transactional
	public void update(FoodBill foodBill) {
		em.merge(foodBill);
		em.flush();
	}


	
	@SuppressWarnings("unchecked")
	public Collection<FoodBill> getAllData() {
		Query query = em
				.createQuery("SELECT o FROM FoodBill o ORDER BY o.id ASC");

		return (Collection<FoodBill>) query.getResultList();
	}


	@Override
	public TypedQuery<FoodBill> getIndivualData(String userName, String month) {
		TypedQuery<FoodBill> q = em.createQuery(
				"SELECT o FROM FoodBill o where o.userName = '"+userName+"' and o.month='"+month+"' ",
				FoodBill.class);
		return q;
	}

}
