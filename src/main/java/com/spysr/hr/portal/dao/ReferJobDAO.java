package com.spysr.hr.portal.dao;

import com.spysr.hr.portal.entity.ReferJob;

public interface ReferJobDAO {

	public void save(ReferJob referJob);
}
