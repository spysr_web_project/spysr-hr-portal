/**
 * 
 */
package com.spysr.hr.portal.dao;


import javax.persistence.TypedQuery;

import com.spysr.hr.portal.entity.HolidayList;




public interface HolidayDAO {
	
	public TypedQuery<HolidayList> holidayList();
	
	public HolidayList createUser(HolidayList holidayList) ;
	
	public HolidayList findId(Long id);
	
	public TypedQuery<HolidayList> findByHolidayName(String serialNo);

	public void Update(HolidayList holidayList);
	

}
