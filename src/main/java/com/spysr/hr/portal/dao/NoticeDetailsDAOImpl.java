/**
 * 
 */
package com.spysr.hr.portal.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spysr.hr.portal.entity.NoticeDetails;



/**
 * @author Sudhir
 *
 */
@Repository
public class NoticeDetailsDAOImpl implements NoticeDetailsDAO {

	@PersistenceContext
	EntityManager em;

	public EntityManager entityManager() {
		EntityManager em = new NoticeDetailsDAOImpl().em;
		if (em == null)
			throw new IllegalStateException(
					"Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
		return em;
	}

	
	public TypedQuery<NoticeDetails> noticeList() {
		TypedQuery<NoticeDetails> q = em.createQuery(
				"SELECT o FROM NoticeDetails o",NoticeDetails.class);
      return q;
	}
	
	

	public TypedQuery<NoticeDetails> findByNoticeDetailsName(String serialNo) {
		if (serialNo==null ||serialNo.length()==0)
			throw new IllegalArgumentException(
					"The username argument is required");

		TypedQuery<NoticeDetails> q = em.createQuery(
				"SELECT o FROM NoticeDetails AS o WHERE  o.serialNo= :serialNo",NoticeDetails.class);
		q.setParameter("serialNo", serialNo);
	
	   return q;
	}
	
	@Transactional
	public NoticeDetails createNotice(NoticeDetails noticeDetails) {

		em.persist(noticeDetails);
		
		return noticeDetails;
	}
	
	public NoticeDetails findId(Long id) {

		return em.find(NoticeDetails.class, id);

	}
	
	@Transactional
	public void Update(NoticeDetails noticeDetails){
		em.merge(noticeDetails);
		em.flush();
	}
}
