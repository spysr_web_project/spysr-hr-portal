package com.spysr.hr.portal.dao;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spysr.hr.portal.entity.EndUser;


@Repository
public class EndUserDAOImpl implements EndUserDAO {

	@PersistenceContext
	EntityManager em;

	public EntityManager entityManager() {
		EntityManager em = new EndUserDAOImpl().em;
		if (em == null)
			throw new IllegalStateException(
					"Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
		return em;
	}

	public TypedQuery<EndUser> findByUsername(String username) {
		if (username == null || username.length() == 0)
			throw new IllegalArgumentException(
					"The username argument is required");

		TypedQuery<EndUser> q = em.createQuery(
				"SELECT o FROM EndUser AS o WHERE o.userName = :username",
				EndUser.class);
		q.setParameter("username", username);
		return q;
	}

	public TypedQuery<EndUser> findByUsernameAndEmail(String username,
			String email) {
		if (username == null || username.length() == 0)
			throw new IllegalArgumentException(
					"The username argument is required");

		TypedQuery<EndUser> q = em
				.createQuery(
						"SELECT o FROM EndUser AS o WHERE o.userName = :username and o.email =:email",
						EndUser.class);
		q.setParameter("username", username);
		q.setParameter("email", email);
		return q;
	}

	
	
	public TypedQuery<EndUser> findByAddManager(String addbyManager) {
		if ( addbyManager == null || addbyManager.length() == 0 )
			throw new IllegalArgumentException(
					"The username argument is required");

		TypedQuery<EndUser> q = em
				.createQuery(
						"SELECT o FROM EndUser AS o WHERE o.currentRole = 'ROLE_EMP'",
						EndUser.class);
	
		//q.setParameter("addbyManager", addbyManager);
	
		return q;
	}
	
	
	@SuppressWarnings("unchecked")
	public Collection<EndUser> getList() {

		Query query = em
				.createQuery("SELECT o FROM EndUser o where o.currentRole = 'ROLE_BANKEMP' and o.status = 'Pending' or o.status = 'Rejected'");

		return (Collection<EndUser>) query.getResultList();
	}

	@Override
	public TypedQuery<EndUser> getByRole() {

		TypedQuery<EndUser> q = em
				.createQuery(
						"SELECT o FROM EndUser o where o.createdby = 'MANAGER' and o.status = 'Pending'",
						EndUser.class);
		return q;

	}
	
	
	
	@Override
	public TypedQuery<EndUser> getByEmpList() {

		TypedQuery<EndUser> q = em.createQuery(
				"SELECT o FROM EndUser o where o.currentRole = 'ROLE_EMP'or o.currentRole='ROLE_MANAGER' ",
				EndUser.class);
		return q;

	}
	
	@Override
	public TypedQuery<EndUser> getEmployees() {

		TypedQuery<EndUser> q = em.createQuery(
				"SELECT o FROM EndUser o where o.currentRole != 'ROLE_ADMIN'",
				EndUser.class);
		return q;

	}

	
	@Override
	public TypedQuery<EndUser> getByRoleList() {

		TypedQuery<EndUser> q = em.createQuery(
				"SELECT o FROM EndUser o where o.currentRole = 'ROLE_EMP' or o.currentRole = 'ROLE_OFFICEMANAGER' ",EndUser.class);
		return q;

	}
	

	@Override
	public TypedQuery<EndUser> getByBankEmp() {

		TypedQuery<EndUser> q = em
				.createQuery(
						"SELECT o FROM EndUser o where o.currentRole = 'ROLE_BANKEMP' and o.status = 'Approved'",
						EndUser.class);
		return q;

	}
	
	
	
	//
	
	@Override
	public TypedQuery<EndUser> getByBankEmp(Date doj) {

		if (doj == null || ((CharSequence) doj).length() == 0)
			throw new IllegalArgumentException(
					"The username argument is required");

		TypedQuery<EndUser> q = em
				.createQuery(
						"SELECT o FROM EndUser o where o.currentRole = 'ROLE_BANKEMP' and o.status = 'Approved'",
						EndUser.class);
		return q;

	}

	@Override
	public TypedQuery<EndUser> getByApprovalMng() {

		TypedQuery<EndUser> q = em
				.createQuery(
						"SELECT o FROM EndUser o where o.currentRole = 'ROLE_APPROVALMNG' ",
						EndUser.class);
		return q;

	}

	public EndUser findId(Long id) {

		return em.find(EndUser.class, id);

	}

	
	public EndUser findEmpid(Long employeeId) {

		return em.find(EndUser.class, employeeId);

	}
	@Transactional
	public EndUser createUser(EndUser user) {

		em.persist(user);
		return user;
	}



	@Transactional
	public void update(EndUser endUser) {

		em.merge(endUser);

		em.flush();
	}

	public EndUser GetUser(String userName) {

		return em.find(EndUser.class, userName);

	}

	/**
	 * Method to get Users for Blocking/Unblocking
	 * @return
	 */
	@Override
	public List<EndUser> getUsersForBlockUnblock() {
		TypedQuery<EndUser> q = em.createQuery(
									"SELECT NEW EndUser(o.id,o.userName,o.currentRole,o.accessStatus) FROM EndUser o WHERE o.role NOT IN (1,2,3)",								
								EndUser.class);
		return q.getResultList();
	}
	
	/**
	 * Method to get Users for Blocking/Unblocking
	 * @return
	 */
	@Override
	public List<EndUser> getUsersForBlockUnblockApproval() {
		TypedQuery<EndUser> q = em.createQuery(
									"SELECT new EndUser(o.id,o.userName,o.currentRole,o.accessStatus) FROM EndUser o WHERE o.role NOT IN (1,2,3) and ((o.accessStatus='Block' or o.accessStatus='Unblock') or o.accRenewStatus='Pending')",								
								EndUser.class);
		return q.getResultList();
	}
	
	/**
	 * Method to get BankEmp,ApprovalMng for Blocking/Unblocking/Renewing
	 * @return
	 */
	@Override
	public List<EndUser> getUsersForBlockUnblockRenew() {
		TypedQuery<EndUser> q = em.createQuery(
									"SELECT o FROM EndUser o WHERE o.role IN (2,3,4)",								
								EndUser.class);
		return q.getResultList();
	}

	
	/**
	 * Method to get all users except Admin
	 */
	@Override
	public List<EndUser> getUsersExceptAdmin() {
		TypedQuery<EndUser> q = em.createQuery(
				"SELECT o FROM EndUser o WHERE o.role NOT IN (1)",								
				EndUser.class);
		return q.getResultList();
	}

	/* (non-Javadoc)
	 * @see com.spysr.hr.portal.dao.EndUserDAO#getByRoleList()
	 */
	
	
}
