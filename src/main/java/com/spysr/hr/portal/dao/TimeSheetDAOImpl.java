package com.spysr.hr.portal.dao;

import java.util.Collection;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spysr.hr.portal.entity.TimeSheet;

/**
 * @author spysr_dev
 *
 */
@Repository
public class TimeSheetDAOImpl implements TimeSheetDAO {
	
	@PersistenceContext
	EntityManager em;

	@Transactional
	public void add(TimeSheet timeSheet) {
		em.persist(timeSheet);
	}

	@SuppressWarnings("unchecked")
	public Collection<TimeSheet> gettingData() {
		Query query = em.createQuery("SELECT o FROM TimeSheet o");

		return (Collection<TimeSheet>) query.getResultList();
	}

	@SuppressWarnings("unchecked")
	public Collection<TimeSheet> checkData(String userName,Date date) {
		Query query = em.createQuery("SELECT o FROM TimeSheet o where o.userName ='"+userName+"' and o.monday ='"+date+"'");

		return (Collection<TimeSheet>) query.getResultList();
	}

	@Override
	public TimeSheet getById(Long Id) {
		return em.find(TimeSheet.class, Id);
	}

	@Transactional
	public void update(TimeSheet timeSheet) {
		em.merge(timeSheet);
		em.flush();
	}

	@Override
	public TypedQuery<TimeSheet> gettingTimesheetlist() {

		TypedQuery<TimeSheet> q = em.createQuery("SELECT o FROM TimeSheet o ",TimeSheet.class);
		return q;

	}
	
	@SuppressWarnings("unchecked")
	public Collection<TimeSheet> gettingApproveData() {
		Query query = em.createQuery("SELECT o FROM TimeSheet o where o.Status ='Pending'");

		return (Collection<TimeSheet>) query.getResultList();
	}


	
	
	@SuppressWarnings("unchecked")
	public Collection<TimeSheet> rejectedData(String userName) {
		Query query = em.createQuery("SELECT o FROM TimeSheet o where o.userName ='"+userName+"' and o.Status='Rejected'");

		return (Collection<TimeSheet>) query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public Collection<TimeSheet> findTimeSheet(String userName) {
		Query query = em.createQuery("SELECT o FROM TimeSheet o where o.userName ='"+userName+"' and o.Status='Approved'");

		return (Collection<TimeSheet>) query.getResultList();
	}
	


	
	@SuppressWarnings("unchecked")
	public Collection<TimeSheet> gettingOwnData(String userName) {
		Query query = em.createQuery("SELECT o FROM TimeSheet o where o.userName ='"+userName+"'");

		return (Collection<TimeSheet>) query.getResultList();
	}

	
	@SuppressWarnings("unchecked")
	public Collection<TimeSheet> gettingEmployeeData() {
		Query query = em.createQuery("SELECT o FROM TimeSheet o where o.role ='ROLE_EMP'");

		return (Collection<TimeSheet>) query.getResultList();
	}

	
	@SuppressWarnings("unchecked")
	public Collection<TimeSheet> getDetails(Date date) {
		Query query = em.createQuery("SELECT o FROM TimeSheet o where o.monday ='"+date+"' or o.tuesday ='"+date+"' or o.wednesday ='"+date+"' or o.thursday ='"+date+"' or o.friday ='"+date+"'");

		return (Collection<TimeSheet>) query.getResultList();
	}

	@Override
	public Double getMondayBill(Date date) {
		Double bill = (Double) em
				.createQuery("SELECT sum(mondayBill) FROM TimeSheet o where o.monday =:date or o.tuesday =:date or o.wednesday =:date or o.thursday =:date or o.friday =:date")
				.setParameter("date", date).getSingleResult();

		return bill;
	}

	@Override
	public Double getTuesdayBill(Date date) {
		Double bill = (Double) em
				.createQuery("SELECT sum(tuesdayBill) FROM TimeSheet o where o.monday =:date or o.tuesday =:date or o.wednesday =:date or o.thursday =:date or o.friday =:date")
				.setParameter("date", date).getSingleResult();

		return bill;
	}

	@Override
	public Double getWednesdayBill(Date date) {
		Double bill = (Double) em
				.createQuery("SELECT sum(wednesdayBill) FROM TimeSheet o where o.monday =:date or o.tuesday =:date or o.wednesday =:date or o.thursday =:date or o.friday =:date")
				.setParameter("date", date).getSingleResult();

		return bill;
	}

	@Override
	public Double getThursdayBill(Date date) {
		Double bill = (Double) em
				.createQuery("SELECT sum(thursdayBill) FROM TimeSheet o where o.monday =:date or o.tuesday =:date or o.wednesday =:date or o.thursday =:date or o.friday =:date")
				.setParameter("date", date).getSingleResult();

		return bill;
	}

	@Override
	public Double getFridayBill(Date date) {
		Double bill = (Double) em
				.createQuery("SELECT sum(fridayBill) FROM TimeSheet o where o.monday =:date or o.tuesday =:date or o.wednesday =:date or o.thursday =:date or o.friday =:date")
				.setParameter("date", date).getSingleResult();

		return bill;
	}

	
	@SuppressWarnings("unchecked")
	public Collection<TimeSheet> getEmployeeDetails(String userName, Date date) {
		Query query = em.createQuery("SELECT o FROM TimeSheet o where o.userName ='"+userName+"' and o.monday ='"+date+"' or o.tuesday ='"+date+"' or o.wednesday ='"+date+"' or o.thursday ='"+date+"' or o.friday ='"+date+"'");

		return (Collection<TimeSheet>) query.getResultList();
	}

	
	public TimeSheet getIndividualDetails(Date date) {
		Query query = em.createQuery("SELECT o FROM TimeSheet o where o.monday ='"+date+"' or o.tuesday ='"+date+"' or o.wednesday ='"+date+"' or o.thursday ='"+date+"' or o.friday ='"+date+"'");

		return (TimeSheet) query.getSingleResult();
	}


	@SuppressWarnings("unchecked")
	public Collection<TimeSheet> getIndividualDetails(String userName,String month, int year) {
		Query query = em.createQuery("SELECT o FROM TimeSheet o where o.userName ='"+userName+"' and to_char(o.monday,'MM') = '"+month+"' and to_char(o.monday,'YYYY') = '"+year+"'");

		return (Collection<TimeSheet>) query.getResultList();
	}

	@Override
	public TimeSheet getEmployeeData(String userName, Date date) {
		Query query = em.createQuery("SELECT o FROM TimeSheet o where o.userName ='"+userName+"' and o.monday ='"+date+"' or o.tuesday ='"+date+"' or o.wednesday ='"+date+"' or o.thursday ='"+date+"' or o.friday ='"+date+"'");

		return (TimeSheet) query.getSingleResult();
	}

	
	@SuppressWarnings("unchecked")
	public Collection<TimeSheet> monday(String userName, Date date) {
		Query query = em.createQuery("SELECT o FROM TimeSheet o where o.userName ='"+userName+"' and o.monday ='"+date+"'");

		return (Collection<TimeSheet>) query.getResultList();
	}

	
	@SuppressWarnings("unchecked")
	public Collection<TimeSheet> tuesday(String userName, Date date) {
		Query query = em.createQuery("SELECT o FROM TimeSheet o where o.userName ='"+userName+"' and o.tuesday ='"+date+"'");

		return (Collection<TimeSheet>) query.getResultList();
	}

	
	@SuppressWarnings("unchecked")
	public Collection<TimeSheet> wednesday(String userName, Date date) {
		Query query = em.createQuery("SELECT o FROM TimeSheet o where o.userName ='"+userName+"' and o.wednesday ='"+date+"'");

		return (Collection<TimeSheet>) query.getResultList();
	}

	
	@SuppressWarnings("unchecked")
	public Collection<TimeSheet> thursday(String userName, Date date) {
		Query query = em.createQuery("SELECT o FROM TimeSheet o where o.userName ='"+userName+"' and o.thursday ='"+date+"'");

		return (Collection<TimeSheet>) query.getResultList();
	}

	
	@SuppressWarnings("unchecked")
	public Collection<TimeSheet> friday(String userName, Date date) {
		Query query = em.createQuery("SELECT o FROM TimeSheet o where o.userName ='"+userName+"' and o.friday ='"+date+"'");

		return (Collection<TimeSheet>) query.getResultList();
	}

	
	@SuppressWarnings("unchecked")
	public Collection<TimeSheet> getForFoodBill(String month, int year) {
		Query query = em.createQuery("SELECT o FROM TimeSheet o where to_char(o.monday,'MM') = '"+month+"' and to_char(o.monday,'YYYY') = '"+year+"'");

		return (Collection<TimeSheet>) query.getResultList();
	}

}
