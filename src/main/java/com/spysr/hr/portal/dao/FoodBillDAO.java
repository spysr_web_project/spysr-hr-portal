package com.spysr.hr.portal.dao;

import java.util.Collection;

import javax.persistence.TypedQuery;

import com.spysr.hr.portal.entity.FoodBill;

/**
 * @author spysr_dev
 *
 */
public interface FoodBillDAO {
	
	/**
	 * Method for saving data
	 * @param foodBill
	 */
	public void save(FoodBill foodBill);
	
	/**
	 * Method for get id data
	 * @param id
	 * @return
	 */
	public FoodBill findId(long id);
	
	/**
	 * Method for get update
	 * @param foodBill
	 */
	public void update(FoodBill foodBill);
	
	/**
	 * Method for getting data
	 * @param month
	 * @return
	 */
	public Collection<FoodBill> getData(String month,int year);
	
	/**
	 *  Method for getting data
	 * @param userName
	 * @return
	 */
	public Collection<FoodBill> getIndividualData(String userName);
	
	/**
	 * Method for getting data
	 * @param userName
	 * @param month
	 * @return
	 */
	public TypedQuery<FoodBill> getIndivualData(String userName,String month);
	
	
	/**
	 * Method for getting all data
	 * @return
	 */
	public Collection<FoodBill> getAllData();

}
