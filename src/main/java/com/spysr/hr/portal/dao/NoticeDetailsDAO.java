/**
 * 
 */
package com.spysr.hr.portal.dao;

import javax.persistence.TypedQuery;

import com.spysr.hr.portal.entity.NoticeDetails;



/**
 * @author Sudhir
 *
 */
public interface NoticeDetailsDAO {
	
	   /**
     * @return holidayList
     */
    public TypedQuery<NoticeDetails> noticeList();
	
	/**
	 * @param createNew Holiday
	 * @return
	 */
	public NoticeDetails createNotice(NoticeDetails noticeDetails) ;
	
	/**
	 * @param existing holiday Exist in database
	 * @return
	 */
	public TypedQuery<NoticeDetails> findByNoticeDetailsName(String serialNo);
	
	
	
	public NoticeDetails findId(Long id);
	
	
	public void Update(NoticeDetails noticeDetails);


}
