package com.spysr.hr.portal.dao;

import java.util.Collection;

import com.spysr.hr.portal.entity.JobOffers;

/**
 * @author spysr_dev
 *
 */
public interface JobOffersDAO {

	/**
	 * Method for adding job details
	 * @param jobOffers
	 */
	public void addDetails(JobOffers jobOffers);
	
	/**
	 * Method for getting all Job Details
	 * @return
	 */
	public Collection<JobOffers> getAll();
	
	/**
	 * Method for getting Details 
	 * @param id
	 * @return
	 */
	public JobOffers getById(long id);
	
	/**
	 * Method for update
	 * @param jobOffers
	 */
	public void update(JobOffers jobOffers);
}
