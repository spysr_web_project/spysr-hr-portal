package com.spysr.hr.portal.dao;

import java.util.Collection;
import java.util.Date;

import javax.persistence.TypedQuery;

import com.spysr.hr.portal.entity.TimeSheet;

/**
 * @author spysr_dev
 *
 */
public interface TimeSheetDAO {
	
	/**
	 * Method for adding time sheets
	 * @param timeSheet
	 */
	public void add(TimeSheet timeSheet);
	
	/**
	 * Method for checking data
	 * @param date
	 * @return
	 */
	public Collection<TimeSheet> checkData(String userName,Date date);
	
	
	/**
	 * Method for get data
	 * @param date
	 * @return
	 */
	public Collection<TimeSheet> rejectedData(String userName);
	
	/**
	 * Method for getting employees time sheets data
	 * @return
	 */
	public Collection<TimeSheet> gettingApproveData();
	
	/**
	 * Method for getting employees time sheets data
	 * @return
	 */
	public Collection<TimeSheet> gettingData();
	
	/**
	 * Method for getting Id details
	 * @return
	 */
	public TimeSheet getById(Long id);
	
	
	
	/**
	 * Method to update 
	 * @param timeSheet
	 */
	public void update(TimeSheet timeSheet);
	
	
	public TypedQuery<TimeSheet> gettingTimesheetlist();
	

	public Collection<TimeSheet> findTimeSheet(String userName);

	
	/**
	 * Method for getting own data
	 * @param userName
	 * @return
	 */
	public Collection<TimeSheet> gettingOwnData(String userName);
	
	/**
	 * Method for getting specific managerdata
	 * @param mng
	 * @return
	 */
	public Collection<TimeSheet> gettingEmployeeData();
	
	
	/**
	 * Method for getting details
	 * @param date
	 * @return
	 */
	public Collection<TimeSheet> getDetails(Date date);
	
	/**
	 * Method for getting individual details
	 * @param date
	 * @return
	 */
	public TimeSheet getIndividualDetails(Date date);
	
	/**
	 * Method for getting details
	 * @param date
	 * @return
	 */
	public Collection<TimeSheet> getEmployeeDetails(String userName,Date date);
	
	
	/**
	 * Getting individual data
	 * @param userName
	 * @param date
	 * @return
	 */
	public TimeSheet getEmployeeData(String userName,Date date);
	
	/**
	 * Method for bill
	 * @param date
	 * @return
	 */
	public Double getMondayBill(Date date);
	
	/**
	 * Method for bill
	 * @param date
	 * @return
	 */
	public Double getTuesdayBill(Date date);
	
	/**
	 * Method for bill
	 * @param date
	 * @return
	 */
	public Double getWednesdayBill(Date date);

	/**
	 * Method for bill
	 * @param date
	 * @return
	 */
	public Double getThursdayBill(Date date);
	
	/**
	 * Method for bill
	 * @param date
	 * @return
	 */
	public Double getFridayBill(Date date);
	
	/**
	 * Method for getting individual data
	 * @param month
	 * @param year
	 * @return
	 */
	public Collection<TimeSheet> getIndividualDetails(String userName,String month,int year);
	
	
	/**
	 * Method for food bill
	 * @param month
	 * @param year
	 * @return
	 */
	public Collection<TimeSheet> getForFoodBill(String month,int year);
	
	/**
	 * Method for comparing monday details
	 * @param userName
	 * @param date
	 * @return
	 */
	public Collection<TimeSheet> monday(String userName,Date date);
	
	/**
	 * Method for comparing tuesday details
	 * @param userName
	 * @param date
	 * @return
	 */
	public Collection<TimeSheet> tuesday(String userName,Date date);
	
	/**
	 * Method for comparing wednesday details
	 * @param userName
	 * @param date
	 * @return
	 */
	public Collection<TimeSheet> wednesday(String userName,Date date);
	
	/**
	 * Method for comparing thurday details
	 * @param userName
	 * @param date
	 * @return
	 */
	public Collection<TimeSheet> thursday(String userName,Date date);
	
	/**
	 * Method for comparing friday details
	 * @param userName
	 * @param date
	 * @return
	 */
	public Collection<TimeSheet> friday(String userName,Date date);
	
	


}
