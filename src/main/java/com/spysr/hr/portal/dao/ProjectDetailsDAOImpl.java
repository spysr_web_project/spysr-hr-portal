/**
 * 
 */
package com.spysr.hr.portal.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spysr.hr.portal.entity.HolidayList;
import com.spysr.hr.portal.entity.NoticeDetails;
import com.spysr.hr.portal.entity.ProjectDetails;



/**
 * @author Sudhir
 *
 */
@Repository
public class ProjectDetailsDAOImpl implements ProjectDetailsDAO {

	@PersistenceContext
	EntityManager em;

	public EntityManager entityManager() {
		EntityManager em = new ProjectDetailsDAOImpl().em;
		if (em == null)
			throw new IllegalStateException(
					"Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
		return em;
	}

	
	public TypedQuery<ProjectDetails> productList() {
		TypedQuery<ProjectDetails> q = em.createQuery(
				"SELECT o FROM ProjectDetails o",ProjectDetails.class);
      return q;
	}
	
	
	
	public TypedQuery<ProjectDetails> findByProductName(String serialNo) {
		if (serialNo==null ||serialNo.length()==0)
			throw new IllegalArgumentException(
					"The username argument is required");

		TypedQuery<ProjectDetails> q = em.createQuery(
				"SELECT o FROM ProjectDetails AS o WHERE  o.serialNo= :serialNo",ProjectDetails.class);
		q.setParameter("serialNo", serialNo);
	
	   return q;
	}
	
	
	
	@Transactional
	public ProjectDetails createUser(ProjectDetails projectDetails) {

		em.persist(projectDetails);
		
		return projectDetails;
	}

	@Transactional
	public void Update(ProjectDetails projectDetails){
		em.merge(projectDetails);
		em.flush();
	}
	
	
	public ProjectDetails findId(Long id) {

		return em.find(ProjectDetails.class, id);

	}
}
