package com.spysr.hr.portal.dao;

import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spysr.hr.portal.entity.JobOffers;

/**
 * @author spysr_dev
 *
 */
@Repository
public class JobOffersDAOImpl implements JobOffersDAO {
	
	@PersistenceContext
	EntityManager em;

	@Transactional
	public void addDetails(JobOffers jobOffers) {
		em.persist(jobOffers);
	}


	@SuppressWarnings("unchecked")
	public Collection<JobOffers> getAll() {
		Query query = em.createQuery("SELECT o FROM JobOffers o where CURRENT_DATE<=o.lastDate");

		return (Collection<JobOffers>) query.getResultList();
	}


	@Override
	public JobOffers getById(long id) {
		
		return em.find(JobOffers.class, id);
	}


	@Transactional
	public void update(JobOffers jobOffers) {
		em.merge(jobOffers);
	}

}
