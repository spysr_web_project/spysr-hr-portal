package com.spysr.hr.portal.dao;

import java.util.Collection;
import java.util.Date;

import com.spysr.hr.portal.entity.BankTransferDetails;

/**
 * @author spysr_dev
 *
 */
public interface BankTransferDetailsDAO {
	
	/**
	 * Method for saving details
	 * @param bankTransferDetails
	 */
	public void save(BankTransferDetails bankTransferDetails);
	
	/**
	 * Method for getting details
	 * @param date
	 * @return
	 */
	public Collection<BankTransferDetails> getDetails(Date date);
	
	/**
	 * Method for getting total
	 * @param date
	 * @return
	 */
	public Double getTotal(Date date);

}
