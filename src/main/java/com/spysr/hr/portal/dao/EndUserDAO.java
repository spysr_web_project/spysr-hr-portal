package com.spysr.hr.portal.dao;

import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.TypedQuery;

import com.spysr.hr.portal.entity.EndUser;


public interface EndUserDAO {


	/** Method to get User details
	 * @param username
	 * @return
	 */
	public TypedQuery<EndUser> findByUsername(String username);
	
	
	
	/**Method to get User details
	 * @param username
	 * @param email
	 * @return
	 */
	public TypedQuery<EndUser> findByUsernameAndEmail(String username,String email);
	
	

	
	
	/**
	 * @param username
	 * @param email
	 * @return
	 */
	public TypedQuery<EndUser> findByAddManager(String addbyManager);
	
	
	/**Method to get all Emp List
	 * @return
	 */
	
	public TypedQuery<EndUser> getByEmpList();

	/**
	 * Method to get bank employees details
	 * @return
	 */
	public TypedQuery<EndUser> getByBankEmp();

	/**
	 * Method to get pending bank employees details
	 * @return
	 */
	public TypedQuery<EndUser> getByRole();

	/**
	 * Method to get bank employee details
	 * @return
	 */
	public TypedQuery<EndUser> getByRoleList();

	/**
	 * Method to save users details
	 * @return
	 */
	public EndUser createUser(EndUser user);

	/**
	 * Method to get all users details
	 * @return
	 */
	public EndUser findId(Long id);
	
	
	public EndUser findEmpid(Long employeeId);

	/**
	 * Method to get all users details
	 * @return
	 */
	public EndUser GetUser(String userName);

	/**
	 * Method to merge user data
	 */
	public void update(EndUser endUser);

	/**
	 * Method to get approval manager details
	 * @return
	 */
	public TypedQuery<EndUser> getByApprovalMng();

	/**
	 * Method to get List of all users
	 * @return
	 */
	public Collection<EndUser> getList();
	
/*	public TypedQuery<EndUser> getByRoleList(String userName);*/
	
	public TypedQuery<EndUser> getByBankEmp(Date doj);
	
	public List<EndUser> getUsersForBlockUnblock();
	
	public List<EndUser> getUsersForBlockUnblockApproval() ;
	
	public List<EndUser> getUsersForBlockUnblockRenew();

	/**
	 * Method to get all users except Admin
	 */
	List<EndUser> getUsersExceptAdmin();
	
	/**
	 * Getting employees records
	 * @return
	 */
	public TypedQuery<EndUser> getEmployees();

}
