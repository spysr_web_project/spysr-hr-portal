/**
 * 
 */
package com.spysr.hr.portal.dao;

import javax.persistence.TypedQuery;

import com.spysr.hr.portal.entity.HolidayList;
import com.spysr.hr.portal.entity.ProjectDetails;

/**
 * @author Sudhir
 *
 */
public interface ProjectDetailsDAO {
	
       /**
     * @return holidayList
     */
    public TypedQuery<ProjectDetails> productList();
	
	/**
	 * @param createNew Holiday
	 * @return
	 */
	public ProjectDetails createUser(ProjectDetails productDetails) ;
	
	/**
	 * @param existing holiday Exist in database
	 * @return
	 */
	public TypedQuery<ProjectDetails> findByProductName(String serialNo);
	
	
	public ProjectDetails findId(Long id);
	
	
	public void Update(ProjectDetails productDetails) ;


	

}
