/**
 * 
 */
package com.spysr.hr.portal.dao;

/**
 * @author sudhir
 *
 */




import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spysr.hr.portal.entity.HolidayList;


@Repository
public class HolidayDAOImpl implements HolidayDAO {

	@PersistenceContext
	EntityManager em;

	public EntityManager entityManager() {
		EntityManager em = new HolidayDAOImpl().em;
		if (em == null)
			throw new IllegalStateException(
					"Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
		return em;
	}

	
	public TypedQuery<HolidayList> holidayList() {
		TypedQuery<HolidayList> q = em.createQuery(
				"SELECT o FROM HolidayList o",HolidayList.class);
      return q;
	}
	
	

	public TypedQuery<HolidayList> findByHolidayName(String serialNo) {
		if (serialNo == null || serialNo.length() == 0)
			throw new IllegalArgumentException(
					"The username argument is required");

		TypedQuery<HolidayList> q = em
				.createQuery(
						"SELECT o FROM HolidayList AS o WHERE  o.serialNo=:serialNo ",
						HolidayList.class);
		q.setParameter("serialNo", serialNo);	
		
	
		return q;
	}
	
	
	public HolidayList findId(Long id) {

		return em.find(HolidayList.class, id);

	}
	@Transactional
	public HolidayList createUser(HolidayList holidayList) {

		em.persist(holidayList);
		
		return holidayList;
	}

	@Transactional
	public void Update(HolidayList holidayList) {

		em.merge(holidayList);

		em.flush();
	}
	
	
	
}

