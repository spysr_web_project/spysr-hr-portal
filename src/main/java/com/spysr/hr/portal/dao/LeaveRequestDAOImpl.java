/**
 * 
 */
package com.spysr.hr.portal.dao;

import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.spysr.hr.portal.entity.EndUser;
import com.spysr.hr.portal.entity.FoodBill;
import com.spysr.hr.portal.entity.LeaveRequest;
import com.spysr.hr.portal.entity.LeaveType;


/**
 * @author Sudhir
 *
 */
@Repository
public class LeaveRequestDAOImpl implements LeaveRequestDAO {

	@PersistenceContext
	EntityManager em;

	public EntityManager entityManager() {
		EntityManager em = new LeaveRequestDAOImpl().em;
		if (em == null)
			throw new IllegalStateException(
					"Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
		return em;
	}




	@Transactional
	public LeaveRequest insert(LeaveRequest leaveRequest) {

		em.persist(leaveRequest);

		return leaveRequest;
	}


	@Override
	public TypedQuery<LeaveRequest> getLeaveRequestDetails() {

		TypedQuery<LeaveRequest> q = em
				.createQuery(
						"SELECT o FROM LeaveRequest o  WHERE  o.status = 'Pending'",
						LeaveRequest.class);
		return q;

	}

	@Override
	public TypedQuery<LeaveRequest> getLeaveRequestDetailsmanager() {

		TypedQuery<LeaveRequest> q = em
				.createQuery(
						"SELECT o FROM LeaveRequest o  WHERE o.currentRole !='ROLE_MANAGER'and  o.status = 'Pending'",
						LeaveRequest.class);
		return q;

	}	

	public LeaveRequest findId(Long id) {

		return em.find(LeaveRequest.class, id);

	}

	@Transactional
	public void update(LeaveRequest leaveRequest) {

		em.merge(leaveRequest);

		em.flush();
	}


	@Override
	public TypedQuery<LeaveRequest> getLeaveList(String userName) {

		TypedQuery<LeaveRequest> q = em
				.createQuery(
						"SELECT o FROM LeaveRequest o where o.userName ='"+userName+"' and o.status = 'Approved' and o.leaveStatus = 'Not Cancel'",
						LeaveRequest.class);
		return q;

	}




	@Override
	public TypedQuery<LeaveRequest> getAppliedLeaves(String userName) {
		TypedQuery<LeaveRequest> q = em
				.createQuery(
						"SELECT o FROM LeaveRequest o where o.userName ='"+userName+"' and o.leaveStatus ='Not Cancel' ",
						LeaveRequest.class);
		return q;
	}


	@SuppressWarnings("unchecked")
	public Collection<FoodBill> getIndividualData(String userName) {
		Query query = em
				.createQuery("SELECT o FROM FoodBill o where o.userName = '"+userName+"' ORDER BY o.id ASC");

		return (Collection<FoodBill>) query.getResultList();
	}




	@SuppressWarnings("unchecked")
	@Override
	public Collection<LeaveType> getLeaveTypeList(String userName) {
		Query query = em
				.createQuery("SELECT o FROM LeaveType o  ORDER BY o.id ASC");

		return (Collection<LeaveType>) query.getResultList();
	}
}