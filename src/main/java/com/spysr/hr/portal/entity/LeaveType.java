package com.spysr.hr.portal.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Configurable;

@Entity
@Configurable
@Table(name = "leave_type")
public class LeaveType {

	@Id
	@GeneratedValue
	private Long id;

	@Column(name="name")
	private String name;
	
	@Column(name="leave_type")
	private String leaveType;

	@Column(name="leave_unit")
	private String leaveUnit;

	@Column(name="description")
	private String description;
	
	@Column(name="leave_length")
	private String leaveLength;

	@Column(name="leave_period")
	private String leavePeriod;

	@Column(name="leave_for")
	private String leaveFor;
	
	@Column(name="leave_applicable_for")
	private String leaveApplicableFor;

	@Column(name="leave_count")
	private Integer leaveCount;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLeaveType() {
		return leaveType;
	}

	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}

	public String getLeaveUnit() {
		return leaveUnit;
	}

	public void setLeaveUnit(String leaveUnit) {
		this.leaveUnit = leaveUnit;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLeaveLength() {
		return leaveLength;
	}

	public void setLeaveLength(String leaveLength) {
		this.leaveLength = leaveLength;
	}

	public String getLeavePeriod() {
		return leavePeriod;
	}

	public void setLeavePeriod(String leavePeriod) {
		this.leavePeriod = leavePeriod;
	}

	public String getLeaveFor() {
		return leaveFor;
	}

	public void setLeaveFor(String leaveFor) {
		this.leaveFor = leaveFor;
	}

	public String getLeaveApplicableFor() {
		return leaveApplicableFor;
	}

	public void setLeaveApplicableFor(String leaveApplicableFor) {
		this.leaveApplicableFor = leaveApplicableFor;
	}

	public Integer getLeaveCount() {
		return leaveCount;
	}

	public void setLeaveCount(Integer leaveCount) {
		this.leaveCount = leaveCount;
	}

		
}
