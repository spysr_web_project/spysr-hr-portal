package com.spysr.hr.portal.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Configurable;

@Entity
@Configurable
@Table(name = "leave_request")
public class LeaveRequest {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	private String userName;
	
	private String currentRole;

	private String designation;

	private String department;

	private Date doj;

	private String name;

	private String typesofLeave;

	private String reason;

	private String leaveLength;

	private Date leaveDate;

	private Date fromDate;

	private Date toDate;
	
	private Integer totalnumberleaveDays;

	private Integer eligibleLeave;

	private Integer totalLeave;
	
	private Integer lop;



	private String email;

	private String createdby;

	private String status;
	
	private String leaveStatus;

	private String comment;
	
	private String deducted;
	
	
	/**
	 * 
	 */
	public LeaveRequest() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the designation
	 */
	public String getDesignation() {
		return designation;
	}

	/**
	 * @param designation the designation to set
	 */
	public void setDesignation(String designation) {
		this.designation = designation;
	}

	/**
	 * @return the department
	 */
	public String getDepartment() {
		return department;
	}

	/**
	 * @param department the department to set
	 */
	public void setDepartment(String department) {
		this.department = department;
	}

	/**
	 * @return the doj
	 */
	public Date getDoj() {
		return doj;
	}

	/**
	 * @param doj the doj to set
	 */
	public void setDoj(Date doj) {
		this.doj = doj;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the typesofLeave
	 */
	public String getTypesofLeave() {
		return typesofLeave;
	}

	/**
	 * @param typesofLeave the typesofLeave to set
	 */
	public void setTypesofLeave(String typesofLeave) {
		this.typesofLeave = typesofLeave;
	}

	/**
	 * @return the reason
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * @param reason the reason to set
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	/**
	 * @return the noofdays
	 */
	
	/**
	 * @return the leaveDate
	 */
	public Date getLeaveDate() {
		return leaveDate;
	}

	/**
	 * @param leaveDate the leaveDate to set
	 */
	public void setLeaveDate(Date leaveDate) {
		this.leaveDate = leaveDate;
	}

	/**
	 * @return the fromDate
	 */
	public Date getFromDate() {
		return fromDate;
	}

	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * @return the toDate
	 */
	public Date getToDate() {
		return toDate;
	}

	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	/**
	 * @return the totalnumberleaveDays
	 */
	public Integer getTotalnumberleaveDays() {
		return totalnumberleaveDays;
	}

	/**
	 * @param totalnumberleaveDays the totalnumberleaveDays to set
	 */
	public void setTotalnumberleaveDays(Integer totalnumberleaveDays) {
		this.totalnumberleaveDays = totalnumberleaveDays;
	}

	/**
	 * @return the eligibleLeave
	 */
	public Integer getEligibleLeave() {
		return eligibleLeave;
	}

	/**
	 * @param eligibleLeave the eligibleLeave to set
	 */
	public void setEligibleLeave(Integer eligibleLeave) {
		this.eligibleLeave = eligibleLeave;
	}

	/**
	 * @return the totalLeave
	 */
	public Integer getTotalLeave() {
		return totalLeave;
	}

	/**
	 * @param totalLeave the totalLeave to set
	 */
	public void setTotalLeave(Integer totalLeave) {
		this.totalLeave = totalLeave;
	}

	/**
	 * @return the lop
	 */
	public Integer getLop() {
		return lop;
	}

	/**
	 * @param lop the lop to set
	 */
	public void setLop(Integer lop) {
		this.lop = lop;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the createdby
	 */
	public String getCreatedby() {
		return createdby;
	}

	/**
	 * @param createdby the createdby to set
	 */
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the leaveStatus
	 */
	public String getLeaveStatus() {
		return leaveStatus;
	}

	/**
	 * @param leaveStatus the leaveStatus to set
	 */
	public void setLeaveStatus(String leaveStatus) {
		this.leaveStatus = leaveStatus;
	}

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * @param id
	 * @param userName
	 * @param designation
	 * @param department
	 * @param doj
	 * @param name
	 * @param typesofLeave
	 * @param reason
	 * @param noofdays
	 * @param leaveDate
	 * @param fromDate
	 * @param toDate
	 * @param totalnumberleaveDays
	 * @param eligibleLeave
	 * @param totalLeave
	 * @param lop
	 * @param email
	 * @param createdby
	 * @param status
	 * @param leaveStatus
	 * @param comment
	 */
	public LeaveRequest(Long id, String userName, String designation, String department, Date doj, String name,
			String typesofLeave, String reason, String leaveLength, Date leaveDate, Date fromDate, Date toDate,
			Integer totalnumberleaveDays, Integer eligibleLeave, Integer totalLeave, Integer lop, String email,
			String createdby, String status, String leaveStatus, String comment) {
		super();
		this.id = id;
		this.userName = userName;
		this.designation = designation;
		this.department = department;
		this.doj = doj;
		this.name = name;
		this.typesofLeave = typesofLeave;
		this.reason = reason;
		this.setLeaveLength(leaveLength);
		this.leaveDate = leaveDate;
		this.fromDate = fromDate;
		this.toDate = toDate;
		this.totalnumberleaveDays = totalnumberleaveDays;
		this.eligibleLeave = eligibleLeave;
		this.totalLeave = totalLeave;
		this.lop = lop;
		this.email = email;
		this.createdby = createdby;
		this.status = status;
		this.leaveStatus = leaveStatus;
		this.comment = comment;
	}

	/**
	 * @return the leaveLength
	 */
	public String getLeaveLength() {
		return leaveLength;
	}

	/**
	 * @param leaveLength the leaveLength to set
	 */
	public void setLeaveLength(String leaveLength) {
		this.leaveLength = leaveLength;
	}

	/**
	 * @return the currentRole
	 */
	public String getCurrentRole() {
		return currentRole;
	}

	/**
	 * @param currentRole the currentRole to set
	 */
	public void setCurrentRole(String currentRole) {
		this.currentRole = currentRole;
	}

	public String getDeducted() {
		return deducted;
	}

	public void setDeducted(String deducted) {
		this.deducted = deducted;
	}


	

}
