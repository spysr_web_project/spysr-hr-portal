/**
 * 
 */
package com.spysr.hr.portal.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Sudhir
 *
 */

@Entity
@Configurable
@Table(name = "project_details")
public class ProjectDetails {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	private String serialNo;

	private String projectName;

	private String version;

	private int membersInvolved;

	private String projectDetails;

	private String projectMembers;

	private String imageName;

	private byte[] image;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the serialNo
	 */
	public String getSerialNo() {
		return serialNo;
	}

	/**
	 * @param serialNo the serialNo to set
	 */
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	/**
	 * @return the projectName
	 */
	public String getProjectName() {
		return projectName;
	}

	/**
	 * @param projectName the projectName to set
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * @return the projectDetails
	 */
	public String getProjectDetails() {
		return projectDetails;
	}

	/**
	 * @param projectDetails the projectDetails to set
	 */
	public void setProjectDetails(String projectDetails) {
		this.projectDetails = projectDetails;
	}

	/**
	 * @return the projectMembers
	 */
	public String getProjectMembers() {
		return projectMembers;
	}

	/**
	 * @param projectMembers the projectMembers to set
	 */
	public void setProjectMembers(String projectMembers) {
		this.projectMembers = projectMembers;
	}

	/**
	 * @return the imageName
	 */
	public String getImageName() {
		return imageName;
	}

	/**
	 * @param imageName the imageName to set
	 */
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	/**
	 * @return the image
	 */
	public byte[] getImage() {
		return image;
	}

	/**
	 * @param image the image to set
	 */
	public void setImage(byte[] image) {
		this.image = image;
	}

	/**
	 * @param id
	 * @param serialNo
	 * @param projectName
	 * @param version
	 * @param projectDetails
	 * @param projectMembers
	 */
	public ProjectDetails(Long id, String serialNo, String projectName, String version, String projectDetails,
			String projectMembers) {
		super();
		this.id = id;
		this.serialNo = serialNo;
		this.projectName = projectName;
		this.version = version;
		this.projectDetails = projectDetails;
		this.projectMembers = projectMembers;
	}

	/**
	 * 
	 */
	public ProjectDetails() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getMembersInvolved() {
		return membersInvolved;
	}

	public void setMembersInvolved(int membersInvolved) {
		this.membersInvolved = membersInvolved;
	}



	

	

}
