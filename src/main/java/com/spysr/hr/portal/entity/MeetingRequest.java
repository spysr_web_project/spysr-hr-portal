package com.spysr.hr.portal.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Configurable;
@Entity
@Configurable
@Table(name = "meeting_request")
public class MeetingRequest {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column
	private long id;
	
	private String time;
	
	private String description;
	
	
	private Date meetingDate;


	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}


	/**
	 * @return the time
	 */
	public String getTime() {
		return time;
	}


	/**
	 * @param time the time to set
	 */
	public void setTime(String time) {
		this.time = time;
	}


	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}


	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}


	/**
	 * @return the meetingDate
	 */
	public Date getMeetingDate() {
		return meetingDate;
	}


	/**
	 * @param meetingDate the meetingDate to set
	 */
	public void setMeetingDate(Date meetingDate) {
		this.meetingDate = meetingDate;
	}


	/**
	 * @param id
	 * @param time
	 * @param description
	 * @param meetingDate
	 */
	public MeetingRequest(long id, String time, String description, Date meetingDate) {
		super();
		this.id = id;
		this.time = time;
		this.description = description;
		this.meetingDate = meetingDate;
	}


	/**
	 * 
	 */
	public MeetingRequest() {
		super();
		// TODO Auto-generated constructor stub
	}



}
