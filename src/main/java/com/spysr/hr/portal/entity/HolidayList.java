/**
 * 
 */
package com.spysr.hr.portal.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Configurable;


@Entity
@Table(name = "holiday_list")
@Configurable
public class HolidayList {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	
	private Long id;
	
	private String serialNo;
	
	private Date holidayDate;
	
	private String holidayName;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the serialNo
	 */
	public String getSerialNo() {
		return serialNo;
	}

	/**
	 * @param serialNo the serialNo to set
	 */
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	/**
	 * @return the holidayDate
	 */
	public Date getHolidayDate() {
		return holidayDate;
	}

	/**
	 * @param holidayDate the holidayDate to set
	 */
	public void setHolidayDate(Date holidayDate) {
		this.holidayDate = holidayDate;
	}

	/**
	 * @return the holidayName
	 */
	public String getHolidayName() {
		return holidayName;
	}

	/**
	 * @param holidayName the holidayName to set
	 */
	public void setHolidayName(String holidayName) {
		this.holidayName = holidayName;
	}

	/**
	 * @param id
	 * @param serialNo
	 * @param holidayDate
	 * @param holidayName
	 */
	public HolidayList(Long id, String serialNo, Date holidayDate, String holidayName) {
		super();
		this.id = id;
		this.serialNo = serialNo;
		this.holidayDate = holidayDate;
		this.holidayName = holidayName;
	}

	/**
	 * 
	 */
	public HolidayList() {
		super();
		// TODO Auto-generated constructor stub
	}


	

}
