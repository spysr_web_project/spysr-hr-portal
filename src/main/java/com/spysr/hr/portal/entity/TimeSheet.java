package com.spysr.hr.portal.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Configurable;



@Entity
@Configurable
@Table(name = "time_sheet")
public class TimeSheet {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="id")
	private long id;
	
	private String role;
	
	private String userName;
	
	private String name;
	
	private String email;
	
	private Date monday;
	
	private String mondayStatus;
	
	private String mondayWorkDescription;
	
	private Date tuesday;
	
	private String tuesdayStatus;
	
	private String tuesdayWorkDescription;
	
	private Date wednesday;
	
	private String wednesdayStatus;
	
	private String wednesdayWorkDescription;
	
	private Date thursday;
	
	private String thursdayStatus;
	
	private String thursdayWorkDescription;
	
	private Date friday;
	
	private String fridayStatus;
	
	private String fridayWorkDescription;
	
	private String Status;
	
	private String comment;
	
	private Float mondayBill;
	
	private Float tuesdayBill;
	
	private Float wednesdayBill;
	
	private Float thursdayBill;
	
	private Float fridayBill;
	
	

	public Float getMondayBill() {
		return mondayBill;
	}

	public void setMondayBill(Float mondayBill) {
		this.mondayBill = mondayBill;
	}

	public Float getTuesdayBill() {
		return tuesdayBill;
	}

	public void setTuesdayBill(Float tuesdayBill) {
		this.tuesdayBill = tuesdayBill;
	}

	public Float getWednesdayBill() {
		return wednesdayBill;
	}

	public void setWednesdayBill(Float wednesdayBill) {
		this.wednesdayBill = wednesdayBill;
	}

	public Float getThursdayBill() {
		return thursdayBill;
	}

	public void setThursdayBill(Float thursdayBill) {
		this.thursdayBill = thursdayBill;
	}

	public Float getFridayBill() {
		return fridayBill;
	}

	public void setFridayBill(Float fridayBill) {
		this.fridayBill = fridayBill;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getMonday() {
		return monday;
	}

	public void setMonday(Date monday) {
		this.monday = monday;
	}

	

	public String getMondayStatus() {
		return mondayStatus;
	}

	public void setMondayStatus(String mondayStatus) {
		this.mondayStatus = mondayStatus;
	}

	public String getMondayWorkDescription() {
		return mondayWorkDescription;
	}

	public void setMondayWorkDescription(String mondayWorkDescription) {
		this.mondayWorkDescription = mondayWorkDescription;
	}

	public Date getTuesday() {
		return tuesday;
	}

	public void setTuesday(Date tuesday) {
		this.tuesday = tuesday;
	}

	public String getTuesdayStatus() {
		return tuesdayStatus;
	}

	public void setTuesdayStatus(String tuesdayStatus) {
		this.tuesdayStatus = tuesdayStatus;
	}

	public String getTuesdayWorkDescription() {
		return tuesdayWorkDescription;
	}

	public void setTuesdayWorkDescription(String tuesdayWorkDescription) {
		this.tuesdayWorkDescription = tuesdayWorkDescription;
	}

	public Date getWednesday() {
		return wednesday;
	}

	public void setWednesday(Date wednesday) {
		this.wednesday = wednesday;
	}

	public String getWednesdayStatus() {
		return wednesdayStatus;
	}

	public void setWednesdayStatus(String wednesdayStatus) {
		this.wednesdayStatus = wednesdayStatus;
	}

	public String getWednesdayWorkDescription() {
		return wednesdayWorkDescription;
	}

	public void setWednesdayWorkDescription(String wednesdayWorkDescription) {
		this.wednesdayWorkDescription = wednesdayWorkDescription;
	}

	public Date getThursday() {
		return thursday;
	}

	public void setThursday(Date thursday) {
		this.thursday = thursday;
	}

	public String getThursdayStatus() {
		return thursdayStatus;
	}

	public void setThursdayStatus(String thursdayStatus) {
		this.thursdayStatus = thursdayStatus;
	}

	public String getThursdayWorkDescription() {
		return thursdayWorkDescription;
	}

	public void setThursdayWorkDescription(String thursdayWorkDescription) {
		this.thursdayWorkDescription = thursdayWorkDescription;
	}

	public Date getFriday() {
		return friday;
	}

	public void setFriday(Date friday) {
		this.friday = friday;
	}

	public String getFridayStatus() {
		return fridayStatus;
	}

	public void setFridayStatus(String fridayStatus) {
		this.fridayStatus = fridayStatus;
	}

	public String getFridayWorkDescription() {
		return fridayWorkDescription;
	}

	public void setFridayWorkDescription(String fridayWorkDescription) {
		this.fridayWorkDescription = fridayWorkDescription;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
	
	
	

}
