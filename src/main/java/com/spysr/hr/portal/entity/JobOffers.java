package com.spysr.hr.portal.entity;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.springframework.beans.factory.annotation.Configurable;

/**
 * @author spysr_dev
 *
 */
@Entity
@Configurable
@Table(name = "job_offers")
public class JobOffers {
	
	@Column
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Id
	private long id;
	
	private String technology;
	
	private String jobTiltle;
	
	private String Exp;
	
	private Date postedDate;
	
	private Date lastDate;
	
	private String refferedBy;
	
	private Date uploadDate;
	
	private String positions;
	
	private String jobDescription;
	
	@ElementCollection
	@LazyCollection(LazyCollectionOption.FALSE)
	private Set<byte[]> files;
	
	@ElementCollection
	@LazyCollection(LazyCollectionOption.FALSE)
	private Set<String> fileNames;
	

	public Date getUploadDate() {
		return uploadDate;
	}

	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}

	public Set<byte[]> getFiles() {
		return files;
	}

	public void setFiles(Set<byte[]> files) {
		this.files = files;
	}

	public Set<String> getFileNames() {
		return fileNames;
	}

	public void setFileNames(Set<String> fileNames) {
		this.fileNames = fileNames;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTechnology() {
		return technology;
	}

	public void setTechnology(String technology) {
		this.technology = technology;
	}

	public String getJobTiltle() {
		return jobTiltle;
	}

	public void setJobTiltle(String jobTiltle) {
		this.jobTiltle = jobTiltle;
	}

	public String getExp() {
		return Exp;
	}

	public void setExp(String exp) {
		Exp = exp;
	}

	public Date getPostedDate() {
		return postedDate;
	}

	public void setPostedDate(Date postedDate) {
		this.postedDate = postedDate;
	}

	public Date getLastDate() {
		return lastDate;
	}

	public void setLastDate(Date lastDate) {
		this.lastDate = lastDate;
	}

	public String getRefferedBy() {
		return refferedBy;
	}

	public void setRefferedBy(String refferedBy) {
		this.refferedBy = refferedBy;
	}

	public String getPositions() {
		return positions;
	}

	public void setPositions(String positions) {
		this.positions = positions;
	}

	public String getJobDescription() {
		return jobDescription;
	}

	public void setJobDescription(String jobDescription) {
		this.jobDescription = jobDescription;
	}
	
	
	
	
	

}
