package com.spysr.hr.portal.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Configurable;

@Entity
@Configurable
@Table(name = "end_user")
public class EndUser {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	private String userName;

	private Integer role;

	private String createdby;

	private String fatherName;

	private String localAddress;

	private String permanentAddress;

	private String displayName;

	private String designation;

	private String department;

	private Date doj;

	private String managerName;

	private String bname;;

	private Integer eligibleLeave;

	private Integer totalLeave;

	private String branch;

	private String accountNo;

	private String name;

	private String gender;

	private String pan;

	private String address;

	private String description;

	private Date meetingDate;

	private String time;

	private Date dob;

	private Float totalPackage;
	
	private Float totalPack;

	private Float fixedPackage;

	private Float variablePackage;

	private Float basicSalary;

	private Float hra;

	private Float conveyAllowance;

	private Float medicalAllowance;

	private Float specialAllowance;

	private Float professionalTax;

	private Float foodamountDeduct;

	private Float tds;

	private Float total;
	
	private Float totaldeduct;

	private String roleType;

	private String password;

	private String comment;

	private String currentRole;

	private Integer flag;

	private String theme;

	private String prefferedLanguage;

	private String contactNo;

	private Integer lop;

	private String email;

	private String status;

	private String accessStatus;

	private String reason;

	private String addbyManager;

	private String addbyAdmin;

	private Integer passwordFlag;

	private String imageName;

	private byte[] image;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the role
	 */
	public Integer getRole() {
		return role;
	}

	/**
	 * @param role the role to set
	 */
	public void setRole(Integer role) {
		this.role = role;
	}

	/**
	 * @return the createdby
	 */
	public String getCreatedby() {
		return createdby;
	}

	/**
	 * @param createdby the createdby to set
	 */
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	/**
	 * @return the fatherName
	 */
	public String getFatherName() {
		return fatherName;
	}

	/**
	 * @param fatherName the fatherName to set
	 */
	public void setFatherName(String fatherName) {
		this.fatherName = fatherName;
	}

	/**
	 * @return the localAddress
	 */
	public String getLocalAddress() {
		return localAddress;
	}

	/**
	 * @param localAddress the localAddress to set
	 */
	public void setLocalAddress(String localAddress) {
		this.localAddress = localAddress;
	}

	/**
	 * @return the permanentAddress
	 */
	public String getPermanentAddress() {
		return permanentAddress;
	}

	/**
	 * @param permanentAddress the permanentAddress to set
	 */
	public void setPermanentAddress(String permanentAddress) {
		this.permanentAddress = permanentAddress;
	}

	/**
	 * @return the displayName
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * @param displayName the displayName to set
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	/**
	 * @return the designation
	 */
	public String getDesignation() {
		return designation;
	}

	/**
	 * @param designation the designation to set
	 */
	public void setDesignation(String designation) {
		this.designation = designation;
	}

	/**
	 * @return the department
	 */
	public String getDepartment() {
		return department;
	}

	/**
	 * @param department the department to set
	 */
	public void setDepartment(String department) {
		this.department = department;
	}

	/**
	 * @return the doj
	 */
	public Date getDoj() {
		return doj;
	}

	/**
	 * @param doj the doj to set
	 */
	public void setDoj(Date doj) {
		this.doj = doj;
	}

	/**
	 * @return the managerName
	 */
	public String getManagerName() {
		return managerName;
	}

	/**
	 * @param managerName the managerName to set
	 */
	public void setManagerName(String managerName) {
		this.managerName = managerName;
	}

	/**
	 * @return the bname
	 */
	public String getBname() {
		return bname;
	}

	/**
	 * @param bname the bname to set
	 */
	public void setBname(String bname) {
		this.bname = bname;
	}

	/**
	 * @return the eligibleLeave
	 */
	public Integer getEligibleLeave() {
		return eligibleLeave;
	}

	/**
	 * @param eligibleLeave the eligibleLeave to set
	 */
	public void setEligibleLeave(Integer eligibleLeave) {
		this.eligibleLeave = eligibleLeave;
	}

	/**
	 * @return the totalLeave
	 */
	public Integer getTotalLeave() {
		return totalLeave;
	}

	/**
	 * @param totalLeave the totalLeave to set
	 */
	public void setTotalLeave(Integer totalLeave) {
		this.totalLeave = totalLeave;
	}

	/**
	 * @return the branch
	 */
	public String getBranch() {
		return branch;
	}

	/**
	 * @param branch the branch to set
	 */
	public void setBranch(String branch) {
		this.branch = branch;
	}

	/**
	 * @return the accountNo
	 */
	public String getAccountNo() {
		return accountNo;
	}

	/**
	 * @param accountNo the accountNo to set
	 */
	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the pan
	 */
	public String getPan() {
		return pan;
	}

	/**
	 * @param pan the pan to set
	 */
	public void setPan(String pan) {
		this.pan = pan;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the meetingDate
	 */
	public Date getMeetingDate() {
		return meetingDate;
	}

	/**
	 * @param meetingDate the meetingDate to set
	 */
	public void setMeetingDate(Date meetingDate) {
		this.meetingDate = meetingDate;
	}

	/**
	 * @return the time
	 */
	public String getTime() {
		return time;
	}

	/**
	 * @param time the time to set
	 */
	public void setTime(String time) {
		this.time = time;
	}

	/**
	 * @return the dob
	 */
	public Date getDob() {
		return dob;
	}

	/**
	 * @param dob the dob to set
	 */
	public void setDob(Date dob) {
		this.dob = dob;
	}

	/**
	 * @return the totalPackage
	 */
	public Float getTotalPackage() {
		return totalPackage;
	}

	/**
	 * @param totalPackage the totalPackage to set
	 */
	public void setTotalPackage(Float totalPackage) {
		this.totalPackage = totalPackage;
	}

	/**
	 * @return the fixedPackage
	 */
	public Float getFixedPackage() {
		return fixedPackage;
	}

	/**
	 * @param fixedPackage the fixedPackage to set
	 */
	public void setFixedPackage(Float fixedPackage) {
		this.fixedPackage = fixedPackage;
	}

	/**
	 * @return the variablePackage
	 */
	public Float getVariablePackage() {
		return variablePackage;
	}

	/**
	 * @param variablePackage the variablePackage to set
	 */
	public void setVariablePackage(Float variablePackage) {
		this.variablePackage = variablePackage;
	}

	/**
	 * @return the basicSalary
	 */
	public Float getBasicSalary() {
		return basicSalary;
	}

	/**
	 * @param basicSalary the basicSalary to set
	 */
	public void setBasicSalary(Float basicSalary) {
		this.basicSalary = basicSalary;
	}

	/**
	 * @return the hra
	 */
	public Float getHra() {
		return hra;
	}

	/**
	 * @param hra the hra to set
	 */
	public void setHra(Float hra) {
		this.hra = hra;
	}

	/**
	 * @return the conveyAllowance
	 */
	public Float getConveyAllowance() {
		return conveyAllowance;
	}

	/**
	 * @param conveyAllowance the conveyAllowance to set
	 */
	public void setConveyAllowance(Float conveyAllowance) {
		this.conveyAllowance = conveyAllowance;
	}

	/**
	 * @return the medicalAllowance
	 */
	public Float getMedicalAllowance() {
		return medicalAllowance;
	}

	/**
	 * @param medicalAllowance the medicalAllowance to set
	 */
	public void setMedicalAllowance(Float medicalAllowance) {
		this.medicalAllowance = medicalAllowance;
	}

	/**
	 * @return the specialAllowance
	 */
	public Float getSpecialAllowance() {
		return specialAllowance;
	}

	/**
	 * @param specialAllowance the specialAllowance to set
	 */
	public void setSpecialAllowance(Float specialAllowance) {
		this.specialAllowance = specialAllowance;
	}

	/**
	 * @return the professionalTax
	 */
	public Float getProfessionalTax() {
		return professionalTax;
	}

	/**
	 * @param professionalTax the professionalTax to set
	 */
	public void setProfessionalTax(Float professionalTax) {
		this.professionalTax = professionalTax;
	}

	/**
	 * @return the foodamountDeduct
	 */
	public Float getFoodamountDeduct() {
		return foodamountDeduct;
	}

	/**
	 * @param foodamountDeduct the foodamountDeduct to set
	 */
	public void setFoodamountDeduct(Float foodamountDeduct) {
		this.foodamountDeduct = foodamountDeduct;
	}

	/**
	 * @return the tds
	 */
	public Float getTds() {
		return tds;
	}

	/**
	 * @param tds the tds to set
	 */
	public void setTds(Float tds) {
		this.tds = tds;
	}

	/**
	 * @return the total
	 */
	public Float getTotal() {
		return total;
	}

	/**
	 * @param total the total to set
	 */
	public void setTotal(Float total) {
		this.total = total;
	}

	/**
	 * @return the roleType
	 */
	public String getRoleType() {
		return roleType;
	}

	/**
	 * @param roleType the roleType to set
	 */
	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * @return the currentRole
	 */
	public String getCurrentRole() {
		return currentRole;
	}

	/**
	 * @param currentRole the currentRole to set
	 */
	public void setCurrentRole(String currentRole) {
		this.currentRole = currentRole;
	}

	/**
	 * @return the flag
	 */
	public Integer getFlag() {
		return flag;
	}

	/**
	 * @param flag the flag to set
	 */
	public void setFlag(Integer flag) {
		this.flag = flag;
	}

	/**
	 * @return the theme
	 */
	public String getTheme() {
		return theme;
	}

	/**
	 * @param theme the theme to set
	 */
	public void setTheme(String theme) {
		this.theme = theme;
	}

	/**
	 * @return the prefferedLanguage
	 */
	public String getPrefferedLanguage() {
		return prefferedLanguage;
	}

	/**
	 * @param prefferedLanguage the prefferedLanguage to set
	 */
	public void setPrefferedLanguage(String prefferedLanguage) {
		this.prefferedLanguage = prefferedLanguage;
	}

	/**
	 * @return the contactNo
	 */
	public String getContactNo() {
		return contactNo;
	}

	/**
	 * @param contactNo the contactNo to set
	 */
	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	/**
	 * @return the lop
	 */
	public Integer getLop() {
		return lop;
	}

	/**
	 * @param lop the lop to set
	 */
	public void setLop(Integer lop) {
		this.lop = lop;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the accessStatus
	 */
	public String getAccessStatus() {
		return accessStatus;
	}

	/**
	 * @param accessStatus the accessStatus to set
	 */
	public void setAccessStatus(String accessStatus) {
		this.accessStatus = accessStatus;
	}

	/**
	 * @return the reason
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * @param reason the reason to set
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	/**
	 * @return the addbyManager
	 */
	public String getAddbyManager() {
		return addbyManager;
	}

	/**
	 * @param addbyManager the addbyManager to set
	 */
	public void setAddbyManager(String addbyManager) {
		this.addbyManager = addbyManager;
	}

	/**
	 * @return the addbyAdmin
	 */
	public String getAddbyAdmin() {
		return addbyAdmin;
	}

	/**
	 * @param addbyAdmin the addbyAdmin to set
	 */
	public void setAddbyAdmin(String addbyAdmin) {
		this.addbyAdmin = addbyAdmin;
	}

	/**
	 * @return the passwordFlag
	 */
	public Integer getPasswordFlag() {
		return passwordFlag;
	}

	/**
	 * @param passwordFlag the passwordFlag to set
	 */
	public void setPasswordFlag(Integer passwordFlag) {
		this.passwordFlag = passwordFlag;
	}

	/**
	 * @return the imageName
	 */
	public String getImageName() {
		return imageName;
	}

	/**
	 * @param imageName the imageName to set
	 */
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	/**
	 * @return the image
	 */
	public byte[] getImage() {
		return image;
	}

	/**
	 * @param image the image to set
	 */
	public void setImage(byte[] image) {
		this.image = image;
	}

	/**
	 * 
	 */
	public EndUser() {
		super();
		// TODO Auto-generated constructor stub
	}

	public EndUser(Long id, String userName, String currentRole, String accessStatus) {
		super();
		this.id = id;
		this.userName = userName;
		this.currentRole = currentRole;
		this.accessStatus = accessStatus;
		
	}

	/**
	 * @return the totaldeduct
	 */
	public Float getTotaldeduct() {
		return totaldeduct;
	}

	/**
	 * @param totaldeduct the totaldeduct to set
	 */
	public void setTotaldeduct(Float totaldeduct) {
		this.totaldeduct = totaldeduct;
	}

	/**
	 * @return the totalPack
	 */
	public Float getTotalPack() {
		return totalPack;
	}

	/**
	 * @param totalPack the totalPack to set
	 */
	public void setTotalPack(Float totalPack) {
		this.totalPack = totalPack;
	}

}
