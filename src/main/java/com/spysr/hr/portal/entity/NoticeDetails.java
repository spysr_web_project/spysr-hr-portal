/**
 * 
 */
package com.spysr.hr.portal.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.beans.factory.annotation.Configurable;

/**
 * @author Sudhir
 *
 */
@Entity
@Configurable
@Table(name = "notice_details")
public class NoticeDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	private String serialNo;

	private String title;
	
	private Date noticeDate;

	private String description;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the serialNo
	 */
	public String getSerialNo() {
		return serialNo;
	}

	/**
	 * @param serialNo the serialNo to set
	 */
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the noticeDate
	 */
	public Date getNoticeDate() {
		return noticeDate;
	}

	/**
	 * @param noticeDate the noticeDate to set
	 */
	public void setNoticeDate(Date noticeDate) {
		this.noticeDate = noticeDate;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @param id
	 * @param serialNo
	 * @param title
	 * @param noticeDate
	 * @param description
	 */
	public NoticeDetails(Long id, String serialNo, String title, Date noticeDate, String description) {
		super();
		this.id = id;
		this.serialNo = serialNo;
		this.title = title;
		this.noticeDate = noticeDate;
		this.description = description;
	}

	/**
	 * 
	 */
	public NoticeDetails() {
		super();
		// TODO Auto-generated constructor stub
	}

	

}
