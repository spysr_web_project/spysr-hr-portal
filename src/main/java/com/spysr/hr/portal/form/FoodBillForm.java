package com.spysr.hr.portal.form;

import java.util.Collection;
import java.util.Date;

import org.springframework.stereotype.Component;

import com.spysr.hr.portal.entity.FoodBill;

@Component
public class FoodBillForm {
	
private long id;
	
	private String userName;
	
	private String name;
	
	private String month;
	
	private Date date;
	
	private int presents;
	
	private float money;
	
	private int year;
	
	private int veg;
	
	private int nonVeg;
	
	private float foodBill;
	
	private Collection<FoodBill> foodBills;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getPresents() {
		return presents;
	}

	public void setPresents(int presents) {
		this.presents = presents;
	}

	public float getMoney() {
		return money;
	}

	public void setMoney(float money) {
		this.money = money;
	}

	public Collection<FoodBill> getFoodBills() {
		return foodBills;
	}

	public void setFoodBills(Collection<FoodBill> foodBills) {
		this.foodBills = foodBills;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getVeg() {
		return veg;
	}

	public void setVeg(int veg) {
		this.veg = veg;
	}

	public int getNonVeg() {
		return nonVeg;
	}

	public void setNonVeg(int nonVeg) {
		this.nonVeg = nonVeg;
	}

	public float getFoodBill() {
		return foodBill;
	}

	public void setFoodBill(float foodBill) {
		this.foodBill = foodBill;
	}
	
	


}
