/**
 * 
 */
package com.spysr.hr.portal.form;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Sudhir
 *
 */

@Component
public class ProjectDetailsForm {
	
	
	private Long id;
	
	
	private String serialNo;
	
	private String projectName;
	
	private String version;
	
	private String projectDetails;
	
	private int membersInvolved;
	
	private String projectMembers;
	
	private String imageName;

	private byte[] image;
	
	private MultipartFile file;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the serialNo
	 */
	public String getSerialNo() {
		return serialNo;
	}

	/**
	 * @param serialNo the serialNo to set
	 */
	public void setSerialNo(String serialNo) {
		this.serialNo = serialNo;
	}

	/**
	 * @return the projectName
	 */
	public String getProjectName() {
		return projectName;
	}

	/**
	 * @param projectName the projectName to set
	 */
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	/**
	 * @return the version
	 */
	public String getVersion() {
		return version;
	}

	/**
	 * @param version the version to set
	 */
	public void setVersion(String version) {
		this.version = version;
	}

	/**
	 * @return the projectDetails
	 */
	public String getProjectDetails() {
		return projectDetails;
	}

	/**
	 * @param projectDetails the projectDetails to set
	 */
	public void setProjectDetails(String projectDetails) {
		this.projectDetails = projectDetails;
	}

	/**
	 * @return the projectMembers
	 */
	public String getProjectMembers() {
		return projectMembers;
	}

	/**
	 * @param projectMembers the projectMembers to set
	 */
	public void setProjectMembers(String projectMembers) {
		this.projectMembers = projectMembers;
	}

	/**
	 * @return the imageName
	 */
	public String getImageName() {
		return imageName;
	}

	/**
	 * @param imageName the imageName to set
	 */
	public void setImageName(String imageName) {
		this.imageName = imageName;
	}

	/**
	 * @return the image
	 */
	public byte[] getImage() {
		return image;
	}

	/**
	 * @param image the image to set
	 */
	public void setImage(byte[] image) {
		this.image = image;
	}

	/**
	 * @return the file
	 */
	public MultipartFile getFile() {
		return file;
	}

	/**
	 * @param file the file to set
	 */
	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public int getMembersInvolved() {
		return membersInvolved;
	}

	public void setMembersInvolved(int membersInvolved) {
		this.membersInvolved = membersInvolved;
	}
	

	

}
