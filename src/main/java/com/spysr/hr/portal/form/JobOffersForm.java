package com.spysr.hr.portal.form;

import java.util.Date;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;


/**
 * @author spysr_dev
 *
 */
@Component
public class JobOffersForm {
	
    private long id;
	
	private String technology;
	
	private String jobTiltle;
	
	private String Exp;
	
	private Date postedDate;
	
	private Date lastDate;
	
	private Date uploadDate;
	
	private MultipartFile file;
	
    private String form;
    
    private String nameOfCandidate;
	
	private String candidateExp;
	
	private String companyName;
	
    private String positions;
	
	private String jobDescription;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTechnology() {
		return technology;
	}

	public void setTechnology(String technology) {
		this.technology = technology;
	}

	public String getJobTiltle() {
		return jobTiltle;
	}

	public void setJobTiltle(String jobTiltle) {
		this.jobTiltle = jobTiltle;
	}

	public String getExp() {
		return Exp;
	}

	public void setExp(String exp) {
		Exp = exp;
	}

	public Date getPostedDate() {
		return postedDate;
	}

	public void setPostedDate(Date postedDate) {
		this.postedDate = postedDate;
	}

	public Date getLastDate() {
		return lastDate;
	}

	public void setLastDate(Date lastDate) {
		this.lastDate = lastDate;
	}

	public Date getUploadDate() {
		return uploadDate;
	}

	public void setUploadDate(Date uploadDate) {
		this.uploadDate = uploadDate;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public String getForm() {
		return form;
	}

	public void setForm(String form) {
		this.form = form;
	}

	
	public String getNameOfCandidate() {
		return nameOfCandidate;
	}

	public void setNameOfCandidate(String nameOfCandidate) {
		this.nameOfCandidate = nameOfCandidate;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getPositions() {
		return positions;
	}

	public void setPositions(String positions) {
		this.positions = positions;
	}

	public String getJobDescription() {
		return jobDescription;
	}

	public void setJobDescription(String jobDescription) {
		this.jobDescription = jobDescription;
	}

	public String getCandidateExp() {
		return candidateExp;
	}

	public void setCandidateExp(String candidateExp) {
		this.candidateExp = candidateExp;
	}
	
	
	
	

}
