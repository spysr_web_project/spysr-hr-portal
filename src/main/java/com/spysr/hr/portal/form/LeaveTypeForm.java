package com.spysr.hr.portal.form;

import org.springframework.stereotype.Component;

@Component
public class LeaveTypeForm {

	private Long id;

	private String name;
	
	private String leaveType;

	private String leaveUnit;

	private String description;
	
	private String leaveLength;

	private String leavePeriod;

	private String leaveFor;
	
	private String leaveApplicableFor;

	private Integer leaveCount;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLeaveType() {
		return leaveType;
	}

	public void setLeaveType(String leaveType) {
		this.leaveType = leaveType;
	}

	public String getLeaveUnit() {
		return leaveUnit;
	}

	public void setLeaveUnit(String leaveUnit) {
		this.leaveUnit = leaveUnit;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getLeaveLength() {
		return leaveLength;
	}

	public void setLeaveLength(String leaveLength) {
		this.leaveLength = leaveLength;
	}

	public String getLeavePeriod() {
		return leavePeriod;
	}

	public void setLeavePeriod(String leavePeriod) {
		this.leavePeriod = leavePeriod;
	}

	public String getLeaveFor() {
		return leaveFor;
	}

	public void setLeaveFor(String leaveFor) {
		this.leaveFor = leaveFor;
	}

	public String getLeaveApplicableFor() {
		return leaveApplicableFor;
	}

	public void setLeaveApplicableFor(String leaveApplicableFor) {
		this.leaveApplicableFor = leaveApplicableFor;
	}

	public Integer getLeaveCount() {
		return leaveCount;
	}

	public void setLeaveCount(Integer leaveCount) {
		this.leaveCount = leaveCount;
	}

		
}
