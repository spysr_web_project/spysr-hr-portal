package com.spysr.hr.portal.form;

import org.springframework.stereotype.Component;

/**
 * @author spysr_dev
 *
 */
@Component
public class RequestedDocsForm {
	
	private String name;
	
	private String userName;
	
	private String docName;
	
	private String address;
	
	private String email;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getDocName() {
		return docName;
	}

	public void setDocName(String docName) {
		this.docName = docName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	
	

}
