package com.spysr.hr.portal.form;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Component;

@Component
public class LeaveRequestForm {

	private Long id;

	private String userName;

	private Integer role;

	private String designation;

	private String department;

	private Date doj;

	private String name;

	private String pan;

	private String typesofLeave;

	private String reason;
	
	private String currentRole;

	private String leaveLength;

	private Date leaveDate;

	private Date fromDate;

	private Date toDate;
	
	private Integer totalnumberleaveDays;
	
	private Integer lop;

	private Integer eligibleLeave;

	private Integer totalLeave;

	private String email;
	
	private String createdby;
	
	private String status;
	
	private String comment;
	
	private String leaveStatus;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the role
	 */
	public Integer getRole() {
		return role;
	}

	/**
	 * @param role the role to set
	 */
	public void setRole(Integer role) {
		this.role = role;
	}

	/**
	 * @return the designation
	 */
	public String getDesignation() {
		return designation;
	}

	/**
	 * @param designation the designation to set
	 */
	public void setDesignation(String designation) {
		this.designation = designation;
	}

	/**
	 * @return the department
	 */
	public String getDepartment() {
		return department;
	}

	/**
	 * @param department the department to set
	 */
	public void setDepartment(String department) {
		this.department = department;
	}

	/**
	 * @return the doj
	 */
	public Date getDoj() {
		return doj;
	}

	/**
	 * @param doj the doj to set
	 */
	public void setDoj(Date doj) {
		this.doj = doj;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the pan
	 */
	public String getPan() {
		return pan;
	}

	/**
	 * @param pan the pan to set
	 */
	public void setPan(String pan) {
		this.pan = pan;
	}

	/**
	 * @return the typesofLeave
	 */
	public String getTypesofLeave() {
		return typesofLeave;
	}

	/**
	 * @param typesofLeave the typesofLeave to set
	 */
	public void setTypesofLeave(String typesofLeave) {
		this.typesofLeave = typesofLeave;
	}

	/**
	 * @return the reason
	 */
	public String getReason() {
		return reason;
	}

	/**
	 * @param reason the reason to set
	 */
	public void setReason(String reason) {
		this.reason = reason;
	}

	
	/**
	 * @return the leaveDate
	 */
	public Date getLeaveDate() {
		return leaveDate;
	}

	/**
	 * @param leaveDate the leaveDate to set
	 */
	public void setLeaveDate(Date leaveDate) {
		this.leaveDate = leaveDate;
	}

	/**
	 * @return the fromDate
	 */
	public Date getFromDate() {
		return fromDate;
	}

	/**
	 * @param fromDate the fromDate to set
	 */
	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	/**
	 * @return the toDate
	 */
	public Date getToDate() {
		return toDate;
	}

	/**
	 * @param toDate the toDate to set
	 */
	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	/**
	 * @return the eligibleLeave
	 */
	public Integer getEligibleLeave() {
		return eligibleLeave;
	}

	/**
	 * @param eligibleLeave the eligibleLeave to set
	 */
	public void setEligibleLeave(Integer eligibleLeave) {
		this.eligibleLeave = eligibleLeave;
	}

	/**
	 * @return the totalLeave
	 */
	public Integer getTotalLeave() {
		return totalLeave;
	}

	/**
	 * @param totalLeave the totalLeave to set
	 */
	public void setTotalLeave(Integer totalLeave) {
		this.totalLeave = totalLeave;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the createdby
	 */
	public String getCreatedby() {
		return createdby;
	}

	/**
	 * @param createdby the createdby to set
	 */
	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * @return the lop
	 */
	public Integer getLop() {
		return lop;
	}

	/**
	 * @param lop the lop to set
	 */
	public void setLop(Integer lop) {
		this.lop = lop;
	}

	/**
	 * @return the leaveStatus
	 */
	public String getLeaveStatus() {
		return leaveStatus;
	}

	/**
	 * @param leaveStatus the leaveStatus to set
	 */
	public void setLeaveStatus(String leaveStatus) {
		this.leaveStatus = leaveStatus;
	}

	/**
	 * @return the totalnumberleaveDays
	 */
	public Integer getTotalnumberleaveDays() {
		return totalnumberleaveDays;
	}

	/**
	 * @param totalnumberleaveDays the totalnumberleaveDays to set
	 */
	public void setTotalnumberleaveDays(Integer totalnumberleaveDays) {
		this.totalnumberleaveDays = totalnumberleaveDays;
	}

	/**
	 * @return the leaveLength
	 */
	public String getLeaveLength() {
		return leaveLength;
	}

	/**
	 * @param leaveLength the leaveLength to set
	 */
	public void setLeaveLength(String leaveLength) {
		this.leaveLength = leaveLength;
	}

	/**
	 * @return the currentRole
	 */
	public String getCurrentRole() {
		return currentRole;
	}

	/**
	 * @param currentRole the currentRole to set
	 */
	public void setCurrentRole(String currentRole) {
		this.currentRole = currentRole;
	}



	
}
